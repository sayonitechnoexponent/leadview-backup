 // tabbed content
    // http://www.entheosweb.com/tutorials/css/tabs.asp
    $(".tab_content").hide();
    $(".tab_content:first").show();

  /* if in tab mode */
    $(document).on('click','ul.tabs li',function() {
      $(".tab_content").hide();
      var activeTab = $(this).attr("rel");
        // $("#"+activeTab).fadeIn();
        var ret = parseInt(activeTab.replace('tab',''));
        ret = ret-1;
        $( ".tab_content" ).eq( ret ).fadeIn();

      $("ul.tabs li").removeClass("active");
      $(this).addClass("active");

	  $(".tab_drawer_heading").removeClass("d_active");
	  $(".tab_drawer_heading[rel^='"+activeTab+"']").addClass("d_active");
	  
    });
	/* if in drawer mode */
    $(document).on('click',".tab_drawer_heading",function() {
      
      $(".tab_content").hide();
      var d_activeTab = $(this).attr("rel"); 
      //$("#"+d_activeTab).fadeIn();

        var ret = parseInt(d_activeTab.replace('tab',''));
        ret = ret-1;
        $( ".tab_content" ).eq( ret ).fadeIn();

	  $(".tab_drawer_heading").removeClass("d_active");
      $(this).addClass("d_active");
	  
	  $("ul.tabs li").removeClass("active");
	  $("ul.tabs li[rel^='"+d_activeTab+"']").addClass("active");
    });
	
	
	/* Extra class "tab_last" 
	   to add border to right side
	   of last tab */
	$('ul.tabs li').last().addClass("tab_last");
	
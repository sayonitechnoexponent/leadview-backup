
function getTlData(){
    var fd = new FormData();
    //curr_user: 44
    fd.append('list_tab', '#allpost');
    fd.append('api_token', user_api_token);
    fd.append('tl_type', 'contact');
    fd.append('curr_user', contact_id);
    fd.append('archive_status', 1);
    fd.append('start_date', '');
    fd.append('end_date', '');
    fd.append('keyword', '');
    fd.append('is_remote', 1);
    // fd.append('page', 1);
    // fd.append('limit', 10);
    httpRequest(apiUrl+'get-time-line','POST',fd,'tl');

}
function checkBookmarks(){
    var fd = new FormData();
    //curr_user: 44
    fd.append('contact_id',contact_id);
    // fd.append('page', 1);
    // fd.append('limit', 10);
    httpRequest(apiUrl+'get-bookmarks','POST',fd,'bmtl');

}

function showBookMarkTimeline(data){
    // console.log(data);
    var tlHtml = '';
    document.getElementById('tlPost').innerHTML = tlHtml;
    if(data.feed && data.feed.length>0 ){
        data.feed.forEach(function (val) {
            tlHtml += setBookMarkContent(val);
        });
    }
    document.getElementById('tlPost').innerHTML = tlHtml;
}

function setBookMarkContent(res) {
    // var post = res.data;
    // var contact = res.contact;
    var txtcolor='';
    var cupic=assets_path+'images/avatar.png';
    var cpic = assets_path+'images/avatar.png';
    var ptype = "'B'";
    var pcomments ='';
    if(chat_box_setting && chat_box_setting.text_color)
        txtcolor=chat_box_setting.text_color+' !important';
        
    if (res.contact && res.contact.profile_picture != null)
    {
        cpic = ext_server_path+'profile_picture/'+res.contact.profile_picture;
    }
    if (res.comments && res.comments.length >0)
    {
        pcomments += '<ul class="commntList list-unstyled">';
        res.comments.forEach(function (val) {
            var cname ='';
            var cattachment ='';
            if(val.user_id!=null){
                
                cname = val.user.first_name+' '+val.user.last_name;
                if(val.user.profile_picture!=null)    cupic = ext_server_path+'profile_picture/'+val.user.profile_picture;
            }
            if(val.contact_id!=null){
                
                cname = val.contact.first_name+' '+val.contact.last_name;
                if(val.contact.profile_picture!=null)    cupic = ext_server_path+'profile_picture/'+val.contact.profile_picture;
            }
            if(val.attachment!=null){
                cattachment = '<span><a href="'+ext_server_path+'comment_attachment/'+val.attachment+'" target="_blank">Download attachment</a></span>';
            }

            pcomments +=
                '<li style="color: '+txtcolor+'">\n' +
                '  <figure class="round pull-left">\n' +
                '\t<img src="'+cupic+'" class="img-responsive">\n' +
                '  </figure>\n' +
                '  <h4 style="color: '+txtcolor+'"><strong>'+cname+'</strong></h4><p style="color: '+txtcolor+'"> '+val.content+'</p>\n' +
                cattachment+
                '</li>';
        });
        pcomments += '</ul>';
        if(res.comments.length >2)    pcomments += '<a href="javascript:void(0);" class="ldmore text-orange">View more</a>';
    }

    cname = res.contact.first_name+' '+res.contact.last_name;
    var pHtml ='<li class="bookmarked">\n' +
        '\t\t\t\t\t\t<figure>\n' +
        '                          <a href="#"><img src="'+cpic+'" class="img-responsive"></a>\n' +
        '                        </figure>\n' +
        '                        <h4><strong><a href="#">'+cname+'</a></strong> Bookmarked <a target="_blank" href="'+res.link+'">'+res.link+'</a></h4>\n' +
        '                        <div class="lvcommnt">\n' +
        '\t                        <div class="form-grouplv">\n' +
        '\t                          <div class="withfileup">\n' +
        '\t                            <textarea class="form-control" onkeydown="keyDownFunction(event,'+res.id+','+ptype+')" placeholder="Comment" rows="2"></textarea>\n' +
        '\t                            <label class="upfiletimel">\n' +
        '\t                              <input type="file" class="hidit" name="">\n' +
        '\t                              <span><img src="'+assets_path+'/images/attachmenticon.png" class="img-responsive"></span>\n' +
        '\t                            </label>\n' +
        '\t                          </div>\n' +
        '\t                        </div>\n' +
                                pcomments+
        '\t                    </div>\n' +
        '\t\t\t\t\t</li>';

    return pHtml;
}
function showTimeline(data){
    // console.log(data);
    var tlHtml = '';
    if(data.feed && data.feed.allPosts.length>0 ){
        data.feed.allPosts.forEach(function (val) {
            if(val.post.chat_box_visibility==1)
            {
                    tlHtml += setPostContent(val);
            }
        });
    }
    document.getElementById('tlPost').innerHTML = tlHtml;
}

function setPostContent(res) {
    var post = res.data;
    var content = '';
    var about_post = '';
    var pcomments = '';
    var upic = assets_path+'images/avatar.png';
    var cupic = assets_path+'images/avatar.png';
    // console.log(res.post_type);
    var uname = '';
    var txtcolor='';
    if(chat_box_setting && chat_box_setting.text_color)
        txtcolor=chat_box_setting.text_color+' !important';

    if (post.user)
    {
        uname = post.user.first_name +' ' +post.user.last_name;
        upic = ext_server_path+'profile_picture/'+post.user.profile_picture;
    }
    if (post.comments && post.comments.length >0)
    {
        pcomments += '<ul class="commntList list-unstyled">';
        post.comments.forEach(function (val) {
            var cattachment = '';
            var cname ='';
            if(val.user_id!=null){
                
                cname = val.user.first_name+' '+val.user.last_name;
                if(val.user.profile_picture!=null)    cupic = ext_server_path+'profile_picture/'+val.user.profile_picture;
            }
            if(val.contact_id!=null){
                
                cname = val.contact.first_name+' '+val.contact.last_name;
                if(val.contact.profile_picture!=null)    cupic = ext_server_path+'profile_picture/'+val.contact.profile_picture;
            }
            if(val.attachment!=null){
                cattachment = '<span><a href="'+ext_server_path+'comment_attachment/'+val.attachment+'" target="_blank">Download attachment</a></span>';
            }
            pcomments +=
                '<li style="color: '+txtcolor+'">\n' +
                '  <figure class="round pull-left">\n' +
                '\t<img src="'+cupic+'" class="img-responsive">\n' +
                '  </figure>\n' +
                '  <h4 style="color: '+txtcolor+'"><strong>'+cname+'</strong></h4><p style="color: '+txtcolor+'"> '+val.content+'</p>\n' +
                cattachment+
                '</li>';
        });
        pcomments += '</ul>';
        if(post.comments.length >2)    pcomments += '<a href="javascript:void(0);" class="ldmore text-orange">View more</a>';
    }

    switch(res.post_type) {
        case 'N':
            content = post.details;
            about_post = 'added a note';
            break;

        case 'T':
            content = post.notes;
            about_post = 'created a task';
            break;
        case 'O':
            content = post.description;
            about_post = 'created a opportunity';
            break;

        /*case 'T':
            content = post.notes;
            about_post = 'created a task';
            break;*/
        // default:
        // code block
    }
    var ptype = "'"+res.post_type+"'";

    // console.log(res.post_type);
    var pHtml =
        '<li style="color: '+txtcolor+'">\n' +
        '\t<figure>\n' +
        '\t\t<a href="#"><img src="'+upic+'" class="img-responsive"></a>\n' +
        '\t</figure>\n' +
        '\t<h4 style="color: '+txtcolor+'"><strong><a href="#">'+uname+'</a></strong> '+about_post+'</h4>\n' +
        '\t<p style="color: '+txtcolor+'">'+content+'</p>\n' +
        '\t<div class="lvcommnt">\n' +
        '\t\t<div class="form-grouplv">\n' +
        '\n' +
        '\t\t\t<div class="withfileup">\n' +
        '\t\t\t\t<textarea class="form-control" onkeydown="keyDownFunction(event,'+post.id+','+ptype+')" placeholder="Comment" rows="2"></textarea>\n' +
        '\t\t\t\t<label class="upfiletimel">\n' +
        '\t\t\t\t\t<input type="file" class="hidit" name="">\n' +
        '\t\t\t\t\t<span><img src="'+assets_path+'images/attachmenticon.png" class="img-responsive"></span>\n' +
        '\t\t\t\t</label>\n' +
        '\t\t\t</div>\n' +
        '\t\t</div>\n' +
            pcomments+
        '\n' +
        '\n' +
        '\n' +
        '\t</div>\n' +
        '\n' +
        '</li>';

    return pHtml;
}

    
    
    function keyDownFunction(event,postId,postType) 
    {
        console.log(postType);
        // console.log(event.keyCode);
        if(event.keyCode == 13) {
            
            console.log(event.srcElement.value);
            var msg= event.srcElement.value;
            saveComment(msg,postId,postType.toString());
            // alert('you just clicked enter');
            // rest of your code
            // createComment(f,typ,pid);
            //console.log(f);
        }
    }


    function saveComment(msg,postId,postType){
    //     type: N
    // post_id: 177
    // contact_id: 177
    // content: Hi avi
    // tl_type: contact
    
    var fd = new FormData();
    fd.append('contact_id', contact_id);
    fd.append('post_id', postId);
    fd.append('type', postType);
    fd.append('content', msg);
    // if(blob_file!='')    fd.append('message_file', blob_file);
    // console.log(fd);
    var resp = httpRequest(apiUrl+'save-post-comment','POST',fd,'pc');
    
    }

    function tlCommentRes(res){
        // status
        console.log(res.status);
        if(res.status=='success')
        {
            if(res.feed.type=='B'){
                checkBookmarks();
            }
            else    showtiemlineFunction();

        }else if(res.status=='fail'){
            alert(res.errors.content[0]);
        }
    }

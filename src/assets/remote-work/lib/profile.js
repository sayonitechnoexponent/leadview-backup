
function setContactProfile(response){
    // console.log(response);
    document.getElementById('contactProfile').innerHTML = response;
}
function updateContactInfo(){

    var isNew = 0;
    // if (sessionStorage.sessionCount) {
    //     // isNewSession = false;
    // } else { }

    if(sessionStorage.isSessionFlag) {
        // if(sessionStorage.isNewSession==0) {
        //     isNew = 1;
        //     sessionStorage.isNewSession = 1;
        // }
    }else{            
            isNew = 1;
            // alert(sessionStorage.isSessionFlag);
    }
    var vfrm = document.forms["contactInf"];
    var fields_arr = [ "first_name", "last_name", "region", "country","address"];
    if(validateForm(vfrm,fields_arr)==true){
        // console.log(vfrm);
        var fd = new FormData(vfrm);
        fd.append('is_new_session',isNew);
        httpRequest(apiUrl+'update-live-contact-info','POST',fd,'uci');
    }
}

function savePhoneInfo(type){
    var isNew = 0;
    if(sessionStorage.isSessionFlag) {
    }else{            
            isNew = 1;
    }
    
    if(type=='edit')    var vfrm = document.forms["ephoneInf"];
    else                var vfrm = document.forms["phoneInf"];
    // console.log(vfrm);
    var fields_arr = [ "dial_code", "phone_number"];
    if(validateForm(vfrm,fields_arr)==true){
        var fd = new FormData(vfrm);
        fd.append('is_new_session',isNew);
        httpRequest(apiUrl+'save-contact-phone-info','POST',fd,'cp');
    }
}

function editPhoneInfo(pdata){
    leadvioewopenpopup('showeditphoneModal');
    console.log('pdata',pdata);
    var ephoneInf = document.forms["ephoneInf"];
    ephoneInf['type'].value = pdata.type;
    ephoneInf['phone_number'].value = pdata.phone_number;
    ephoneInf['dial_code'].value = pdata.dial_code;
    ephoneInf['ph_id'].value = pdata.id;

}
function saveEmailInfo(type){
    var isNew = 0;
    if(sessionStorage.isSessionFlag) {
    }else{            
            isNew = 1;
    }

    if(type=='edit')    var vfrm = document.forms["editEmailInf"];
    else                var vfrm = document.forms["emailInf"];
    // console.log(vfrm);
    var fields_arr = [ "email"];
    if(validateForm(vfrm,fields_arr)==true){
        var fd = new FormData(vfrm);
        fd.append('is_new_session',isNew);
        httpRequest(apiUrl+'save-contact-email-info','POST',fd,'cp');
    }
}

function editEmailInfo(pdata){
    leadvioewopenpopup('showeditemailModal')
    console.log('pdata',pdata);
    var editInf = document.forms["editEmailInf"];
    editInf['type'].value = pdata.type;
    editInf['email'].value = pdata.email;
    editInf['email_id'].value = pdata.id;

}
function saveSocialInfo(){
    var isNew = 0;
    if(sessionStorage.isSessionFlag) {
    }else{            
            isNew = 1;
    }
    var vfrm = document.forms["socialInf"];
    // console.log(vfrm);
    var fields_arr = [ "type","link"];
    if(validateForm(vfrm,fields_arr)==true){
        var fd = new FormData(vfrm);
        fd.append('is_new_session',isNew);
        httpRequest(apiUrl+'save-contact-social-info','POST',fd,'cs');
    }
}

function requestRemoval(pid,contactId){
    // alert(pid);
    // alert(contactId);
    var fd = new FormData();
    fd.append('project_id',pid);
    fd.append('contact_id',contactId);

    httpRequest(apiUrl+'send-personal-removal-request','POST',fd,'rpdr');
}

function showRemovalMsg(status){
    var removalMsg = document.getElementById('removalMsg');
    if(status == 'success') {
        removalMsg.innerHTML ='Mail Send Successfull';
        setTimeout(function(){ removalMsg.innerHTML =''; }, 3000);
    }else{
        removalMsg.innerHTML ='Error';
        setTimeout(function(){ removalMsg.innerHTML =''; }, 3000);
    }
}

function changeDp(event){
    if (event.target.files && event.target.files.length > 0) {
        var dpfile = event.target.files[0];
        // console.log(dpfile);
        // let blob = null;
        // if (file) blob = this.getBase64(file);
        // var that = this;
        // blob.then(function (result) {
        //     //console.log(result);
        //     //that.blob_picture = that.sanitizer.bypassSecurityTrustUrl(result);
        //     that.browsImage = (result);
        // });
        var fd = new FormData();        
        fd.append('contact_id',contact_id);        
        fd.append('display_pic',dpfile);
        
        httpRequest(apiUrl+'update-contact-dp','POST',fd,'cdp');
    }
}

function chnageProfilePic(res){
    // console.log(res);
    if(res.status=="success" && res.feed != null && res.feed.profile_picture != null)
    {
        var dpurl = ext_server_path+'profile_picture/'+res.feed.profile_picture;  
        document.getElementById('cntDp').src = dpurl;      
    }
}



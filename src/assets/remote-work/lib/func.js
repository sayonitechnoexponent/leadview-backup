// console.log(currUrl);
var myTags = [];
var managerRoom = null;
var currentManager = null;

function setChatTemplate() {

}

function createChatBox(cboxData)
{
    blob_file = '';
    blob_file_name = '';
    var res_data = (cboxData);
    currProject = res_data.project;
    currRoom = currProject.title;

    contact_managers = currProject.users;
    // console.log(currProject);
    var project = (res_data.project);
    var messages = (res_data.messages);
    if (res_data.default_operator){
        default_chat_operator = res_data.default_operator;
        cm_id = res_data.default_operator.id;
    }

    visitor =  (res_data.visitor);
    if(visitor){
        vtagscores = visitor.tagscores;
        my_visitor_id = visitor.id;
        if(res_data.contact != null){
            contact_data = res_data.contact;
            contact_id = res_data.contact.id?res_data.contact.id:'';
        }
        // console.log(myTags);
    }
    if(currProject.user){
        user_api_token = currProject.user.api_token;
    }
    // alert(user_api_token);
    var data = (res_data.chat_box);
    chat_box_setting = res_data.chat_box;
    db_languages = res_data.languages;
    // console.log(chat_box_setting);
    var chat_template = (res_data.chat_template);
    chat_template = chat_template.replace(new RegExp('{{public_url}}', 'g'), 'http://www.leadview360.com/');
// .replace("{{public_url}}", "http://www.leadview360.com/");

    if(chat_box_setting !== null && chat_box_setting.status == 1){
        // console.log('chat_box_setting',chat_box_setting);

        if(chat_box_setting.chat_tab_config)   chat_tab_config = chat_box_setting.chat_tab_config;
        if(chat_box_setting.directory_tab_config)   directory_tab_config = chat_box_setting.directory_tab_config;
        if(chat_box_setting.preference_tab_config)   preference_tab_config = chat_box_setting.preference_tab_config;
        if(chat_box_setting.social_tab_config)   social_tab_config = chat_box_setting.social_tab_config;
        if(chat_box_setting.profile_tab_config)   profile_tab_config = chat_box_setting.profile_tab_config;

        /*if(chat_box_setting.show_directory==0 && document.getElementById('directoryTab'))   document.getElementById('directoryTab').style.display='none';
        if(chat_box_setting.show_preferences==0 && document.getElementById('prefTab'))   document.getElementById('prefTab').style.display='none';
        if(chat_box_setting.show_social==0 && document.getElementById('socialTab'))   document.getElementById('socialTab').style.display='none';
        if(chat_box_setting.show_timeline==0 && document.getElementById('tlTab'))   document.getElementById('tlTab').style.display='none';
        if(chat_box_setting.show_profile==0 && document.getElementById('proTab'))   document.getElementById('proTab').style.display='none';*/

        chat_template = modifyContent(chat_template);

        var popStyle = 'popStyle'+chat_box_setting.layout;
        chat_template = chat_template.replace(new RegExp('{{pop_style}}', 'g'), popStyle);

        var headerStyle = 'background:'+chat_box_setting.header_color+' !important;';
        if(chat_box_setting.header_text_color)    headerStyle += 'color:'+chat_box_setting.header_text_color;
        chat_template = chat_template.replace(new RegExp('{{header_color}}', 'g'), headerStyle);
        var backgoundStyle = 'background-color:'+chat_box_setting.bg_color+';';
        backgoundStyle += 'color:'+chat_box_setting.text_color;

        chat_template = chat_template.replace(new RegExp('{{backgound_color}}', 'g'), backgoundStyle);

        var textStyle = 'color:'+chat_box_setting.text_color+' !important;';
        chat_template = chat_template.replace(new RegExp('{{text_color}}', 'g'), textStyle);

        if(visitor!=null) {
            // alert('1');
            var fromObject = { 
                'functional_coockie': visitor.functional_cookie, 
                'analytical_coockie': visitor.analytical_cookie, 
                'marketing_coockie': visitor.marketing_cookie, 
            };
            // console.log('fromObject',fromObject);
            
            localStorage.setItem('cookie_option', JSON.stringify(fromObject));

            var cookie_option = JSON.parse(localStorage.getItem('cookie_option'));

            chat_template = chat_template.replace(new RegExp('{{check_cookie_opt}}', 'g'), 'display:none');
            // console.log(cookie_option.functional_coockie);
            var functional_coockie_checked = (visitor.functional_cookie==1)?'checked':'';
            chat_template = chat_template.replace(new RegExp('{{functional_coockie_checked}}', 'g'), functional_coockie_checked);

            var analytical_coockie_checked = (visitor.analytical_cookie==1)?'checked':'';
            chat_template = chat_template.replace(new RegExp('{{analytical_coockie_checked}}', 'g'), analytical_coockie_checked);

            var marketing_coockie_checked = (visitor.marketing_cookie==1)?'checked':'';
            chat_template = chat_template.replace(new RegExp('{{marketing_coockie_checked}}', 'g'), marketing_coockie_checked);

        }
       /* var tagStyle = 'background-color:'+chat_box_setting.text_color+' !important;';
        chat_template = chat_template.replace(new RegExp('{{text_color}}', 'g'), tagStyle);*/
        // console.log(chat_template);
        var _str = '<link rel="stylesheet" href="'+ext_path+'custom-cb.css">\\n' +
            '<link rel="stylesheet" href="'+ext_path+'jquery.mCustomScrollbar.css">\n' +chat_template;


        var divTag = document.createElement('div');
        divTag.innerHTML = _str;
        // divTag.innerHTML += '<object type="text/html" data="'+ext_path+'cb.html" ></object>';
        // divTag.innerHTML += _styl;

        divTag.setAttribute('class', 'chat_window');
        divTag.setAttribute('id', 'chat_window');


        if(myIp){
            var element = document.getElementById('chat_window');
            if(element) element.parentNode.removeChild(element);
            document.body.appendChild(divTag);
            if(project) {
                if(currUrl=='undefined')    currUrl='';
                if(currTitle=='undefined')    currTitle='';

                var sdata = {room: currRoom,project_id: currProject.id, socket_id: socket_id, type: 'visitor', vip: myIp, host: host_name, page_url: currUrl, page_title: currTitle};
                socket.emit('room', sdata);
                // alert(socket.id);
            }
        }

        if(messages !== null &&  document.getElementById("chatArea")){
            getMyMessages(messages);
        }

        createManagerList(contact_managers);
    }
    

}

function createManagerList(data) {
    // console.log(data);
    var listHtml = '';
    var txtcolor='';
    if(chat_box_setting && chat_box_setting.text_color)
        txtcolor=chat_box_setting.text_color+' !important';

    if (data.length >0){
        data.forEach(function(val) {
            var is_available = 'offline'; var usr_chat_setting = null;
            if(val.chat_setting){
                usr_chat_setting = val.chat_setting;
                if(usr_chat_setting.available_status==1) is_available = 'online';
            }

            listHtml += '<li>\n' +
                '                            <a href="javascript:;">\n' +
                '                                <figure>\n' +
                '                                    <img src="'+ext_server_path+'profile_picture/'+val.profile_picture+'" class="img-responsive">\n' +
                '                                </figure>\n' +
                '                                <h4 style="color: '+txtcolor+'" title="'+is_available+'"><i class="'+is_available+'"></i>'+val.name+'<small style="color: '+txtcolor+'">'+(val.designation!='null'?val.designation:'')+'</small></h4>\n' +
                '                            </a>\n' +
                '                            <div class="userlistbuttons">\n' +
                '                                <span class="userbutton"><a href class="schedulemetting">Schedule a Meeting</a></span>\n';

                if (usr_chat_setting && usr_chat_setting.available_status==1)
                {
                    listHtml +=' <span class="userbutton"><a href="javascript:;" class="leavemessage" onclick="leavemessage('+val.id+')">Leave a Message</a></span>\n';
                }
                else if (usr_chat_setting && usr_chat_setting.available_status==0)
                {
                    var offmsg =(usr_chat_setting.offline_message !=null)?usr_chat_setting.offline_message:'Not Available Now';
                    listHtml +=' <br/><span class="warning">'+offmsg+'</span>\n';
                }

                    listHtml +=' </div>\n' +
                ' </li>';
        });
    }

    document.getElementById('managerList').innerHTML = listHtml;
}

function selectFile(event) {
    blob_file = '';
    blob_file_name = '';
    var anyfile = event.target.files[0];
    // console.log(anyfile);
    if(anyfile) {
        blob_file_name = anyfile.name;
        blob_file = anyfile;
    }
    var logElem = document.getElementById("errDv");
    logElem.style.color = '#105591';
    logElem.innerHTML = blob_file_name;

}

function getMyMessages(msgs){
    var chatElmnt = document.getElementById("chatArea");
    var cHtml ='';
    chatElmnt.innerHTML = cHtml;
    var txtcolor='';
    if(chat_box_setting && chat_box_setting.text_color)
        txtcolor=chat_box_setting.text_color+' !important';

    msgs.forEach(function(val) {

        var msg_obj = val.message;
        if(val.type == 2){
            msg_obj = '<a download=""  target="_blank" href="'+ext_server_path+'message_attachment/'+val.msg_file+'">'+val.msg_file+'</a>';
        }
        // console.log(val);
        if(val.from_ip == myIp){
            cHtml += '<li class="me">\n' +
                // '                    <a data-toggle="tooltip" data-placement="top" title="Tooltip on left" href="javascript:void(0);" class="avatarchatimg round">\n' +
                // '                        <img src="'+ext_path+'avatar2.png" class="img-responsive">\n' +
                // '                    </a>\n' +
                '                    <div class="chattxt">\n' + msg_obj +
                '                    </div>\n' +
                '                    <div class="btn-block"></div>\n' +
                '                    <span class="timechat" style="color: '+txtcolor+'">'+val.created_at+'</span>\n' +
                '                    <div class="clearfix"></div>\n' +
                '                </li>';
        }else{
            cHtml += '\n' +
                '                <li class="she">\n' +
                '                    <a data-toggle="tooltip" data-placement="top" title="Tooltip on left" href="javascript:void(0);" class="avatarchatimg round">\n' +
                '                        <img src="'+ext_path+'avatar.png" class="img-responsive">\n' +
                '                    </a>\n' +
                '                    <div class="chattxt">\n' +msg_obj +
                '                    </div>\n' +
                '                    <div class="btn-block"></div>\n' +
                '                    <span class="timechat" style="color: '+txtcolor+'">'+val.created_at+'</span>\n' +
                '                    <div class="clearfix"></div>\n' +
                '                </li>';
        }
    });
    chatElmnt.innerHTML = cHtml;

    // console.log(chatElmnt.scrollHeight);
    document.getElementById("chatmessage").scrollTop = chatElmnt.scrollHeight;
}

function toggleBox(){
    var cobj = document.getElementById('chbx');
    // alert(cobj.style.display);
    if(cobj.style.display == 'block' || cobj.style.display == '')  cobj.style.display = 'none';
    else                               cobj.style.display = 'block';
}


function typeMessage(event,msg){
    // event.preventDefault();
    var param = {user:socket_id , type: 'remote'};
    // console.log(socket);
    socket.emit('start_typing', param);
    // Number 13 is the "Enter" key on the keyboard
    if (event.keyCode === 13) {
        msg = msg.trim();

        if(agent_socket_id!='' ){

            document.getElementById("errDv").innerHTML = "";
            if(msg !=''){
                var chatParam = {from_user: visitor, room: currRoom, socketid: agent_socket_id, is_file:'N', message: msg, sender: socket_id, sender_ip: myIp, type : 'v', msgtype : operator_type}
                console.log(chatParam);
                socket.emit('send_message', chatParam);

                var chatElmnt = document.getElementById("chatArea");
                var cHtml = chatElmnt.innerHTML;
                cHtml += '<li class="me">\n' +
                    // '                    <a data-toggle="tooltip" data-placement="top" title="Tooltip on left" href="javascript:void(0);" class="avatarchatimg round">\n' +
                    // '                        <img src="'+ext_path+'avatar2.png" class="img-responsive">\n' +
                    // '                    </a>\n' +
                    '                    <div class="chattxt">\n' + msg +
                    '                    </div>\n' +
                    '                    <div class="btn-block"></div>\n' +
                    '                    <span class="timechat">10:22 am</span>\n' +
                    '                    <div class="clearfix"></div>\n' +
                    '                </li>';
                chatElmnt.innerHTML = cHtml;
                document.getElementById("chatmessage").scrollTop = chatElmnt.scrollHeight;
            }

            // var postParam = 'visitor_id='+my_visitor_id+'&from_ip='+myIp+'&to_ip='+agent_ip+'&message='+msg+'&message_file='+blob_file;
            var fd = new FormData();
            fd.append('visitor_id', my_visitor_id);
            fd.append('from_ip', myIp);
            fd.append('sender_type', 'V');
            fd.append('to_ip', agent_ip);
            fd.append('message', msg);
            fd.append('chat_user', cm_id);
            if(blob_file!='')    fd.append('message_file', blob_file);
            // console.log(fd);
            if(blob_file!='' || msg!=''){
            var resp = httpRequest(apiUrl+'send-message','POST',fd,'sm');
            }

            document.getElementById('inputArea').value = "";
            document.getElementById("fileAttach").value = "";
        }
    }
}

function getTagScores(response) {
    if(response.activity){
        currentActivity = response.activity;
        if(response.activity.tag_scores)
        {
            // console.log(response.activity.tag_scores);
            tagsObj = response.activity.tag_scores;
            response.activity.tag_scores.forEach(element => {
                // console.log(element);
                setTagScore(element);
        });
            document.addEventListener("scroll", scrollFunction);
        }
    }
    checkCurrentSession();
}

function setTagScore(elem) {

    if(elem.delay >0 && elem.remove_score == 0){
        var tagDelay = parseInt(elem.delay) * 1000;

        setTimeout(function () {
            // console.log(tagDelay);
            // console.log(elem);
            // console.log(my_visitor_id);
            var fd = new FormData();
            fd.append('tag_score_id',elem.id);
            fd.append('visitor_id',my_visitor_id);

            if(!inIframe()){
                var tgx = httpRequest(apiUrl+'visitor-score-add','POST',fd,'tg');
            }
        }, tagDelay);
    }
}

function scrollFunction() {
    /* console.log(window.innerHeight );
     console.log(window.pageYOffset );*/
    var scrollPercentage = Math.round((window.pageYOffset/document.body.offsetHeight)*100);
    // console.log( scrollPercentage);
    var isScroll = false;
    tagsObj.filter(function (entry) {
        if(entry.scroll >= scrollPercentage){
            isScroll =  true;
            var fd = new FormData();
            fd.append('tag_score_id',entry.id);
            fd.append('visitor_id',my_visitor_id);

            if(!inIframe()){
                var tgx = httpRequest(apiUrl+'visitor-score-add','POST',fd,'tg');
            }
        }
    });
    // console.log( isScroll);
}

// popup related js
function saveVisitorInfo(){
    var projectId='';
    var vfrm = document.forms["visitorInf"];
    // alert(vfrm["email_address"].value);
    // console.log((vfrm.tc.checked));
    // console.log((vfrm.tc.type));

    var fields_arr = ["salute", "first_name", "last_name", "company", "role", "phone", "email", "language", "terms_condition"];

    if (chat_tab_config.is_company==0)  fields_arr.splice(3,1);
    if (chat_tab_config.is_role==0)  fields_arr.splice(4,1);
    if (chat_tab_config.is_phone==0)  fields_arr.splice(5,1);

    if(validateForm(vfrm,fields_arr)==true){

        // alert('Your information successfully saved!');
        // openchatlist();
        if(currProject.id)  projectId = currProject.id;
        // alert(myIp+'~'+currProject.id);
        var fd = new FormData(vfrm);
        // fd.append('visitor_id', my_visitor_id);
        fd.append('project_id', projectId);
        fd.append('local_ip', myIp);
        fd.append('socket_id', socket_id);
        fd.append('page_url',currUrl);
        fd.append('page_title',currTitle);
        httpRequest(apiUrl+'save-live-contact-info','POST',fd,'ci');
    }

}

function saveCmVisitorInfo(){
    var projectId='';
    var vfrm = document.forms["cmVisitorInf"];
    // alert(vfrm["email_address"].value);
    // console.log((vfrm.tc.checked));
    // console.log((vfrm.tc.type));

    var fields_arr = ["salute", "first_name", "last_name", "company", "role", "phone", "email", "message"];

    if (chat_tab_config.is_company==0)  fields_arr.splice(3,1);
    if (chat_tab_config.is_role==0)  fields_arr.splice(4,1);
    if (chat_tab_config.is_phone==0)  fields_arr.splice(5,1);

    if(validateForm(vfrm,fields_arr)==true){

        // alert('Your information successfully saved!');
        // openchatlist();
        if(currProject.id)  projectId = currProject.id;
        // alert(myIp+'~'+currProject.id);
        var fd = new FormData(vfrm);
        // fd.append('visitor_id', my_visitor_id);
        fd.append('project_id', projectId);
        fd.append('local_ip', myIp);
        fd.append('socket_id', socket_id);
        fd.append('page_url',currUrl);
        fd.append('page_title',currTitle);
        httpRequest(apiUrl+'save-live-contact-info','POST',fd,'ci');
    }

}


function setTagSection(){
    myTags =[];

    if(visitor && vtagscores && vtagscores.length>0){
        vtagscores.forEach(function (val) {
            myTags.push({'id':val.id,'tag_id':val.tag_id, 'title':val.tag.title});
        });
    }
    // console.log(myTags);
    var tagcolor='';
    if(chat_box_setting && chat_box_setting.text_color)
        tagcolor=chat_box_setting.text_color+' !important';
    var tgHtml = '';
    var tagList = document.getElementById('tagList');

    if(myTags.length >0 && preference_tab_config.show_tags==1){
        myTags.forEach(function (val) {
            tgHtml +=
                '<li>\n' +
                '\t<label>\n' +
                '\t\t<input checked="" value="'+val.id+'" onclick="deleteTag(event)" lick="" type="checkbox" class="hideitt" name="">\n' +
                '\t\t<div class="checkitemm" style="background-color:'+tagcolor+';border-color: '+tagcolor+';">\n' +
                val.title +
                '\t\t</div>\n' +
                '\t</label>\n' +
                '</li>';
        });
    }

    tagList.innerHTML=tgHtml;
}

function deleteTag(e) {
    // console.log(e.srcElement.checked);
    if (e.srcElement.checked == false){
        if(confirm("Are you sure to remove this tag?")) {
            var tag_score_id = e.srcElement.value;
            console.log(tag_score_id);
            var fd = new FormData();
            fd.append('visitor_id', my_visitor_id);
            fd.append('tag_score_id', tag_score_id);

            var resp = httpRequest(apiUrl + 'untag-contact', 'POST', fd, 'ut');
        }
        else    e.srcElement.checked = true;
    }

}
function openChatTab(){
    operator_type = 'nm';
    
    showchatFunction();
}
function leavemessage(managerId) {
    operator_type = 'cm';
    showchatFunction();
    if (managerId)    cm_id = managerId;
    // console.log(contact_managers);
    var conSort = contact_managers.filter(function (a) {
        return a.id == managerId;
    });

    if (currentManager != null){
        var ioParam = {action_type: 0, room: managerRoom, room_leave: currProject.title+'_'+ myIp, msgtype: operator_type}
        socket.emit('join_or_leave_room', ioParam);
    }

    currentManager = conSort[0];
    managerRoom = currentManager.email.trim()+'_cm_'+managerId;
    var ioParam = {action_type: 1, room: managerRoom, room_leave: currProject.title+'_'+ myIp, msgtype: operator_type};
    socket.emit('join_or_leave_room', ioParam);
    currRoom = managerRoom;
    // console.log(ioParam);

    var fd = new FormData();
    fd.append('vip', my_visitor_id);
    fd.append('chat_user_id', cm_id);
    var resp = httpRequest(apiUrl+'get-msgs-by-user','POST',fd,'dmsg');
    var chatElmnt = document.getElementById("chatArea");
    var cHtml ='';
    chatElmnt.innerHTML = cHtml;
    // alert(cm_id);
    // var sdata = {room: currRoom,project_id: currProject.id, socket_id: socket_id, type: 'visitor', vip: myIp, host: host_name, page_url: currUrl, page_title: currTitle};
    // socket.emit('room', sdata);
    /*if(contact_id!=''){

    }else{
        document.getElementById('searchmain').style.display = 'none';
        document.getElementById('leavemessagebox').style.display = 'block';
    }*/
}

function modifyContent(chat_template){
    if(chat_box_setting !=null){
        if(chat_box_setting.show_chat==0)   chat_template = chat_template.replace(new RegExp('{{hide_ct}}', 'g'), 'display:none;');
        if(chat_box_setting.show_directory==0)   chat_template = chat_template.replace(new RegExp('{{hide_directory}}', 'g'), 'display:none;');
        if(chat_box_setting.show_preferences==0)   chat_template = chat_template.replace(new RegExp('{{hide_pref}}', 'g'), 'display:none;');
        if(chat_box_setting.show_social==0)   chat_template = chat_template.replace(new RegExp('{{hide_social}}', 'g'), 'display:none;');
        if(chat_box_setting.show_timeline==0)   chat_template = chat_template.replace(new RegExp('{{hide_tl}}', 'g'), 'display:none;');
        if(chat_box_setting.show_profile==0)   chat_template = chat_template.replace(new RegExp('{{hide_pro}}', 'g'), 'display:none;');
    }
    if(chat_tab_config !=null){
        chat_template = chat_template.replace(new RegExp('{{chat_intro_text}}', 'g'), chat_tab_config.intro_text);
        chat_template = chat_template.replace(new RegExp('{{first_name_text}}', 'g'), chat_tab_config.first_name_text);
        chat_template = chat_template.replace(new RegExp('{{last_name_text}}', 'g'), chat_tab_config.last_name_text);
        chat_template = chat_template.replace(new RegExp('{{company_text}}', 'g'), chat_tab_config.company_text);
        chat_template = chat_template.replace(new RegExp('{{role_text}}', 'g'), chat_tab_config.role_text);
        chat_template = chat_template.replace(new RegExp('{{phone_text}}', 'g'), chat_tab_config.phone_text);
        chat_template = chat_template.replace(new RegExp('{{email_text}}', 'g'), chat_tab_config.email_text);
        chat_template = chat_template.replace(new RegExp('{{gdpr_text}}', 'g'), chat_tab_config.gdpr_text);
        chat_template = chat_template.replace(new RegExp('{{start_chat_text}}', 'g'), chat_tab_config.start_chat_text);
        chat_template = chat_template.replace(new RegExp('{{language_text}}', 'g'), chat_tab_config.language_text);
        
        if (chat_tab_config.is_company==0){
            chat_template = chat_template.replace(new RegExp('{{hide_company}}', 'g'), 'display:none;');
        }
        if (chat_tab_config.is_role==0){
            chat_template = chat_template.replace(new RegExp('{{hide_role}}', 'g'), 'display:none;');
        }
        if (chat_tab_config.is_phone==0){
            chat_template = chat_template.replace(new RegExp('{{hide_phone}}', 'g'), 'display:none;');
        }
        if (chat_tab_config.is_language==0){
            chat_template = chat_template.replace(new RegExp('{{hide_language}}', 'g'), 'display:none;');
        }
        if (db_languages.length >0){
            var lang_html = '';
            db_languages.forEach(function(val) {
                lang_html += '<option value="'+val.id+'">'+val.name+'</option>';
            });
            chat_template = chat_template.replace(new RegExp('{{set_languages}}', 'g'), lang_html);
        }

    }
    if(directory_tab_config !=null){
        chat_template = chat_template.replace(new RegExp('{{dir_intro_text}}', 'g'), directory_tab_config.intro_dir);
        chat_template = chat_template.replace(new RegExp('{{first_name_dir}}', 'g'), directory_tab_config.first_name_dir);
        chat_template = chat_template.replace(new RegExp('{{last_name_dir}}', 'g'), directory_tab_config.last_name_dir);
        chat_template = chat_template.replace(new RegExp('{{company_dir}}', 'g'), directory_tab_config.company_dir);
        chat_template = chat_template.replace(new RegExp('{{role_dir}}', 'g'), directory_tab_config.role_dir);
        chat_template = chat_template.replace(new RegExp('{{phone_dir}}', 'g'), directory_tab_config.phone_dir);
        chat_template = chat_template.replace(new RegExp('{{email_dir}}', 'g'), directory_tab_config.email_dir);
        chat_template = chat_template.replace(new RegExp('{{start_chat_dir}}', 'g'), directory_tab_config.start_chat_dir);
        chat_template = chat_template.replace(new RegExp('{{message_dir}}', 'g'), directory_tab_config.message_dir);
        
        if (directory_tab_config.is_company_dir==0){
            chat_template = chat_template.replace(new RegExp('{{hide_company_dir}}', 'g'), 'display:none;');
        }
        if (directory_tab_config.is_role_dir==0){
            chat_template = chat_template.replace(new RegExp('{{hide_role_dir}}', 'g'), 'display:none;');
        }
        if (directory_tab_config.is_phone_dir==0){
            chat_template = chat_template.replace(new RegExp('{{hide_phone_dir}}', 'g'), 'display:none;');
        }
        if (directory_tab_config.is_message_dir==0){
            chat_template = chat_template.replace(new RegExp('{{hide_message_dir}}', 'g'), 'display:none;');
        }

    }
    if(social_tab_config !=null){
        
        chat_template = chat_template.replace(new RegExp('{{bookmark_text}}', 'g'), social_tab_config.bookmark_text);

        if (social_tab_config.is_bookmark==0){
            chat_template = chat_template.replace(new RegExp('{{hide_bookmark}}', 'g'), 'display:none;');
        }
        if (social_tab_config.is_facebook==0){
            chat_template = chat_template.replace(new RegExp('{{hide_facebook}}', 'g'), 'display:none;');
        }
        if (social_tab_config.is_linkedin==0){
            chat_template = chat_template.replace(new RegExp('{{hide_linkedin}}', 'g'), 'display:none;');
        }
        if (social_tab_config.is_pinterest==0){
            chat_template = chat_template.replace(new RegExp('{{hide_pinterest}}', 'g'), 'display:none;');
        }
        if (social_tab_config.is_twitter==0){
            chat_template = chat_template.replace(new RegExp('{{hide_twitter}}', 'g'), 'display:none;');
        }

    }

    if(preference_tab_config !=null){
        if (preference_tab_config.show_email_me==0){
            chat_template = chat_template.replace(new RegExp('{{show_email_me}}', 'g'), 'display:none;');
        }
        if (preference_tab_config.show_marketing==0){
            chat_template = chat_template.replace(new RegExp('{{show_marketing}}', 'g'), 'display:none;');
        }
        if (preference_tab_config.show_gdpr==0){
            chat_template = chat_template.replace(new RegExp('{{show_pref_gdpr}}', 'g'), 'display:none;');
        }
        if (preference_tab_config.show_more==0){
            chat_template = chat_template.replace(new RegExp('{{show_more}}', 'g'), 'display:none;');
        }
        chat_template = chat_template.replace(new RegExp('{{email_me_text}}', 'g'), preference_tab_config.email_me_text);
        chat_template = chat_template.replace(new RegExp('{{marketing_text}}', 'g'), preference_tab_config.marketing_text);
        chat_template = chat_template.replace(new RegExp('{{gdpr_pref}}', 'g'), preference_tab_config.gdpr_pref);
        chat_template = chat_template.replace(new RegExp('{{more_text}}', 'g'), preference_tab_config.more_text);
    }

    if(profile_tab_config !=null){

        /*if (profile_tab_config.about_show_type==0){
            chat_template = chat_template.replace(new RegExp('{{about_show_type}}', 'g'), 'display:none;');
        }*/
        /* NEED TO CHECK ON THE PROFILE BLADE PAGE*/
    }
    return chat_template;
}

function saveAsBookmark(){
    // alert(currUrl);
    var fd = new FormData();
    fd.append('link',currUrl);
    fd.append('contact_id',contact_id);

    httpRequest(apiUrl+'make-book-mark','POST',fd,'bm');
}

function showBookMarkMsg(res){
    // alert(res.status);
    var dvMsg = document.getElementById('bookMarkMsg');
    if(res.status == 'success') {
        dvMsg.innerHTML ='BookMark Successfull';
        setTimeout(function(){ dvMsg.innerHTML =''; }, 3000);
    }else{
        dvMsg.innerHTML ='Error';
        setTimeout(function(){ dvMsg.innerHTML =''; }, 3000);
    }
}

function saveCookieInfo(){
    var projectId='';
    var vfrm = document.forms["cookieInf"];
    
    // console.log('vfrm',vfrm.functional_coockie.value);
    var fromObject = { 
        'functional_coockie': vfrm.functional_coockie.checked, 
        'analytical_coockie': vfrm.analytical_coockie.checked, 
        'marketing_coockie': vfrm.marketing_coockie.checked, 
    };
    console.log('fromObject',fromObject);
    
        // fd.append('local_ip', myIp);
  // Put the object into storage
    localStorage.setItem('cookie_option', JSON.stringify(fromObject));

// Retrieve the object from storage
var retrievedObject = JSON.parse(localStorage.getItem('cookie_option'));
    var fd = new FormData();
    fd.append('visitor_id', my_visitor_id);    
    fd.append('functional_coockie', vfrm.functional_coockie.checked);    
    fd.append('analytical_coockie', vfrm.analytical_coockie.checked);    
    fd.append('marketing_coockie', vfrm.marketing_coockie.checked);    
    // console.log(fd);
    var resp = httpRequest(apiUrl+'save-visitor-cookie','POST',fd,'vc');

// console.log('retrievedObject: ', JSON.parse(retrievedObject));
    lvclosemodalFunction();
}
function chnageCookie(res){
    console.log(res);
    setLiveChat();
}
function chat_filter(){

}

// function bowserClose(){
//     alert(1);
// }
// if (window.addEventListener) window.addEventListener("unload", bowserClose, false);
//   else if (window.attachEvent) window.attachEvent("onunload", bowserClose);
//   else window.onunload = bowserClose;

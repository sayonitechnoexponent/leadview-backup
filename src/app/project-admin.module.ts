import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes } from '@angular/router';
import { RouterModule } from '@angular/router';
import {GrowlModule} from 'primeng/primeng';
import {EditorModule} from 'primeng/editor';
import { CKEditorModule } from 'ng2-ckeditor';
import {AuthGuard} from './guards/auth.guard';
import {PublicGuard} from './guards/public.guard';
import { SharedModule } from './shared.module';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { CustomFormsModule } from 'ng2-validation';

import { MultiSelectModule } from 'primeng/multiselect';

import { MyTeamComponent } from './components/my-team/my-team.component';
import { TeamUsersComponent } from './components/my-team/team-users/team-users.component';
import { MyTagsComponent } from './components/my-tags/my-tags.component';
import { OpportunityStageComponent } from './components/opportunity-stage/opportunity-stage.component';
import { OpportunityTypeComponent } from './components/opportunity-type/opportunity-type.component';
import { NoteTypeComponent } from './components/note-type/note-type.component';
import { TaskTypeComponent } from './components/task-type/task-type.component';
import { GroupsComponent } from './components/groups/groups.component';
import { ChatSetupComponent } from './components/chat-setup/chat-setup.component';
import { GmapSetupComponent } from './components/gmap-setup/gmap-setup.component';
import { AutoTagComponent } from './components/auto-tag/auto-tag.component';
import { EmailRequestTemplateComponent } from './components/email-request-template/email-request-template.component';


const routes: Routes = [
    //{ path: '', component: MyProjectComponent, canActivate: [AuthGuard] },

    { path: 'my-team',  component: MyTeamComponent, canActivate: [AuthGuard] },
     { path: 'team-users/:teamId',  component: TeamUsersComponent, canActivate: [AuthGuard] },
     { path: 'team-users',  component: TeamUsersComponent, canActivate: [AuthGuard] },
    { path: 'my-tags',  component: MyTagsComponent, canActivate: [AuthGuard] },
    { path: 'auto-tags',  component: AutoTagComponent, canActivate: [AuthGuard] },
    { path: 'opportunity-stage',  component: OpportunityStageComponent, canActivate: [AuthGuard] },
    { path: 'opportunity-type',  component: OpportunityTypeComponent, canActivate: [AuthGuard] },
    { path: 'note-type',  component: NoteTypeComponent, canActivate: [AuthGuard] },
    { path: 'task-type',  component: TaskTypeComponent, canActivate: [AuthGuard] },
    { path: 'groups',  component: GroupsComponent, canActivate: [AuthGuard] },
    { path: 'chat-setup',  component: ChatSetupComponent, canActivate: [AuthGuard] },
    { path: 'map-setup',  component: GmapSetupComponent, canActivate: [AuthGuard] },
    { path: 'email-request-template',  component: EmailRequestTemplateComponent, canActivate: [AuthGuard] },
];

@NgModule({
  imports: [
    CommonModule,
      CustomFormsModule,
      SharedModule,
      FormsModule,
      GrowlModule,
      AngularMultiSelectModule,
      MultiSelectModule,
      EditorModule,
      CKEditorModule,
      RouterModule.forChild(routes)
  ],
  declarations: [MyTeamComponent,TeamUsersComponent,MyTagsComponent,OpportunityStageComponent,OpportunityTypeComponent,NoteTypeComponent, TaskTypeComponent, GroupsComponent, ChatSetupComponent, GmapSetupComponent, AutoTagComponent,EmailRequestTemplateComponent],
    providers: [
        PublicGuard,
        AuthGuard
    ]
})
export class ProjectAdminModule { }

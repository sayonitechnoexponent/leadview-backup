import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {AuthGuard} from '../guards/auth.guard';
import {AdminGuard} from '../guards/admin.guard';
import {PublicGuard} from '../guards/public.guard';

import { SignupComponent } from '../components/@auth/signup/signup.component';
import { LoginComponent } from '../components/@auth/login/login.component';
import { CreateProfileComponent } from '../components/@auth/create-profile/create-profile.component';
import { MyAccountComponent } from '../components/my-account/my-account.component';
import { ChangePasswordComponent } from '../components/@auth/change-password/change-password.component';
import { ForgotPasswordComponent } from '../components/@auth/forgot-password/forgot-password.component';

//import { MyProjectComponent } from '../components/my-project/my-project.component';
//import { ProjectDetailsComponent } from '../components/my-project/project-details/project-details.component';
// import { MyTeamComponent } from '../components/my-team/my-team.component';
// import { TeamUsersComponent } from '../components/my-team/team-users/team-users.component';
// import { MyTagsComponent } from '../components/my-tags/my-tags.component';
// import { OpportunityStageComponent } from '../components/opportunity-stage/opportunity-stage.component';
// import { OpportunityTypeComponent } from '../components/opportunity-type/opportunity-type.component';

//import { AddPackageComponent } from '../components/pricing/add-package/add-package.component';
// import { PricingComponent } from '../components/pricing/pricing.component';
//import { AdminComponent } from '../components/admin/admin.component';
// import { ProjectsComponent } from '../components/admin/projects/projects.component';
// import { TaxTypeComponent } from '../components/admin/tax-type/tax-type.component';

//import { UsersTimelineComponent } from '../components/users-timeline/users-timeline.component';
// import { NoteTypeComponent } from '../components/note-type/note-type.component';


const routes: Routes = [
    { path: '', redirectTo: '/login', pathMatch: 'full' }, // remove this code when home page created
    { path: 'login',  component: LoginComponent, canActivate: [PublicGuard] },
    { path: 'signup',  component: SignupComponent, canActivate: [PublicGuard] },
    { path: 'create-profile/:id',  component: CreateProfileComponent, canActivate: [PublicGuard] },
    { path: 'change-password/:id',  component: ChangePasswordComponent, canActivate: [PublicGuard] },
    { path: 'forgot-password',  component: ForgotPasswordComponent, canActivate: [PublicGuard] },
    { path: 'my-account',  component: MyAccountComponent, canActivate: [AuthGuard] },

    { path: 'my-project', loadChildren: '../project.module#ProjectModule' },

    { path: 'manage', loadChildren: '../project-admin.module#ProjectAdminModule' },

    { path: 'admin',  loadChildren:'../admin.module#AdminModule' },

    { path: 'time-line', loadChildren: '../timeline.module#TimelineModule' },
    { path: 'time-line/:id', loadChildren: '../timeline.module#TimelineModule' },
    { path: 'contacts/time-line/:contact_id', loadChildren: '../timeline.module#TimelineModule' },
    { path: 'visitor/time-line/:visitor_id', loadChildren: '../timeline.module#TimelineModule' },

    { path: 'contacts', loadChildren: '../user.module#UserModule' },
    { path: 'user', loadChildren: '../user.module#UserModule' },
    { path: 'visitor', loadChildren: '../user.module#UserModule' },

];

@NgModule({
    imports: [
        RouterModule.forRoot(
            routes,
            { enableTracing: true } // <-- debugging purposes only
        )
    ],
    exports: [ RouterModule ]
})
export class AppRoutingModule {}


import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes } from '@angular/router';
import { RouterModule } from '@angular/router';

import {AuthGuard} from './guards/auth.guard';
import {AdminGuard} from './guards/admin.guard';
import {PublicGuard} from './guards/public.guard';

import {GrowlModule} from 'primeng/primeng';

import { MyProjectComponent } from './components/my-project/my-project.component';
import { ProjectDetailsComponent } from './components/my-project/project-details/project-details.component';
import { SubscriptionComponent } from './components/my-project/subscription/subscription.component';
import { PlansComponent } from './components/my-project/plans/plans.component';


declare var $: any;

const routes: Routes = [
    { path: '', component: MyProjectComponent, canActivate: [AuthGuard] },
    { path: 'project-details/:id', component: ProjectDetailsComponent, canActivate: [AuthGuard] },
    { path: 'subscription/:id', component: SubscriptionComponent, canActivate: [AuthGuard] },
    { path: 'plans/:id', component: PlansComponent, canActivate: [AuthGuard] }

];



@NgModule({
  imports: [
    CommonModule,
      FormsModule,
      GrowlModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MyProjectComponent,ProjectDetailsComponent, SubscriptionComponent,PlansComponent],
    providers: [
        PublicGuard,
        AuthGuard,
        AdminGuard
    ],
})
export class ProjectModule { }

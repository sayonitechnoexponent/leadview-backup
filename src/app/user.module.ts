import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes } from '@angular/router';
import { RouterModule } from '@angular/router';
import { ModalModule } from 'ngx-bootstrap';

import {AuthGuard} from './guards/auth.guard';
import {PublicGuard} from './guards/public.guard';

import {GrowlModule} from 'primeng/primeng';
import { MomentModule } from 'angular2-moment';


import { SharedModule } from './shared.module';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { TableModule } from 'primeng/table';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { MultiSelectModule } from 'primeng/multiselect';
import { Ng4GeoautocompleteModule } from 'ng4-geoautocomplete';
import {CalendarModule} from 'primeng/calendar';
import { FullCalendarModule } from 'ng-fullcalendar';
import {FileUploadModule} from 'primeng/fileupload';
import {EditorModule} from 'primeng/editor';

import { TasksComponent } from './components/tasks/tasks.component';
import { ContactsManagementComponent } from './components/contacts-management/contacts-management.component';
import { MessageComponent } from './components/message/message.component';
import { VisitorsComponent } from './components/visitors/visitors.component';
import { OpportunitiesComponent } from './components/opportunities/opportunities.component';
import { TasksCalendarComponent } from './components/tasks/tasks-calendar/tasks-calendar.component';
import { OpportunitiesCalendarComponent } from './components/opportunities/opportunities-calendar/opportunities-calendar.component';
import { MyFilesComponent } from './components/my-files/my-files.component';
import { FileTimeLineComponent } from './components/file-time-line/file-time-line.component';
import { UrlTimeLineComponent } from './components/url-time-line/url-time-line.component';
import { MailBoxComponent } from './components/mail-box/mail-box.component';
import { EmailTimelineComponent } from './components/email-timeline/email-timeline.component';


const routes: Routes = [
    { path: '',  component: ContactsManagementComponent, canActivate: [AuthGuard] },
    { path: 'list',  component: VisitorsComponent, canActivate: [AuthGuard] },
    { path: 'messages',  component: MessageComponent, canActivate: [AuthGuard] },
    { path: 'tasks',  component: TasksComponent, canActivate: [AuthGuard] },
    { path: 'opportunities',  component: OpportunitiesComponent, canActivate: [AuthGuard] },
    { path: 'files',  component: MyFilesComponent, canActivate: [AuthGuard] },
    { path: 'file-timeline',  component: FileTimeLineComponent, canActivate: [AuthGuard] },
    { path: 'url-timeline',  component: UrlTimeLineComponent, canActivate: [AuthGuard] },
    { path: 'emails',  component: MailBoxComponent, canActivate: [AuthGuard] },
    { path: 'email-timeline',  component: EmailTimelineComponent, canActivate: [AuthGuard] },
];

@NgModule({
  imports: [
      ModalModule.forRoot(),
    CommonModule,
      FormsModule,
      GrowlModule,
      SharedModule,
      MomentModule,
      AngularMultiSelectModule,
      TableModule,
      AutoCompleteModule,
      MultiSelectModule,
      Ng4GeoautocompleteModule,
      CalendarModule,
      FullCalendarModule,
      FileUploadModule,
      EditorModule,
      RouterModule.forChild(routes)
  ],
  declarations: [ContactsManagementComponent, MessageComponent, VisitorsComponent, TasksComponent, OpportunitiesComponent, TasksCalendarComponent, OpportunitiesCalendarComponent, MyFilesComponent, FileTimeLineComponent, UrlTimeLineComponent, MailBoxComponent, EmailTimelineComponent],
    providers: [
        PublicGuard,
        AuthGuard,
    ],
})
export class UserModule { }

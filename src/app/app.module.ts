// @angular
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { ModalModule } from 'ngx-bootstrap';
// route
import { AppRoutingModule } from './router/app-routing.module';

// third party
import { CustomFormsModule } from 'ng2-validation';
import {GrowlModule} from 'primeng/primeng';
import { RecaptchaModule } from 'ng-recaptcha';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';

import { ServiceWorkerModule } from '@angular/service-worker';
import { AppComponent } from './app.component';

import { environment } from '../environments/environment';

import {AuthGuard} from './guards/auth.guard';
import {AdminGuard} from './guards/admin.guard';
import {PublicGuard} from './guards/public.guard';

// services
import { AuthServices } from './services/auth.service';
import { UserService } from './services/user.service';
import { CommonService } from './services/common.service';
import { DataService } from "./services/data.service";

// app
import { SignupComponent } from './components/@auth/signup/signup.component';
import { LoginComponent } from './components/@auth/login/login.component';
import { CreateProfileComponent } from './components/@auth/create-profile/create-profile.component';
import { MyAccountComponent } from './components/my-account/my-account.component';
import { ProfileComponent } from './components/my-account/profile/profile.component';
import { SecurityComponent } from './components/my-account/security/security.component';
import { EmailSettingComponent } from './components/my-account/email-setting/email-setting.component';
import { ChatSettingComponent } from './components/my-account/chat-setting/chat-setting.component';

import { SharedModule } from './shared.module';
import { ChangePasswordComponent } from './components/@auth/change-password/change-password.component';
import { ForgotPasswordComponent } from './components/@auth/forgot-password/forgot-password.component';

@NgModule({
  declarations: [
    AppComponent,
    SignupComponent,
    LoginComponent,
    CreateProfileComponent,
    MyAccountComponent,
    ProfileComponent,
    SecurityComponent,
    EmailSettingComponent,
    ChatSettingComponent,
    ChangePasswordComponent,
    ForgotPasswordComponent,

  ],
  imports: [
      ModalModule,
    SharedModule,
    BrowserModule,
      BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    RecaptchaModule.forRoot(),
    AppRoutingModule,
    CustomFormsModule,
    GrowlModule,
    AngularMultiSelectModule,
    ServiceWorkerModule.register('/ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [
      DataService,
    CommonService,
    AuthServices,
    UserService,
    PublicGuard,
    AuthGuard,
    AdminGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { AuthServices } from './services/auth.service';
import { UserService } from './services/user.service';
import { DataService } from "./services/data.service";
import { appConfig } from './apiurl.config';
import {Message} from 'primeng/primeng';
import { DomSanitizer } from '@angular/platform-browser';


import * as socketIo from 'socket.io-client';
const socket = socketIo(appConfig.socketUrl);

declare var $: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{

    /**
     * @property \{{{any}}\} {{appConfig}} {{this will hold the constants for the app}}
     */

    appConfig: any = {};
  /**
   * @property \{{{any}}\} {{authUser}} {{this will hold the authuser}}
   */
    /**
     * @property \{{{Message[]}}\} {{msgs}} {{this is growl message model}}
     */

    msgs: Message[] = [];
    authUser: any = '';
  /**
   * @property \{{{any}}\} {{isLoggedinUser}} {{this will hold the isLoggedinUser}}
   */
  isLoggedinUser: boolean = false;

    myProjects: any = [];
    timezones: any = [];

    remote_page;
  /**
   * @constructor
   * @description {{DI will pushed here}}
   */
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthServices,
    private userService: UserService,
    private dataService: DataService,
    private sanitizer: DomSanitizer
  ) {
      this.isLoggedinUser = false;
  }

    changeIf(){
        /*this.userService.postadd()
            .subscribe((data: any) => {

            });
      this.remote_page = newPostaddedMessage;*/
    }
  /**
   * @func {{ngOnInit}}
   * @description {{angular lifecycle hook}}
   */
  ngOnInit() {
    // assign the auth user in the property
      this.dataService.currentMessage.subscribe(message => this.remote_page = this.sanitizer.bypassSecurityTrustResourceUrl(message));
      // this.remote_page =this.sanitizer.bypassSecurityTrustResourceUrl(this.remote_page);

      this.appConfig = appConfig;
    this.authUser = this.authService.getAuthUser();


      this.isLoggedinUser = false;
    // conditional load view change
    // if (this.authUser.api_token !== undefined) {
    if (this.authUser !=null && this.authUser.api_token !== undefined) {
      this.isLoggedinUser = true;
      if(this.authUser.type==2)  this.getMyProjects();
    }

  }
    /**
     * @func {{getMyProjects}}
     * @description {{get user projects}}
     */
    getMyProjects() {
        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);
        this.userService.myProjects(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        //this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Project Post Successful'});
                        this.myProjects =  (data.projects);
                        // this.timezones = JSON.parse(data.timezones);
                        //console.log(this.timezones);
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );


    }

    /**
     * @func {{logout}}
     * @description {{logout user}}
     */

    logout() {
        this.isLoggedinUser = false;
        localStorage.removeItem('leaduser');
        localStorage.removeItem('chat_setting');
        this.router.navigate(['/login']);
        location.href="/login";
    }

    setProjectlive(projectId){
        // alert(projectId);
        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);
        fd.append('project_id', projectId);
        fd.append('live_status', '1');

        this.userService.setUserProjectLive(fd)
            .subscribe(
                (data: any) => {
                    console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        //this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Project Post Successful'});
                        this.myProjects =  (data.projects);
                        this.authUser.live_project = data.live_project;
                        localStorage.setItem('leaduser', JSON.stringify(this.authUser));
                        // this.timezones = JSON.parse(data.timezones);
                        location.reload();
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );
    }


}

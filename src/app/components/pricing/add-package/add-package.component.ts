import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../../../services/user.service';
import { appConfig } from '../../../apiurl.config';
import {Message} from 'primeng/primeng';


@Component({
  selector: 'app-add-package',
  templateUrl: './add-package.component.html',
  styleUrls: ['./add-package.component.css']
})
export class AddPackageComponent implements OnInit {

    /**
     * @property \{{{any}}\} {{packagType}} {{this will hold the type of package}}
     */
    packageType: any = '';

    /**
     * @property \{{{any}}\} {{appConfig}} {{this will hold the constants for the app}}
     */

    appConfig: any = {};

    /**
     * @property \{{{any}}\} {{model}} {{security form model}}
     */

    model: any = {
        platform_id : '',
        package_id : '',
        addon_package_id: ''
    };
    /**
     * @property \{{{any}}\} {{authUser}} {{this will hold the authuser}}
     */

    authUser: any ={};
    /**
     * @property \{{{any}}\} {{platforms}} {{hold all platforms}}
     */

    platforms: any ;

    /**
     * @property \{{{any}}\} {{platforms}} {{hold all features of packages }}
     */

    features: any ;

    /**
     * @property \{{{Message[]}}\} {{msgs}} {{this is growl message model}}
     */

    msgs: Message[] = [];
    selected_features:any = [];
    packages: any=[];
    addonpackageId; any;
    packageId: any;
    /**
     * @constructor
     * @description {{DI will pushed here}}
     */

    constructor(
                private userService: UserService,
                private route: ActivatedRoute,
                private router: Router
    ) { }

    /**
     * @func {{ngOnInit}}
     * @description {{angular lifecycle hook}}
     */

    ngOnInit() {
        this.authUser= this.userService.getAuthUser();
        this.appConfig = appConfig;
        this.getAllPlatforms();
        this.getAllFeatures();
        this.model.features = [];

        this.route.params.subscribe(params => {
            if(params['type'] == 'add-on')  this.packageType = 2;
            else                            this.packageType = 1;
            if(params['id'])
            {
                // alert(params['id']);
                this.findCurrentPackage(params['id']);
            }
        });
         this.getPackages();
    }
    findCurrentPackage(package_id) {
        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);
        fd.append('package_id', package_id);
        if(this.packageType ==1){
            fd.append('type', '1');
        }
        else if(this.packageType ==2){
             fd.append('type', '2');
        }
        let listf = [];
        this.userService.findPackage(fd)
            .subscribe(
                (data: any) => {
                    console.log('getalldata',data);
                    if (data.status === 'success') {
                        this.model = data.package;
                        this.selected_features = data.features.map(item =>{
                            return Number(item);
                        });
                        // this.selected_features = data.features;
                        this.model.features = this.selected_features;
                        if(this.packageType == 1){
                            this.model.package_id = data.package.id;
                        }
                        else if(this.packageType == 2){
                            this.model.addon_package_id = data.package.id;
                        }
                        // this.model.package_id = data.package.id;
                        //this.model.features  = [];

                        data.package.features.forEach(function (element, i) {
                            listf.push(element.feature_id)
                            //this.model.features[i]=(element.feature_id);

                        });
                        // console.log(listf);
                        // this.model.features = listf;
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );

    }
    getAllPlatforms() {

        this.userService.getPlatforms()
            .subscribe(
                (data: any) => {
                    // console.log(data);
                    if (data.status === 'success') {
                        this.platforms = data.platforms;
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );

    }

    /**
     * @func {{savePackage}}
     * @param \{{{formobject}}\} {{myform}} {{this will pass the form object}}
     * @description {{this will submit the form data and add or update package data}}
     */
    savePackage(myform) {
        this.model.api_token =  this.authUser.api_token;
        this.model.type =  this.packageType;
        this.model.features = this.selected_features;
        console.log(this.model);

        this.userService.addPackage(this.model)
            .subscribe(
                (data: any) => {
                    console.log('savepackage',data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Package Successfully saved'});

                        // go to  admin packages page
                        this.router.navigate(['/admin/pricing']);
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );

    }

    getAllFeatures() {

        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);
        fd.append('locale', this.authUser.locale);
        this.userService.getFeatures(fd)
            .subscribe(
                (data: any) => {
                    // console.log(data);
                    if (data.status === 'success') {
                        this.features = data.features;

                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );

    }
    changeFrature($event) {
        console.log('event',$event.srcElement.value);
        if($event.srcElement.checked == true)
        {
           if(this.selected_features.indexOf($event.srcElement.value*1)== -1)
               this.selected_features.push($event.srcElement.value*1);
               console.log(this.selected_features);
        }else {

            this.selected_features.splice(this.selected_features.indexOf($event.srcElement.value*1), 1);
            console.log(this.selected_features);
        }
    }
    /**
     * @func {{getPackages}}
     * @param \{{{formobject}}\} {{myform}} {{this will pass the form object}}
     * @description {{this will submit the form data and add or update package data}}
     */
    getPackages() {
        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);
        fd.append('type', '1');

        this.userService.getCreatedPackages(fd)
            .subscribe(
                (data: any) => {
                    if (data.status === 'success') {
                        this.packages = data.packages;
                        console.log('packages',this.packages);
                    }
                },
                error => {
                    console.log(error);
                }

            );
    }
}

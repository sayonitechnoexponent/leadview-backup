import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { CommonService } from '../../services/common.service';
import { appConfig } from '../../apiurl.config';
import { Message } from 'primeng/primeng';

@Component({
    selector: 'app-pricing',
    templateUrl: './pricing.component.html',
    styleUrls: ['./pricing.component.css']
})
export class PricingComponent implements OnInit {
    /**
     * @property \{{{any}}\} {{appConfig}} {{this will hold the constants for the app}}
     */

    appConfig: any = {};

    /**
     * @property \{{{any}}\} {{authUser}} {{this will hold the authuser}}
     */

    authUser: any = {};
    /**
     * @property \{{{any}}\} {{packages}} {{hold all created packages}}
     */

    packages: any;
    /**
     * @property \{{{any}}\} {{addonPackages}} {{hold all created add-on packages}}
     */

    addonPackages: any=[];

    /**
     * @property \{{{Message[]}}\} {{msgs}} {{this is growl message model}}
     */

    msgs: Message[] = [];
    /**
     * @property \{{{any}}\} {{currentPackage}} {{hold all current package}}
     */

    currentPackage: any = [];
    /**
     * @constructor
     * @description {{DI will pushed here}}
     */

    constructor(
        private commonService: CommonService,
        private userService: UserService
    ) { }

    /**
     * @func {{openCopyModal}}
     * @param \{{{string}}\} {{$data}} {{popup data val class}}
     * @description {{this will me open the popup accordingly on click by pass data val}}
     */
    openCopyModal($data, pack) {
        this.commonService.openModal($data);
        this.currentPackage = pack;
        console.log(this.currentPackage.short_code)
    }

    /**
     * @func {{getPackages}}
     * @param \{{{formobject}}\} {{str}} {{this will pass the copy string}}
     * @description {{this will copy the string to the Clipboard}}
     */

    copyToClipboard(str) {
        let copy_string = str;
        //alert(copy_string);
        document.addEventListener('copy', (e: ClipboardEvent) => {
            e.clipboardData.setData('text/plain', copy_string);
            e.preventDefault();
            document.removeEventListener('copy');
        });
        document.execCommand('copy');
        this.msgs = [];
        this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Shortcode copied Successfully'});
    }

    /**
     * @func {{ngOnInit}}
     * @description {{angular lifecycle hook}}
     */

    ngOnInit() {
        this.authUser = this.userService.getAuthUser();
        this.appConfig = appConfig;
        this.getPackages();
    }

    /**
     * @func {{getPackages}}
     * @param \{{{formobject}}\} {{myform}} {{this will pass the form object}}
     * @description {{this will submit the form data and add or update package data}}
     */
    getPackages() {
        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);

        this.userService.getCreatedPackages(fd)
            .subscribe(
                (data: any) => {
                    // console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        this.packages = data.packages;
                        this.addonPackages = data.addonPackages;
                        console.log('addonPackages',this.addonPackages);
                    }
                },
                error => {
                    console.log(error);
                }

            );
    }
    /**
     * @func {{delPackage}}
     * @description {{delete user package}}
     */
    delPackage(packId) {
        console.log(packId);
        if (confirm("Are you sure to delete the package?")) {
            let fd = new FormData();
            fd.append('package_id', packId);
            fd.append('api_token', this.authUser.api_token);

            this.userService.deletePackage(fd)
                .subscribe(
                    (data: any) => {
                        if (data.status === 'success') {
                            this.getPackages();
                            // message
                            this.msgs = [];
                            this.msgs.push({ severity: 'success', summary: 'Success Message', detail: 'Package Successfully Deleted' });

                            // go to  my package page
                            // location.href = '/pricing';
                        }
                        if (data.status === 'fail') {
                            // message
                            this.msgs = [];
                            if (data.errors && Object.keys(data.errors).length > 0) {

                                for (var key in data.errors) {
                                    if (data.errors.hasOwnProperty(key)) {
                                        //console.log(key + " -> " + p[key]);
                                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                                    }
                                }

                            }
                            else if (data.error_message) {
                                this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                            }
                            else {
                                this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                            }
                        }
                    },
                    error => {
                        console.log(error);
                        this.msgs = [];
                        if (error && error.message) {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

                        }
                        //alert('Something went wrong!');
                    }
                );

        }


    }
    /**
     * @func {{delAddonPackage}}
     * @description {{delete Add on package}}
     */
    delAddonPackage(addonPackId){
        if (confirm("Are you sure to delete the package?")) {
            let fd = new FormData();
            fd.append('package_id', addonPackId);
            fd.append('api_token', this.authUser.api_token);
            this.userService.deleteAddonPackage(fd)
                .subscribe(
                    (data: any) => {
                        //console.log(data);
                        if (data.status === 'success') {
                            this.getPackages();
                            // message
                            this.msgs = [];
                            this.msgs.push({ severity: 'success', summary: 'Success Message', detail: 'Package Successfully Deleted' });
                        }
                        if (data.status === 'fail') {
                            // message
                            this.msgs = [];
                            if (data.errors && Object.keys(data.errors).length > 0) {

                                for (var key in data.errors) {
                                    if (data.errors.hasOwnProperty(key)) {
                                        //console.log(key + " -> " + p[key]);
                                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                                    }
                                }

                            }
                            else if (data.error_message) {
                                this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                            }
                            else {
                                this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                            }
                        }
                    },
                    error => {
                        console.log(error);
                        this.msgs = [];
                        if (error && error.message) {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

                        }
                    }
                );

        }
    }
}

import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';

import { CommonService } from '../../../services/common.service';
import { UserService } from '../../../services/user.service';
import { Message} from 'primeng/primeng';
import { appConfig } from '../../../apiurl.config';

import { CalendarComponent } from 'ng-fullcalendar';
import { Options } from 'fullcalendar';

@Component({
  selector: 'app-tasks-calendar',
  templateUrl: './tasks-calendar.component.html',
  styleUrls: ['./tasks-calendar.component.css']
})
export class TasksCalendarComponent implements OnInit {
    /**
     * @property \{{{any}}\} {{appConfig}} {{this will hold the constants for the app}}
     */

    appConfig: any = {};

    /**
     * @property \{{{Message[]}}\} {{msgs}} {{this is growl message model}}
     */

    msgs: Message[] = [];

    /**
     * @property \{{{any}}\} {{model}} {{this form model}}
     */

    authUser: any ={};
    /**
     * @property \{{{any}}\} {{myList}} {{this will hold the list data}}
     */

    taskList: any =[];
    calendarOptions: Options;
    displayEvent: any;
    @ViewChild(CalendarComponent) ucCalendar: CalendarComponent;
    @Input() taskData: any;

    @Output() viewEvent = new EventEmitter<string>();
    @Output() openModalEvent = new EventEmitter<string>();

    constructor(
      private commonService: CommonService,
      private userService: UserService) { }

    ngOnInit() {
        this.authUser = this.userService.getAuthUser();
        this.fetchEventData();
        // console.log(this.taskData);
    }

    changeView(v){
        this.viewEvent.emit(v);
    }

    openAddModal(d){
        this.openModalEvent.emit(d);
    }

    /*let data: any = [
                   {
                       title: 'All Day Event',
                       start: yearMonth + '-01'
                   },
                   {
                       title: 'Long Event',
                       start: yearMonth + '-07',
                       end: yearMonth + '-10'
                   },
                   {
                       id: 999,
                       title: 'Repeating Event',
                       start: yearMonth + '-09T16:00:00'
                   },
                   {
                       id: 999,
                       title: 'Repeating Event',
                       start: yearMonth + '-16T16:00:00'
                   },
                   {
                       title: 'Conference',
                       start: yearMonth + '-11',
                       end: yearMonth + '-13'
                   },
                   {
                       title: 'Meeting',
                       start: yearMonth + '-12T10:30:00',
                       end: yearMonth + '-12T12:30:00'
                   },
                   {
                       title: 'Lunch',
                       start: yearMonth + '-12T12:00:00'
                   },
                   {
                       title: 'Meeting',
                       start: yearMonth + '-12T14:30:00'
                   },
                   {
                       title: 'Happy Hour',
                       start: yearMonth + '-12T17:30:00'
                   },
                   {
                       title: 'Dinner',
                       start: yearMonth + '-12T20:00:00'
                   },
                   {
                       title: 'Birthday Party',
                       start: yearMonth + '-13T07:00:00'
                   },
                   {
                       title: 'Click for Google',
                       url: 'http://google.com/',
                       start: yearMonth + '-28'
                   }];*/

    fetchEventData() {
        let that = this;
        // const dateObj = new Date();
        // const yearMonth = dateObj.getUTCFullYear() + '-' + (dateObj.getUTCMonth() + 1);

        this.taskData.forEach((value: any) => {
            if(value.id != this.authUser.id) {
                let ed = null;
                if(value.task.due_date){
                    ed = new Date(value.task.due_date*1000);
                    // console.log(ed)
                }
                this.taskList.push({title: value.task.title , start: value.task.created_at, end: ed });
            }
        });
        // console.log(this.taskList);
        this.calendarOptions = {
            editable: true,
            eventLimit: false,
            header: {
                left: 'custom1 agendaWeek month', /*,agendaDay,listMonth*/
                center: 'title',
                right: 'prev,today,next custom2',

            },
            buttonText:{
                today:    'Today',
                month:    'Month View',
                week:     'Week View',
            },
            customButtons: {
                custom1: {
                    text: 'List View',
                    click: function() {
                        // alert('clicked custom button 1!');
                        that.changeView('L');
                    }
                },
                custom2: {
                    text: 'Add Task',
                    click: function() {
                        // alert('clicked custom button 1!');
                        that.openAddModal('addtaskmodal');
                    }
                },

            },

            events: this.taskList
        };
    }

    clickButton(model: any) {
      // console.log(model);
        this.displayEvent = model;
    }

    eventClick(model: any) {
        model = {
            event: {
                id: model.event.id,
                start: model.event.start,
                end: model.event.end,
                title: model.event.title,
                allDay: model.event.allDay
                // other params
            },
            duration: {}
        }
        this.displayEvent = model;
    }

    updateEvent(model: any) {
        model = {
            event: {
                id: model.event.id,
                start: model.event.start,
                end: model.event.end,
                title: model.event.title
                // other params
            },
            duration: {
                _data: model.duration._data
            }
        }
        this.displayEvent = model;
    }

}

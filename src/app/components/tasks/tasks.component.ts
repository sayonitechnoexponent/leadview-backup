import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../services/common.service';
import { UserService } from '../../services/user.service';
import { Message} from 'primeng/primeng';
import { appConfig } from '../../apiurl.config';


@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent implements OnInit {
    /**
     * @property \{{{any}}\} {{appConfig}} {{this will hold the constants for the app}}
     */

    appConfig: any = {};

    /**
     * @property \{{{Message[]}}\} {{msgs}} {{this is growl message model}}
     */

    msgs: Message[] = [];

    /**
     * @property \{{{any}}\} {{model}} {{this form model}}
     */

    taskModel: any = {};

    /**
     * @property \{{{any}}\} {{authUser}} {{this will hold the authuser}}
     */

    authUser: any ={};
    /**
     * @property \{{{any}}\} {{myList}} {{this will hold the list data}}
     */

    myList: any;

    /**
     * @property \{{{any}}\} {{selArr}} {{this will hold the selected from list}}
     */

    selArr: any =[];
    contactList : any =[];
    companyList : any =[];
    googoleAutoData : any = {};
    taskTypeList : any =[];
    selectedItems : any =[];
    selectedContact : any =[];
    selectedCompany : any =[];
    allUsers: any = [];
    managerList: any = [];
    manager_filter: any = [];
    selectedMngrs: any = [];
    searchText : any;
    checkLocation:boolean = false;
    checkDate:boolean = false;
    checkTime:boolean = false;
    opportunityList : any =[];
    opportunityDd : any =[];
    is_submit:boolean = false;

    dueDate: Date;
    closerDate: Date;
    dateTime: Date;
    startDate: Date;
    endDate: Date;

    dropdownSettings = {};
    userSettings1 : any ={'inputString': ''};
    showAttachment : any = '';
    search_key : string = '';
    dudtKey : string = '';
    status_key : string = '';
    current_view : string = 'L';

    constructor(
        private commonService: CommonService,
        private userService: UserService) { }

  ngOnInit() {

      this.authUser = this.userService.getAuthUser();
      this.appConfig = appConfig;
      this.fetchTasks();
      this.fetchTaskType();
      this.fetchTeamUsers();
      this.listContacts();
      this.fetchOpportunity();
      this.dropdownSettings = {
          singleSelection: false,
          text:"Select",
          enableCheckAll: false,
          selectAllText:'Select All',
          unSelectAllText:'UnSelect All',
          enableSearchFilter: true,
          classes:"myclass custom-class"
      };
  }

    changeView(v){

        this.current_view = v;
    }
    receiveView(data){
        this.current_view = data;
    }

    showStatus(dueDate){
        let date = new Date();
        let currentTimestamp = date.getTime();
       var status = 'Open';
        if(dueDate !== null){
            let dudt = dueDate*1000;
            if(currentTimestamp < dudt) status = 'Open';
            else status = 'Close';
        }

        return status;
    }

    strToTimestamp(strDate){
        var datum = Date.parse(strDate);
        return datum/1000;
    }
    /**
     * @func {{onFileChange}}
     * *@param  {{event}} {{this will pass the change event}}
     * @description {{get the file}}
     */

    onFileChange(event,modType) {

        if(event.target.files && event.target.files.length > 0) {
            let file = event.target.files[0];
            //console.log(file);
            this.taskModel.attachment = file;
        }
    }

    /**
     * @func {{pageLoad}}
     * @description {{reload page after a certain time}}
     */
    pageLoad(){
        setTimeout(()=>{    //<<<---    using ()=> syntax
            location.reload()
        },1000);
    }

    autoCompleteCallback1(selectedData:any) {
        //do any necessery stuff.
        //console.log(selectedData);
        this.googoleAutoData = {};
        this.googoleAutoData = selectedData;
        // console.log(this.googoleAutoData.data.geometry.location);
        if(this.googoleAutoData.data){
            let loc = this.googoleAutoData.data.geometry.location;
            // this.addressModel.lat = loc.lat;
            // this.addressModel.lng = loc.lng;

        }
    }

    onItemSelect(item:any){
        //console.log(item.id);

        if( this.selectedItems.indexOf(item.id)== -1)
        {
            this.selectedItems.push(item.id);
        }

        //console.log(this.selectedItems);
        console.log(this.taskModel.managers);
    }

    OnItemDeSelect(item:any){
        //console.log(item.id);

        if(this.selectedItems.indexOf(item.id) != -1)
            this.selectedItems.splice(this.selectedItems.indexOf(item.id), 1);
        //console.log(this.selectedItems);

    }
    /**
     * @func {{openCreateTask}}
     * @param \{{{string}}\} {{$data}} {{popup data val class}}
     * @description {{this will be open the popup accordingly on click by pass data val}}
     */
    openCreateTask($data,task){
        this.userSettings1 ={'inputString': ''};
        this.dateTime = null;
        if(task){
            // console.log(task);
            this.selectedItems =[];
            this.taskModel = task;
            this.taskModel.managers =[];
            this.taskModel.type = "Edit";
            // this.taskModel._id = task.id;
            this.taskModel.task_type = this.taskModel.task_type_id;
            if(task.attachment &&task.attachment !='' ){
                this.showAttachment =  this.taskModel.attachment;
            }
            delete this.taskModel.attachment;

             task.user_posts.forEach((value: any) => {
                 if(this.authUser.id != value.user_id){
                     this.selectedItems.push(value.user_id);
                     let mngr = {id: value.user_id, itemName: value.user.name};

                     this.taskModel.managers.push(mngr);
                 }
            });

            let selctDta = this.taskTypeList.find(item => item.id == this.taskModel.task_type_id);

            this.checkLocation = selctDta.show_location == 1 ? true:false;
            this.checkDate = selctDta.show_date == 1 ? true:false;
            this.checkTime = selctDta.show_time == 1 ? true:false;

            if(this.checkDate)  this.closerDate = new Date(task.due_date*1000);
            // if(this.checkTime)  this.dateTime = new Date(parseInt(task.due_time)*1000);
            if(this.checkTime){
                let dt =  new Date();
                let dudt = dt.toDateString()+ ' ' + task.due_time;
                this.dateTime = new Date(dudt);
                console.log(new Date(dudt));
            }
            if(this.checkLocation){

                this.userSettings1['inputString'] = task.location;
            }

                // console.log(this.userSettings1);
        }
        else{
            this.contactList =[];
            this.companyList =[];
            this.googoleAutoData = {};
            // this.taskTypeList =[];
            this.selectedItems =[];
            this.selectedContact =[];
            this.selectedCompany =[];
            this.taskModel = {};
            this.taskModel.type = "Add";
        }
        this.commonService.openModal($data);

        // this.listContacts();
        //this.listCompanies();
    }


    kywordFilter(event){
        console.log(event.srcElement.value);
        this.search_key = event.srcElement.value;
        this.fetchTasks();
    }
    /**
     * @func {{fetchType}}
     * @description {{get all Type}}
     */
    fetchTasks() {
        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);
        fd.append('search_key', this.search_key);
        fd.append('manager_filter', this.manager_filter);
        fd.append('due_date', this.dudtKey);
        fd.append('status', this.status_key);

        this.userService.fetchAssignTasks(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        //this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Project Post Successful'});
                        this.myList =  (data.feed);
                        //console.log(this.myTeam);
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );

    }

    changeTaskForm($event) {
        //console.log($event.srcElement.value);
        let jid = $event.srcElement.value;
        let selctDta = this.taskTypeList.find(item => item.id == jid);
        //console.log(selctDta);
        this.checkLocation = selctDta.show_location == 1 ? true:false;
        this.checkDate = selctDta.show_date == 1 ? true:false;
        this.checkTime = selctDta.show_time == 1 ? true:false;
    }
    /**
     * @func {{fetchTaskType}}
     * @description {{get all Task Types}}
     */

    fetchTaskType() {
        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);

        this.userService.getTaskTypes(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        //this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Project Post Successful'});
                        this.taskTypeList =  (data.feed);
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );

    }

    managerFilter(event){
        // console.log(event.srcElement.value);
        // this.manager_filter = event.srcElement.value;
        this.manager_filter = this.selectedMngrs;
        this.fetchTasks();
    }
    statusFilter(event){
        this.status_key = event.target.value;
        console.log(this.status_key);
        this.fetchTasks();
    }
    clearFilter(event){
        this.dueDate = null;
        this.dudtKey='';
        this.fetchTasks();
    }
    ddFilter(duDate){
        // console.log(event.srcElement.value);
        // this.manager_filter = event.srcElement.value;
        // this.manager_filter = this.dueDate;
        // console.log(duDate);
        this.dudtKey = (duDate.getTime()/1000).toString();
        console.log(this.dudtKey);
        this.fetchTasks();
    }

    /**
     * @func {{fetchTeamUsers}}
     * @description {{get user teams}}
     */
    fetchTeamUsers() {
        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);
        fd.append('page_type', 'contacts');
        this.userService.getTeamUsers(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        //this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Project Post Successful'});
                        //this.allUsers =  (data.users);
                        data.users.forEach((value: any) => {
                            if(value.id != this.authUser.id) {
                                this.allUsers.push({'id':value.id ,'itemName': value.name });
                                this.managerList.push({'value':value.id ,'label': value.name });
                            }
                        });
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});
                    }
                    //alert('Something went wrong!');
                }
            );

    }

    listContacts(){
        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);
        let group2 =[];
        this.userService.getContacts(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        //this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Project Post Successful'});
                        //conlist =  (data.feed);
                        data.feed.forEach((value: any) => {
                            if(value.type==1)    this.contactList.push({'id':value.id ,'itemName': value.name });
                            else if(value.type==2)    this.companyList.push({'id':value.id ,'itemName': value.name });

                        });
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );
        //console.log(this.contactList);

    }
    /**
     * @func {{fetchOpportunity}}
     * @description {{get user User Time Line}}
     */
    fetchOpportunity() {
        //alert(this.listTab);
        let fd = new FormData();
        fd.append('list_tab', 'all');
        fd.append('api_token', this.authUser.api_token);
        this.userService.getOpportunity(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        this.opportunityList = data.feed;
                        this.opportunityList.forEach((value: any) => {
                            if(value.id)   this.opportunityDd.push({'id':value.id ,'itemName': value.title });
                        });
                        //console.log(this.opportunityDd);
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});
                    }
                    //alert('Something went wrong!');
                }
            );

    }

    /**
     * @func {{addTask}}
     * @param \{{{formobject}}\} {{myform}} {{this will pass the form object}}
     * @description {{this will submit the form data and update the project data}}
     */
    addTask(myform)
    {
        this.is_submit = true;
        if(this.closerDate) this.taskModel.due_date= (this.closerDate.getTime())/1000;
        //if(this.dateTime) this.taskModel.duetime= this.dateTime;
        if(this.dateTime) this.taskModel.due_time= this.dateTime.toLocaleTimeString();


        //console.log(this.taskModel);
        let fd = new FormData();
        if(this.googoleAutoData && this.googoleAutoData.data){
            fd.append('location', this.googoleAutoData.data.formatted_address);
            fd.append('lat', this.googoleAutoData.data.geometry.location.lat);
            fd.append('lng', this.googoleAutoData.data.geometry.location.lng);
        }
        if(this.taskModel.location){
            fd.append('location', this.taskModel.location);
        }
        if(this.taskModel.id){
            fd.append('_id', this.taskModel.id);
        }
        fd.append('task_type', this.taskModel.task_type);
        fd.append('attachment', this.taskModel.attachment);
        fd.append('title', this.taskModel.title);
        fd.append('notes', this.taskModel.notes);
        fd.append('due_date', this.taskModel.due_date);
        fd.append('due_time', this.taskModel.due_time);
        fd.append('managers', this.selectedItems);
        fd.append('contacts', this.taskModel.contacts);
        fd.append('companies', this.taskModel.companies);
        fd.append('api_token', this.authUser.api_token);

        fd.append('curr_user', this.authUser.id);
        fd.append('tl_type', 'user');
        // console.log(this.taskModel);
        //this.noteModel.api_token = this.authUser.api_token;

        this.userService.addEditTask(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    this.is_submit = false;
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Task added successfully'});

                        //this.fetchUserTimeLine();
                        // reload page
                        this.pageLoad();
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    this.is_submit = false;
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );

    }

    /**
     * @func {{delTask}}
     * @description {{delete tax}}
     */
    delTask(_id)
    {
        //console.log(this.model);
        if(confirm("Are you sure to delete the Task?"))
        {
            let fd = new FormData();
            fd.append('_ids',_id);
            fd.append('api_token', this.authUser.api_token);
            //return;
            this.userService.deleteTask(fd)
                .subscribe(
                    (data: any) => {
                        //console.log(data);
                        if (data.status === 'success') {
                            // message
                            this.msgs = [];
                            this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Type Successfully Deleted'});

                            // go to  my team page
                            this.pageLoad();
                        }
                        if (data.status === 'fail') {
                            // message
                            this.msgs = [];
                            if (data.errors && Object.keys(data.errors).length > 0)
                            {

                                for (var key in data.errors) {
                                    if (data.errors.hasOwnProperty(key)) {
                                        //console.log(key + " -> " + p[key]);
                                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                    }
                                }

                            }
                            else if(data.error_message)
                            {
                                this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                            }
                            else
                            {
                                this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                            }
                        }
                    },
                    error => {
                        console.log(error);
                        this.msgs = [];
                        if (error && error.message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                        }
                        //alert('Something went wrong!');
                    }
                );

        }
    }
    /**
     * @func {{delAll}}
     * @description {{delete selected taxes}}
     */
    delAll()
    {
        if(confirm("Are you sure to delete all selected tasks?"))
        {
            let fd = new FormData();
            fd.append('_ids',this.selArr);
            fd.append('api_token', this.authUser.api_token);
            //return;
            this.userService.deleteTask(fd)
                .subscribe(
                    (data: any) => {
                        //console.log(data);
                        if (data.status === 'success') {
                            // message
                            this.msgs = [];
                            this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Type are successfully deleted'});

                            this.pageLoad();
                        }
                        if (data.status === 'fail') {
                            // message
                            this.msgs = [];
                            if (data.errors && Object.keys(data.errors).length > 0)
                            {

                                for (var key in data.errors) {
                                    if (data.errors.hasOwnProperty(key)) {
                                        //console.log(key + " -> " + p[key]);
                                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                    }
                                }

                            }
                            else if(data.error_message)
                            {
                                this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                            }
                            else
                            {
                                this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                            }
                        }
                    },
                    error => {
                        console.log(error);
                        this.msgs = [];
                        if (error && error.message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                        }
                        //alert('Something went wrong!');
                    }
                );

        }
    }

    selctToList($event) {
        //console.log($event.srcElement.checked);
        //console.log($event.srcElement.value);
        if($event.srcElement.checked == true)
        {
            if(this.selArr.indexOf($event.srcElement.value)== -1)
                this.selArr.push($event.srcElement.value);
        }else {

            this.selArr.splice(this.selArr.indexOf($event.srcElement.value), 1);
        }
        console.log(this.selArr);
    }

}


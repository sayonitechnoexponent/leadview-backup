import { Component, OnInit } from '@angular/core';

import { UserService } from '../../../services/user.service';
import {Message} from 'primeng/primeng';

@Component({
  selector: 'app-chat-setting',
  templateUrl: './chat-setting.component.html',
  styleUrls: ['./chat-setting.component.css']
})
export class ChatSettingComponent implements OnInit {

    /**
     * @property \{{{any}}\} {{model}} {{security form model}}
     */

    model: any = {
        available_status : '1',
        chat_directory_status : '1',
        offline_type : 1,
        is_default_operator : '0',
        choosen_operator : '',
    };

    /**
     * @property \{{{any}}\} {{authUser}} {{this will hold the authuser}}
     */

    authUser: any ={};

    /**
     * @property \{{{Message[]}}\} {{msgs}} {{this is growl message model}}
     */

    msgs: Message[] = [];

    /**
     * @property \{{{any}}\} {{isLoggedinUser}} {{this will hold the isLoggedinUser}}
     */
    isAvailable: boolean = true;
    project_id: number = 0;
    project: any = {};
    project_operator_request: any = [];
    is_default: number = 0;

    /**
     * @constructor
     * @description {{DI will pushed here}}
     */
  constructor(private userService: UserService) { }

    /**
     * @func {{ngOnInit}}
     * @description {{angular lifecycle hook}}
     */

  ngOnInit() {

        this.authUser = this.userService.getAuthUser();
        console.log(this.authUser.in_projects.length);
        if(this.authUser.chat_setting){
            /*if(!localStorage.getItem('chat_setting')){

                localStorage.setItem('chat_setting', JSON.stringify(this.authUser.chat_setting));
            }
            let chat_setting  = JSON.parse(localStorage.getItem('chat_setting'));*/
            // this.model = chat_setting;
            /*if(this.model.available_status == 1)  this.isAvailable = true;
            else            this.isAvailable = false;*/
        }
        // console.log(this.authUser.in_projects);
        if(this.authUser.in_projects && this.authUser.in_projects.length >0 ){
            if(this.authUser.in_projects[0].id){
                this.project_id = this.authUser.in_projects[0].id;

                this.fetchProject();
            }
        }
  }
    ngAfterViewInit() {

    }
    /**
     * @func {{fetchProject}}
     * @description {{get user teams}}
     */
    fetchProject() {
        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);
        fd.append('project_id', this.project_id.toString());
        this.userService.getProject(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        //this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Project Post Successful'});
                        this.project =  (data.project);
                        this.project_operator_request =  (data.project.project_operator_request);

                        if(data.chat_setting){
                            this.model = data.chat_setting;
                            this.is_default = data.chat_setting.is_default_operator;
                            this.isAvailable = (data.chat_setting.available_status == 1?true:false);
                            this.isAvailable = (data.chat_setting.available_status == 1?true:false);
                        }
                        /*if(data.project.users){
                            this.model.choosen_operator = data.project.users[0].id;
                            alert(this.model.choosen_operator)
                        }*/
                        console.log(this.model);
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);

                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );

    }

    /**
     * @func {{onStatusChange}}
     * @description {{change available status}}
     */

  onStatusChange(e) {
        let newVal = e.target.value;

        // alert(typeof newVal);
        if(newVal === '1'){
            this.isAvailable = true;
            delete this.model['offline_type'];
        }
        else {
            this.isAvailable = false;
        }

  }

    /**
     * @func {{saveChatSetting}}
     * @param {{myform}} {{this will pass the form object}}
     * @description {{this will submit the form data and save user chat setting}}
     */
    saveChatSetting(myform) {
        //console.log(this.model);
        this.model.api_token = this.authUser.api_token;
        if(this.model.offline_type !=1){
            delete this.model['offline_message'];
            // this.model.choosen_operator = null;
        }
        // if(this.model.offline_type !=3) delete this.model['choosen_operator'];

        this.userService.updateChatSetting(this.model)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        /*if(this.model.is_default_operator==1)
                            this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Please wait for default operator approval.'});*/
                        this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Setting successfully saved.'});
                        this.fetchProject();
                        /*if(data.chat_setting){
                            // localStorage.setItem('chat_setting', JSON.stringify(data.user.chat_setting));
                            this.model = data.chat_setting;
                        }*/
                        // console.log(data.user);
                        if(this.model.offline_type !=1) delete this.model['offline_message'];
                        // if(this.model.offline_type !=3) delete this.model['choosen_operator'];
                        /*this.authUser = this.userService.getAuthUser();
                        this.authUser.chat_setting = data.user.chat_setting;*/
                        // set the logged in user

                        localStorage.setItem('leaduser', JSON.stringify(data.user));
                        // console.log(data.user);
                        // console.log(this.model);
                        // go to  profile page
                        // location.href = '/my-account';
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);

                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    console.log(error);
                    //alert('Something went wrong!');
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});
                    }
                }
            );
    }

}

import { Component, OnInit } from '@angular/core';

import { UserService } from '../../../services/user.service';
import { appConfig } from '../../../apiurl.config';
import {Message} from 'primeng/primeng';

@Component({
  selector: 'app-security',
  templateUrl: './security.component.html',
  styleUrls: ['./security.component.css']
})
export class SecurityComponent implements OnInit {
    /**
     * @property \{{{any}}\} {{model}} {{security form model}}
     */

  model: any = {
      current_password : null,
      new_password : null,
  };
    /**
     * @property \{{{any}}\} {{authUser}} {{this will hold the authuser}}
     */

  authUser: any ={};

    /**
     * @property \{{{Message[]}}\} {{msgs}} {{this is growl message model}}
     */

  msgs: Message[] = [];

    /**
     * @constructor
     * @description {{DI will pushed here}}
     */
  constructor(private userService: UserService) { }

    /**
     * @func {{ngOnInit}}
     * @description {{angular lifecycle hook}}
     */
  ngOnInit() {
      this.authUser = this.userService.getAuthUser();
  }
    /**
     * @func {{changeUserPassword}}
     * @param \{{{formobject}}\} {{myform}} {{this will pass the form object}}
     * @description {{this will submit the form data and changed user password}}
     */
    changeUserPassword(myform) {

        //console.log(this.model);
        this.model.api_token = this.authUser.api_token;
        //return;
        this.userService.changePassword(this.model)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Password Successfully Changed.'});

                        // set the logged in user
                        localStorage.setItem('leaduser', JSON.stringify(data.user));
                        // this.model = {current_password : null, new_password : null};

                        // go to  profile page
                        location.href = '/my-account';
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    console.log(error);
                    //alert('Something went wrong!');
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});
                    }
                }
            );

    }
}

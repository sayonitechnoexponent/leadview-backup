import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../services/user.service';
import { appConfig } from '../../../apiurl.config';

import { DomSanitizer, SafeUrl } from '@angular/platform-browser';

import {Message} from 'primeng/primeng';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

    /**
     * @property \{{{any}}\} {{appConfig}} {{this will hold the constants for the app}}
     */

    appConfig: any = {};

    /**
     * @property \{{{any}}\} {{model}} {{security form model}}
     */

    model: any = {
        //profile_pic : null
        language:1
    };

    /**
     * @property \{{{any}}\} {{languages}} {{security form model}}
     */

    languages: any ;

    /**
     * @property \{{{Message[]}}\} {{msgs}} {{this is growl message model}}
     */

    msgs: Message[] = [];
    blob_picture : any = null;
    /**
     * @constructor
     * @description {{DI will pushed here}}
     */

  constructor( private userService: UserService, private sanitizer: DomSanitizer ) { }

    /**
     * @func {{ngOnInit}}
     * @description {{angular lifecycle hook}}
     */

  ngOnInit() {
      this.model= this.userService.getAuthUser();
      if(this.model.phone_number == 'null')   this.model.phone_number='';
      if(this.model.designation == 'null')   this.model.designation='';
      this.appConfig = appConfig;
      this.getAllLanguages();
  }

    /**
     * @func {{getAllLanguages}}
     * @description {{this will fetch all Languages}}
     */
    getAllLanguages() {

      this.userService.getLanguages()
          .subscribe(
              (data: any) => {
                   //console.log(data);
                  if (data.status === 'success') {
                        this.languages = data.language;
                  }
                  if (data.status === 'fail') {
                      // message
                      this.msgs = [];
                      if (data.errors && Object.keys(data.errors).length > 0)
                      {

                          for (var key in data.errors) {
                              if (data.errors.hasOwnProperty(key)) {
                                  //console.log(key + " -> " + p[key]);
                                  this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                              }
                          }

                      }
                      else if(data.error_message)
                      {
                          this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                      }
                      else
                      {
                          this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                      }
                  }
              },
              error => {
                  //console.log(error);
                  this.msgs = [];
                  if (error && error.message)
                  {
                      this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                  }
                  //alert('Something went wrong!');
              }
          );

    }

    /**
     * @func {{updateProfile}}
     * @param \{{{formobject}}\} {{myform}} {{this will pass the form object}}
     * @description {{this will submit the form data and update the logged in user data}}
     */
    updateProfile(myform) {
      //console.log(myform);
      this.model.name =  this.model.first_name+' '+ this.model.last_name;
      console.log(this.model);
      //return;
      this.userService.editProfile(this.model)
          .subscribe(
              (data: any) => {
                   console.log(data);
                  if (data.status === 'success') {
                      // message
                      this.msgs = [];
                      this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Profile Update Successful'});

                      // set the logged in user
                      localStorage.setItem('leaduser', JSON.stringify(data.user));
                      // go to  profile page
                      location.href = '/my-account';
                  }
                  if (data.status === 'fail') {
                      // message
                      this.msgs = [];
                      if (data.errors && Object.keys(data.errors).length > 0)
                      {

                          for (var key in data.errors) {
                              if (data.errors.hasOwnProperty(key)) {
                                  //console.log(key + " -> " + p[key]);
                                  this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                              }
                          }

                      }
                      else if(data.error_message)
                      {
                          this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                      }
                      else
                      {
                          this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                      }
                  }
              },
              error => {
                  console.log(error);
                  this.msgs = [];
                  if (error && error.message)
                  {
                      this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                  }
                  //alert('Something went wrong!');
              }
          );

  }

    /**
     * @func {{selectFile}}
     * @param {{$event}} {{this will pass the file object}}
     * @description {{this update the logged in user profile image }}
     */

   selectFile(event) {
       //console.log(event.srcElement.files[0]);
        /* var image = event.target.files[0]; */
        /*var pattern = /image-*!/;
        var reader = new FileReader();

        if (!image.type.match(pattern)) {
            console.error('File is not an image');
            //of course you can show an alert message here
            return;
        }*/

       let imgfile = event.target.files[0];
        //this.blob_picture = imgfile;
        let blob = null;
        if(imgfile) blob = this.getBase64( imgfile );
        var that = this;
        blob.then(function(result) {
            //console.log(result);
            //that.blob_picture = that.sanitizer.bypassSecurityTrustUrl(result);
            that.blob_picture = (result);
         });

       let fd = new FormData();
       fd.append('profile_picture', imgfile);

       fd.append('api_token', this.model.api_token);

       this.userService.uploadProfileImage(fd)
           .subscribe(
               (data: any) => {
                   //console.log(data);
                   if (data.status === 'success') {
                       // message
                       this.msgs = [];
                       this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Picture Upload Successful'});

                       // set the logged in user
                       localStorage.setItem('leaduser', JSON.stringify(data.user));
                       // go to  profile page
                       location.href = '/my-account';
                   }
                   if (data.status === 'fail') {
                       // message
                       this.msgs = [];
                       if (data.errors && Object.keys(data.errors).length > 0)
                       {

                           for (var key in data.errors) {
                               if (data.errors.hasOwnProperty(key)) {
                                   //console.log(key + " -> " + p[key]);
                                   this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                               }
                           }

                       }
                       else if(data.error_message)
                       {
                           this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                       }
                       else
                       {
                           this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                       }

                   }
               },
               error => {
                   console.log(error);
                   this.msgs = [];
                   if (error && error.message)
                   {
                       this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                   }
                   //alert('Something went wrong!');
               }
           );

    }

    // file to base64
    getBase64(file ) {
       //console.log(file);
        return new Promise(function(resolve, reject) {
            var reader = new FileReader();
            reader.onload = function() { resolve(reader.result); };
            reader.onerror = reject;

            //console.log(reader.readAsDataURL(file));
            reader.readAsDataURL(file);
        });
    }


}

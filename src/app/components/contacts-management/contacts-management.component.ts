import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { CommonService } from '../../services/common.service';
import { UserService } from '../../services/user.service';
import {Message} from 'primeng/primeng';
import { appConfig } from '../../apiurl.config';
declare var $ : any;

@Component({
  selector: 'app-contacts-management',
  templateUrl: './contacts-management.component.html',
  styleUrls: ['./contacts-management.component.css']
})
export class ContactsManagementComponent implements OnInit {

    /**
     * @property \{{{any}}\} {{appConfig}} {{this will hold the constants for the app}}
     */

    appConfig: any = {};

    /**
     * @property \{{{Message[]}}\} {{msgs}} {{this is growl message model}}
     */

    msgs: Message[] = [];
    /**
     * @property \{{{any}}\} {{authUser}} {{this will hold the authuser}}
     */

    authUser: any ={};

    contactModel : any ={};
    allCountries : any =[];
    blob_picture : any =null;
    contactLists : any =[];
    selArr : any =[];
    selectedItems : any =[];
    selectedManagers : any =[];
    myGroups : any =[];
    dropdownSettings : any ={};
    dropdownSettings2 : any ={};
    searchText : string ='';
    flType : string ='';
    assignModel : any ={};
    languages : any =[];
    allManagers : any =[];
    managerList : any =[];
    assignManagerModel : any ={};
    tags : any =[];

    is_submit : boolean = false;
    /**
     * @property \{{{any}}\} {{displayColFrm}} {{this will hold the display form dta}}
     */

    displayColFrm: any = {
        name : true,
        email : true,
        visit : true,
        view : true,
        tag : true,
        manager : true,
        phone : false,
        country : false,
        region : false,
        city : false,
        lang : false,
        firstActivity : false,
        recentActivity : false,
        firstPostActivity : false,
        lastPostActivity : false,
    };

    colNumber:number = 6;
    showNamecol:boolean = true;
    showemailcol:boolean = true;
    showvisitcol:boolean = true;
    showviewcol:boolean = true;
    showtagcol:boolean = true;
    showmanagercol:boolean = true;
    showphonecol:boolean = false;
    showcountrycol:boolean = false;
    showregioncol:boolean = false;
    showcitycol:boolean = false;
    showlangcol:boolean = false;
    showfirstActivity:boolean = false;
    showrecentActivity:boolean = false;
    showfirstPostActivity:boolean = false;
    showlastPostActivity:boolean = false;

    cols: any=[];
    addCompanyModel: any = [];
    comresults: any = [];
    comresdata: any = [];
    comText: string;
    assignCom: string;
    comList: any = [];
    selctComContacts: any = [];
    search_key: string ='';
    type_filter: string ='';
    manager_filter: any =[];
    selectedGroups: any = [];
    selectedTags: any = [];
    selectedMngrs: any = [];
    groupList: any = [];
    group_filter: any = [];
    merge_fields: any = [];
    selctdField: string ='';
    filteredFields: any = [];
    type_arr: any = [];
    map_arr: any = [1];
    fieldMapModel : any ={};
    ff_arr : any =[];
    tf_arr : any =[];
    actionf_arr : any =[];
    liveProject : any = {};
    postEmailModel : any = {
        content : '',
        template: ''
    };
    templateList : any = [];

  constructor(
      private commonService: CommonService,
      private userService: UserService,
      private route: ActivatedRoute,
      private router: Router
  ) { }

    extMapNo(event){
      this.map_arr.push(1);
    }
    ngOnInit() {

        this.authUser = this.userService.getAuthUser();
        this.appConfig = appConfig;
        this.dropdownSettings = {
            singleSelection: false,
            text:"Select Group",
            enableCheckAll: false,
            selectAllText:'Select All',
            unSelectAllText:'UnSelect All',
            enableSearchFilter: true,
            classes:"myclass custom-class"
        };

        this.dropdownSettings2 = {
            singleSelection: false,
            text:"Select Managers",
            enableCheckAll: false,
            selectAllText:'Select All',
            unSelectAllText:'UnSelect All',
            enableSearchFilter: true,
            classes:"myclass custom-class"
        };


        // console.log(this.merge_fields);
        /*$('#sortingTable').DataTable({
            "paging":   true
        });*/
		$(".leftpanelfilteremenuicon").click(function(event) {
			event.preventDefault();
			$('.leftpanelpopup').addClass("show");
			$('.filtermask').addClass("show");
			
		});
		$(".filtermask").click(function(event) {
			event.preventDefault();
			$('.leftpanelpopup').removeClass("show");
			$('.filtermask').removeClass("show");
			
		});
		
    }
    ngAfterViewInit(){

        this.fetchContacts();
        this.fetchUserGroups();
        this.getAllLanguages();
        this.fetchTeamUsers();
        this.getEmailTemplates();
        this.getTags();
    }

    totalViewCounts(data) {
        let total = 0;
        if(data)
        {
            data.forEach((d) => {
                total += parseInt(d.visit_count, 10);
            });
        }
        return total;
    }
    totalTagScores(data) {
        let total = 0;
        if(data) {
            data.forEach((d) => {
                if (d.remove_score == 0) total += parseInt(d.score, 10);
            });
        }

        return total;
    }
    filterFields(event) {
        this.filteredFields = [];
        for(let i = 0; i < this.merge_fields.length; i++) {
            let brand = this.merge_fields[i];
            if(brand.toLowerCase().indexOf(event.query.toLowerCase()) == 0) {
                this.filteredFields.push(brand);
            }
        }
        //this.filteredFields=this.merge_fields;
    }
    selectField(value){
      // console.log(value);

    }

    /**
     * @func {{mergeContacts}}
     * @description {{Merge Contacts}}
     */
    mergeContacts(myForm) {
        // console.log(this.selArr);
        let selcContct = this.contactLists.find(item => item.name == this.selctdField).id;
        // console.log(selcContct);
        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);
        fd.append('contacts', this.selArr);
        fd.append('merge_field', selcContct);

        this.userService.mergeContacts(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Contacts Merge Successful'});

                        this.pageLoad();
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );
    }
    changeGrp(event){
        // console.log(this.selectedGroups);
        this.group_filter = this.selectedGroups;
        this.fetchContacts();
    }
    /**
     * @func {{fetchTeamUsers}}
     * @description {{get user teams}}
     */
    fetchTeamUsers() {
        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);
        fd.append('page_type', 'contacts');
        this.userService.getTeamUsers(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        //this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Project Post Successful'});
                        //this.allManagers =  (data.users);
                        data.users.forEach((value: any) => {
                            if(value.id != this.authUser.id){
                                let name = value.first_name+' '+value.last_name;
                                this.allManagers.push({'id':value.id ,'itemName': name });
                                this.managerList.push({'value':value.id ,'label': name });
                            }
                        });
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});
                    }
                    //alert('Something went wrong!');
                }
            );

    }

    checkColNumber($event) {
        // console.log($event.srcElement.checked);
        //console.log($event.srcElement.value);
        if($event.srcElement.checked == true)
        {
            this.colNumber = this.colNumber + 1;
        }else {
            this.colNumber = this.colNumber - 1;
        }
        // console.log(this.colNumber);
    }

    /**
     * @func {{saveDisplay}}
     * @description {{toggle display of list}}
     */
    saveDisplay(myForm) {
        //console.log(myForm);
        //console.log(this.displayColFrm);
        if(this.colNumber <7){
            this.showNamecol = this.displayColFrm.name;
            this.showemailcol = this.displayColFrm.email;
            this.showvisitcol = this.displayColFrm.visit;
            this.showviewcol = this.displayColFrm.view;
            this.showtagcol = this.displayColFrm.tag;
            this.showmanagercol = this.displayColFrm.manager;
            this.showphonecol = this.displayColFrm.phone;
            this.showcountrycol = this.displayColFrm.country;
            this.showregioncol = this.displayColFrm.region;
            this.showcitycol = this.displayColFrm.city;
            this.showlangcol = this.displayColFrm.lang;
            this.showfirstActivity = this.displayColFrm.firstActivity;
            this.showrecentActivity = this.displayColFrm.recentActivity;
            this.showfirstPostActivity = this.displayColFrm.firstPostActivity;
            this.showlastPostActivity = this.displayColFrm.lastPostActivity;
            if(myForm)    $(".shownewdrop").hide();
        }else{
            this.msgs = [];
            this.msgs.push({severity: 'error', summary: 'Error Message', detail: 'Max 6 fields will be selected.'});
        }

    }

    /**
     * @func {{pageLoad}}
     * @description {{reload page after a certain time}}
     */
    pageLoad(){
        setTimeout(()=>{    //<<<---    using ()=> syntax
            location.reload()
        },1000);
    }

    /**
     * @func {{openContactbx}}
     * @param \{{{string}}\} {{$data}} {{popup data val class}}
     * @description {{this will be open the popup accordingly on click by pass data val}}
     */
    openContactbx($data){
        //console.log($data);

        //console.log(this.allCountries);
        this.getAllCountries();
        this.commonService.openModal($data);
        this.blob_picture = '';
        this.contactModel = {};
        if($data =='addCcompany')   this.contactModel.type = '2';
        else if($data =='addContact')   this.contactModel.type = '1';
    }

    selctToList($event) {
        //console.log($event.srcElement.checked);
        //console.log($event.srcElement.value);
        let chkval = this.contactLists.find(item => item.id == $event.srcElement.value).name;
        let typ = this.contactLists.find(item => item.id == $event.srcElement.value).type;

        // if(type_arr.indexOf(typ) == -1) type_arr.push(typ);
        if($event.srcElement.checked == true)
        {
            if(this.selArr.indexOf($event.srcElement.value)== -1)
            {
                this.selArr.push($event.srcElement.value);

                this.merge_fields.push(chkval);
                this.type_arr.push(typ);
            }
        }else {
            this.selArr.splice(this.selArr.indexOf($event.srcElement.value), 1);
            this.merge_fields.splice(this.merge_fields.indexOf(chkval), 1);
            this.type_arr.splice(this.type_arr.indexOf(typ),1);
        }
        // console.log(this.merge_fields);
        // console.log(this.type_arr);
    }

    getAllCountries() {

        this.userService.allCountries()
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        this.allCountries = data.countries;
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});
                    }
                    //alert('Something went wrong!');
                }
            );

    }

    /**
     * @func {{onFileChange}}
     * *@param  {{event}} {{this will pass the change event}}
     * @description {{get the file}}
     */

    onFileChange(event,modType) {
        //console.log(event);
        if(event.target.files && event.target.files.length > 0) {
            let file = event.target.files[0];
            // console.log(file);
            if(modType == 'contct')  this.contactModel.attachment = file;
            else if(modType == 'm')  this.postEmailModel.attachment = file;
            // console.log(this.contactModel.attachment);
        }
    }

    // file to base64
    getBase64(file ) {
        //console.log(file);
        return new Promise(function(resolve, reject) {
            var reader = new FileReader();
            reader.onload = function() { resolve(reader.result); };
            reader.onerror = reject;

            //console.log(reader.readAsDataURL(file));
            reader.readAsDataURL(file);
        });
    }

    selectFile(event) {
        //console.log(event);
        if(event.target.files && event.target.files.length > 0) {

            let imgfile = event.target.files[0];
            this.contactModel.profile_picture = imgfile;
            //console.log(this.contactModel.profile_picture);
            //this.blob_picture = imgfile;
            let blob = null;
            if (imgfile) blob = this.getBase64(imgfile);
            var that = this;
            blob.then(function (result) {
                //console.log(result);
                //that.blob_picture = that.sanitizer.bypassSecurityTrustUrl(result);
                that.blob_picture = (result);
                //alert(that.blob_picture);
            });
        }
    }
    /**
     * @func {{saveContact}}
     * @param \{{{formobject}}\} {{myform}} {{this will pass the form object}}
     * @description {{this will submit the form data and update the project data}}
     */
    saveContact(myform)
    {
        this.is_submit = true;
        let fd = new FormData();
        fd.append('profile_picture', this.contactModel.profile_picture);
        fd.append('attachment', this.contactModel.attachment);
        fd.append('gender', this.contactModel.gender);
        fd.append('email', this.contactModel.email);
        fd.append('first_name', this.contactModel.first_name);
        fd.append('last_name', this.contactModel.last_name);
        fd.append('phone_number', this.contactModel.phone);
        fd.append('mobile', this.contactModel.mobile);
        fd.append('country', this.contactModel.country);
        fd.append('type', this.contactModel.type);
        fd.append('language', this.contactModel.lang);
        //fd.append('gender', this.contactModel.gender);

        fd.append('api_token', this.authUser.api_token);

        // console.log(this.contactModel);
        // console.log(fd);
        //this.noteModel.api_token = this.authUser.api_token;

        this.userService.saveContact(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Contact successfully added'});
                        this.contactModel={};
                        this.fetchContacts();
                        this.closePopup();
                        this.is_submit = false;

                        // reload page
                    }
                    if (data.status === 'fail') {
                        // message

                        this.is_submit = false;
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);

                    this.is_submit = false;
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );

    }
    /**
     * @func {{saveCompany}}
     * @param \{{{formobject}}\} {{myform}} {{this will pass the form object}}
     * @description {{this will submit the form data and update the project data}}
     */
    saveCompany(myform)
    {
        this.is_submit = true;
        let fd = new FormData();
        fd.append('profile_picture', this.contactModel.profile_picture);
        fd.append('attachment', this.contactModel.attachment);
        fd.append('email', this.contactModel.email);
        fd.append('phone_number', this.contactModel.phone);
        fd.append('mobile', this.contactModel.mobile);
        fd.append('country', this.contactModel.country);
        fd.append('type', this.contactModel.type);
        fd.append('name', this.contactModel.name);
        fd.append('language', this.contactModel.lang);

        fd.append('api_token', this.authUser.api_token);

        console.log(this.contactModel);
        console.log(fd);
        //this.noteModel.api_token = this.authUser.api_token;

        this.userService.saveCompany(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Company successfully added'});
                        this.contactModel={};
                        this.fetchContacts();
                        this.closePopup();
                        this.is_submit = false;
                        // reload page
                    }
                    if (data.status === 'fail') {
                        // message
                        this.is_submit = false;
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.is_submit = false;
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );
    }

    /**
     * @func {{delAll}}
     * @description {{delete selected taxes}}
     */
    delAll()
    {
        if(confirm("Are you sure to delete all selected contacts?"))
        {
            let fd = new FormData();
            fd.append('_ids',this.selArr);
            fd.append('api_token', this.authUser.api_token);
            //return;
            this.userService.deleteContacts(fd)
                .subscribe(
                    (data: any) => {
                        //console.log(data);
                        if (data.status === 'success') {
                            // message
                            this.msgs = [];
                            this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'contact are successfully deleted'});

                            // this.fetchContacts();
                            this.pageLoad();
                        }
                        if (data.status === 'fail') {
                            // message
                            this.msgs = [];
                            if (data.errors && Object.keys(data.errors).length > 0)
                            {

                                for (var key in data.errors) {
                                    if (data.errors.hasOwnProperty(key)) {
                                        //console.log(key + " -> " + p[key]);
                                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                    }
                                }

                            }
                            else if(data.error_message)
                            {
                                this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                            }
                            else
                            {
                                this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                            }
                        }
                    },
                    error => {
                        console.log(error);
                        this.msgs = [];
                        if (error && error.message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                        }
                        //alert('Something went wrong!');
                    }
                );

        }
    }

    onItemSelect(item:any){
        //console.log(item.id);

        if(this.selectedItems.indexOf(item.id)== -1)
            this.selectedItems.push(item.id);

        //console.log(this.selectedItems);
    }

    OnItemDeSelect(item:any){
        //console.log(item.id);

        if(this.selectedItems.indexOf(item.id) != -1)
            this.selectedItems.splice(this.selectedItems.indexOf(item.id), 1);
        //console.log(this.selectedItems);

    }

    onManagerSelect(item:any){
        //console.log(item.id);

        if(this.selectedManagers.indexOf(item.id)== -1)
            this.selectedManagers.push(item.id);

        //console.log(this.selectedManagers);
    }

    OnManagerDeSelect(item:any){
        //console.log(item.id);

        if(this.selectedManagers.indexOf(item.id) != -1)
            this.selectedManagers.splice(this.selectedManagers.indexOf(item.id), 1);
        //console.log(this.selectedManagers);

    }

    /**
     * @func {{openAssignGroup}}
     * @param \{{{string}}\} {{$data}} {{popup data val class}}
     * @description {{this will me open the popup accordingly on click by pass data val}}
     */
    openAssignGroup($data){
        this.commonService.openModal($data);
        //this.selUserArr = [];
        this.selectedItems = [];

    }
    /**
     * @func {{openAssignManager}}
     * @param \{{{string}}\} {{$data}} {{popup data val class}}
     * @description {{this will me open the popup accordingly on click by pass data val}}
     */
    openAssignManager($data){
        this.commonService.openModal($data);
        //this.selUserArr = [];
        this.selectedManagers = [];
        this.assignManagerModel.managers = [];
    }
    commonElm(arr1, arr2) {
        var newArr = [];
        newArr = arr1.filter(function(v){ return arr2.indexOf(v) >= 0;})
        newArr.concat(arr2.filter(function(v){ return newArr.indexOf(v) >= 0;}));

        return newArr;
    }
    /**
     * @func {{openAssignCompany}}
     * @param \{{{string}}\} {{$data}} {{popup data val class}}
     * @description {{this will me open the popup accordingly on click by pass data val}}
     */
    openAssignCompany($data){

        //this.selectedManagers = [];
        this.addCompanyModel = [];
        this.comText = null;
        this.assignCom = '';
        this.msgs = [];
        this.selctComContacts = [];
        let common = this.commonElm(this.selArr,this.comList);

        // console.log(common);
        if(common.length >0)
        {
            this.msgs.push({severity: 'error', summary: 'Error Message', detail: 'Please select contacts only'});
        }else{
            this.commonService.openModal($data);
        }
        // console.log(this.comList);
    }
    /**
     * @func {{openFieldMapBx}}
     * @param \{{{string}}\} {{$data}} {{popup data val class}}
     * @description {{this will me open the popup accordingly on click by pass data val}}
     */
    openFieldMapBx($data){

        this.fieldMapModel = {};
        this.map_arr = [1];
        let type_arr = this.type_arr.filter((x, i, a) => a.indexOf(x) == i);
        // console.log(type_arr);
        if(type_arr.length > 1)
        {
            this.msgs.push({severity: 'error', summary: 'Error Message', detail: 'Please select either contacts or companies'});
        }
        else if(this.selArr.length < 1)
        {
            this.msgs.push({severity: 'error', summary: 'Error Message', detail: 'Please select atleast one contact'});
        }else{
            this.commonService.openModal($data);
        }
        // console.log(this.comList);
    }

    openMergeBx($data){

        //this.selectedManagers = [];
        this.addCompanyModel = [];
        this.filteredFields = [];
        this.msgs = [];
        this.selctdField = '';
        // let common = this.commonElm(this.selArr,this.comList);
        let type_arr = this.type_arr.filter((x, i, a) => a.indexOf(x) == i);
        // console.log(type_arr);
        if(type_arr.length > 1)
        {
            this.msgs.push({severity: 'error', summary: 'Error Message', detail: 'Please select either contacts or companies'});
        }
        else if(this.selArr.length < 2)
        {
            this.msgs.push({severity: 'error', summary: 'Error Message', detail: 'Please select atleast 2 contacts to merge'});
        }else{
            this.commonService.openModal($data);
        }
        // console.log(this.comList);
    }

    comSelect(value) {

        this.assignCom = this.comresdata.find(item => item.name == value).id;
        this.listCompanyContacts();
    }

    assignCompanyToContact()
    {
        let fd = new FormData();
        fd.append('contacts', this.selArr);
        fd.append('company', this.assignCom);

        fd.append('api_token', this.authUser.api_token);

        this.userService.assignContactToCompany(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Contacts successfully assigned to company'});

                        this.selctComContacts =  (data.feed);
                        this.selArr = [];

                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );
    }

    listCompanyContacts() {
        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);
        fd.append('company', this.assignCom);
        this.userService.getCompanyContacts(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        // this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Contacts successfully assigned to company'});
                        this.selctComContacts = [];
                        this.selctComContacts =  (data.feed);
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );

    }

    companySearch(event) {
        // console.log(event.query);
        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);
        fd.append('query', event.query);

        this.userService.searchCompanies(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        this.comresults = [];
                        //this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Project Post Successful'});
                        this.comresdata =  (data.feed);
                        data.feed.forEach((value: any) => {
                            // this.comresults.push({'id':value.id ,'itemName': value.title });
                            this.comresults.push(value.name);
                        });
                        //alert(this.myGroups.length);
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );
    }

    unassignCompany(contactId)
    {
        if(confirm("Are you sure to unassign the contact?"))
        {
            let fd = new FormData();
            fd.append('contact_id',contactId);
            fd.append('api_token', this.authUser.api_token);
            //return;
            this.userService.companyUnassigned(fd)
                .subscribe(
                    (data: any) => {
                        //console.log(data);
                        if (data.status === 'success') {
                            // message
                            this.msgs = [];
                            this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'contact are successfully deleted'});
                            this.listCompanyContacts();
                        }
                        if (data.status === 'fail') {
                            // message
                            this.msgs = [];
                            if (data.errors && Object.keys(data.errors).length > 0)
                            {

                                for (var key in data.errors) {
                                    if (data.errors.hasOwnProperty(key)) {
                                        //console.log(key + " -> " + p[key]);
                                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                    }
                                }

                            }
                            else if(data.error_message)
                            {
                                this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                            }
                            else
                            {
                                this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                            }
                        }
                    },
                    error => {
                        console.log(error);
                        this.msgs = [];
                        if (error && error.message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                        }
                        //alert('Something went wrong!');
                    }
                );

        }
    }

    fetchUserGroups(){
        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);
        let group2 =[];
        this.userService.fetchGroups(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        //this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Project Post Successful'});
                        //this.contactList =  (data.feed);
                        if(data.feed){

                            data.feed.forEach((value: any) => {
                                this.myGroups.push({'id':value.id ,'itemName': value.title });
                                this.groupList.push({'value':value.id ,'label': value.title });
                            });
                        }
                        //alert(this.myGroups.length);

                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );

    }

    /**
     * @func {{assignGrp}}
     * @param \{{{formobject}}\} {{myform}} {{this will pass the form object}}
     * @description {{this will submit the form data and update the project data}}
     */
    assignGrp(myform)
    {
        //console.log(this.model);

        let fd = new FormData();

        fd.append('api_token', this.authUser.api_token);

        fd.append('groups', this.selectedItems);
        fd.append('contacts', this.selArr);

        this.userService.assignGroups(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Contacts successfully assigned to groups'});

                        this.selArr=[];
                        this.selectedItems=[];
                        // reload page
                        this.pageLoad();
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});
                    }
                    //alert('Something went wrong!');
                }
            );

    }

    /**
     * @func {{assignManagersToContacts}}
     * @param \{{{formobject}}\} {{myform}} {{this will pass the form object}}
     * @description {{this will submit the form data and update the project data}}
     */
    assignManagersToContacts(myform)
    {
        //console.log(this.model);

        let fd = new FormData();

        fd.append('api_token', this.authUser.api_token);

        fd.append('managers', this.selectedManagers);
        fd.append('contacts', this.selArr);

        this.userService.assignContactToManagers(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Contacts successfully assigned to managers'});
                        this.selArr=[];
                        this.selectedManagers=[];
                        // reload page
                        this.pageLoad();
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );

    }

    /**
     * @func {{getAllLanguages}}
     * @description {{this will fetch all Languages}}
     */
    getAllLanguages() {

        this.userService.getLanguages()
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        this.languages = data.language;
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );

    }

    kywordFilter(event){
         // console.log(event.srcElement.value);
        this.search_key = event.srcElement.value;
        this.fetchContacts();
    }

    typeFilter(event){
        // console.log(event.srcElement.value);
        this.type_filter = event.srcElement.value;
        this.fetchContacts();
    }

    managerFilter(event){
        // console.log(event.srcElement.value);
        // this.manager_filter = event.srcElement.value;
        this.manager_filter = this.selectedMngrs;
        this.fetchContacts();
    }


    /**
     * @func {{fetchContacts}}
     * @description {{this will fetch & search contacts }}
     */

    fetchContacts(){
        let fd = new FormData();
        fd.append('group_filter', this.group_filter);
        fd.append('search_key', this.search_key);
        fd.append('type_filter', this.type_filter);
        fd.append('manager_filter', this.manager_filter);
        fd.append('api_token', this.authUser.api_token);
        fd.append('tag_filter', this.selectedTags);
        let group2 = [];
        this.userService.getContacts(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        //this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Project Post Successful'});
                        this.liveProject =  (data.project);
                        this.contactLists =  (data.feed);
                        if(data.feed){
                            data.feed.forEach((value: any) => {
                                if(value.type==2)    this.comList.push(value.id.toString());

                            });
                        }

                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );
    }

    closePopup(){
        $("body").removeClass("open_modal");
        //$(".formwrap").css({"top": - formwrapH, "min-height":"1px"});
        $(".formwrap").css({"display":"none","top": -5200});
        setTimeout(function(){
            $(".formwrap").css({"display":"block","min-height":"1px"});
        }, 1000);
        $(".ovelay").addClass("hidden");
        $(".cntactadd .container-fluid").css({"min-height":0});
        $(".cntactadd").removeClass("open_popup");
    }

    changeFormField(event,index){
        // console.log(event.srcElement.value);
        // console.log(index);
        this.ff_arr[index] = event.srcElement.value;

        console.log(this.ff_arr);
    }

    changeToField(event,index){
        // console.log(event.srcElement.value);
        // console.log(index);
        this.tf_arr[index] = event.srcElement.value;

        console.log(this.tf_arr);
    }

    changeActionOnField(event,index){
        // console.log(event.srcElement.value);
        // console.log(index);
        this.actionf_arr[index] = event.srcElement.value;

        console.log(this.actionf_arr);
    }

    fieldMapSubmit(myForm)
    {
        this.fieldMapModel.from_filelds = this.ff_arr;
        this.fieldMapModel.to_filelds = this.tf_arr;
        this.fieldMapModel.fileld_actions = this.actionf_arr;
        this.fieldMapModel.contacts = this.selArr;
        console.log(this.fieldMapModel);

        if(this.ff_arr.length != this.tf_arr.length || this.ff_arr.length != this.actionf_arr.length || this.tf_arr.length != this.actionf_arr.length)
        {
            this.msgs = [];
            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Please fill all set with proper data"});
        }else
        {
            this.userService.mapField(this.fieldMapModel)
                .subscribe(
                    (data: any) => {
                        //console.log(data);
                        if (data.status === 'success') {
                            // message
                            this.msgs = [];
                            this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Field mapping successful'});

                            // reload page
                            this.fetchContacts();
                            this.closePopup();
                        }
                        if (data.status === 'fail') {
                            // message
                            this.msgs = [];
                            if (data.errors && Object.keys(data.errors).length > 0)
                            {

                                for (var key in data.errors) {
                                    if (data.errors.hasOwnProperty(key)) {
                                        //console.log(key + " -> " + p[key]);
                                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                    }
                                }

                            }
                            else if(data.error_message)
                            {
                                this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                            }
                            else
                            {
                                this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                            }
                        }
                    },
                    error => {
                        //console.log(error);
                        this.msgs = [];
                        if (error && error.message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                        }
                        //alert('Something went wrong!');
                    }
                );
        }

    }

    getEmailTemplates() {

        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);


        this.userService.emailTemplates(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        // this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Template Successfully saved'});

                        this.templateList = data.feed;
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );
    }

    setTemplate(event){
        let templtId = this.postEmailModel.template;
        console.log(templtId);
        if(templtId == ''){
            this.msgs=[];
            this.msgs.push({severity: 'error', summary: 'Error Message', detail: 'Please choose a valid template'});
        }
        else    this.postEmailModel.content = this.templateList.find(item => item.id == templtId).content;
    }

    mailSend(myform){

        // console.log(this.postEmailModel);
        // this.postEmailModel.api_token = this.authUser.api_token;
        // this.postEmailModel.to_email = this.contactData.email;
        // console.log(this.postEmailModel);
        if(this.is_submit == false){
            let fd = new FormData();
            fd.append('api_token', this.authUser.api_token);
            fd.append('to_contacts', this.selArr);
            fd.append('subject', this.postEmailModel.subject);
            fd.append('content', this.postEmailModel.content);
            fd.append('attachment', this.postEmailModel.attachment);

            this.is_submit = true;
            this.userService.massMailToContacts(fd)
                .subscribe(
                    (data: any) => {
                        //console.log(data);
                        if (data.status === 'success') {
                            // message
                            this.msgs = [];
                            this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Mail Send Successful'});

                            this.postEmailModel={
                                content : '',
                                template: ''
                            };

                            this.is_submit = false;
                            // $('#newmessage3').modal('toggle');
                            // this.templateList = data.feed;
                        }
                        if (data.status === 'fail') {
                            // message

                            this.is_submit = false;
                            this.msgs = [];
                            if (data.errors && Object.keys(data.errors).length > 0)
                            {

                                for (var key in data.errors) {
                                    if (data.errors.hasOwnProperty(key)) {
                                        //console.log(key + " -> " + p[key]);
                                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                    }
                                }

                            }
                            else if(data.error_message)
                            {
                                this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                            }
                            else
                            {
                                this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                            }
                        }
                    },
                    error => {
                        console.log(error);

                        this.is_submit = false;
                        this.msgs = [];
                        if (error && error.message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                        }
                        //alert('Something went wrong!');
                    }
                );
        }
    }


    getTags() {

        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);


        this.userService.featchTags(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        // this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Template Successfully saved'});

                        // this.tags = data.feed;
                        if(data.tags){

                            data.tags.forEach((value: any) => {
                                // this.tags.push({'id':value.id ,'itemName': value.title });
                                this.tags.push({'value':value.id ,'label': value.title });
                            });
                        }
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );
    }
}

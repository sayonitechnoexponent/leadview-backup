import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../services/common.service';
import { UserService } from '../../services/user.service';
import {Message} from 'primeng/primeng';
import { appConfig } from '../../apiurl.config';


@Component({
  selector: 'app-my-tags',
  templateUrl: './my-tags.component.html',
  styleUrls: ['./my-tags.component.css']
})
export class MyTagsComponent implements OnInit {

    searchText :string = '';
    selcArr :any =[];
    /**
     * @property \{{{any}}\} {{appConfig}} {{this will hold the constants for the app}}
     */

    appConfig: any = {};

    /**
     * @property \{{{Message[]}}\} {{msgs}} {{this is growl message model}}
     */

    msgs: Message[] = [];

    /**
     * @property \{{{any}}\} {{model}} {{user form model}}
     */

    model: any = {
        title: null,
        user_id: null,
        description: null,
        script: null,
    };

    /**
     * @property \{{{any}}\} {{authUser}} {{this will hold the authuser}}
     */

    authUser: any ={};

    /**
     * @property \{{{any}}\} {{allTags}} {{this will hold the current user added all tags}}
     */

    allTags: any = [];
    /**
     * @property \{{{any}}\} {{displayColFrm}} {{this will hold the display form dta}}
     */

    /**
     * @property \{{{any}}\} {{showCreateDateCol}} {{this will hold the Create Date column is show or not}}
     */

    showCreateDateCol: boolean =true;
    showTagName: boolean = true;
    showVisit: boolean = true;
    showContents: boolean = true;
    showShortCode: boolean = true;
    showUniqQuery: boolean = true;

    displayColFrm: any = {
        tagName : true,
        createDate : true,
        noofVisit : true,
        noofContents : true,
        shortCode : true,
        uniqQuery : true,
    };


  constructor(
      private commonService: CommonService,
      private userService: UserService
  ) { }

    /**
     * @func {{saveDisplay}}
     * @description {{toggle display of list}}
     */
    saveDisplay(myForm) {

        console.log(this.displayColFrm);
        this.showCreateDateCol = this.displayColFrm.createDate;
        this.showTagName = this.displayColFrm.tagName;
        this.showVisit = this.displayColFrm.noofVisit;
        this.showContents = this.displayColFrm.noofContents;
        this.showShortCode = this.displayColFrm.shortCode;
        this.showUniqQuery = this.displayColFrm.uniqQuery;

    }

    /**
     * @func {{getAllTags}}
     * @description {{get all Tags}}
     */
    getAllTags() {
        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);
        this.userService.featchTags(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        //this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Project Post Successful'});
                        this.allTags =  (data.tags);
                        /*data.tags.forEach((value: any) => {
                            //console.log(value);
                            this.allTags.push({'id':value.id ,'itemName': value.title });
                        });*/
                        //console.log(this.myTeam);
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );


    }


    /**
     * @func {{openTagModal}}
     * @param \{{{string}}\} {{$data}} {{popup data val class}}
     * @description {{this will me open the popup accordingly on click by pass data val}}
     */
    openTagModal($data){
        this.commonService.openModal($data);
        this.model.tag_id = null;
        this.model.type = 'Add';
        //console.log(user);

    }
    /**
     * @func {{openTagModal}}
     * @param \{{{string}}\} {{$data}} {{popup data val class}}
     * @description {{this will me open the popup accordingly on click by pass data val}}
     */
    openEditTagModal($data,tag){
        this.commonService.openModal($data);

        this.model.type = 'Edit';
        this.model = tag;
        this.model.tag_id = tag.id;
        //console.log(user);

    }
    /**
     * @func {{getAllTags}}
     * @description {{get all Tags}}
     */
    addSaveTag(myform) {

        this.model.api_token = this.authUser.api_token;
        this.userService.createTag(this.model)
            .subscribe(
                (data: any) => {
                    console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Tag successfully added'});

                        // go to  my project page
                        location.reload();
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );
    }


    selctToDel($event) {
        //console.log($event.srcElement.checked);
        //console.log($event.srcElement.value);
        if($event.srcElement.checked == true)
        {
            if(this.selcArr.indexOf($event.srcElement.value)== -1)
                this.selcArr.push($event.srcElement.value);
        }else {

            this.selcArr.splice(this.selcArr.indexOf($event.srcElement.value), 1);
        }
        console.log(this.selcArr);
    }
    /**
     * @func {{delAll}}
     * @description {{delete selected tags}}
     */
    delAll()
    {
        if(confirm("Are you sure to delete all selected tags?"))
        {
            let fd = new FormData();
            fd.append('tag_ids',this.selcArr);
            fd.append('api_token', this.authUser.api_token);
            //return;
            this.userService.deleteTag(fd)
                .subscribe(
                    (data: any) => {
                        //console.log(data);
                        if (data.status === 'success') {
                            // message
                            this.msgs = [];
                            this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Tags Successfully Deleted'});

                            location.reload();
                        }
                        if (data.status === 'fail') {
                            // message
                            this.msgs = [];
                            if (data.errors && Object.keys(data.errors).length > 0)
                            {

                                for (var key in data.errors) {
                                    if (data.errors.hasOwnProperty(key)) {
                                        //console.log(key + " -> " + p[key]);
                                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                    }
                                }

                            }
                            else if(data.error_message)
                            {
                                this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                            }
                            else
                            {
                                this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                            }
                        }
                    },
                    error => {
                        console.log(error);
                        this.msgs = [];
                        if (error && error.message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                        }
                        //alert('Something went wrong!');
                    }
                );

        }
    }


    /**
     * @func {{delaTag}}
     * @description {{delete user tag}}
     */
    delaTag(tagId)
    {
        //console.log(this.model);
        if(confirm("Are you sure to delete the tag?"))
        {
            let fd = new FormData();
            fd.append('tag_ids',tagId);
            fd.append('api_token', this.authUser.api_token);
            //return;
            this.userService.deleteTag(fd)
                .subscribe(
                    (data: any) => {
                        //console.log(data);
                        if (data.status === 'success') {
                            // message
                            this.msgs = [];
                            this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Tag Successfully Deleted'});

                            // go to  my team page
                            location.reload();
                        }
                        if (data.status === 'fail') {
                            // message
                            this.msgs = [];
                            if (data.errors && Object.keys(data.errors).length > 0)
                            {

                                for (var key in data.errors) {
                                    if (data.errors.hasOwnProperty(key)) {
                                        //console.log(key + " -> " + p[key]);
                                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                    }
                                }

                            }
                            else if(data.error_message)
                            {
                                this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                            }
                            else
                            {
                                this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                            }
                        }
                    },
                    error => {
                        console.log(error);
                        this.msgs = [];
                        if (error && error.message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                        }
                        //alert('Something went wrong!');
                    }
                );

        }


    }

    copyToClipboard(str)
    {
        let copy_string = str;
        document.addEventListener('copy', (e: ClipboardEvent) => {
            e.clipboardData.setData('text/plain', copy_string);
            e.preventDefault();
            document.removeEventListener('copy');
        });
        document.execCommand('copy');
    }

    ngOnInit() {
        this.authUser = this.userService.getAuthUser();
        this.appConfig = appConfig;
        this.getAllTags();
    }
}

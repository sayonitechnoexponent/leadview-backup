import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataActivityComponent } from './data-activity.component';

describe('DataActivityComponent', () => {
  let component: DataActivityComponent;
  let fixture: ComponentFixture<DataActivityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataActivityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataActivityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

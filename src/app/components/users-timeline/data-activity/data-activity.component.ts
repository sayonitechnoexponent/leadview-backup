import { Component, OnInit, Input } from '@angular/core';

import { CommonService } from '../../../services/common.service';
import { UserService } from '../../../services/user.service';
import { Message } from 'primeng/primeng';
import { appConfig } from '../../../apiurl.config';

@Component({
  selector: 'app-data-activity',
  templateUrl: './data-activity.component.html',
  styleUrls: ['./data-activity.component.css']
})
export class DataActivityComponent implements OnInit {
  
  appConfig: any = {};

  /**
   * @property \{{{Message[]}}\} {{msgs}} {{this is growl message model}}
   */
  msgs: Message[] = [];

  /**
     * @property \{{{any}}\} {{authUser}} {{this will hold the authuser}}
     */
  authUser: any = null;

  tlType :string = '';

  popListLimit: number = 3;

  dataActivityList: any = [];
  dataActivityInfoArr =['','first Name','last Name','company','role','phone','email','address','state','country','social'];
  multi_arr: any = ['5','6','10'];
  contactData: any;

  @Input('contactId') urlid: string;
  

  constructor(
    private commonService: CommonService,
    private userService: UserService) { }

  ngOnInit() {
    this.tlType='contact';
    this.authUser = this.userService.getAuthUser();
    
    //console.log(this.authUser);
    this.appConfig = appConfig;
    this.msgs = [];
    // this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Component Working'});
    // console.log('msgs',this.msgs);
  }

  ngAfterViewInit(){
    if(this.authUser!=null && this.urlid)    this.showDataActivity();
  }

  checkMultiple(type){
      console.log(type);
      console.log(this.multi_arr.includes(type.toString()));
    return  this.multi_arr.includes(type.toString());
  }
  showDataActivity(){
        
    let fd = new FormData();

    // fd.append('curr_user', this.urlid);
    // fd.append('tl_type', this.tlType);
    if(this.tlType=='contact'){            
        fd.append('contact_id', this.urlid);
    }
    fd.append('api_token', this.authUser.api_token);
    // fd.append('limit', this.popListLimit.toString());

    this.userService.fetchDataActivity(fd)
        .subscribe(
            (data: any) => {
                //console.log(data);
                if (data.status === 'success') {
                    // message
                    this.msgs = [];
                    //this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Project Post Successful'});
                    this.dataActivityList = (data.feed);
                    // this.totalShowCount = (data.total_count);
                    //console.log(this.myTeam);
                }
                if (data.status === 'fail') {
                    // message
                    this.msgs = [];
                    if (data.errors && Object.keys(data.errors).length > 0) {

                        for (var key in data.errors) {
                            if (data.errors.hasOwnProperty(key)) {
                                //console.log(key + " -> " + p[key]);
                                this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                            }
                        }

                    }
                    else if (data.error_message) {
                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                    }
                    else {
                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                    }
                }
            },
            error => {
                //console.log(error);
                this.msgs = [];
                if (error && error.message) {
                    this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

                }
                //alert('Something went wrong!');
            }
        );
  }

  undoRedoLog(_stage,changeLogId){
      console.log(_stage);
      let isNew = false;
      if(sessionStorage.isSessFlag) {
      }else   isNew = true;

      let fd = new FormData();
      if(this.tlType=='contact'){            
          fd.append('contact_id', this.urlid);
      }
      fd.append('is_new_session',isNew.toString());
      fd.append('change_stage', _stage);
      fd.append('change_log_id', changeLogId);
      fd.append('api_token', this.authUser.api_token);
      // fd.append('limit', this.popListLimit.toString());

      this.userService.changeUndoRedoContact(fd)
          .subscribe(
              (data: any) => {
                  //console.log(data);
                  if (data.status === 'success') {
                      sessionStorage.isSessFlag = 1;
                      // message
                      this.msgs = [];
                      this.showDataActivity();
                      //this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Project Post Successful'});
                      // this.dataActivityList = (data.feed);
                      // this.totalShowCount = (data.total_count);
                      //console.log(this.myTeam);
                  }
                  if (data.status === 'fail') {
                      // message
                      this.msgs = [];
                      if (data.errors && Object.keys(data.errors).length > 0) {

                          for (var key in data.errors) {
                              if (data.errors.hasOwnProperty(key)) {
                                  //console.log(key + " -> " + p[key]);
                                  this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                              }
                          }

                      }
                      else if (data.error_message) {
                          this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                      }
                      else {
                          this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                      }
                  }
              },
              error => {
                  //console.log(error);
                  this.msgs = [];
                  if (error && error.message) {
                      this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

                  }
                  //alert('Something went wrong!');
              }
          );
  }

  deleteLog(logId){
      if (confirm("Are you sure to delete the log?")) {
          let fd = new FormData();

          fd.append('curr_user', this.urlid);
          fd.append('tl_type', this.tlType);
          fd.append('api_token', this.authUser.api_token);
          fd.append('change_log_id', logId);
          
          this.userService.deleteContactChnageLog(fd)
              .subscribe(
                  (data: any) => {
                      //console.log(data);
                      if (data.status === 'success') {
                          // message
                          this.msgs = [];
                          this.msgs.push({ severity: 'success', summary: 'Success Message', detail: 'Log Successfully Deleted' });

                          // go to  my team page
                          this.showDataActivity();
                      }
                      if (data.status === 'fail') {
                          // message
                          this.msgs = [];
                          if (data.errors && Object.keys(data.errors).length > 0) {

                              for (var key in data.errors) {
                                  if (data.errors.hasOwnProperty(key)) {
                                      //console.log(key + " -> " + p[key]);
                                      this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                                  }
                              }

                          }
                          else if (data.error_message) {
                              this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                          }
                          else {
                              this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                          }
                      }
                  },
                  error => {
                      // console.log(error);
                      this.msgs = [];
                      if (error && error.message) {
                          this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

                      }
                      //alert('Something went wrong!');
                  }
              );

      }
  }

  openContactModal($data,contactDta) {
      //console.log($data);
      this.contactData = contactDta;
      this.popListLimit = 3;
      this.commonService.openChildModal($data);
  }

}

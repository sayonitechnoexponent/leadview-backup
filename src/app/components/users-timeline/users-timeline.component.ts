import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { CommonService } from '../../services/common.service';
import { UserService } from '../../services/user.service';
import { Message } from 'primeng/primeng';
import { appConfig } from '../../apiurl.config';
import { CreateNewAutocompleteGroup, SelectedAutocompleteItem, NgAutocompleteComponent } from "ng-auto-complete";
import index from "@angular/cli/lib/cli";
declare var $: any;
declare var google: any;

@Component({
    selector: 'app-users-timeline',
    templateUrl: './users-timeline.component.html',
    styleUrls: ['./users-timeline.component.css']
})
export class UsersTimelineComponent implements OnInit {

    phone_countries: any = [];
    /**
     * @property \{{{any}}\} {{timeLimeUser}} {{this will hold the time-line user data }}
     */

    timeLimeUser: any = {};

    /**
     * @property \{{{any}}\} {{appConfig}} {{this will hold the constants for the app}}
     */

    appConfig: any = {};

    /**
     * @property \{{{Message[]}}\} {{msgs}} {{this is growl message model}}
     */

    msgs: Message[] = [];

    /**
     * @property \{{{any}}\} {{noteModel}} {{this form model}}
     */

    noteModel: any = {};
    /**
     * @property \{{{any}}\} {{opportunityModel}} {{this form model}}
     */
    opportunityModel: any = {};
    taskModel: any = {};
    emailModel: any = {
        type: 1,
        is_primary: 0,
    };
    labelModel: any = {};

    reminderModel: any = {
        upto_val: '1',
        upto_dur: 'w',
        repeat_val: '1',
    };

    closerDate: Date;
    dateTime: Date;
    startDate: Date;
    endDate: Date;

    googoleAutoData: any = {};
    /**
     * @property \{{{any}}\} {{authUser}} {{this will hold the authuser}}
     */

    authUser: any = {};
    /**
     * @property \{{{any}}\} {{myList}} {{this will hold the list data}}
     */

    myList: any = [];

    /**
     * @property \{{{any}}\} {{postScopeModel}} {{this form model}}
     */

    postScopeModel: any = {};

    mylabels: any = [];
    noteTypeList: any = [];
    oppTypeList: any = [];
    taskTypeList: any = [];
    oppStageList: any = [];
    opportunityDd: any = [];
    opportunityRel: any = [];

    allUsers: any = [];
    managerList: any = [];
    projectUsers: any = [];
    selectedItems: any = [];
    post_count: any = [];

    dropdownSettings = {};
    is_submit: boolean = false;
    draftStop: boolean = false;

    reminders: any = [
        { 'id': 10, 'itemName': '10  minutes' },
        { 'id': 15, 'itemName': '15  minutes' },
        { 'id': 20, 'itemName': '20  minutes' },
        { 'id': 25, 'itemName': '25  minutes' },
        { 'id': 30, 'itemName': '30  minutes' },
    ];
    profitMargin: number = 0;
    budgetVal: number = 0;
    profitVal: number = 0;
    group: any = [];
    contactList: any = [];
    companyList: any = [];
    selectedContact: any = [];
    selectedCompany: any = [];
    checkLocation: boolean = false;
    checkDate: boolean = false;
    checkTime: boolean = false;
    listTab: string = '#allpost';
    noteList: any = [];
    taskList: any = [];
    opportunityList: any = [];
    emailtlList: any = [];
    phnList: any = [];
    addressList: any = [];
    filterList: any = [];
    opportunityPostList: any = [];
    allPostList: any = [];
    activityList: any = [];
    chatList: any = [];
    mailList: any = [];

    emailList: any = [];
    filter_id: any = '';
    noteTypeFilter: any = '';
    taskTypeFilter: any = '';
    searchModel: any = {
        start_date: '',
        end_date: '',
        keyword: '',
    };
    selctedPost: string = '';
    userToNotify: any = [];
    urlid: string = '';
    userPhoneModel: any = {
        dial_code: 1,
        country_code: 'us'
    };
    addressModel: any = {};
    groupModel: any = {};
    popListLimit: number = 3;
    p: number = 1;
    page: number = 1;
    limit: number = 10;
    isBottom: boolean = false;
    postCommentModel: any = {};

    lat: number = 51.678418;
    lng: number = 7.809007;
    allCountries: any = [];

    tlType: string = 'user';
    contactData: any = [];
    comresults: any = [];
    comresdata: any = [];
    comText: string;
    composition: string;
    assignCom: string;
    selctComContacts: any = [];
    selectedManagers: any = [];
    selctManagers: any = [];
    assignManagerModel: any = {
        managers: []
    };
    dropdownSettings2: any = {};
    socialModel: any = {
        type: 1,
    };
    socailList: any = [];
    options: any;
    overlays: any = [];

    socailImageArr: any = ['', 'fbf', 'gplusf', 'pinterestf', 'youtubef', 'twitterf'];
    showSocailImage: any = ['instagram', 'fb', 'gplus', 'pinterestf', 'youtubef', 'twitter'];
    // map: google.maps.Map;
    groupList: any = [];
    archive_status: boolean = false;
    assigned_groups: any = [];
    mapLat: number = 0;
    mapLng: number = 0;
    searchMapModel: any = {
        distType: 1
    };
    map_search_result: any = [];
    markers: any = [];
    currnt_address: any = [];
    map_setting: any = null;
    totalShowCount: number = 0;
    totalVisitCount: number = 0;
    visit_tags: any = [];
    visit_tagscores: any = [];
    dash_visitors: any = [];
    approveModel: any = {};
    templateList: any = [];
    draftCheck: boolean = false;
    postCommentFile: any = [];
    myProjects: any = [];

    projFilter: string = '';

    postEmailModel: any = {
        content: '',
        template: '',
        managers: []
    };

    curr_tab: string = '';

    constructor(
        private commonService: CommonService,
        private userService: UserService,
        private completer: NgAutocompleteComponent,
        private route: ActivatedRoute,
        private router: Router
    ) {

        let that = this;

        $(document).on("click", ".tabmenuwrap a", function () {
            var curr_boj = $(this);
            var tb = curr_boj.attr('href');
            // alert(tb);
            //
            if (that.listTab! = tb) that.page = 1;
            that.listTab = tb;
            that.searchModel.keyword = '';
            that.searchModel.start_date = '';
            that.searchModel.start_date = '';
            that.searchModel.end_date = '';
            that.startDate = null;
            that.endDate = null;
            that.fetchUserTimeLine();
            //that.myList = [];
            //alert(that.listTab);
        });

        $(document).on("click", ".emailbx ul li a", function () {
            $("#emailtyp").val($(this).children('.typ').text());
        });
        $(document).on("click", ".phnbx ul li a", function () {
            $("#phntyp").val($(this).children('.typ').text());
        });
        //$("#mobile-number").intlTelInput();
        $("#phone").intlTelInput({
            autoPlaceholder: false,
            utilsScript: "assets/build/js/utils.js"
        });

        $(document).on("click", "ul.country-list  li", function () {
            let dc = $(this).attr('data-dial-code');
            that.userPhoneModel.dial_code = dc;
            let cc = $(this).attr('data-country-code');
            that.userPhoneModel.country_code = cc;
        });

        $(window).scroll(function () {
            //console.log($(window).scrollTop()+$(window).height());
            //console.log($(window).height());
            // console.log($(document).height());
            //console.log( $(document).height());
            if (that.isBottom == false) {

                // if($(window).scrollTop() == ($(document).height() - $(window).height()-150))

                if (($(window).scrollTop() + $(window).height()) == ($(document).height())) {
                    //alert( that.isBottom);
                    that.isBottom = true;
                    if (that.isBottom) {
                        that.p = that.p + 1
                        that.page = that.page + 1;
                        // console.log(that.page);
                        //alert(that.page);
                        that.fetchUserTimeLine();

                    }
                }
            }

        });

        $(document).on("click", ".shwtopsec", function () {
            $(this).parent().parent().children(".topsecboxbox").show();
            $(this).parent("").hide();
        });



        $(document).on('keyup', '.showccbcc', function (e) {
            $(".withmsgact").show();
        });

        $(document).on("click", ".opencc", function () {
            $(".showCc").show();
        });

        $(document).on("click", ".openbcc", function () {
            $(".showBcc").show();
        });
        $(document).on("click", ".offccbcc", function () {
            $(this).parent().parent().hide();
        });

        $(document).on("click", ".rmvmap", function () {
            // alert('open');
            $(".userLoged").addClass("modal-open2");
        });
        $(document).on("click", ".ovelay_map,.cmm", function () {
            //$("body").hasClass("modal-open2").removeClass("modal-open2");
            //alert("ok");
            if ($(".userLoged").hasClass("modal-open2")) {

                // alert('close');
                $(".userLoged").removeClass("modal-open2");
            }
        });

        $(document).on("click", ".statubtntoapprove", function () {
            $(this).parent().addClass("showaprovebox");
        });
    }

    initMap(addr) {
        var myLatLng = { lat: parseFloat(addr.lat), lng: parseFloat(addr.lng) };
        // console.log(addr);
        var markers = [];
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 12,
            center: myLatLng
        });

        var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            icon: 'http://maps.google.com/mapfiles/ms/icons/green-dot.png',
            title: addr.address.toString()
        });
        this.clearMarkers(markers);
        for (var i = 0; i < this.map_search_result.length; i++) {
            this.addMarkerWithTimeout(map, markers, this.map_search_result[i], i * 200);
        }
    }

    addMarkerWithTimeout(map, markers, position, timeout) {
        window.setTimeout(function () {
        }, timeout);
        markers.push(new google.maps.Marker({
            position: { lat: parseFloat(position.lat), lng: parseFloat(position.lng) },
            map: map,
            animation: google.maps.Animation.DROP
        }));
    }

    clearMarkers(markers) {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(null);
        }
        markers = [];
    }

    searchMap(myform) {
        let show_company = 1;
        let show_contact = 1;
        if (this.map_setting != null) {
            show_company = this.map_setting.show_company;
            show_contact = this.map_setting.show_contact;
        }
        this.searchMapModel.api_token = this.authUser.api_token;
        this.searchMapModel.show_contact = show_contact.toString();
        this.searchMapModel.show_company = show_company.toString();
        this.searchMapModel.contact_id = this.urlid;

        // console.log(this.searchMapModel);
        this.userService.searchOnMap(this.searchMapModel)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        // this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'successfully '});
                        this.map_search_result = [];
                        this.map_search_result = data.feed;
                        this.initMap(this.currnt_address);
                        // reload page
                        // this.pageLoad();
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0) {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    // alert(data.errors[key]);
                                    this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                                }
                            }

                        }
                        else if (data.error_message) {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                        }
                        else {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message) {
                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

                    }
                    //alert('Something went wrong!');
                }
            );
    }

    /*setNgMap(){
      this.options = {
          center: {lat: 36.890257, lng: 30.707417},
          zoom: 12
      };
      this.overlays = [
          /!*new google.maps.Marker({position: {lat: 36.879466, lng: 30.667648}, title:"Konyaalti"}),
          new google.maps.Marker({position: {lat: 36.883707, lng: 30.689216}, title:"Ataturk Park"}),
          new google.maps.Marker({position: {lat: 36.885233, lng: 30.702323}, title:"Oldtown"}),*!/
          /!*new google.maps.Polygon({paths: [
              {lat: 36.9177, lng: 30.7854},{lat: 36.8851, lng: 30.7802},{lat: 36.8829, lng: 30.8111},{lat: 36.9177, lng: 30.8159}
          ], strokeOpacity: 0.5, strokeWeight: 1,fillColor: '#1976D2', fillOpacity: 0.35
          }),
          new google.maps.Circle({center: {lat: 36.90707, lng: 30.56533}, fillColor: '#1976D2', fillOpacity: 0.35, strokeWeight: 1, radius: 1500}),*!/
          new google.maps.Polyline({path: [{lat: 36.86149, lng: 30.63743},{lat: 36.86341, lng: 30.72463}], geodesic: true, strokeColor: '#FF0000', strokeOpacity: 0.5, strokeWeight: 2})
      ];
  }
    */

    ngOnInit() {
        // this.setNgMap();
        this.authUser = this.userService.getAuthUser();
        // console.log(this.route.snapshot.url);

        //console.log(this.authUser);


        this.allPostList = [];
        this.appConfig = appConfig;
        //this.fetchUserTimeLine();

        this.dropdownSettings = {
            singleSelection: false,
            text: "Select",
            enableCheckAll: false,
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: true,
            classes: "myclass custom-class"
        };

        this.dropdownSettings2 = {
            singleSelection: false,
            text: "Select Managers",
            enableCheckAll: false,
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: true,
            classes: "myclass custom-class"
        };
        this.userToNotify = [];
        this.userToNotify.push(this.authUser.id.toString());
    }

    ngAfterViewInit() {

        this.route.params.subscribe(params => {
            // console.log(params['id'])
            let flg = 0;
            if (this.urlid != params['id'] && params['id'] != '') flg = 1;
            if (params['id']) {
                this.urlid = params['id'];
                this.fetchUser();
            }
            else this.urlid = '';

            if (params['contact_id']) {
                this.urlid = params['contact_id'];
                this.tlType = 'contact';
                flg = 1;
                this.fetchSingleContact();
                this.fetchSocials();
            }
            else if (params['visitor_id']) {
                this.urlid = params['visitor_id'];
                this.tlType = 'visitor';
                flg = 1;
                this.fetchSingleContact();
                this.fetchSocials();
            }

            if (flg == 1) {
                this.fetchUserTimeLine();
            }

        });
        // console.log(this.tlType)
        this.fetchTeamUsers();
        this.fetchOpportunity();
        this.fetchNoteType();
        this.fetchTaskType();
        this.getAllCountries();
        this.getEmailTemplates();
        this.getMyProjects();
        // alert($('#all').height());

        // console.log(this.tlType)
        if (this.tlType == 'user') {
            this.fetchUserDashboard();
        }
        let that = this;

        $(document).on('keydown', '.commntarea', function () {
            //console.log($(this).val());
            that.postCommentModel.content = $(this).val();
        });
        /*setTimeout(()=>{
            that.initMap();
        },2000);*/

        $(document).on('click', 'a.ldmore', function () {
            //alert('ldmore ');
            if ($(this).text() == 'View more') {
                $(this).text('View less');
                $(this).parent().parent().children('.commntList').children('li').show();
            }
            else {
                $(this).text('View more');
                $(this).parent().parent().children('.commntList').children('li').hide();
            }
        });

        $(document).on('click', 'a.cldmore', function () {
            // alert('ldmore ');
            if ($(this).text() == 'View more') {
                $(this).text('View less');
                $(this).parent().parent().children('.msgdata').toggleClass('toshort');
            }
            else {
                $(this).text('View more');
                $(this).parent().parent().children('.msgdata').toggleClass('toshort');
            }
        });


    }

    showMap(address) {
        // console.log(address);
        this.mapLat = address.lat;
        this.mapLng = address.lng;
        this.map_search_result = [];
        this.searchMapModel = {};
        this.searchMapModel.curr_lat = this.mapLat;
        this.searchMapModel.curr_lng = this.mapLng;
        // this.searchMapModel.contact_id = address.id;
        this.searchMapModel.distType = 1;

        this.currnt_address = address;
        this.initMap(address);
    }
    /**
     * @func {{openAssignManager}}
     * @param \{{{string}}\} {{$data}} {{popup data val class}}
     * @description {{this will me open the popup accordingly on click by pass data val}}
     */
    openAssignManager($data) {
        this.commonService.openModal($data);
        //this.selUserArr = [];
        this.selectedManagers = [];
        // this.assignManagerModel.managers = [];

    }
    onManagerSelect(item: any) {
        //console.log(item.id);

        if (this.selectedManagers.indexOf(item.id) == -1)
            this.selectedManagers.push(item.id);

        //console.log(this.selectedManagers);
    }

    OnManagerDeSelect(item: any) {
        //console.log(item.id);

        if (this.selectedManagers.indexOf(item.id) != -1)
            this.selectedManagers.splice(this.selectedManagers.indexOf(item.id), 1);
        // console.log(this.selectedManagers);

    }

    /**
     * @func {{assignManagersToContacts}}
     * @param \{{{formobject}}\} {{myform}} {{this will pass the form object}}
     * @description {{this will submit the form data and update the project data}}
     */
    assignManagersToContacts(myform) {
        // console.log(this.assignManagerModel.managers);
        let manag_arr: any = [];
        this.assignManagerModel.managers.forEach((value: any) => {
            // this.comresults.push({'id':value.id ,'itemName': value.title });
            manag_arr.push(value.id);
        });

        let fd = new FormData();

        fd.append('api_token', this.authUser.api_token);

        fd.append('managers', manag_arr);
        fd.append('contacts', this.urlid);
        fd.append('page_type', 'tl');

        this.userService.assignContactToManagers(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        this.msgs.push({ severity: 'success', summary: 'Success Message', detail: 'Contacts successfully assigned to managers' });
                        this.selectedManagers = [];
                        // reload page
                        // this.pageLoad();
                        this.fetchSingleContact();
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0) {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                                }
                            }

                        }
                        else if (data.error_message) {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                        }
                        else {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message) {
                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

                    }
                    //alert('Something went wrong!');
                }
            );

    }


    listCompanyContacts() {
        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);
        fd.append('company', this.assignCom);
        this.userService.getCompanyContacts(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        // this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Contacts successfully assigned to company'});
                        this.selctComContacts = [];
                        this.selctComContacts = (data.feed);
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0) {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                                }
                            }

                        }
                        else if (data.error_message) {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                        }
                        else {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message) {
                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

                    }
                    //alert('Something went wrong!');
                }
            );

    }

    comSelect(value) {

        this.assignCom = this.comresdata.find(item => item.name == value).id;
        this.listCompanyContacts();
    }

    companySearch(event) {
        // console.log(event.query);
        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);
        fd.append('query', event.query);

        this.userService.searchCompanies(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        this.comresults = [];
                        //this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Project Post Successful'});
                        this.comresdata = (data.feed);
                        data.feed.forEach((value: any) => {
                            // this.comresults.push({'id':value.id ,'itemName': value.title });
                            this.comresults.push(value.name);
                        });
                        //alert(this.myGroups.length);
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0) {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                                }
                            }

                        }
                        else if (data.error_message) {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                        }
                        else {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message) {
                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

                    }
                    //alert('Something went wrong!');
                }
            );
    }

    assignCompanyToContact() {
        let fd = new FormData();
        fd.append('contacts', this.urlid);
        fd.append('company', this.assignCom);
        fd.append('position', this.composition);
        fd.append('api_token', this.authUser.api_token);

        // console.log(this.composition);

        this.userService.assignContactToCompany(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        this.msgs.push({ severity: 'success', summary: 'Success Message', detail: 'Contacts successfully assigned to company' });
                        this.composition = null;
                        this.comText = null;

                        this.selctComContacts = (data.feed);
                        this.fetchSingleContact();
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0) {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                                }
                            }

                        }
                        else if (data.error_message) {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                        }
                        else {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message) {
                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

                    }
                    //alert('Something went wrong!');
                }
            );
    }

    Selected(item: SelectedAutocompleteItem, typ) {
        //console.log(item.item);
        if (typ == '1') {
            this.selectedContact.push(item.item);
            this.completer.RemovableValues('completer', this.selectedContact)
        }
        else if (typ == '1') {
            this.selectedCompany.push(item.item);
            this.completer.RemovableValues('completer', this.selectedCompany)
        }


        /**
         * Remove selected values from list,
         * selected value must be of type: {id: string(based on GUID's), [value: string]: any}[]
         */
    }

    /**
     * @func {{pageLoad}}
     * @description {{reload page after a certain time}}
     */
    pageLoad() {
        setTimeout(() => {    //<<<---    using ()=> syntax
            location.reload()
        }, 1000);
    }

    listContacts() {
        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);
        let group2 = [];
        this.userService.getContacts(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        //this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Project Post Successful'});
                        //conlist =  (data.feed);
                        data.feed.forEach((value: any) => {
                            let nam = (value.first_name ? value.first_name : '') + ' ' + (value.last_name ? value.last_name : '');
                            if (value.type == 1) this.contactList.push({ 'id': value.id, 'itemName': nam });
                            else if (value.type == 2) this.companyList.push({ 'id': value.id, 'itemName': nam });

                        });
                        if (this.tlType == 'visitor') this.contactList = [];
                        //console.log(this.group);
                        /*this.contactList = [
                            CreateNewAutocompleteGroup(
                                'Search from list',
                                'completer',
                                this.group,
                                {titleKey: 'title', childrenKey: null}
                            ),
                        ];
                        this.companyList = [
                            CreateNewAutocompleteGroup(
                                'Search from list',
                                'completer',
                                group2,
                                {titleKey: 'title', childrenKey: null}
                            ),
                        ];*/
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0) {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                                }
                            }

                        }
                        else if (data.error_message) {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                        }
                        else {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message) {
                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

                    }
                    //alert('Something went wrong!');
                }
            );
        //console.log(this.contactList);

    }

    /**
     * @func {{openCreateNote}}
     * @param \{{{string}}\} {{$data}} {{popup data val class}}
     * @description {{this will be open the popup accordingly on click by pass data val}}
     */
    openCreateNote($data) {
        //console.log($data);
        this.contactList = [];
        this.companyList = [];
        this.selectedItems = [];
        this.selectedContact = [];
        this.selectedCompany = [];
        this.commonService.openModal($data);
        this.noteModel = {};
        this.noteModel.type = "Add";
        this.fetchNoteType();

        this.listContacts();
        //this.listCompanies();
    }
    /**
     * @func {{openCreateOppt}}
     * @param \{{{string}}\} {{$data}} {{popup data val class}}
     * @description {{this will be open the popup accordingly on click by pass data val}}
     */
    openCreateOppt($data) {
        //console.log($data);

        this.contactList = [];
        this.companyList = [];
        this.googoleAutoData = {};
        this.selectedItems = [];
        this.selectedContact = [];
        this.selectedCompany = [];
        this.commonService.openModal($data);
        this.noteModel = {};
        this.noteModel.type = "Add";

        this.fetchOppType();
        this.fetchStage();
        this.listContacts();
        //this.listCompanies();
    }
    /**
     * @func {{openCreateTask}}
     * @param \{{{string}}\} {{$data}} {{popup data val class}}
     * @description {{this will be open the popup accordingly on click by pass data val}}
     */
    openCreateTask($data) {
        //console.log($data);

        this.contactList = [];
        this.companyList = [];
        this.googoleAutoData = {};
        this.taskTypeList = [];
        this.selectedItems = [];
        this.selectedContact = [];
        this.selectedCompany = [];
        this.commonService.openModal($data);
        this.taskModel = {};
        this.taskModel.type = "Add";

        this.fetchTaskType();
        this.listContacts();
        //this.listCompanies();
    }
    /**
     * @func {{openAddEmail}}
     * @param \{{{string}}\} {{$data}} {{popup data val class}}
     * @description {{this will be open the popup accordingly on click by pass data val}}
     */
    openAddEmail($data) {
        //console.log($data);
        this.popListLimit = 3;
        this.commonService.openModal($data);
        this.emailModel = {};
        this.fetchUserEmails();
    }
    /**
     * @func {{openAddPhone}}
     * @param \{{{string}}\} {{$data}} {{popup data val class}}
     * @description {{this will be open the popup accordingly on click by pass data val}}
     */
    openAddPhone($data) {
        //console.log($data);
        $("#phone").intlTelInput("setCountry", 'us');
        this.popListLimit = 3;
        this.phnList = [];
        this.commonService.openModal($data);
        this.userPhoneModel = {
            dial_code: 1,
            country_code: 'us',
            type: 1,
            phone: ''
        };
        this.fetchUserPhNumbers();
    }
    /**
     * @func {{openDirectionbx}}
     * @param \{{{string}}\} {{$data}} {{popup data val class}}
     * @description {{this will be open the popup accordingly on click by pass data val}}
     */
    openDirectionbx($data) {
        this.commonService.openModal($data);
    }
    /**
     * @func {{openAddressbx}}
     * @param \{{{string}}\} {{$data}} {{popup data val class}}
     * @description {{this will be open the popup accordingly on click by pass data val}}
     */
    openAddressbx($data) {
        //console.log($data);
        this.popListLimit = 3;

        //console.log(this.allCountries);

        this.googoleAutoData = {};
        this.commonService.openModal($data);

        this.addressModel = {};

        this.fetchUserAddress();
    }

    /**
     * @func {{openAddGroupbx}}
     * @param \{{{string}}\} {{$data}} {{popup data val class}}
     * @description {{this will be open the popup accordingly on click by pass data val}}
     */
    openAddGroupbx($data) {
        //console.log($data);
        this.popListLimit = 3;
        this.fetchUserGroups();
        //console.log();

        this.commonService.openModal($data);

        this.groupModel = {};

    }
    /**
     * @func {{openAddComBx}}
     * @param \{{{string}}\} {{$data}} {{popup data val class}}
     * @description {{this will be open the popup accordingly on click by pass data val}}
     */
    openAddComBx($data) {
        //console.log($data);
        this.comText = null;
        this.composition = null;
        this.assignCom = '';
        this.msgs = [];
        // this.selctComContacts = [];
        this.popListLimit = 3;

        //console.log(this.allCountries);

        this.commonService.openModal($data);

        // this.groupModel = { };

    }
    /**
     * @func {{openSocialBx}}
     * @param \{{{string}}\} {{$data}} {{popup data val class}}
     * @description {{this will be open the popup accordingly on click by pass data val}}
     */
    openSocialBx($data) {
        //console.log($data);
        this.comText = null;
        this.assignCom = '';
        this.msgs = [];
        this.selctComContacts = [];
        this.popListLimit = 3;

        //console.log(this.allCountries);

        this.commonService.openModal($data);
        this.fetchSocials();
        // this.groupModel = { };

    }

    addrsEdit(obj) {
        this.addressModel = obj;
        this.addressModel._id = obj.id;
        //console.log(this.userPhoneModel);
        //alert(obj.phone_number);
        //$("#phone").val(obj.phone_number);
    }

    getAllCountries() {

        this.userService.allCountries()
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        this.allCountries = data.countries;
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0) {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                                }
                            }

                        }
                        else if (data.error_message) {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                        }
                        else {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message) {
                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

                    }
                    //alert('Something went wrong!');
                }
            );

    }

    /**
     * @func {{fetchUserEmails}}
     * @description {{get all Stages}}
     */
    fetchUserEmails() {
        this.totalShowCount = 0;
        let fd = new FormData();

        fd.append('curr_user', this.urlid);
        fd.append('tl_type', this.tlType);
        fd.append('api_token', this.authUser.api_token);
        fd.append('limit', this.popListLimit.toString());

        this.userService.getUserEmails(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        //this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Project Post Successful'});
                        this.emailList = (data.feed);
                        this.totalShowCount = (data.total_count);
                        //console.log(this.myTeam);
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0) {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                                }
                            }

                        }
                        else if (data.error_message) {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                        }
                        else {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message) {
                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

                    }
                    //alert('Something went wrong!');
                }
            );

    }

    /**
     * @func {{fetchNoteType}}
     * @description {{get all Stages}}
     */
    fetchNoteType() {
        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);

        this.userService.getNoteTypes(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        //this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Project Post Successful'});
                        this.noteTypeList = (data.feed);
                        //console.log(this.noteTypeList[0]);
                        if (data.feed.length > 0) {
                            this.noteModel.note_type = this.noteTypeList[0].id;
                        };
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0) {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                                }
                            }

                        }
                        else if (data.error_message) {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                        }
                        else {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message) {
                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

                    }
                    //alert('Something went wrong!');
                }
            );

    }

    /**
     * @func {{fetchTeamUsers}}
     * @description {{get user teams}}
     */
    fetchTeamUsers() {
        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);
        fd.append('page_type', 'contacts');
        this.userService.getTeamUsers(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        //this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Project Post Successful'});
                        this.projectUsers = (data.users);
                        if (data.users) {
                            data.users.forEach((value: any) => {
                                let nam = value.first_name + ' ' + value.last_name;
                                if (value.id != this.authUser.id) {
                                    this.allUsers.push({ 'id': value.id, 'itemName': nam });
                                    this.managerList.push({ 'value': value.id, 'label': nam });
                                }
                            });
                        }
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0) {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                                }
                            }

                        }
                        else if (data.error_message) {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                        }
                        else {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message) {
                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });
                    }
                    //alert('Something went wrong!');
                }
            );

    }

    /**
     * @func {{fetchOpportunity}}
     * @description {{get user User Time Line}}
     */
    fetchOpportunity() {
        //alert(this.listTab);
        let fd = new FormData();
        fd.append('list_tab', this.listTab);
        fd.append('api_token', this.authUser.api_token);
        this.userService.getOpportunity(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        this.opportunityList = data.feed;
                        this.opportunityList.forEach((value: any) => {
                            if (value.id) {
                                this.opportunityDd.push({ 'id': value.id, 'itemName': value.title });
                                this.opportunityRel.push({ 'value': value.id, 'label': value.title });
                            }
                        });
                        //console.log(this.opportunityDd);
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0) {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                                }
                            }

                        }
                        else if (data.error_message) {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                        }
                        else {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message) {
                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });
                    }
                    //alert('Something went wrong!');
                }
            );

    }

    getNoteFilter(typId) {
        if (typId != 0) this.noteTypeFilter = typId;
        else this.noteTypeFilter = '';
        //alert(this.noteTypeFilter);
        //this.fetchUserTimeLine();
    }

    getTaskFilter(typId) {
        if (typId != 0) this.taskTypeFilter = typId;
        else this.taskTypeFilter = '';
        //alert(this.noteTypeFilter);
        //this.fetchUserTimeLine();
    }

    onItemSelect(item: any) {
        //console.log(item.id);

        if (this.selectedItems.indexOf(item.id) == -1) {
            this.selectedItems.push(item.id);
        }

        //console.log(this.selectedItems);
    }

    OnItemDeSelect(item: any) {
        //console.log(item.id);

        if (this.selectedItems.indexOf(item.id) != -1)
            this.selectedItems.splice(this.selectedItems.indexOf(item.id), 1);
        //console.log(this.selectedItems);

    }

    onSelectAll(items: any) {
        //console.log(items);
    }
    onDeSelectAll(items: any) {
        // console.log(items);
    }

    /**
     * @func {{onFileChange}}
     * *@param  {{event}} {{this will pass the change event}}
     * @description {{get the file}}
     */

    onFileChange(event, modType, pid) {
        this.postCommentFile = [];
        this.postCommentModel.attachment = null;

        if (event.target.files && event.target.files.length > 0) {
            let file = event.target.files[0];
            //console.log(file);
            if (modType == 'n') this.noteModel.attachment = file;
            else if (modType == 'o') this.opportunityModel.attachment = file;
            else if (modType == 't') this.taskModel.attachment = file;
            else if (modType == 'm') this.postEmailModel.attachment = file;
            else if (modType == 'cmnt') {
                this.postCommentModel.attachment = file;
                if (pid != '') this.postCommentFile.push({ post_id: pid, file: file });
            }
        }

        // console.log(this.postCommentFile)
    }

    shwCntFile(postId) {

        if (this.postCommentFile.find(item => item.post_id == postId)) {
            // console.log(this.postCommentFile.find(item => item.post_id == postId));
            return this.postCommentFile.find(item => item.post_id == postId).file.name;
        }
        //
        //name
    }

    /**
     * @func {{addNote}}
     * @param \{{{formobject}}\} {{myform}} {{this will pass the form object}}
     * @description {{this will submit the form data and update the project data}}
     */
    addNote(myform) {
        this.is_submit = true;
        let fd = new FormData();
        fd.append('attachment', this.noteModel.attachment);
        fd.append('note_type', this.noteModel.note_type);
        fd.append('details', this.noteModel.details);
        fd.append('managers', this.selectedItems);
        fd.append('contacts', this.noteModel.contacts);
        fd.append('companies', this.noteModel.companies);
        fd.append('opportunities', this.noteModel.opportunities);
        fd.append('reminders', this.noteModel.reminders);
        fd.append('api_token', this.authUser.api_token);

        fd.append('curr_user', this.urlid);
        fd.append('tl_type', this.tlType);
        //this.noteModel.api_token = this.authUser.api_token;
        //console.log(this.noteModel);

        this.userService.addEditNote(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    this.is_submit = false;
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        this.msgs.push({ severity: 'success', summary: 'Success Message', detail: 'Note added successfully' });

                        //this.fetchUserTimeLine();
                        // reload page
                        this.pageLoad();
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0) {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                                }
                            }

                        }
                        else if (data.error_message) {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                        }
                        else {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.is_submit = false;
                    this.msgs = [];
                    if (error && error.message) {
                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

                    }
                    //alert('Something went wrong!');
                }
            );

    }

    /**
     * @func {{addOpportunity}}
     * @param \{{{formobject}}\} {{myform}} {{this will pass the form object}}
     * @description {{this will submit the form data and update the project data}}
     */
    addOpportunity(myform) {
        this.is_submit = true;
        if (this.closerDate) this.opportunityModel.closer_date = (this.closerDate.getTime()) / 1000;
        //console.log(this.closerDate);
        let fd = new FormData();
        fd.append('opportunity_type', this.opportunityModel.opp_type);
        fd.append('attachment', this.opportunityModel.attachment);
        fd.append('title', this.opportunityModel.title);
        fd.append('description', this.opportunityModel.description);
        fd.append('managers', this.selectedItems);
        fd.append('contacts', this.opportunityModel.contacts);
        fd.append('companies', this.opportunityModel.companies);
        fd.append('api_token', this.authUser.api_token);
        fd.append('closer_date', this.opportunityModel.closer_date);
        fd.append('budget', this.opportunityModel.budget);
        fd.append('opportunity_stage', this.opportunityModel.opp_stage);
        fd.append('profit_margin', this.profitVal.toString());

        fd.append('curr_user', this.urlid);
        fd.append('tl_type', this.tlType);
        //console.log(fd);
        //this.noteModel.api_token = this.authUser.api_token;

        this.userService.addEditOpportunity(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    this.is_submit = false;
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        this.msgs.push({ severity: 'success', summary: 'Success Message', detail: 'Opportunity added successfully' });

                        //this.fetchUserTimeLine();
                        // reload page
                        this.pageLoad();
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0) {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                                }
                            }

                        }
                        else if (data.error_message) {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                        }
                        else {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    this.is_submit = false;
                    if (error && error.message) {
                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

                    }
                    //alert('Something went wrong!');
                }
            );

    }

    /**
     * @func {{addTask}}
     * @param \{{{formobject}}\} {{myform}} {{this will pass the form object}}
     * @description {{this will submit the form data and update the project data}}
     */
    addTask(myform) {
        this.is_submit = true;
        if (this.closerDate) this.taskModel.due_date = (this.closerDate.getTime()) / 1000;
        //if(this.dateTime) this.taskModel.duetime= this.dateTime;
        if (this.dateTime) this.taskModel.due_time = this.dateTime.toLocaleTimeString();


        //console.log(this.taskModel);
        let fd = new FormData();
        if (this.googoleAutoData.data) {
            fd.append('location', this.googoleAutoData.data.formatted_address);
            fd.append('lat', this.googoleAutoData.data.geometry.location.lat);
            fd.append('lng', this.googoleAutoData.data.geometry.location.lng);
        }

        fd.append('task_type', this.taskModel.task_type);
        fd.append('attachment', this.taskModel.attachment);
        fd.append('title', this.taskModel.title);
        fd.append('notes', this.taskModel.notes);
        fd.append('due_date', this.taskModel.due_date);
        fd.append('due_time', this.taskModel.due_time);
        fd.append('managers', this.selectedItems);
        fd.append('contacts', this.taskModel.contacts);
        fd.append('companies', this.taskModel.companies);
        fd.append('api_token', this.authUser.api_token);

        fd.append('curr_user', this.urlid);
        fd.append('tl_type', this.tlType);
        // console.log(this.taskModel);
        //this.noteModel.api_token = this.authUser.api_token;

        this.userService.addEditTask(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    this.is_submit = false;
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        this.msgs.push({ severity: 'success', summary: 'Success Message', detail: 'Task added successfully' });

                        //this.fetchUserTimeLine();
                        // reload page
                        this.pageLoad();
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0) {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                                }
                            }

                        }
                        else if (data.error_message) {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                        }
                        else {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                        }
                    }
                },
                error => {
                    this.is_submit = false;
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message) {
                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

                    }
                    //alert('Something went wrong!');
                }
            );

    }

    emailEdit(obj) {
        this.emailModel = obj;
        this.emailModel._id = obj.id;
    }
    /**
     * @func {{addEmail}}
     * @param \{{{formobject}}\} {{myform}} {{this will pass the form object}}
     * @description {{this will submit the form data and update the project data}}
     */
    addEmail(myform) {
        this.is_submit = true;
        this.emailModel.type = 1;
        let etyp = $("#emailtyp").val();
        if (etyp != '') this.emailModel.type = etyp;

        //console.log(this.emailModel);
        //alert(etyp);
        this.emailModel.curr_user = this.urlid;
        this.emailModel.tl_type = this.tlType;
        this.emailModel.api_token = this.authUser.api_token;

        this.userService.addEditUserEmail(this.emailModel)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    this.is_submit = false;
                    if (data.status === 'success') {
                        // message
                        this.is_submit = false;
                        this.msgs = [];
                        this.msgs.push({ severity: 'success', summary: 'Success Message', detail: 'Email successfully saved' });
                        this.emailModel = {};
                        this.fetchUserEmails();
                        this.fetchSingleContact();
                        // reload page
                        //this.pageLoad();
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0) {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                                }
                            }

                        }
                        else if (data.error_message) {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                        }
                        else {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                        }
                    }
                },
                error => {
                    this.is_submit = false;
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message) {
                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

                    }
                    //alert('Something went wrong!');
                }
            );

    }

    /**
     * @func {{delaEmail}}
     * @description {{delete user tag}}
     */
    delaEmail(Id) {
        //console.log(this.model);
        if (confirm("Are you sure to delete the email?")) {
            let fd = new FormData();

            fd.append('curr_user', this.urlid);
            fd.append('tl_type', this.tlType);
            fd.append('_ids', Id);
            fd.append('api_token', this.authUser.api_token);
            //return;
            this.userService.deleteUserEmail(fd)
                .subscribe(
                    (data: any) => {
                        //console.log(data);
                        if (data.status === 'success') {
                            // message
                            this.msgs = [];
                            this.msgs.push({ severity: 'success', summary: 'Success Message', detail: 'Email Successfully Deleted' });

                            // go to  my team page
                            this.fetchUserEmails();
                        }
                        if (data.status === 'fail') {
                            // message
                            this.msgs = [];
                            if (data.errors && Object.keys(data.errors).length > 0) {

                                for (var key in data.errors) {
                                    if (data.errors.hasOwnProperty(key)) {
                                        //console.log(key + " -> " + p[key]);
                                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                                    }
                                }

                            }
                            else if (data.error_message) {
                                this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                            }
                            else {
                                this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                            }
                        }
                    },
                    error => {
                        // console.log(error);
                        this.msgs = [];
                        if (error && error.message) {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

                        }
                        //alert('Something went wrong!');
                    }
                );

        }


    }

    /**
     * @func {{fetchOppType}}
     * @description {{get all Opportunity Types}}
     */

    fetchOppType() {
        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);

        this.userService.getOpportunityTypes(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        //this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Project Post Successful'});
                        this.oppTypeList = (data.feed);
                        //console.log(this.oppTypeList);
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0) {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                                }
                            }

                        }
                        else if (data.error_message) {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                        }
                        else {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message) {
                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

                    }
                    //alert('Something went wrong!');
                }
            );

    }

    /**
     * @func {{fetchTaskType}}
     * @description {{get all Task Types}}
     */

    fetchTaskType() {
        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);

        this.userService.getTaskTypes(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        //this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Project Post Successful'});
                        this.taskTypeList = (data.feed);
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0) {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                                }
                            }

                        }
                        else if (data.error_message) {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                        }
                        else {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message) {
                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

                    }
                    //alert('Something went wrong!');
                }
            );

    }

    /**
     * @func {{fetchStage}}
     * @description {{get all Stages}}
     */
    fetchStage() {
        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);

        this.userService.getOpportunityStages(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        //this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Project Post Successful'});
                        this.oppStageList = (data.feed);
                        //console.log(this.myTeam);
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0) {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                                }
                            }

                        }
                        else if (data.error_message) {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                        }
                        else {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message) {
                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

                    }
                    //alert('Something went wrong!');
                }
            );

    }

    changeProfitMagin($event) {
        //console.log($event.srcElement.value);
        let jid = $event.srcElement.value;
        this.profitMargin = this.oppStageList.find(item => item.id == jid).profit_margin;
        this.calculateProfit();
    }

    getBudget($event) {
        //console.log($event.srcElement.value);
        this.budgetVal = $event.srcElement.value;
        this.calculateProfit();
    }

    calculateProfit() {
        //console.log($event.srcElement.value);
        this.profitVal = (this.budgetVal * this.profitMargin) / 100;
    }

    autoCompleteCallback1(selectedData: any) {
        //do any necessery stuff.
        //console.log(selectedData);
        this.googoleAutoData = {};
        this.googoleAutoData = selectedData;
        // console.log(this.googoleAutoData.data.geometry.location);
        if (this.googoleAutoData.data) {
            let loc = this.googoleAutoData.data.geometry.location;
            this.addressModel.lat = loc.lat;
            this.addressModel.lng = loc.lng;

        }
    }

    changeTaskForm($event) {
        //console.log($event.srcElement.value);
        let jid = $event.srcElement.value;
        let selctDta = this.taskTypeList.find(item => item.id == jid);
        //console.log(selctDta);
        this.checkLocation = selctDta.show_location == 1 ? true : false;
        this.checkDate = selctDta.show_date == 1 ? true : false;
        this.checkTime = selctDta.show_time == 1 ? true : false;
    }

    getBase64(file) {
        //console.log(file);
        return new Promise(function (resolve, reject) {
            var reader = new FileReader();
            reader.onload = function () { resolve(reader.result); };
            reader.onerror = reject;

            //console.log(reader.readAsDataURL(file));
            reader.readAsDataURL(file);
        });
    }

    /**
     * @func {{createLabel}}
     * @param \{{{formobject}}\} {{myform}} {{this will pass the form object}}
     * @description {{this will submit the form data and update the project data}}
     */
    createLabel(myform, post_id) {

        //alert(etyp);

        this.labelModel.curr_user = this.urlid;
        this.labelModel.tl_type = this.tlType;
        this.labelModel.api_token = this.authUser.api_token;
        this.labelModel.post_id = post_id.toString();
        // console.log(this.labelModel);
        this.userService.addLabel(this.labelModel)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        this.mylabels = data.feed;
                        // message
                        this.msgs = [];
                        this.msgs.push({ severity: 'success', summary: 'Success Message', detail: 'Label successfully saved' });
                        $('.openlabelbx').hide();
                        $('.offforcreatelabel').show();
                        this.fetchUserTimeLine();

                        //this.fetchUserEmails();
                        // reload page
                        //this.pageLoad();
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0) {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                                }
                            }

                        }
                        else if (data.error_message) {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                        }
                        else {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message) {
                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

                    }
                    //alert('Something went wrong!');
                }
            );

    }

    getFilterData(filterId) {
        this.filter_id = filterId;
        this.fetchUserTimeLine();
    }

    addToLabel($event, post_id) {
        //console.log($event.srcElement.checked);
        //console.log($event.srcElement.value);
        if ($event.srcElement.value && post_id) {
            //alert($event.srcElement.value+'='+post_id);
            let fd = new FormData();
            fd.append('api_token', this.authUser.api_token);
            fd.append('post_id', post_id);
            fd.append('label_id', $event.srcElement.value);

            this.userService.setLabel(fd)
                .subscribe(
                    (data: any) => {
                        //console.log(data);
                        if (data.status === 'success') {
                            // message
                            this.msgs = [];
                            this.msgs.push({ severity: 'success', summary: 'Success Message', detail: 'Label successfully saved' });
                            this.fetchUserTimeLine();
                        }
                        if (data.status === 'fail') {
                            // message
                            this.msgs = [];
                            if (data.errors && Object.keys(data.errors).length > 0) {

                                for (var key in data.errors) {
                                    if (data.errors.hasOwnProperty(key)) {
                                        //console.log(key + " -> " + p[key]);
                                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                                    }
                                }

                            }
                            else if (data.error_message) {
                                this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                            }
                            else {
                                this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                            }
                        }
                    },
                    error => {
                        //console.log(error);
                        this.msgs = [];
                        if (error && error.message) {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

                        }
                        //alert('Something went wrong!');
                    }
                );
        }
    }

    toggleStatus($event, orginal_post_id, post_type) {
        // console.log($event.srcElement.checked);
        //console.log($event.srcElement.value);
        // console.log(orginal_post_id);
        // console.log(post_type);
        let vStatus: string = '1';
        if ($event.srcElement.checked == true) {
            vStatus = '2';
        }

        if (post_type && orginal_post_id) {
            //alert($event.srcElement.value+'='+post_id);
            let fd = new FormData();
            fd.append('api_token', this.authUser.api_token);
            fd.append('orginal_post_id', orginal_post_id);
            fd.append('post_type', post_type);
            fd.append('status', vStatus);
            this.userService.changePostScope(fd)
                .subscribe(
                    (data: any) => {
                        //console.log(data);
                        if (data.status === 'success') {
                            // message
                            this.msgs = [];
                            this.msgs.push({ severity: 'success', summary: 'Success Message', detail: 'Satus successfully saved' });
                            this.fetchUserTimeLine();
                        }
                        if (data.status === 'fail') {
                            // message
                            this.msgs = [];
                            if (data.errors && Object.keys(data.errors).length > 0) {

                                for (var key in data.errors) {
                                    if (data.errors.hasOwnProperty(key)) {
                                        //console.log(key + " -> " + p[key]);
                                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                                    }
                                }

                            }
                            else if (data.error_message) {
                                this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                            }
                            else {
                                this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                            }
                        }
                    },
                    error => {
                        //console.log(error);
                        this.msgs = [];
                        if (error && error.message) {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

                        }
                        //alert('Something went wrong!');
                    }
                );
        }
    }


    toggleStatusPrivate($event, orginal_post_id, post_type) {
        console.log($event.srcElement.checked);
        console.log($event.srcElement.value);
        // console.log(post_type);
        let vStatus: string = '2';
        if ($event.srcElement.checked == true) {
            vStatus = '2';
        }
        if (post_type && orginal_post_id) {
            let fd = new FormData();
            fd.append('api_token', this.authUser.api_token);
            fd.append('orginal_post_id', orginal_post_id);
            fd.append('post_type', post_type);
            fd.append('status', vStatus);
            this.userService.changePostScope(fd)
                .subscribe(
                    (data: any) => {
                        //console.log(data);
                        if (data.status === 'success') {
                            // message
                            this.msgs = [];
                            this.msgs.push({ severity: 'success', summary: 'Success Message', detail: 'Satus successfully saved' });
                            this.fetchUserTimeLine();
                        }
                        if (data.status === 'fail') {
                            // message
                            this.msgs = [];
                            if (data.errors && Object.keys(data.errors).length > 0) {

                                for (var key in data.errors) {
                                    if (data.errors.hasOwnProperty(key)) {
                                        //console.log(key + " -> " + p[key]);
                                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                                    }
                                }

                            }
                            else if (data.error_message) {
                                this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                            }
                            else {
                                this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                            }
                        }
                    },
                    error => {
                        //console.log(error);
                        this.msgs = [];
                        if (error && error.message) {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

                        }
                        //alert('Something went wrong!');
                    }
                );
        }
    }
    toggleStatusPublic($event, orginal_post_id, post_type) {
        console.log($event.srcElement.checked);
        console.log($event.srcElement.value);
        let vStatus: string = '1';
        if ($event.srcElement.checked == true) {
            vStatus = '1';
        }

        if (post_type && orginal_post_id) {
            let fd = new FormData();
            fd.append('api_token', this.authUser.api_token);
            fd.append('orginal_post_id', orginal_post_id);
            fd.append('post_type', post_type);
            fd.append('status', vStatus);
            this.userService.changePostScope(fd)
                .subscribe(
                    (data: any) => {
                        //console.log(data);
                        if (data.status === 'success') {
                            // message
                            this.msgs = [];
                            this.msgs.push({ severity: 'success', summary: 'Success Message', detail: 'Satus successfully saved' });
                            this.fetchUserTimeLine();
                        }
                        if (data.status === 'fail') {
                            // message
                            this.msgs = [];
                            if (data.errors && Object.keys(data.errors).length > 0) {

                                for (var key in data.errors) {
                                    if (data.errors.hasOwnProperty(key)) {
                                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                                    }
                                }

                            }
                            else if (data.error_message) {
                                this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                            }
                            else {
                                this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                            }
                        }
                    },
                    error => {
                        this.msgs = [];
                        if (error && error.message) {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

                        }
                    }
                );
        }
    }
    /**
     * @func {{searchPost}}
     * @param \{{{formobject}}\} {{myform}} {{this will pass the form object}}
     * @description {{this will submit the form data and update the project data}}
     */
    searchPost(myform) {
        if (this.startDate) this.searchModel.start_date = (this.startDate.getTime()) / 1000;
        if (this.endDate) this.searchModel.end_date = ((this.endDate.getTime()) / 1000) + 86400;
        //console.log(this.searchModel);
        this.fetchUserTimeLine();
    }

    /**
     * @func {{fetchUserTimeLine}}
     * @description {{get user User Time Line}}
     */

    fetchUserTimeLine() {
        //alert(this.listTab);
        this.postCommentFile = [];
        let sts = 1;
        if (this.archive_status == true) sts = 0;
        this.commonService.startLoader(this.listTab);
        this.myList = [];
        this.filterList = [];
        //this.allPostList =[];
        let that = this;
        let fd = new FormData();
        fd.append('archive_status', sts.toString());
        fd.append('filtr_id', this.filter_id);
        fd.append('list_tab', this.listTab);
        fd.append('api_token', this.authUser.api_token);

        fd.append('keyword', this.searchModel.keyword);
        fd.append('start_date', this.searchModel.start_date);
        fd.append('end_date', this.searchModel.end_date);
        fd.append('page', this.page.toString());
        fd.append('limit', this.limit.toString());

        fd.append('curr_user', this.urlid);
        fd.append('tl_type', this.tlType);
        fd.append('project_filter', this.projFilter);

        if (this.noteTypeFilter != "") {
            fd.append('note_type', this.noteTypeFilter);
        }
        if (this.taskTypeFilter != "") {
            fd.append('task_type', this.taskTypeFilter);
        }

        this.userService.getUserTimeLine(fd)
            .subscribe(
                (data: any) => {
                    //console.log(ta);
                    if (data.status === 'success') {
                        //data = JSON.parse(data);
                        // message

                        this.isBottom = false;
                        this.msgs = [];
                        this.noteList = data.feed.notes;
                        this.taskList = data.feed.tasks;
                        this.emailtlList = data.feed.emails;
                        this.opportunityPostList = data.feed.opportunitys;
                        that.allPostList = data.feed.allPosts;
                        this.mylabels = data.feed.labels;
                        this.activityList = data.feed.activities;
                        this.chatList = data.feed.chats;
                        if (data.feed.mails)
                            this.mailList = data.feed.mails;

                        if (data.feed.labelData.length > 0)
                            this.filterList = data.feed.labelData;
                        //console.log($('.tabmenuwrap ul li.active'));
                        $('.tabmenuwrap ul li.active').trigger("click");



                        /*alert( this.allPostList.length);*/

                        /*this.noteList = data.feed.notes;*/
                        /*this.opportunityList = data.oppList;
                        this.opportunityList.forEach((value: any) => {
                            if(value.id)   this.opportunityDd.push({'id':value.id ,'itemName': value.title });
                        });*/
                        //console.log(this.opportunityDd);
                    }
                    if (data.status === 'fail') {
                        // message

                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0) {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                                }
                            }

                        }
                        else if (data.error_message) {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                        }
                        else {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                        }
                    }

                    setTimeout(() => {
                        this.commonService.stopLoader(this.listTab);
                    }, 1000);

                },
                error => {
                    setTimeout(() => {
                        this.commonService.stopLoader(this.listTab);
                    }, 1000);
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message) {
                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });
                    }
                    //alert('Something went wrong!');
                }
            );

    }

    selctNotifyUsr($event) {
        /*console.log($event.srcElement.value);
        console.log($event.srcElement.checked);
        console.log(type_post_id);*/


        if ($event.srcElement.checked == true) {
            if (this.userToNotify.indexOf($event.srcElement.value) == -1)
                this.userToNotify.push($event.srcElement.value);
        } else {

            this.userToNotify.splice(this.userToNotify.indexOf($event.srcElement.value), 1);
        }

        console.log(this.userToNotify);
    }

    openReminder($event, post_id) {
        if (this.selctedPost != post_id) {
            this.selctedPost = post_id;
            this.userToNotify = [];
            this.userToNotify.push(this.authUser.id.toString());
        }
        this.userToNotify = [];
        this.userToNotify.push(this.authUser.id.toString());
        this.reminderModel = {
            upto_val: '1',
            upto_dur: 'w',
            repeat_val: '1',
        };
    }
    /**
     * @func {{saveReminder}}
     * @param \{{{formobject}}\} {{myform}} {{this will pass the form object}}
     * @description {{this will submit the form data and update the project data}}
     */
    saveReminder(myform) {
        this.reminderModel.notify_user = this.userToNotify;
        this.reminderModel.post_id = this.selctedPost;
        this.reminderModel.api_token = this.authUser.api_token;
        console.log(this.reminderModel);
        this.userService.setReminder(this.reminderModel)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        this.msgs.push({ severity: 'success', summary: 'Success Message', detail: 'Set Reminder Successful' });
                        // console.log(data.feed);
                        this.fetchUserTimeLine();
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0) {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                                }
                            }

                        }
                        else if (data.error_message) {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                        }
                        else {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message) {
                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

                    }
                    //alert('Something went wrong!');
                }
            );
    }

    setDefltPhnCd() {
        $(this).attr('data-dial-code', 1);
        $(this).attr('data-country-code', 'us');
    }
    /**
     * @func {{addPhNumbr}}
     * @param \{{{formobject}}\} {{myform}} {{this will pass the form object}}
     * @description {{this will submit the form data and update the project data}}
     */
    addPhNumbr(myform) {
        this.is_submit = true;

        this.userPhoneModel.type = 1;
        let etyp = $("#phntyp").val();
        if (etyp != '') {
            this.userPhoneModel.type = etyp;

        }
        if (!this.userPhoneModel.country_code) this.userPhoneModel.country_code = 'us';
        if (!this.userPhoneModel.dial_code) this.userPhoneModel.dial_code = 1;

        // alert(this.userPhoneModel.country_code);
        this.userPhoneModel.api_token = this.authUser.api_token;

        this.userPhoneModel.curr_user = this.urlid;
        this.userPhoneModel.tl_type = this.tlType;
        // console.log(this.userPhoneModel);


        this.userService.saveUserPhNumber(this.userPhoneModel)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    $("#phone").intlTelInput("setCountry", 'us');
                    this.is_submit = false;
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        this.msgs.push({ severity: 'success', summary: 'Success Message', detail: 'Phone number successfully saved' });
                        this.userPhoneModel = {
                            // dial_code : '1',
                            type: 1,
                            phone: ''
                        };
                        // this.setDefltPhnCd();
                        this.fetchUserPhNumbers();
                        this.fetchSingleContact();

                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0) {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                                }
                            }

                        }
                        else if (data.error_message) {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                        }
                        else {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                        }
                    }
                },
                error => {
                    this.is_submit = false;

                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message) {
                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

                    }
                    //alert('Something went wrong!');
                }
            );

    }

    /**
     * @func {{fetchUserPhNumbers}}
     * @description {{get all Stages}}
     */
    fetchUserPhNumbers() {
        this.totalShowCount = 0;
        let fd = new FormData();

        fd.append('curr_user', this.urlid);
        fd.append('tl_type', this.tlType);
        fd.append('api_token', this.authUser.api_token);
        fd.append('limit', this.popListLimit.toString());

        this.userService.getUserPhNumbers(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        //this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Project Post Successful'});
                        this.phnList = [];

                        this.phnList = (data.feed);
                        this.totalShowCount = (data.total_count);
                        // alert(this.phnList.length + this.totalShowCount )
                        //console.log(this.phnList);
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0) {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                                }
                            }

                        }
                        else if (data.error_message) {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                        }
                        else {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message) {
                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

                    }
                    //alert('Something went wrong!');
                }
            );

    }

    onShowmore(lstyp) {
        this.popListLimit = this.popListLimit + 2;
        switch (lstyp) {
            case "email":
                this.fetchUserEmails();
                break;
            case "phn":
                this.fetchUserPhNumbers();
                break;
            case "addrss":
                this.fetchUserAddress();
                break;
            case "social":
                this.fetchSocials();
                break;
        }

    }

    phnEdit(obj) {
        // console.log(obj);
        this.userPhoneModel = obj;
        this.userPhoneModel._id = obj.id;
        //console.log(this.userPhoneModel);
        //alert(obj.phone_number);
        $("#phone").val(obj.phone_number);
        $("#phone").intlTelInput("setCountry", obj.country_code);
    }

    /**
     * @func {{delPhn}}
     * @description {{delete user tag}}
     */
    delPhn(Id) {
        //console.log(this.model);
        if (confirm("Are you sure to delete the phone?")) {
            let fd = new FormData();

            fd.append('curr_user', this.urlid);
            fd.append('tl_type', this.tlType);
            fd.append('_ids', Id);
            fd.append('api_token', this.authUser.api_token);
            //return;
            this.userService.deleteUserPhNumber(fd)
                .subscribe(
                    (data: any) => {
                        //console.log(data);
                        if (data.status === 'success') {
                            // message
                            this.msgs = [];
                            this.msgs.push({ severity: 'success', summary: 'Success Message', detail: 'Phone Successfully Deleted' });

                            // go to  my team page
                            this.fetchUserPhNumbers();
                        }
                        if (data.status === 'fail') {
                            // message
                            this.msgs = [];
                            if (data.errors && Object.keys(data.errors).length > 0) {

                                for (var key in data.errors) {
                                    if (data.errors.hasOwnProperty(key)) {
                                        //console.log(key + " -> " + p[key]);
                                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                                    }
                                }

                            }
                            else if (data.error_message) {
                                this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                            }
                            else {
                                this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                            }
                        }
                    },
                    error => {
                        // console.log(error);
                        this.msgs = [];
                        if (error && error.message) {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

                        }
                        //alert('Something went wrong!');
                    }
                );

        }


    }

    keyDownFunction(event, f, typ, pid) {
        // console.log(event.keyCode);
        if (event.keyCode == 13) {
            // alert('you just clicked enter');
            // rest of your code
            if (this.is_submit == false) this.createComment(f, typ, pid);
            //console.log(f);
        }
    }

    setComment(event) {
        // console.log(event.srcElement.value);
        this.postCommentModel.content = event.srcElement.value;
    }
    /**
     * @func {{createComment}}
     * @param \{{{formobject}}\} {{myform}} {{this will pass the form object}}
     * @description {{this will submit the form data and update the project data}}
     */
    createComment(myform, typ, pid) {
        this.is_submit = true;
        //console.log(myform);
        this.postCommentModel.api_token = this.authUser.api_token;
        this.postCommentModel.type = typ;
        this.postCommentModel.post_id = pid;
        // console.log(this.postCommentModel);

        let fd = new FormData();
        if (this.postCommentModel.attachment) fd.append('attachment', this.postCommentModel.attachment);
        fd.append('type', this.postCommentModel.type);
        fd.append('post_id', this.postCommentModel.post_id);
        fd.append('content', this.postCommentModel.content);
        fd.append('api_token', this.postCommentModel.api_token);
        fd.append('tl_type', this.tlType);

        this.userService.savePostComment(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        this.msgs.push({ severity: 'success', summary: 'Success Message', detail: 'Comment successful' });
                        this.postCommentModel = {};
                        this.fetchUserTimeLine();
                        this.is_submit = false;
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0) {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                                }
                            }

                        }
                        else if (data.error_message) {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                        }
                        else {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                        }
                        this.is_submit = false;
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message) {
                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

                    }
                    this.is_submit = false;
                    //alert('Something went wrong!');
                }
            );

    }

    /**
     * @func {{fetchUser}}
     * @param \{{{formobject}}\} {{myform}} {{this will pass the form object}}
     * @description {{this will submit the form data and update the project data}}
     */
    fetchUser() {
        let fd = new FormData();
        fd.append('user_id', this.urlid);
        fd.append('api_token', this.authUser.api_token);

        this.userService.getUser(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        //this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Comment successful'});
                        this.timeLimeUser = data.user;
                        /*setTimeout(()=>{
                            this.initMap();
                        },3000);*/
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0) {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                                }
                            }

                        }
                        else if (data.error_message) {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                        }
                        else {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message) {
                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

                    }
                    //alert('Something went wrong!');
                }
            );

    }

    /**
     * @func {{addUserAddress}}
     * @param \{{{formobject}}\} {{myform}} {{this will pass the form object}}
     * @description {{this will submit the form data and update the project data}}
     */
    addUserAddress(myform) {
        this.is_submit = true;
        if (this.googoleAutoData.data) {
            this.addressModel.address = this.googoleAutoData.data.formatted_address;
            /*data.geometry.location.lat;
            data.geometry.location.lng;*/
        }

        this.addressModel.curr_user = this.urlid;
        this.addressModel.tl_type = this.tlType;
        this.addressModel.api_token = this.authUser.api_token;
        //console.log(this.addressModel);

        this.userService.saveUserAddr(this.addressModel)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        this.msgs.push({ severity: 'success', summary: 'Success Message', detail: 'Address successfully saved' });
                        this.addressModel = {};
                        this.googoleAutoData = {};
                        this.fetchUserAddress();
                        this.fetchSingleContact();
                        this.is_submit = false;

                    }
                    if (data.status === 'fail') {
                        // message
                        this.is_submit = false;
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0) {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                                }
                            }

                        }
                        else if (data.error_message) {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                        }
                        else {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    this.is_submit = false;
                    if (error && error.message) {
                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

                    }
                    //alert('Something went wrong!');
                }
            );

    }

    /**
     * @func {{fetchUserAddress}}
     * @description {{get all Stages}}
     */
    fetchUserAddress() {
        this.totalShowCount = 0;
        let fd = new FormData();
        fd.append('curr_user', this.urlid);
        fd.append('tl_type', this.tlType);
        fd.append('api_token', this.authUser.api_token);
        fd.append('limit', this.popListLimit.toString());

        this.userService.getUserAddresses(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];

                        this.addressList = [];
                        this.addressList = (data.feed);
                        this.totalShowCount = (data.total_count);
                        //console.log(this.phnList);
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0) {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                                }
                            }

                        }
                        else if (data.error_message) {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                        }
                        else {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message) {
                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

                    }
                    //alert('Something went wrong!');
                }
            );

    }

    /**
     * @func {{delUserAddress}}
     * @description {{delete user tag}}
     */
    delUserAddress(Id) {
        //console.log(this.model);
        if (confirm("Are you sure to delete the address?")) {
            let fd = new FormData();

            fd.append('curr_user', this.urlid);
            fd.append('tl_type', this.tlType);
            fd.append('_ids', Id);
            fd.append('api_token', this.authUser.api_token);
            //return;
            this.userService.deleteUserAddrs(fd)
                .subscribe(
                    (data: any) => {
                        //console.log(data);
                        if (data.status === 'success') {
                            // message
                            this.msgs = [];
                            this.msgs.push({ severity: 'success', summary: 'Success Message', detail: 'Phone Successfully Deleted' });

                            // go to  my team page
                            this.fetchUserAddress();
                        }
                        if (data.status === 'fail') {
                            // message
                            this.msgs = [];
                            if (data.errors && Object.keys(data.errors).length > 0) {

                                for (var key in data.errors) {
                                    if (data.errors.hasOwnProperty(key)) {
                                        //console.log(key + " -> " + p[key]);
                                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                                    }
                                }

                            }
                            else if (data.error_message) {
                                this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                            }
                            else {
                                this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                            }
                        }
                    },
                    error => {
                        // console.log(error);
                        this.msgs = [];
                        if (error && error.message) {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

                        }
                        //alert('Something went wrong!');
                    }
                );

        }
    }

    /**
     * @func {{addUserGroup}}
     * @param \{{{formobject}}\} {{myform}} {{this will pass the form object}}
     * @description {{this will submit the form data and update the project data}}
     */
    addUserGroup(myform) {

        this.is_submit = true;

        this.groupModel.api_token = this.authUser.api_token;
        this.groupModel.attach_contact = this.urlid;
        // console.log(this.groupModel);

        this.userService.saveUserGroup(this.groupModel)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    this.is_submit = false;

                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        this.msgs.push({ severity: 'success', summary: 'Success Message', detail: 'Group successfully saved' });
                        this.groupModel = {};
                        this.is_submit = true;
                        this.fetchSingleContact();
                        this.fetchUserGroups();
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0) {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                                }
                            }

                        }
                        else if (data.error_message) {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                        }
                        else {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.is_submit = false;

                    this.msgs = [];
                    if (error && error.message) {
                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

                    }
                    //alert('Something went wrong!');
                }
            );

    }

    /**
     * @func {{fetchSingleContact}}
     * @description {{get single contact}}
     */
    fetchSingleContact() {
        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);
        fd.append('contact_id', this.urlid);
        fd.append('type', this.tlType);

        this.userService.getSingleContact(fd).subscribe(
            (data: any) => {
                // console.log(data);
                if (data.status === 'success') {
                    // message
                    this.msgs = [];

                    this.contactData = [];
                    // this.selectedManagers = [];
                    /*if(this.tlType =='visitor'){
                    }
                    else this.contactData =  (data.feed.visitor);*/
                    this.contactData = (data.feed);

                    if (data.feed.visitor && data.feed.visitor.tagscores) {
                        data.feed.visitor.tagscores.forEach((item: any) => {
                            let tgscore = 0;
                            if (item.status == 1) tgscore = parseInt(item.score);

                            if (this.visit_tags.indexOf(item.tag.id) == -1) {
                                this.visit_tags.push(item.tag.id);
                                let tagObj = { tagId: item.tag.id, tagName: item.tag.title, tagScore: tgscore };
                                // this.comresdata.find(item => item.name == value).id
                                this.visit_tagscores.push(tagObj);
                            } else {
                                let pscr = this.visit_tagscores.find(objitem => objitem.tagId == item.tag.id).tagScore;
                                this.visit_tagscores.find(objitem => objitem.tagId == item.tag.id).tagScore = parseInt(pscr) + tgscore;
                            }

                        });
                    }
                    if (data.feed.visitor && data.feed.visitor.activities) {
                        // console.log(data.feed.visitor.activities);
                        data.feed.visitor.activities.forEach((value: any) => {
                            this.totalVisitCount += parseInt(value.visit_count);
                        });
                    }
                    if (data.feed.company) this.selctComContacts = data.feed.company;
                    if (data.feed.managers) {
                        this.assignManagerModel.managers = [];
                        data.feed.managers.forEach((value: any) => {
                            this.assignManagerModel.managers.push({ 'id': value.id, 'itemName': value.name });
                            // this.selectedManagers.push(value.id.toString());
                        });
                    }
                    if (data.feed.groups) {
                        this.assigned_groups = [];
                        data.feed.groups.forEach((value: any) => {
                            this.assigned_groups.push(value.id);
                            // this.selectedManagers.push(value.id.toString());
                        });
                    }
                    if (data.in_project && data.in_project.map_setting != 'null') {
                        this.map_setting = data.in_project.map_setting;
                    }
                    // console.log(this.selectedManagers);
                    // alert(this.totalVisitCount);
                }
                if (data.status === 'fail') {
                    // message
                    this.msgs = [];
                    if (data.errors && Object.keys(data.errors).length > 0) {

                        for (var key in data.errors) {
                            if (data.errors.hasOwnProperty(key)) {
                                //console.log(key + " -> " + p[key]);
                                this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                            }
                        }

                    }
                    else if (data.error_message) {
                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                    }
                    else {
                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                    }
                }
            },
            error => {
                //console.log(error);
                this.msgs = [];
                if (error && error.message) {
                    this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

                }
                //alert('Something went wrong!');
            }
        );

    }

    directionMap() {
        /*var origin = {lat: parseInt(this.authUser.lat), lng: parseInt(this.authUser.lon)};
        var destination = {lat: parseInt(this.timeLimeUser.lat), lng: parseInt(this.timeLimeUser.lon)};*/
        var directionsDisplay = new google.maps.DirectionsRenderer;
        var origin = { lat: 22.5726, lng: 88.3639 };
        var destination = { lat: 22.5686855, lng: 88.4316043 };

        var map = new google.maps.Map(document.getElementById('map'), {
            // center: origin,
            zoom: 12
        });
        directionsDisplay.setMap(map);
        /*var directionsDisplay = new google.maps.DirectionsRenderer({
            map: map
        });*/

        // Set destination, origin and travel mode.
        var request = {
            destination: destination,
            origin: origin,
            travelMode: 'DRIVING',

            optimizeWaypoints: true,
            //provideRouteAlternatives: true,
            avoidFerries: true,
            avoidHighways: true,
            avoidTolls: true,
        };
        //console.log(origin);
        //console.log(destination);

        // Pass the directions request to the directions service.
        var directionsService = new google.maps.DirectionsService();
        directionsService.route(request, function (response, status) {
            if (status == 'OK') {
                console.log(response);
                // Display the route on the map.
                // directionsDisplay.setDirections(response);
            }
        });

        // console.log(allCountries);
    }

    changeComAssign(event) {
        // console.log(event.srcElement.checked);
        if (!event.srcElement.checked) {
            if (confirm("Are you sure to unassign this contact?")) {
                console.log(event.srcElement.value);
                let fd = new FormData();
                fd.append('api_token', this.authUser.api_token);
                fd.append('contact_id', event.srcElement.value);
                fd.append('company_id', this.urlid);

                this.userService.unAssignContact(fd).subscribe(
                    (data: any) => {
                        console.log(data);
                        if (data.status === 'success') {
                            // message
                            this.msgs = [];
                            this.msgs.push({ severity: 'success', summary: 'Success Message', detail: 'Contact successfully unassigned' });
                            //console.log(this.phnList);
                        }
                        if (data.status === 'fail') {
                            // message
                            this.msgs = [];
                            if (data.errors && Object.keys(data.errors).length > 0) {

                                for (var key in data.errors) {
                                    if (data.errors.hasOwnProperty(key)) {
                                        //console.log(key + " -> " + p[key]);
                                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                                    }
                                }

                            }
                            else if (data.error_message) {
                                this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                            }
                            else {
                                this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                            }
                        }
                    },
                    error => {
                        //console.log(error);
                        this.msgs = [];
                        if (error && error.message) {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

                        }
                        //alert('Something went wrong!');
                    }
                );
            } else {
                event.srcElement.checked == 'checked';
            }
            this.contactData = [];
            this.fetchSingleContact();
        }
    }

    setSocialTyp(typ) {
        this.socialModel.type = typ;
    }

    editSocial(obj) {
        this.socialModel = obj;
        this.socialModel._id = obj.id;
    }

    /**
     * @func {{addSocial}}
     * @param \{{{formobject}}\} {{myform}} {{this will pass the form object}}
     * @description {{this will submit the form data and update the project data}}
     */
    addSocial(myform) {
        this.is_submit = true;

        /*let etyp = $("#phntyp").val();
        if(etyp !='')   this.userPhoneModel.type = etyp;*/

        this.socialModel.api_token = this.authUser.api_token;

        this.socialModel.contact_id = this.urlid;
        this.socialModel.tl_type = this.tlType;
        // console.log(this.userPhoneModel);

        this.userService.addSaveSocial(this.socialModel)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    this.is_submit = false;
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        this.msgs.push({ severity: 'success', summary: 'Success Message', detail: 'Social successfully saved' });
                        this.socialModel = {
                            type: 1,
                        };

                        this.fetchSocials();
                        // this.fetchSingleContact();

                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0) {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                                }
                            }

                        }
                        else if (data.error_message) {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                        }
                        else {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                        }
                    }
                },
                error => {
                    this.is_submit = false;

                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message) {
                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

                    }
                    //alert('Something went wrong!');
                }
            );

    }

    /**
     * @func {{fetchSocials}}
     * @description {{get all Stages}}
     */
    fetchSocials() {
        this.totalShowCount = 0;
        let fd = new FormData();
        fd.append('curr_user', this.urlid);
        fd.append('tl_type', this.tlType);
        fd.append('api_token', this.authUser.api_token);
        fd.append('limit', this.popListLimit.toString());

        this.userService.getSocials(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];

                        this.socailList = [];
                        this.socailList = (data.feed);
                        // alert(this.socailList.length);
                        this.totalShowCount = (data.total_count);
                        //console.log(this.phnList);
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0) {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                                }
                            }

                        }
                        else if (data.error_message) {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                        }
                        else {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message) {
                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

                    }
                    //alert('Something went wrong!');
                }
            );
    }

    /**
     * @func {{deltSocial}}
     * @description {{delete user tag}}
     */
    deltSocial(Id) {
        //console.log(this.model);
        if (confirm("Are you sure to delete the social?")) {
            let fd = new FormData();

            fd.append('curr_user', this.urlid);
            fd.append('tl_type', this.tlType);
            fd.append('_ids', Id);
            fd.append('api_token', this.authUser.api_token);
            //return;
            this.userService.delSocials(fd)
                .subscribe(
                    (data: any) => {
                        //console.log(data);
                        if (data.status === 'success') {
                            // message
                            this.msgs = [];
                            this.msgs.push({ severity: 'success', summary: 'Success Message', detail: 'Social Successfully Deleted' });

                            // go to  my team page
                            this.fetchSocials();
                        }
                        if (data.status === 'fail') {
                            // message
                            this.msgs = [];
                            if (data.errors && Object.keys(data.errors).length > 0) {

                                for (var key in data.errors) {
                                    if (data.errors.hasOwnProperty(key)) {
                                        //console.log(key + " -> " + p[key]);
                                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                                    }
                                }

                            }
                            else if (data.error_message) {
                                this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                            }
                            else {
                                this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                            }
                        }
                    },
                    error => {
                        // console.log(error);
                        this.msgs = [];
                        if (error && error.message) {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

                        }
                        //alert('Something went wrong!');
                    }
                );

        }
    }

    unassignCompany(contactId) {
        if (confirm("Are you sure to unassign the contact?")) {
            let fd = new FormData();
            fd.append('contact_id', contactId);
            fd.append('api_token', this.authUser.api_token);
            //return;
            this.userService.companyUnassigned(fd)
                .subscribe(
                    (data: any) => {
                        //console.log(data);
                        if (data.status === 'success') {
                            // message
                            this.msgs = [];
                            this.msgs.push({ severity: 'success', summary: 'Success Message', detail: 'contact are successfully deleted' });
                            this.fetchSingleContact();
                            // this.listCompanyContacts();
                        }
                        if (data.status === 'fail') {
                            // message
                            this.msgs = [];
                            if (data.errors && Object.keys(data.errors).length > 0) {

                                for (var key in data.errors) {
                                    if (data.errors.hasOwnProperty(key)) {
                                        //console.log(key + " -> " + p[key]);
                                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                                    }
                                }

                            }
                            else if (data.error_message) {
                                this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                            }
                            else {
                                this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                            }
                        }
                    },
                    error => {
                        console.log(error);
                        this.msgs = [];
                        if (error && error.message) {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

                        }
                        //alert('Something went wrong!');
                    }
                );

        }
    }

    fetchUserGroups() {
        let fd = new FormData();
        fd.append('gtype', 'UG');
        fd.append('api_token', this.authUser.api_token);
        let group2 = [];
        this.userService.fetchGroups(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        //this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Project Post Successful'});
                        this.groupList = (data.feed);
                        /*data.feed.forEach((value: any) => {
                            this.myGroups.push({'id':value.id ,'itemName': value.title });
                            this.groupList.push({'value':value.id ,'label': value.title });
                        });*/
                        //alert(this.myGroups.length);

                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0) {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                                }
                            }

                        }
                        else if (data.error_message) {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                        }
                        else {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message) {
                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

                    }
                    //alert('Something went wrong!');
                }
            );

    }
    /**
     * @func {{delGroup}}
     * @description {{delete tax}}
     */
    delGroup(_id) {
        //console.log(this.model);
        if (confirm("Are you sure to delete the group?")) {
            let fd = new FormData();
            fd.append('_ids', _id);
            fd.append('api_token', this.authUser.api_token);
            //return;
            this.userService.deleteGroups(fd)
                .subscribe(
                    (data: any) => {
                        //console.log(data);
                        if (data.status === 'success') {
                            // message
                            this.msgs = [];
                            this.msgs.push({ severity: 'success', summary: 'Success Message', detail: 'Group Successfully Deleted' });

                            // go to  my team page
                            this.fetchUserGroups();
                        }
                        if (data.status === 'fail') {
                            // message
                            this.msgs = [];
                            if (data.errors && Object.keys(data.errors).length > 0) {

                                for (var key in data.errors) {
                                    if (data.errors.hasOwnProperty(key)) {
                                        //console.log(key + " -> " + p[key]);
                                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                                    }
                                }

                            }
                            else if (data.error_message) {
                                this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                            }
                            else {
                                this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                            }
                        }
                    },
                    error => {
                        console.log(error);
                        this.msgs = [];
                        if (error && error.message) {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

                        }
                        //alert('Something went wrong!');
                    }
                );

        }


    }

    deManagerContact(Id) {
        if (confirm("Are you sure to unassign the manager?")) {
            let fd = new FormData();
            fd.append('contact_id', this.urlid);
            fd.append('manager_id', Id);
            fd.append('api_token', this.authUser.api_token);
            //return;
            this.userService.unassignManagerContact(fd)
                .subscribe(
                    (data: any) => {
                        //console.log(data);
                        if (data.status === 'success') {
                            // message
                            this.msgs = [];
                            this.msgs.push({ severity: 'success', summary: 'Success Message', detail: 'manager successfully unassigned' });
                            this.fetchSingleContact();
                            // this.listCompanyContacts();
                        }
                        if (data.status === 'fail') {
                            // message
                            this.msgs = [];
                            if (data.errors && Object.keys(data.errors).length > 0) {

                                for (var key in data.errors) {
                                    if (data.errors.hasOwnProperty(key)) {
                                        //console.log(key + " -> " + p[key]);
                                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                                    }
                                }

                            }
                            else if (data.error_message) {
                                this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                            }
                            else {
                                this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                            }
                        }
                    },
                    error => {
                        console.log(error);
                        this.msgs = [];
                        if (error && error.message) {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

                        }
                        //alert('Something went wrong!');
                    }
                );

        }
    }

    /**
     * @func {{unAssignGrp}}
     * @param \{{{formobject}}\} {{myform}} {{this will pass the form object}}
     * @description {{this will submit the form data and update the project data}}
     */
    toggleAssignGrp(event, grpId) {
        console.log(event.srcElement.checked);
        let toDo = 'assign';
        if (event.srcElement.checked == false) toDo = 'un assign';
        if (confirm("Are you sure to " + toDo + " the group?")) {
            let fd = new FormData();

            fd.append('api_token', this.authUser.api_token);

            fd.append('groups', grpId);
            fd.append('contacts', this.urlid);
            fd.append('type', "toggle");

            this.userService.assignGroups(fd)
                .subscribe(
                    (data: any) => {
                        //console.log(data);
                        if (data.status === 'success') {
                            // message
                            this.msgs = [];
                            this.msgs.push({
                                severity: 'success',
                                summary: 'Success Message',
                                detail: 'Group ' + toDo
                            });
                            this.fetchSingleContact();
                            // this.fetchUserGroups();
                            // reload page
                            // this.pageLoad();
                        }
                        if (data.status === 'fail') {
                            // message
                            this.msgs = [];
                            if (data.errors && Object.keys(data.errors).length > 0) {

                                for (var key in data.errors) {
                                    if (data.errors.hasOwnProperty(key)) {
                                        //console.log(key + " -> " + p[key]);
                                        this.msgs.push({
                                            severity: 'error',
                                            summary: 'Error Message',
                                            detail: data.errors[key]
                                        });
                                    }
                                }

                            }
                            else if (data.error_message) {
                                this.msgs.push({
                                    severity: 'error',
                                    summary: 'Error Message',
                                    detail: data.error_message
                                });
                            }
                            else {
                                this.msgs.push({
                                    severity: 'error',
                                    summary: 'Error Message',
                                    detail: "Undefined Error"
                                });
                            }
                        }
                    },
                    error => {
                        //console.log(error);
                        this.msgs = [];
                        if (error && error.message) {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });
                        }
                        //alert('Something went wrong!');
                    }
                );
        }

    }

    toggleArchive($event, post_id) {
        let vStatus: string = '1';
        if ($event.srcElement.checked == true) {
            vStatus = '0';
        }

        if (post_id) {
            let fd = new FormData();
            fd.append('api_token', this.authUser.api_token);
            fd.append('post_id', post_id);
            fd.append('status', vStatus);
            this.userService.postArchiveToggel(fd)
                .subscribe(
                    (data: any) => {
                        //console.log(data);
                        if (data.status === 'success') {
                            // message
                            this.msgs = [];
                            this.msgs.push({ severity: 'success', summary: 'Success Message', detail: 'Archive status changed' });
                            this.fetchUserTimeLine();
                        }
                        if (data.status === 'fail') {
                            // message
                            this.msgs = [];
                            if (data.errors && Object.keys(data.errors).length > 0) {

                                for (var key in data.errors) {
                                    if (data.errors.hasOwnProperty(key)) {
                                        //console.log(key + " -> " + p[key]);
                                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                                    }
                                }

                            }
                            else if (data.error_message) {
                                this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                            }
                            else {
                                this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                            }
                        }
                    },
                    error => {
                        //console.log(error);
                        this.msgs = [];
                        if (error && error.message) {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

                        }
                        //alert('Something went wrong!');
                    }
                );
        }
    }

    showToggelArchive(status) {
        //console.log($event.srcElement.checked);
        //console.log($event.srcElement.value);
        // if ($event.srcElement.checked) this.archive_status = true;
        // else this.archive_status = false;

        this.archive_status = status;
        this.fetchUserTimeLine();
        console.log(this.archive_status);
    }

    /**
     * @func {{fetchUserDashboard}}
     * @description {{get user dashboard}}
     */
    fetchUserDashboard() {
        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);
        fd.append('type', this.tlType);

        this.userService.fetchDashboardUser(fd).subscribe(
            (data: any) => {
                // console.log(data);
                if (data.status === 'success') {
                    // message
                    this.msgs = [];
                    this.dash_visitors = data.visitors;
                    this.post_count = data.post_count;

                }
                if (data.status === 'fail') {
                    // message
                    this.msgs = [];
                    if (data.errors && Object.keys(data.errors).length > 0) {

                        for (var key in data.errors) {
                            if (data.errors.hasOwnProperty(key)) {
                                //console.log(key + " -> " + p[key]);
                                this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                            }
                        }

                    }
                    else if (data.error_message) {
                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                    }
                    else {
                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                    }
                }
            },
            error => {
                //console.log(error);
                this.msgs = [];
                if (error && error.message) {
                    this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

                }
                //alert('Something went wrong!');
            }
        );

    }

    getApprovePermission(note_type) {
        let mngrs = []; let has_permission = 0;
        // console.log(this.authUser.id);
        if (this.noteTypeList != null && note_type != '') {
            if (this.noteTypeList.find(item => item.id == note_type)) {
                mngrs = this.noteTypeList.find(item => item.id == note_type).managers;
                if (mngrs.length > 0) {
                    mngrs.forEach((value: any) => {
                        // console.log(value.id);
                        if (value.id == this.authUser.id) {
                            has_permission = 1;
                        }
                        // this.allUsers.push({'id':value.id ,'itemName': value.name });
                    });
                }
            }
        }
        // console.log(mngrs);
        return has_permission;
    }

    /**
     * @func {{approveNote}}
     * @param \{{{formobject}}\} {{myform}} {{this will pass the form object}}
     * @description {{this will submit the form data and update the project data}}
     */
    approveNote(myform, noteId) {
        this.approveModel.api_token = this.authUser.api_token;
        this.approveModel.note_id = noteId;

        // console.log(this.searchMapModel);
        this.userService.noteApproval(this.approveModel)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        this.msgs.push({ severity: 'success', summary: 'Success Message', detail: 'Note successfully approved' });
                        this.approveModel = {}
                        this.fetchUserTimeLine();
                        // reload page
                        // this.pageLoad();
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0) {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    // alert(data.errors[key]);
                                    this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                                }
                            }

                        }
                        else if (data.error_message) {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                        }
                        else {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message) {
                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

                    }
                    //alert('Something went wrong!');
                }
            );
    }

    countContact(visitors) {
        let cntr = 0;
        if (visitors) {
            visitors.forEach((value: any) => {
                if (value.contact_id != null) {
                    cntr++;
                }
            });
        }
        return cntr;
    }

    countReVisitors(visitors) {
        let cntr = 0;
        if (visitors) {
            visitors.forEach((visitor: any) => {
                if (visitor.activities) {
                    visitor.activities.forEach((value: any) => {
                        if (value.revisit_count > 0) {
                            cntr++;
                        }
                    });
                }
            });
        }
        return cntr;
    }

    countReContacts(visitors) {
        let cntr = 0;
        if (visitors) {
            visitors.forEach((visitor: any) => {
                if (visitor.activities && visitor.contact_id != null) {
                    visitor.activities.forEach((value: any) => {
                        if (value.revisit_count > 0) {
                            cntr++;
                        }
                    });
                }
            });
        }
        return cntr;
    }

    getEmailTemplates() {

        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);


        this.userService.emailTemplates(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        // this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Template Successfully saved'});

                        this.templateList = data.feed;
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0) {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                                }
                            }

                        }
                        else if (data.error_message) {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                        }
                        else {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                        }
                    }
                },
                error => {
                    console.log(error);
                    this.msgs = [];
                    if (error && error.message) {
                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

                    }
                    //alert('Something went wrong!');
                }
            );
    }

    setTemplate() {
        let templtId = this.postEmailModel.template;
        console.log(templtId);
        if (templtId == '') {
            this.msgs = [];
            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: 'Please choose a valid template' });
        }
        else this.postEmailModel.content = this.templateList.find(item => item.id == templtId).content;
    }

    openNewMail() {
        this.draftStop = false;
        this.postEmailModel = { content: '', template: '' };
        this.postEmailModel.to_email = this.contactData.email;
        this.postEmailModel.attachment = null;
    }
    setReply(data) {
        console.log(data);
        this.draftStop = false;
        this.postEmailModel.to_email = data.user.email;
        this.postEmailModel.subject = data.subject;
        this.postEmailModel.parent_id = data.id;
        this.postEmailModel.attachment = null;
    }

    mailSend(myform) {
        this.draftStop = false;
        // console.log(this.postEmailModel);
        // this.postEmailModel.api_token = this.authUser.api_token;
        // this.postEmailModel.to_email = this.contactData.email;
        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);
        fd.append('template', this.postEmailModel.template);
        fd.append('to_email', this.postEmailModel.to_email);
        fd.append('cc_email', this.postEmailModel.cc_email);
        fd.append('bcc_email', this.postEmailModel.bcc_email);
        fd.append('subject', this.postEmailModel.subject);
        fd.append('content', this.postEmailModel.content);
        fd.append('managers', this.postEmailModel.managers);
        fd.append('opportunitys', this.postEmailModel.opportunitys);
        fd.append('attachment', this.postEmailModel.attachment);

        if (this.postEmailModel.parent_id) {
            fd.append('parent_id', this.postEmailModel.parent_id);
        }
        if (this.is_submit == false) {

            this.is_submit = true;
            this.userService.saveSendEmail(fd)
                .subscribe(
                    (data: any) => {
                        //console.log(data);
                        if (data.status === 'success') {
                            // message
                            this.is_submit = false;
                            this.msgs = [];
                            this.msgs.push({
                                severity: 'success',
                                summary: 'Success Message',
                                detail: 'Mail Send Successfull'
                            });

                            this.postEmailModel = {
                                content: '',
                                template: ''
                            };

                            // this.templateList = data.feed;
                        }
                        if (data.status === 'fail') {
                            // message
                            this.is_submit = false;
                            this.msgs = [];
                            if (data.errors && Object.keys(data.errors).length > 0) {

                                for (var key in data.errors) {
                                    if (data.errors.hasOwnProperty(key)) {
                                        //console.log(key + " -> " + p[key]);
                                        this.msgs.push({
                                            severity: 'error',
                                            summary: 'Error Message',
                                            detail: data.errors[key]
                                        });
                                    }
                                }

                            }
                            else if (data.error_message) {
                                this.msgs.push({
                                    severity: 'error',
                                    summary: 'Error Message',
                                    detail: data.error_message
                                });
                            }
                            else {
                                this.msgs.push({
                                    severity: 'error',
                                    summary: 'Error Message',
                                    detail: "Undefined Error"
                                });
                            }
                        }
                    },
                    error => {
                        console.log(error);
                        this.is_submit = false;
                        this.msgs = [];
                        if (error && error.message) {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

                        }
                        //alert('Something went wrong!');
                    }
                );
        }
    }

    saveToDraft() {
        let content = this.postEmailModel.content;
        /*console.log(content);
        if(content!=null)content = content.replace(/<[^>]+>/g, '');*/
        console.log(content);
        if (content != null) {
            this.postEmailModel.api_token = this.authUser.api_token;
            this.userService.saveMailAsDraft(this.postEmailModel)
                .subscribe(
                    (data: any) => {
                        //console.log(data);
                        if (data.status === 'success') {
                            // message
                            this.msgs = [];
                            this.msgs.push({ severity: 'success', summary: 'Success Message', detail: 'Mail Saved As Draft' });
                            this.draftStop = false;
                            /*this.postEmailModel={
                                content : '',
                                template: ''
                            };*/

                        }
                        if (data.status === 'fail') {
                            // message
                            this.msgs = [];
                            if (data.errors && Object.keys(data.errors).length > 0) {

                                for (var key in data.errors) {
                                    if (data.errors.hasOwnProperty(key)) {
                                        //console.log(key + " -> " + p[key]);
                                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                                    }
                                }

                            }
                            else if (data.error_message) {
                                this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                            }
                            else {
                                this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                            }
                        }
                    },
                    error => {
                        console.log(error);
                        this.msgs = [];
                        if (error && error.message) {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

                        }
                        //alert('Something went wrong!');
                    }
                );
        }


    }

    checkDraft(event) {
        //
        console.log(event.textValue);
        this.draftStop = false;
        // let content = this.postEmailModel.content;
        let content = event.textValue;
        this.postEmailModel.content = content;
        // console.log(content);
        // if(content!=null) content = content.replace(/<[^>]+>/g, '');
        // console.log(content);
        if (content != null && content != '') {
            this.draftStop = true;
        }
        console.log(this.draftStop);
        /*if(this.draftCheck == false){
            this.draftCheck = true;
            setTimeout(function(){
                this.draftCheck = false;
                console.log(this.draftCheck);
            }, 3000);
        }*/
    }

    replaceContent(content) {
        //


        let res = content.replace("{ SITE_EMAIL }", this.appConfig.siteEmail);

        res = res.replace("{ SITE_URL }", this.appConfig.siteUrl);
        res = res.replace("{ REG_LINK }", this.appConfig.regLink);

        return res;
    }

    /**
     * @func {{getMyProjects}}
     * @description {{get user projects}}
     */
    getMyProjects() {
        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);
        this.userService.myProjects(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        //this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Project Post Successful'});
                        this.myProjects = (data.projects);
                        // this.timezones = JSON.parse(data.timezones);
                        //console.log(this.timezones);
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0) {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                                }
                            }

                        }
                        else if (data.error_message) {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                        }
                        else {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                        }
                    }
                },
                error => {
                    console.log(error);
                    this.msgs = [];
                    if (error && error.message) {
                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

                    }
                    //alert('Something went wrong!');
                }
            );


    }

    setProjFilter() {
        console.log(this.projFilter);
        this.fetchUserTimeLine();
    }
    toggleShowMessage($event, post_id) {
        console.log($event);
        console.log(post_id);
        let vStatus: string = '1';
        if ($event.srcElement.checked == true) {
            vStatus = '0';
        }

        if (post_id) {
            let fd = new FormData();
            fd.append('api_token', this.authUser.api_token);
            fd.append('post_id', post_id);
            this.userService.toggleChatBox(fd)
                .subscribe(
                    (data: any) => {
                        //console.log(data);
                        if (data.status === 'success') {
                            // message
                            this.msgs = [];
                            this.msgs.push({ severity: 'success', summary: 'Success Message', detail: 'Show Message Status' });
                            this.fetchUserTimeLine();
                        }
                        if (data.status === 'fail') {
                            // message
                            this.msgs = [];
                            if (data.errors && Object.keys(data.errors).length > 0) {

                                for (var key in data.errors) {
                                    if (data.errors.hasOwnProperty(key)) {
                                        //console.log(key + " -> " + p[key]);
                                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                                    }
                                }

                            }
                            else if (data.error_message) {
                                this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                            }
                            else {
                                this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                            }
                        }
                    },
                    error => {
                        //console.log(error);
                        this.msgs = [];
                        if (error && error.message) {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

                        }
                        //alert('Something went wrong!');
                    }
                );
        }
    }
    showProfilePicture(cmnt) {
        let commentProfilePicture = this.appConfig.frontendUrl+'assets/images/twlogo.png';
        if (cmnt.user != null && cmnt.contact == null) {
            commentProfilePicture =this.appConfig.publicUrl+'/uploads/profile_picture/thumbs/'+cmnt.user.profile_picture ;
        }
        else if (cmnt.user.profile_picture == null && cmnt.contact == null) {
            commentProfilePicture = this.appConfig.frontendUrl+'assets/images/twlogo.png';
        }
        else if (cmnt.contact.profile_picture == null && cmnt.user == null) {
            commentProfilePicture = this.appConfig.frontendUrl+'assets/images/twlogo.png';
        }
        else if (cmnt.contact != null && cmnt.user == null) {
            commentProfilePicture = this.appConfig.publicUrl+'/uploads/profile_picture/thumbs/'+cmnt.contact.profile_picture;
        }
       return commentProfilePicture;
    }

    setTab(tabId){
        this.curr_tab = tabId;
        console.log('curr_tab',this.curr_tab);
    }

   

}

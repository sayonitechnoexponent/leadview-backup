import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FileTimeLineComponent } from './file-time-line.component';

describe('FileTimeLineComponent', () => {
  let component: FileTimeLineComponent;
  let fixture: ComponentFixture<FileTimeLineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FileTimeLineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FileTimeLineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

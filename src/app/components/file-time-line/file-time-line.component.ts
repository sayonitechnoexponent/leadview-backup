import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { CommonService } from '../../services/common.service';
import { UserService } from '../../services/user.service';
import { Message} from 'primeng/primeng';
import { appConfig } from '../../apiurl.config';

declare var $ : any;
declare var google : any;

@Component({
  selector: 'app-file-time-line',
  templateUrl: './file-time-line.component.html',
  styleUrls: ['./file-time-line.component.css']
})
export class FileTimeLineComponent implements OnInit {

    /**
     * @property \{{{any}}\} {{appConfig}} {{this will hold the constants for the app}}
     */

    appConfig: any = {};

    /**
     * @property \{{{any}}\} {{authUser}} {{this will hold the authuser}}
     */

    authUser: any ={};
    /**
     * @property \{{{any}}\} {{myProjects}} {{this will hold the dataList}}
     */

    dataList: any =[];

    /**
     * @property \{{{Message[]}}\} {{msgs}} {{this is growl message model}}
     */

    msgs: Message[] = [];

    file_path : string ="";
    browsImage : any ;
    old_file : any ;
    selArr: any =[];
    owner_list: any =[];

    search_key : string ='';
    create_by : string ='';
    stdtKey : string ='';
    dudtKey : string ='';
    tlType : string ='file';

    fromDate: Date;
    dueDate: Date;
    leadFileId : any;
    leadFile : any = {};

    contactList:any = [];
    companyList:any = [];
    selectedContact:any = [];
    selectedCompany:any = [];
    is_submit:boolean = false;
    noteTypeList: any = [];
    selectedItems: any = [];
    allUsers: any = [];
    postCommentModel: any = {};
    taskModel: any = {};
    googoleAutoData: any = {};
    taskTypeList: any = [];

    labelModel: any = { };
    checkLocation:boolean = false;
    checkDate:boolean = false;
    checkTime:boolean = false;
    closerDate: Date;
    dateTime: Date;
    startDate: Date;
    endDate: Date;

    dropdownSettings : any ={};

    opportunityDd: any = [];
    allPostList: any = [];
    mylabels: any = [];
    /**
     * @property \{{{any}}\} {{noteModel}} {{this form model}}
     */

    noteModel: any = {
        contacts:null,
        companies:null,
        opportunities:null,
    };
    searchModel: any = {
        start_date:'',
        end_date:'',
        keyword:'',
    };
    self: any = [];
    selectedFiles: any = [];
    filter_id:string ="";

    reminderModel: any = {
        upto_val:'1',
        upto_dur:'w',
        repeat_val:'1',
    };

    userToNotify: any = [];

    selctedPost: string = '';
    /**
     * @constructor
     * @description {{DI will pushed here}}
     */
    constructor(
        private commonService: CommonService,
        private userService: UserService,
        private router: Router
    ) { }

  ngOnInit() {

      let fileId = localStorage.getItem('leadFile');

      // console.log('test : '+fileId);
      this.leadFileId = fileId;
      this.authUser = this.userService.getAuthUser();
      this.appConfig = appConfig;
      this.fetchFileTimeLine();
      this.fetchTeamUsers();

      this.listContacts();

      this.dropdownSettings = {
          singleSelection: false,
          text:"Select",
          enableCheckAll: false,
          selectAllText:'Select All',
          unSelectAllText:'UnSelect All',
          enableSearchFilter: true,
          classes:"myclass custom-class"
      };

      $(document).on('click','a.ldmore',function(){
          //alert('ldmore ');
          if($(this).text()=='View more'){
              $(this).text('View less');
              $(this).parent().parent().children('.commntList').children('li').show();
          }
          else{
              $(this).text('View more');
              $(this).parent().parent().children('.commntList').children('li').hide();
          }
      });
  }

    onItemSelect(item:any){
        //console.log(item.id);

        if( this.selectedItems.indexOf(item.id)== -1)
        {
            this.selectedItems.push(item.id);
        }

        //console.log(this.selectedItems);
    }

    OnItemDeSelect(item:any){
        //console.log(item.id);

        if(this.selectedItems.indexOf(item.id) != -1)
            this.selectedItems.splice(this.selectedItems.indexOf(item.id), 1);
        //console.log(this.selectedItems);

    }
    /**
     * @func {{pageLoad}}
     * @description {{reload page after a certain time}}
     */
    pageLoad(){
        setTimeout(()=>{    //<<<---    using ()=> syntax
            location.reload()
        },1000);
    }
    /**
     * @func {{openCreateNote}}
     * @param \{{{string}}\} {{$data}} {{popup data val class}}
     * @description {{this will be open the popup accordingly on click by pass data val}}
     */
    openCreateNote($data){
        //console.log($data);
        this.contactList =[];
        this.companyList =[];
        this.selectedItems =[];
        this.selectedContact =[];
        this.selectedCompany =[];
        this.commonService.openModal($data);
        this.noteModel = {};
        this.noteModel.type = "Add";
        this.fetchNoteType();

        // this.listContacts();
        //this.listCompanies();
    }

    /**
     * @func {{openCreateTask}}
     * @param \{{{string}}\} {{$data}} {{popup data val class}}
     * @description {{this will be open the popup accordingly on click by pass data val}}
     */
    openCreateTask($data){
        //console.log($data);

        this.contactList =[];
        this.companyList =[];
        this.googoleAutoData = {};
        this.taskTypeList =[];
        this.selectedItems =[];
        this.selectedContact =[];
        this.selectedCompany =[];
        this.commonService.openModal($data);
        this.taskModel = {};
        this.taskModel.type = "Add";

        this.fetchTaskType();
        // this.listContacts();
        //this.listCompanies();
    }

    /**
     * @func {{fetchTaskType}}
     * @description {{get all Task Types}}
     */

    fetchTaskType() {
        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);

        this.userService.getTaskTypes(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        //this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Project Post Successful'});
                        this.taskTypeList =  (data.feed);
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );

    }

    listContacts(){
        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);
        let group2 =[];
        this.userService.getContacts(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        //this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Project Post Successful'});
                        //conlist =  (data.feed);
                        data.feed.forEach((value: any) => {
                            if(value.type==1)    this.contactList.push({'id':value.id ,'itemName': value.first_name });
                            else if(value.type==2)    this.companyList.push({'id':value.id ,'itemName': value.first_name });

                        });

                        //console.log(this.group);
                        /*this.contactList = [
                            CreateNewAutocompleteGroup(
                                'Search from list',
                                'completer',
                                this.group,
                                {titleKey: 'title', childrenKey: null}
                            ),
                        ];
                        this.companyList = [
                            CreateNewAutocompleteGroup(
                                'Search from list',
                                'completer',
                                group2,
                                {titleKey: 'title', childrenKey: null}
                            ),
                        ];*/
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );
        //console.log(this.contactList);

    }
    /**
     * @func {{fetchNoteType}}
     * @description {{get all Stages}}
     */
    fetchNoteType() {
        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);

        this.userService.getNoteTypes(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        //this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Project Post Successful'});
                        this.noteTypeList =  (data.feed);
                        //console.log(this.noteTypeList[0]);
                        if(data.feed.length > 0){
                            this.noteModel.note_type = this.noteTypeList[0].id;
                        };
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );

    }
    /**
     * @func {{fetchTeamUsers}}
     * @description {{get user teams}}
     */

    fetchTeamUsers() {
        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);
        fd.append('page_type', 'contacts');
        this.userService.getTeamUsers(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        //this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Project Post Successful'});
                        //this.allUsers =  (data.users);
                        data.users.forEach((value: any) => {
                            let tname = value.first_name +' ' + value.last_name;
                            if(value.id != this.authUser.id)   this.allUsers.push({'id':value.id ,'itemName': tname });
                        });
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});
                    }
                    //alert('Something went wrong!');
                }
            );

    }

    onFileChange(event,modType) {

        this.postCommentModel.attachment = null;

        if(event.target.files && event.target.files.length > 0) {
            let file = event.target.files[0];
            //console.log(file);
            if(modType == 'n')  this.noteModel.attachment = file;
            // else if (modType == 'o')  this.opportunityModel.attachment = file;
            else if (modType == 't')  this.taskModel.attachment = file;
            else if (modType == 'cmnt')  this.postCommentModel.attachment = file;
        }
    }

    /**
     * @func {{addNote}}
     * @param \{{{formobject}}\} {{myform}} {{this will pass the form object}}
     * @description {{this will submit the form data and update the project data}}
     */
    addNote(myform)
    {
        this.is_submit = true;
        let fd = new FormData();
        fd.append('attachment', this.noteModel.attachment);
        fd.append('note_type', this.noteModel.note_type);
        fd.append('details', this.noteModel.details);
        fd.append('managers', this.selectedItems);
        fd.append('contacts', this.noteModel.contacts);
        fd.append('companies', this.noteModel.companies);
        fd.append('opportunities', this.noteModel.opportunities);
        fd.append('reminders', this.noteModel.reminders);
        fd.append('api_token', this.authUser.api_token);

        fd.append('curr_user', this.leadFileId);
        fd.append('tl_type', this.tlType);
        //this.noteModel.api_token = this.authUser.api_token;
        //console.log(this.noteModel);

        this.userService.addEditNote(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    this.is_submit = false;
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Note added successfully'});

                        //this.fetchUserTimeLine();
                        // reload page
                        this.pageLoad();
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.is_submit = false;
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );

    }


    autoCompleteCallback1(selectedData:any) {
        //do any necessery stuff.
        console.log(selectedData);
        this.googoleAutoData = {};
        this.googoleAutoData = selectedData;
        // console.log(this.googoleAutoData.data.geometry.location);
        if(this.googoleAutoData.data){
            let loc = this.googoleAutoData.data.geometry.location;
            // this.addressModel.lat = loc.lat;
            // this.addressModel.lng = loc.lng;

        }
    }

    changeTaskForm($event) {
        //console.log($event.srcElement.value);
        let jid = $event.srcElement.value;
        let selctDta = this.taskTypeList.find(item => item.id == jid);
        //console.log(selctDta);
        this.checkLocation = selctDta.show_location == 1 ? true:false;
        this.checkDate = selctDta.show_date == 1 ? true:false;
        this.checkTime = selctDta.show_time == 1 ? true:false;
    }
    /**
     * @func {{addTask}}
     * @param \{{{formobject}}\} {{myform}} {{this will pass the form object}}
     * @description {{this will submit the form data and update the project data}}
     */
    addTask(myform)
    {
        this.is_submit = true;
        if(this.closerDate) this.taskModel.due_date= (this.closerDate.getTime())/1000;
        //if(this.dateTime) this.taskModel.duetime= this.dateTime;
        if(this.dateTime) this.taskModel.due_time= this.dateTime.toLocaleTimeString();


        //console.log(this.taskModel);
        let fd = new FormData();
        if(this.googoleAutoData.data){
            fd.append('location', this.googoleAutoData.data.formatted_address);
            fd.append('lat', this.googoleAutoData.data.geometry.location.lat);
            fd.append('lng', this.googoleAutoData.data.geometry.location.lng);
        }

        fd.append('task_type', this.taskModel.task_type);
        fd.append('attachment', this.taskModel.attachment);
        fd.append('title', this.taskModel.title);
        fd.append('notes', this.taskModel.notes);
        fd.append('due_date', this.taskModel.due_date);
        fd.append('due_time', this.taskModel.due_time);
        fd.append('managers', this.selectedItems);
        fd.append('contacts', this.taskModel.contacts);
        fd.append('companies', this.taskModel.companies);
        fd.append('api_token', this.authUser.api_token);

        fd.append('curr_user', this.leadFileId);
        fd.append('tl_type', this.tlType);
        // console.log(this.taskModel);
        //this.noteModel.api_token = this.authUser.api_token;

        this.userService.addEditTask(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    this.is_submit = false;
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Task added successfully'});

                        //this.fetchUserTimeLine();
                        // reload page
                        this.pageLoad();
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    this.is_submit = false;
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );

    }

    fetchFileTimeLine() {
        //alert(this.listTab);

        // this.commonService.startLoader(this.listTab);

        let that = this;
        let fd = new FormData();

        // fd.append('filtr_id', this.filter_id);
        fd.append('file_id', this.leadFileId);
        fd.append('api_token', this.authUser.api_token);

        fd.append('keyword', this.searchModel.keyword);
        fd.append('start_date', this.searchModel.start_date);
        fd.append('end_date', this.searchModel.end_date);
        fd.append('filter_id', this.filter_id);
        // fd.append('page', this.page.toString());
        // fd.append('limit', this.limit.toString());


        this.userService.fileTimeLine(fd)
            .subscribe(
                (data: any) => {
                    //console.log(ta);
                    if (data.status === 'success') {
                        //data = JSON.parse(data);
                        // message

                        this.msgs = [];
                        this.leadFile = data.lead_file;
                        this.allPostList =  data.feed;

                        this.mylabels = data.labels;
                        // console.log(this.allPostList[0]);
                        /*alert( this.allPostList.length);*/

                    }
                    if (data.status === 'fail') {
                        // message

                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }

                    /*setTimeout(()=>{
                        this.commonService.stopLoader(this.listTab);
                    },1000);*/

                },
                error => {
                    /*setTimeout(()=>{
                        this.commonService.stopLoader(this.listTab);
                    },1000);*/
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});
                    }
                    //alert('Something went wrong!');
                }
            );

    }

    toggleStatus($event,orginal_post_id, post_type)
    {
        // console.log($event.srcElement.checked);
        //console.log($event.srcElement.value);
        // console.log(orginal_post_id);
        // console.log(post_type);
        let vStatus:string = '1';
        if($event.srcElement.checked == true)
        {
            if(post_type == 'A')    vStatus = '0';
            else   vStatus = '2';
        }

        if(post_type && orginal_post_id)
        {
            //alert($event.srcElement.value+'='+post_id);
            let fd = new FormData();
            fd.append('api_token', this.authUser.api_token);
            fd.append('orginal_post_id', orginal_post_id);
            fd.append('post_type', post_type);
            fd.append('status', vStatus);
            this.userService.changePostScope(fd)
                .subscribe(
                    (data: any) => {
                        //console.log(data);
                        if (data.status === 'success') {
                            // message
                            this.msgs = [];
                            this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Satus successfully saved'});
                            this.fetchFileTimeLine();
                        }
                        if (data.status === 'fail') {
                            // message
                            this.msgs = [];
                            if (data.errors && Object.keys(data.errors).length > 0)
                            {

                                for (var key in data.errors) {
                                    if (data.errors.hasOwnProperty(key)) {
                                        //console.log(key + " -> " + p[key]);
                                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                    }
                                }

                            }
                            else if(data.error_message)
                            {
                                this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                            }
                            else
                            {
                                this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                            }
                        }
                    },
                    error => {
                        //console.log(error);
                        this.msgs = [];
                        if (error && error.message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                        }
                        //alert('Something went wrong!');
                    }
                );
        }
    }

    switchFile(fid,fileType){
        console.log(fid);
        let fd = new FormData();
        fd.append('file_id', fid);
        fd.append('file_type', fileType);
        fd.append('api_token', this.authUser.api_token);

        this.userService.setFileVersion(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'File version successfully switched'});

                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                        this.is_submit = false;
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    this.is_submit = false;
                    //alert('Something went wrong!');
                }
            );
    }

    /**
     * @func {{searchPost}}
     * @param \{{{formobject}}\} {{myform}} {{this will pass the form object}}
     * @description {{this will submit the form data and update the project data}}
     */
    searchPost(myform)
    {
        if(this.startDate) this.searchModel.start_date= (this.startDate.getTime())/1000;
        if(this.endDate) this.searchModel.end_date= ((this.endDate.getTime())/1000)+86400;
        //console.log(this.searchModel);
        this.fetchFileTimeLine();
    }


    keyDownFunction(event,f,typ,pid) {
        // console.log(event.keyCode);
        if(event.keyCode == 13) {
            // alert('you just clicked enter');
            // rest of your code
            if(this.is_submit == false)    this.createComment(f,typ,pid);
            //console.log(f);
        }
    }
    setComment(event) {
        // console.log(event.srcElement.value);
        this.postCommentModel.content = event.srcElement.value;
    }
    /**
     * @func {{createComment}}
     * @param \{{{formobject}}\} {{myform}} {{this will pass the form object}}
     * @description {{this will submit the form data and update the project data}}
     */
    createComment(myform,typ,pid)
    {
        this.is_submit = true;
        //console.log(myform);
        this.postCommentModel.api_token = this.authUser.api_token;
        this.postCommentModel.type = typ;
        this.postCommentModel.post_id = pid;
        // console.log(this.postCommentModel);

        let fd = new FormData();
        if(this.postCommentModel.attachment)    fd.append('attachment', this.postCommentModel.attachment);
        fd.append('type', this.postCommentModel.type);
        fd.append('post_id', this.postCommentModel.post_id);
        fd.append('content', this.postCommentModel.content);
        fd.append('api_token', this.authUser.api_token);
        fd.append('tl_type', this.tlType);

        this.userService.savePostComment(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Comment successful'});
                        this.postCommentModel = {};
                        this.fetchFileTimeLine();
                        this.is_submit = false;
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                        this.is_submit = false;
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    this.is_submit = false;
                    //alert('Something went wrong!');
                }
            );

    }

    setUnsetRelFile(fileId){
        console.log(fileId);
        if(this.self.indexOf(fileId.id)== -1)
        {
            this.self.push(fileId.id);
            this.selectedFiles.push(fileId);

        }
    }
    setBlank(){
        this.selectedFiles=[];
        console.log(this.selectedFiles);
    }

    /* @func {{createLabel}}
    * @param \{{{formobject}}\} {{myform}} {{this will pass the form object}}
    * @description {{this will submit the form data and update the label data}}
    */
    createLabel(myform,post_id)
    {
        //alert(etyp);
        this.labelModel.curr_user =this.leadFileId;
        this.labelModel.tl_type =this.tlType;
        this.labelModel.api_token = this.authUser.api_token;
        this.labelModel.post_id = post_id.toString();
        // console.log(this.labelModel);
        this.userService.addLabel(this.labelModel)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        this.mylabels = data.feed;
                        // message
                        this.msgs = [];
                        this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Label successfully saved'});
                        $('.openlabelbx').hide();
                        $('.offforcreatelabel').show();
                        this.fetchFileTimeLine();

                        //this.fetchUserEmails();
                        // reload page
                        //this.pageLoad();
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );

    }

    addToLabel($event,post_id) {
        //console.log($event.srcElement.checked);
        //console.log($event.srcElement.value);
        if($event.srcElement.value && post_id)
        {
            //alert($event.srcElement.value+'='+post_id);
            let fd = new FormData();
            fd.append('api_token', this.authUser.api_token);
            fd.append('post_id', post_id);
            fd.append('label_id', $event.srcElement.value);
            fd.append('tl_type', this.tlType);

            this.userService.setLabel(fd)
                .subscribe(
                    (data: any) => {
                        //console.log(data);
                        if (data.status === 'success') {
                            // message
                            this.msgs = [];
                            this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Label successfully saved'});
                            this.fetchFileTimeLine();
                        }
                        if (data.status === 'fail') {
                            // message
                            this.msgs = [];
                            if (data.errors && Object.keys(data.errors).length > 0)
                            {

                                for (var key in data.errors) {
                                    if (data.errors.hasOwnProperty(key)) {
                                        //console.log(key + " -> " + p[key]);
                                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                    }
                                }

                            }
                            else if(data.error_message)
                            {
                                this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                            }
                            else
                            {
                                this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                            }
                        }
                    },
                    error => {
                        //console.log(error);
                        this.msgs = [];
                        if (error && error.message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                        }
                        //alert('Something went wrong!');
                    }
                );
        }
    }

    getFilterData(filterId){
        this.filter_id = filterId;
        this.fetchFileTimeLine();
    }


    selctNotifyUsr($event)
    {
        /*console.log($event.srcElement.value);
        console.log($event.srcElement.checked);
        console.log(type_post_id);*/


        if($event.srcElement.checked == true)
        {
            if(this.userToNotify.indexOf($event.srcElement.value)== -1)
                this.userToNotify.push($event.srcElement.value);
        }else {

            this.userToNotify.splice(this.userToNotify.indexOf($event.srcElement.value), 1);
        }

        console.log(this.userToNotify);
    }

    openReminder($event,post_id)
    {
        if( this.selctedPost != post_id)
        {
            this.selctedPost = post_id;
            this.userToNotify =[];
            this.userToNotify.push(this.authUser.id.toString());
        }
        this.userToNotify =[];
        this.userToNotify.push(this.authUser.id.toString());
        this.reminderModel = {
            upto_val:'1',
            upto_dur:'w',
            repeat_val:'1',
        };
    }

    /**
     * @func {{saveReminder}}
     * @param \{{{formobject}}\} {{myform}} {{this will pass the form object}}
     * @description {{this will submit the form data and update the project data}}
     */
    saveReminder(myform){
        this.reminderModel.notify_user= this.userToNotify;
        this.reminderModel.post_id= this.selctedPost;
        this.reminderModel.api_token= this.authUser.api_token;
        console.log(this.reminderModel);
        this.userService.setReminder(this.reminderModel)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Set Reminder Successful'});
                        // console.log(data.feed);
                        this.fetchFileTimeLine();
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );
    }



}

import { Component, OnInit } from '@angular/core';

import { CommonService } from '../../services/common.service';
import { UserService } from '../../services/user.service';
import {Message} from 'primeng/primeng';
import { appConfig } from '../../apiurl.config';

@Component({
  selector: 'app-groups',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.css']
})
export class GroupsComponent implements OnInit {
    /**
     * @property \{{{any}}\} {{appConfig}} {{this will hold the constants for the app}}
     */

    appConfig: any = {};

    /**
     * @property \{{{Message[]}}\} {{msgs}} {{this is growl message model}}
     */

    msgs: Message[] = [];

    /**
     * @property \{{{any}}\} {{model}} {{this form model}}
     */

    model: any = {
        title : null,
        created_by : 'PM',
    };

    /**
     * @property \{{{any}}\} {{authUser}} {{this will hold the authuser}}
     */

    authUser: any ={};
    /**
     * @property \{{{any}}\} {{myList}} {{this will hold the list data}}
     */

    myList: any;

    /**
     * @property \{{{any}}\} {{selArr}} {{this will hold the selected from list}}
     */

    selArr: any =[];

    searchText : any;

    showName: boolean = true;
    showReq: boolean = true;
    showLocation: boolean = true;
    showDate: boolean = true;
    showTime: boolean = true;

    displayColFrm: any = {
        itemName : true,
        itemReq : true,
        itemLocation : true,
        itemDate : true,
        itemTime : true,
    };

  constructor( private commonService: CommonService,
               private userService: UserService) { }

    /**
     * @func {{saveDisplay}}
     * @description {{toggle display of list}}
     */
    saveDisplay(myForm) {

        console.log(this.displayColFrm);
        this.showName = this.displayColFrm.itemName;
        this.showReq = this.displayColFrm.itemReq;
        this.showLocation = this.displayColFrm.itemLocation;
        this.showDate = this.displayColFrm.itemDate;
        this.showTime = this.displayColFrm.itemTime;

    }

    /**
     * @func {{pageLoad}}
     * @description {{reload page after a certain time}}
     */
    pageLoad(){
        setTimeout(()=>{    //<<<---    using ()=> syntax
            location.reload()
        },2000);
    }

    /**
     * @func {{openCreatePop}}
     * @param \{{{string}}\} {{$data}} {{popup data val class}}
     * @description {{this will be open the popup accordingly on click by pass data val}}
     */
    openCreatePop($data){
        console.log($data);
        this.commonService.openModal($data);
        this.model.type = "Add";
        //this.model = {};
    }
    /**
     * @func {{openEditModal}}
     * @param \{{{string}}\} {{$data}} {{popup data val class}}
     * @description {{this will me open the popup accordingly on click by pass data val}}
     */
    openEditModal($data,feed){
        this.commonService.openModal($data);
        this.model = feed;
        this.model._id = feed.id;
        this.model.type = "Edit";
        //console.log(this.model);
        if(this.model.show_location ==0)    this.model.show_location=false;
        if(this.model.location_optional ==0)    this.model.location_optional=false;
        if(this.model.show_date ==0)    this.model.show_date=false;
        if(this.model.date_optional ==0)    this.model.date_optional=false;
        if(this.model.show_time ==0)    this.model.show_time=false;
        if(this.model.time_optional ==0)    this.model.time_optional=false;
    }
    /**
     * @func {{addSaveGroup}}
     * @param \{{{formobject}}\} {{myform}} {{this will pass the form object}}
     * @description {{this will submit the form data and add or update the tax data}}
     */

    addSaveGroup(myform)
    {
        this.model.api_token = this.authUser.api_token;
        console.log(this.model);

        this.userService.saveUserGroup(this.model)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Group added successfully'});

                        // go to  my project page
                        this.pageLoad();
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );

    }
    /**
     * @func {{fetchGroup}}
     * @description {{get all Type}}
     */
    fetchGroup() {
        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);

        this.userService.fetchGroups(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        //this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Project Post Successful'});
                        this.myList =  (data.feed);
                        //console.log(this.myTeam);
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );

    }
    /**
     * @func {{delGroup}}
     * @description {{delete tax}}
     */
    delGroup(_id)
    {
        //console.log(this.model);
        if(confirm("Are you sure to delete the group?"))
        {
            let fd = new FormData();
            fd.append('_ids',_id);
            fd.append('api_token', this.authUser.api_token);
            //return;
            this.userService.deleteGroups(fd)
                .subscribe(
                    (data: any) => {
                        //console.log(data);
                        if (data.status === 'success') {
                            // message
                            this.msgs = [];
                            this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Group Successfully Deleted'});

                            // go to  my team page
                            this.pageLoad();
                        }
                        if (data.status === 'fail') {
                            // message
                            this.msgs = [];
                            if (data.errors && Object.keys(data.errors).length > 0)
                            {

                                for (var key in data.errors) {
                                    if (data.errors.hasOwnProperty(key)) {
                                        //console.log(key + " -> " + p[key]);
                                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                    }
                                }

                            }
                            else if(data.error_message)
                            {
                                this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                            }
                            else
                            {
                                this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                            }
                        }
                    },
                    error => {
                        console.log(error);
                        this.msgs = [];
                        if (error && error.message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                        }
                        //alert('Something went wrong!');
                    }
                );

        }


    }

    /**
     * @func {{delAll}}
     * @description {{delete selected taxes}}
     */
    delAll()
    {
        if(confirm("Are you sure to delete all selected groups?"))
        {
            let fd = new FormData();
            fd.append('_ids',this.selArr);
            fd.append('api_token', this.authUser.api_token);
            //return;
            this.userService.deleteGroups(fd)
                .subscribe(
                    (data: any) => {
                        //console.log(data);
                        if (data.status === 'success') {
                            // message
                            this.msgs = [];
                            this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Groups are successfully deleted'});

                            this.pageLoad();
                        }
                        if (data.status === 'fail') {
                            // message
                            this.msgs = [];
                            if (data.errors && Object.keys(data.errors).length > 0)
                            {

                                for (var key in data.errors) {
                                    if (data.errors.hasOwnProperty(key)) {
                                        //console.log(key + " -> " + p[key]);
                                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                    }
                                }

                            }
                            else if(data.error_message)
                            {
                                this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                            }
                            else
                            {
                                this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                            }
                        }
                    },
                    error => {
                        console.log(error);
                        this.msgs = [];
                        if (error && error.message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                        }
                        //alert('Something went wrong!');
                    }
                );

        }
    }

    selctToList($event) {
        //console.log($event.srcElement.checked);
        //console.log($event.srcElement.value);
        if($event.srcElement.checked == true)
        {
            if(this.selArr.indexOf($event.srcElement.value)== -1)
                this.selArr.push($event.srcElement.value);
        }else {

            this.selArr.splice(this.selArr.indexOf($event.srcElement.value), 1);
        }
        //console.log(this.selcArr);
    }

    ngOnInit() {

        this.authUser = this.userService.getAuthUser();
        this.appConfig = appConfig;
        this.fetchGroup();
		$(".leftpanelfilteremenuicon").click(function(event) {
			event.preventDefault();
			$('.leftpanelpopup').addClass("show");
			$('.filtermask').addClass("show");
			
		});
		$(".filtermask").click(function(event) {
			event.preventDefault();
			$('.leftpanelpopup').removeClass("show");
			$('.filtermask').removeClass("show");
			
		});

    }

}

import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonService } from '../../services/common.service';
import { UserService } from '../../services/user.service';
import { Message} from 'primeng/primeng';
import { appConfig } from '../../apiurl.config';

@Component({
  selector: 'app-mail-box',
  templateUrl: './mail-box.component.html',
  styleUrls: ['./mail-box.component.css']
})
export class MailBoxComponent implements OnInit {
    /**
     * @property \{{{any}}\} {{appConfig}} {{this will hold the constants for the app}}
     */

    appConfig: any = {};

    /**
     * @property \{{{Message[]}}\} {{msgs}} {{this is growl message model}}
     */

    msgs: Message[] = [];

    /**
     * @property \{{{any}}\} {{model}} {{this form model}}
     */

    eModel: any = {
        cm1: false,
        cn1: false,
        rc1: false,
        ro1: false,
        cm2: false,
        cn2: false,
        rc2: false,
        ro2: false,
    };

    /**
     * @property \{{{any}}\} {{authUser}} {{this will hold the authuser}}
     */

    authUser: any ={};
    /**
     * @property \{{{any}}\} {{myList}} {{this will hold the list data}}
     */

    mailList: any;
    mailType: string = '1';
    ownerType: string = '1';
    ownerId: string;
    post_count: any=[];
    selArr: any =[];
    relTab: number = 1;
    cmList: any =[];
    contactList: any =[];
    companyList: any =[];
    opportunityList: any =[];
    selectedCm: any ={};
    selectedMail: number;

  constructor(
      private commonService: CommonService,
      private userService: UserService,
      private router: Router) { }

  ngOnInit() {
      this.authUser = this.userService.getAuthUser();
      this.ownerId = this.authUser.id;
      this.appConfig = appConfig;
  }
  ngAfterViewInit(){
    this.fetchMails();
    let that = this;
      $("#updaterelationModal").on("hidden.bs.modal", function () {
          /*console.log('close');*/
          that.eModel = { cm1: false, cn1: false, rc1: false, ro1: false, cm2: false, cn2: false, rc2: false, ro2: false};
      });
  }

    mailSelction(mid,mailRelation){
        console.log(mid);
        this.selectedMail = mid;
        this.eModel = { cm1: false, cn1: false, rc1: false, ro1: false, cm2: false, cn2: false, rc2: false, ro2: false};
        if(mailRelation){
            let rel = mailRelation;
            this.relTab = parseInt(rel.type);
            this.eModel.cm1 = rel.is_copy_to_manager=='1'?true:false;
            this.eModel.rc1 = rel.is_copy_to_company=='1'?true:false;
            this.eModel.cn1 = rel.is_copy_to_contact=='1'?true:false;
            this.eModel.ro1 = rel.is_copy_to_opportunity=='1'?true:false;
            this.eModel.cm2 = rel.is_future_to_manager=='1'?true:false;
            this.eModel.rc2 = rel.is_future_to_company=='1'?true:false;
            this.eModel.cn2 = rel.is_future_to_contact=='1'?true:false;
            this.eModel.ro2 = rel.is_future_to_opportunity=='1'?true:false;

            if(rel.copy_to_managers.length>0){
                let val_arr= [];
                rel.copy_to_managers.forEach((value: any) => {
                    val_arr.push(value.assign_to);
                });
                this.eModel.type1_managers = val_arr;
            }
            if(rel.copy_to_companies.length>0){
                let val_arr= [];
                rel.copy_to_companies.forEach((value: any) => {
                    val_arr.push(value.assign_to);
                });
                this.eModel.type1_company = val_arr;
            }
            if(rel.copy_to_contacts.length>0){
                let val_arr= [];
                rel.copy_to_contacts.forEach((value: any) => {
                    val_arr.push(value.assign_to);
                });
                this.eModel.type1_contact = val_arr;
            }
            if(rel.copy_to_opportunities.length>0){
                let val_arr= [];
                rel.copy_to_opportunities.forEach((value: any) => {
                    val_arr.push(value.assign_to);
                });
                this.eModel.type1_opportunity = val_arr;
            }

            if(rel.future_managers.length>0){
                let val_arr= [];
                rel.future_managers.forEach((value: any) => {
                    val_arr.push(value.assign_to);
                });
                this.eModel.type2_managers = val_arr;
            }
            if(rel.future_companies.length>0){
                let val_arr= [];
                rel.future_companies.forEach((value: any) => {
                    val_arr.push(value.assign_to);
                });
                this.eModel.type2_company = val_arr;
            }
            if(rel.future_contacts.length>0){
                let val_arr= [];
                rel.future_contacts.forEach((value: any) => {
                    val_arr.push(value.assign_to);
                });
                this.eModel.type2_contact = val_arr;
            }
            if(rel.future_opportunities.length>0){
                let val_arr= [];
                rel.future_opportunities.forEach((value: any) => {
                    val_arr.push(value.assign_to);
                });
                this.eModel.type2_opportunity = val_arr;
            }

            console.log(this.eModel);

        }
    }

    showPp(mailData){
        // console.log('mailData');
        let pp = 'assets/images/avatar.png';
        if(mailData.sender_type==1){
            if(mailData.user && mailData.user.profile_picture!=null){
                pp = this.appConfig.publicUrl+'/uploads/profile_picture/'+mailData.user.profile_picture;
            }
        }
        else if(mailData.sender_type==2){
            if(mailData.contact && mailData.contact.profile_picture!=null){
                pp = this.appConfig.publicUrl+'/uploads/profile_picture/'+mailData.contact.profile_picture;
            }
        }
        return pp;
    }

    showName(postData){
        // console.log('postData');
        let sname = this.cutStr(postData.email);
        if(postData.mail_message.sender_type==1){
            if(postData.mail_message.user && postData.mail_message.user.first_name!=null){
                sname = postData.mail_message.user.first_name;
            }
        }
        /*else if(postData.mail_message.sender_type==2){
            if(postData.mail_message.contact && postData.mail_message.contact.profile_picture!=null){
                sname = this.appConfig.publicUrl+'/uploads/profile_picture/thumbs/'+postData.contact.profile_picture;
            }
        }*/
        return sname;
    }

    openEmailTimeLine(eid){
        localStorage.setItem('leadEmailId', eid);
        this.router.navigate(['/user/email-timeline']);
    }
    changeTab(tab){
      // console.log(tab);
        this.mailList = [];
      this.mailType = tab;
        this.selArr =[];
      this.fetchMails();
    }

    fetchMails(){
      let fd = new FormData();
      fd.append('api_token', this.authUser.api_token);
      fd.append('mailType', this.mailType);
      fd.append('ownerType', this.ownerType);
      fd.append('ownerId', this.ownerId);

      this.userService.getMailList(fd)
          .subscribe(
              (data: any) => {
                  //console.log(data);
                  if (data.status === 'success') {
                      // message
                      this.msgs = [];
                      //this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Project Post Successful'});
                      this.mailList =  (data.feed);
                      this.cmList =  [];
                      this.contactList =  [];
                      this.companyList =  [];
                      this.opportunityList =  [];
                      if(data.project_users.length > 0){
                          data.project_users.forEach((value: any) => {
                              let nam = value.first_name+' '+value.last_name;
                              this.cmList.push({'value': value.id, 'label': nam});
                          });
                      }
                      if(data.project_contacts.length > 0){
                          data.project_contacts.forEach((value: any) => {
                              let nam = value.name;
                              if(value.type==1){
                                  this.contactList.push({'value': value.id, 'label': nam});
                              }else{
                                  this.companyList.push({'value': value.id, 'label': nam});
                              }
                          });
                      }

                      if(data.project_opportunities.length > 0){
                          data.project_opportunities.forEach((value: any) => {
                              this.opportunityList.push({'value': value.id, 'label': value.title});
                          });
                      }
                      this.post_count =  (data.post_count);
                      //console.log(this.myTeam);
                  }
                  if (data.status === 'fail') {
                      // message
                      this.msgs = [];
                      if (data.errors && Object.keys(data.errors).length > 0)
                      {

                          for (var key in data.errors) {
                              if (data.errors.hasOwnProperty(key)) {
                                  //console.log(key + " -> " + p[key]);
                                  this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                              }
                          }

                      }
                      else if(data.error_message)
                      {
                          this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                      }
                      else
                      {
                          this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                      }
                  }
              },
              error => {
                  //console.log(error);
                  this.msgs = [];
                  if (error && error.message)
                  {
                      this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                  }
                  //alert('Something went wrong!');
              }
          );
  }

    delThread(mailId){
        if(confirm("Are you sure to delete the message?")) {
            this.changeStage(mailId, 5);
        }
    }
    /*makeArchive(mailId){
        this.changeStage(mailId,4);
    }*/

    changeStage(mailId,status){
        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);
        fd.append('mailType', status);
        fd.append('mailId', mailId);

        this.userService.changeMailStatus(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        //this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Project Post Successful'});
                        // this.mailList =  (data.feed);
                        this.fetchMails();
                        //console.log(this.myTeam);
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );
    }

    selctToList($event) {
        //console.log($event.srcElement.checked);
        //console.log($event.srcElement.value);
        if($event.srcElement.checked == true)
        {
            if(this.selArr.indexOf($event.srcElement.value)== -1)
                this.selArr.push($event.srcElement.value);
        }else {

            this.selArr.splice(this.selArr.indexOf($event.srcElement.value), 1);
        }
        console.log(this.selArr);
    }

    archiveAll(){
        console.log(this.selArr);
        // this.changeStage(this.selArr,4);
    }

    cutStr(str){
        if(str) return str.substring( 0, str.indexOf( "@" ) );
        else    return '';
    }

    mergeTl(){
        console.log(this.selArr);

        if (this.selArr.length > 1){

            if(confirm("Are you sure to merge the timelines?")) {
                let fd = new FormData();
                fd.append('api_token', this.authUser.api_token);
                fd.append('mail_ids', this.selArr);

                this.userService.mergeMail(fd)
                    .subscribe(
                        (data: any) => {
                            //console.log(data);
                            if (data.status === 'success') {
                                // message
                                this.msgs = [];
                                //this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Project Post Successful'});
                                // this.mailList =  (data.feed);
                                this.fetchMails();
                                //console.log(this.myTeam);
                            }
                            if (data.status === 'fail') {
                                // message
                                this.msgs = [];
                                if (data.errors && Object.keys(data.errors).length > 0) {

                                    for (var key in data.errors) {
                                        if (data.errors.hasOwnProperty(key)) {
                                            //console.log(key + " -> " + p[key]);
                                            this.msgs.push({
                                                severity: 'error',
                                                summary: 'Error Message',
                                                detail: data.errors[key]
                                            });
                                        }
                                    }

                                }
                                else if (data.error_message) {
                                    this.msgs.push({
                                        severity: 'error',
                                        summary: 'Error Message',
                                        detail: data.error_message
                                    });
                                }
                                else {
                                    this.msgs.push({
                                        severity: 'error',
                                        summary: 'Error Message',
                                        detail: "Undefined Error"
                                    });
                                }
                            }
                        },
                        error => {
                            //console.log(error);
                            this.msgs = [];
                            if (error && error.message) {
                                this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                            }
                            //alert('Something went wrong!');
                        }
                    );
            }
        }
        else{
            this.msgs = [];
            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Please select at least 2 time-line."});
        }
    }

    openSubOption(event){

        // console.log(event.srcElement.checked);
        // console.log(event.srcElement.value);
        this.relTab = event.srcElement.value;
    }

    setMailRelation(myform){

        /*console.log(this.relTab);
        console.log(this.eModel);*/
        this.eModel.api_token = this.authUser.api_token;
        this.eModel.selc_tab = this.relTab;
        this.eModel.main_mailId = this.selectedMail;
        // modifyMailRelation
        this.userService.modifyMailRelation(this.eModel)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Relation Successfully Saved'});
                        this.fetchMails();
                        // console.log(data);
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );

    }


    makeArchive(mainId) {
        //console.log(event.srcElement.checked);

            //alert($event.srcElement.value+'='+post_id);
            let fd = new FormData();
            fd.append('api_token', this.authUser.api_token);
            fd.append('main_mailId', mainId);

            this.userService.archiveMail(fd)
                .subscribe(
                    (data: any) => {
                        //console.log(data);
                        if (data.status === 'success') {
                            // message
                            this.msgs = [];
                            this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Archive status successfully changed'});
                            this.fetchMails();
                        }
                        if (data.status === 'fail') {
                            // message
                            this.msgs = [];
                            if (data.errors && Object.keys(data.errors).length > 0)
                            {

                                for (var key in data.errors) {
                                    if (data.errors.hasOwnProperty(key)) {
                                        //console.log(key + " -> " + p[key]);
                                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                    }
                                }

                            }
                            else if(data.error_message)
                            {
                                this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                            }
                            else
                            {
                                this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                            }
                        }
                    },
                    error => {
                        //console.log(error);
                        this.msgs = [];
                        if (error && error.message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                        }
                        //alert('Something went wrong!');
                    }
                );
        }



}

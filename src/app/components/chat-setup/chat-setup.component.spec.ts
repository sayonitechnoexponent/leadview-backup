import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChatSetupComponent } from './chat-setup.component';

describe('ChatSetupComponent', () => {
  let component: ChatSetupComponent;
  let fixture: ComponentFixture<ChatSetupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChatSetupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChatSetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

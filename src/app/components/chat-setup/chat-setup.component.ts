import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { CommonService } from '../../services/common.service';
import { UserService } from '../../services/user.service';
import { Message } from 'primeng/primeng';
import { appConfig } from '../../apiurl.config';
import { json } from "ng2-validation/dist/json";

declare var $: any;
declare var google: any;

@Component({
    selector: 'app-chat-setup',
    templateUrl: './chat-setup.component.html',
    styleUrls: ['./chat-setup.component.css']
})
export class ChatSetupComponent implements OnInit {
    /**
     * @property \{{{any}}\} {{appConfig}} {{this will hold the constants for the app}}
     */

    appConfig: any = {};

    /**
     * @property \{{{Message[]}}\} {{msgs}} {{this is growl message model}}
     */

    msgs: Message[] = [];
    /**
     * @property \{{{any}}\} {{authUser}} {{this will hold the auth User data}}
     */

    authUser: any = {};
    user: any;

    /**
     * @property \{{{any}}\} {{model}} {{user form model}}
     */

    model: any = {
        layout : 1,
        visible_desktop : 1,
        visible_ipad : 1,
        visible_mobile : 1,
        default_on : '1',
        show_chat : false,
        show_directory : false,
        show_timeline : false,
        show_social : false,
        show_preferences : false,
        show_profile : true,
        personal_removal_template : '',
        gdpr_data_manager_id : '',
        // avatar_show_type  : 2,
        // status : '1',
    };
    emailtemplate: any = {
        api_token: null,
        message_type: 1,
    };
    short_code : string = "";
    ws_code : any;
    emailTemplateArray: any=[];
    teamUsers: any=[];
  constructor( private commonService: CommonService,
               private userService: UserService,
               private route: ActivatedRoute,
               private router: Router) { }

  ngOnInit() {
      // this.calWs();
      this.authUser = this.userService.getAuthUser();
      this.appConfig = appConfig;

      this.fetchChatBox();
      $("body").on("click",".panel-heading input[type=checkbox]", function(){


          if ($(this).is(':checked')) {

              $(this).closest(".panel").find('.panel-body').slideDown(400);
          }else{
              $(this).closest(".panel").find('.panel-body').slideUp(400);
          }
      });

      $(document).on('click','.color-bx .form-control',function(){
         $(this).next('.colorpicker').trigger('click');
      });
      this.fetchEmailTemplate();
      this.fetchTeamUser();

  }
    copyToClipboard(text:string) {
        var event = (e: ClipboardEvent) => {
            e.clipboardData.setData('text/plain', text);
            e.preventDefault();
            document.removeEventListener('copy', event);
        }
        document.addEventListener('copy', event);
        document.execCommand('copy');
    }
    /**
     * @func {{saveChatBox}}
     * @param \{{{formobject}}\} {{myform}} {{this will pass the form object}}
     * @description {{this will submit the form data and save the ChatBox data}}
     */
    saveChatBox(myform) {
        console.log(this.model);

        /*let fd = new FormData();
        fd.append('profile_picture', this.model.profile_picture);
        fd.append('api_token', this.authUser.api_token);
        fd.append('first_name', this.model.first_name);
        fd.append('last_name', this.model.last_name);
        fd.append('name', this.model.name	);
        fd.append('email', this.model.email);
        fd.append('phone_number', this.model.phone_number);
        fd.append('designation', this.model.designation);

        //console.log(fd);
        if(this.model.user_id)
        {
            fd.append('user_id', this.model.user_id);
        }*/
        this.model.api_token = this.authUser.api_token;
        this.model.project_id = this.authUser.live_project.id;
        this.userService.saveChatBoxFeature(this.model)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        this.msgs.push({ severity: 'success', summary: 'Success Message', detail: 'ChatBox successfully saved' });
                        this.fetchChatBox();
                        // go to  my project page
                        // location.reload();
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0) {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                                }
                            }

                        }
                        else if (data.error_message) {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                        }
                        else {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message) {
                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

                    }
                    //alert('Something went wrong!');
                }
            );

    }
    /**
     * @func {{fetchChatBox}}
     * @description {{this will submit the form data and save the ChatBox data}}
     */
    fetchChatBox() {
        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);

        this.userService.getChatBox(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        // this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'ChatBox successfully saved'});
                        if (data.chat_box != null) {
                            let chstr = "var "; let i = 1;

                            this.model = data.chat_box;
                            /*this.model.show_chat = this.model.show_chat==1?true:false;
                            this.model.show_directory = this.model.show_directory==1?true:false;
                            this.model.show_timeline = this.model.show_timeline==1?true:false;
                            this.model.show_social = this.model.show_social==1?true:false;
                            this.model.show_preferences = this.model.show_preferences==1?true:false;
                            this.model.show_profile = this.model.show_profile==1?true:false;

                            this.model.status = this.model.status ==1?true:false;*/
                            if (data.chat_box && data.chat_box.chat_tab_config) {
                                this.model.intro_text = data.chat_box.chat_tab_config.intro_text;
                                this.model.first_name_text = data.chat_box.chat_tab_config.first_name_text;
                                this.model.last_name_text = data.chat_box.chat_tab_config.last_name_text;
                                this.model.is_company = data.chat_box.chat_tab_config.is_company == 1 ? true : false;
                                this.model.company_text = data.chat_box.chat_tab_config.company_text;
                                this.model.is_role = data.chat_box.chat_tab_config.is_role == 1 ? true : false;
                                this.model.role_text = data.chat_box.chat_tab_config.role_text;
                                this.model.is_phone = data.chat_box.chat_tab_config.is_phone == 1 ? true : false;
                                this.model.phone_text = data.chat_box.chat_tab_config.phone_text;
                                this.model.email_text = data.chat_box.chat_tab_config.email_text;
                                this.model.is_language = data.chat_box.chat_tab_config.is_language == 1 ? true : false;
                                this.model.language_text = data.chat_box.chat_tab_config.language_text;
                                this.model.gdpr_text = data.chat_box.chat_tab_config.gdpr_text;
                                this.model.start_chat_text = data.chat_box.chat_tab_config.start_chat_text;
                            }

                            if (data.chat_box && data.chat_box.directory_tab_config) {
                                this.model.intro_dir = data.chat_box.directory_tab_config.intro_dir;
                                this.model.first_name_dir = data.chat_box.directory_tab_config.first_name_dir;
                                this.model.last_name_dir = data.chat_box.directory_tab_config.last_name_dir;
                                this.model.is_company_dir = data.chat_box.directory_tab_config.is_company_dir == 1 ? true : false;
                                this.model.company_dir = data.chat_box.directory_tab_config.company_dir;
                                this.model.is_role_dir = data.chat_box.directory_tab_config.is_role_dir == 1 ? true : false;
                                this.model.role_dir = data.chat_box.directory_tab_config.role_dir;
                                this.model.is_phone_dir = data.chat_box.directory_tab_config.is_phone_dir == 1 ? true : false;
                                this.model.phone_dir = data.chat_box.directory_tab_config.phone_dir;
                                this.model.email_dir = data.chat_box.directory_tab_config.email_dir;
                                this.model.is_message_dir = data.chat_box.directory_tab_config.is_message_dir == 1 ? true : false;
                                this.model.message_dir = data.chat_box.directory_tab_config.message_dir;
                                this.model.start_chat_dir = data.chat_box.directory_tab_config.start_chat_dir;
                            }

                            if (data.chat_box && data.chat_box.social_tab_config) {

                                this.model.is_facebook = data.chat_box.social_tab_config.is_facebook == 1 ? true : false;
                                this.model.is_twitter = data.chat_box.social_tab_config.is_twitter == 1 ? true : false;
                                this.model.is_pinterest = data.chat_box.social_tab_config.is_pinterest == 1 ? true : false;
                                this.model.is_linkedin = data.chat_box.social_tab_config.is_linkedin == 1 ? true : false;
                                this.model.is_bookmark = data.chat_box.social_tab_config.is_bookmark == 1 ? true : false;
                                this.model.bookmark_text = data.chat_box.social_tab_config.bookmark_text;
                            }

                            if (data.chat_box && data.chat_box.preference_tab_config) {

                                this.model.show_tags = data.chat_box.preference_tab_config.show_tags == 1 ? true : false;
                                this.model.show_email_me = data.chat_box.preference_tab_config.show_email_me == 1 ? true : false;
                                this.model.email_me_text = data.chat_box.preference_tab_config.email_me_text;
                                this.model.show_marketing = data.chat_box.preference_tab_config.show_marketing == 1 ? true : false;
                                this.model.marketing_text = data.chat_box.preference_tab_config.marketing_text;
                                this.model.show_gdpr = data.chat_box.preference_tab_config.show_gdpr == 1 ? true : false;
                                this.model.gdpr_pref = data.chat_box.preference_tab_config.gdpr_pref;
                                this.model.gdpr_intro_msg = data.chat_box.preference_tab_config.gdpr_intro_msg;
                                this.model.show_more = data.chat_box.preference_tab_config.show_more == 1 ? true : false;
                                this.model.more_text = data.chat_box.preference_tab_config.more_text;
                                this.model.is_functional_cookie = data.chat_box.preference_tab_config.is_functional_cookie == 1 ? true : false;
                                this.model.functional_cookie_text = data.chat_box.preference_tab_config.functional_cookie_text;
                                this.model.functional_cookie_msg = data.chat_box.preference_tab_config.functional_cookie_msg;
                                this.model.is_analytical_cookie = data.chat_box.preference_tab_config.is_analytical_cookie == 1 ? true : false;
                                this.model.analytical_cookie_text = data.chat_box.preference_tab_config.analytical_cookie_text;
                                this.model.analytical_cookie_msg = data.chat_box.preference_tab_config.analytical_cookie_msg;
                                this.model.is_marketing_cookie = data.chat_box.preference_tab_config.is_marketing_cookie == 1 ? true : false;
                                this.model.marketing_cookie_text = data.chat_box.preference_tab_config.marketing_cookie_text;
                                this.model.marketing_cookie_msg = data.chat_box.preference_tab_config.marketing_cookie_msg;
                            }

                            if (data.chat_box && data.chat_box.profile_tab_config) {

                                this.model.avatar_show_type = data.chat_box.profile_tab_config.avatar_show_type;
                                this.model.about_show_type = data.chat_box.profile_tab_config.about_show_type;
                                this.model.phone_show_type = data.chat_box.profile_tab_config.phone_show_type;
                                this.model.email_show_type = data.chat_box.profile_tab_config.email_show_type;
                                this.model.social_show_type = data.chat_box.profile_tab_config.social_show_type;
                                this.model.show_IP_data = data.chat_box.profile_tab_config.show_IP_data;
                                this.model.show_personal_data_request = data.chat_box.profile_tab_config.show_personal_data_request == 1 ? true : false;
                                this.model.cta_btn_text = data.chat_box.profile_tab_config.cta_btn_text;
                                this.model.cta_btn_tooltip = data.chat_box.profile_tab_config.cta_btn_tooltip;
                                this.model.cta_template = data.chat_box.profile_tab_config.cta_template;
                                this.model.show_personal_data_removal = data.chat_box.profile_tab_config.show_personal_data_removal == 1 ? true : false;
                                this.model.personal_removal_text = data.chat_box.profile_tab_config.personal_removal_text;
                                this.model.personal_removal_tooltip = data.chat_box.profile_tab_config.personal_removal_tooltip;
                                this.model.personal_removal_template = data.chat_box.profile_tab_config.personal_removal_template;
                                this.model.show_gdpr_data_manager = data.chat_box.profile_tab_config.show_gdpr_data_manager == 1 ? true : false;
                                this.model.gdpr_data_manager_id = data.chat_box.profile_tab_config.gdpr_data_manager_id;
                            }
                            // console.log(this.model);
                            /*for (var ky in this.model){
                                // console.log(ky +' : '+this.model[ky]);
                                chstr += ky +' = "'+this.model[ky]+'"';
                                if(i<Object.keys(this.model).length)    chstr += ',';
                                else chstr += ';\n';
                                i++;
                            }*/

                            // console.log(chstr);
                            let rmurl = this.appConfig.frontendUrl + 'assets/remote-work/remote.js';
                            rmurl = btoa(rmurl);
                            this.short_code = '<script type="text/javascript">\n' +
                                '   var chidbx ="' + btoa(this.model.id) + '";\n' +
                                '                window.onload=(function(d,s,u){\n' +
                                '                    var e=d.getElementsByTagName(s)[0],$=d.createElement(s);\n' +
                                '                    $.src=atob(u);\n' +
                                '                    e.parentNode.insertBefore($,e)\n' +
                                '                })(document,"script","' + rmurl + '");\n' +
                                '            </script>';
                        }
                        // console.log(this.model);
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0) {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                                }
                            }

                        }
                        else if (data.error_message) {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                        }
                        else {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message) {
                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

                    }
                    //alert('Something went wrong!');
                }
            );

    }

    checkOnOff(val) {
        // console.log(val);
        let shwStatus = 'checked'
        if (val == 0) shwStatus = '';

        return shwStatus;
    }

    setOnOffVal(event) {
        // console.log(event.srcElement.checked);
        // console.log(event.srcElement.value);
        let shwStatus = 1;
        if (event.srcElement.checked == false) shwStatus = 0;

        this.model[event.srcElement.name] = shwStatus;
        // console.log(this.model);
    }
    setLayout() {
        /*console.log(this.model.layout)
        let layout_header_color = ['','#556080','#04363d','#00863f'];
        let layout_bg_color = ['','#f1f1f1','#061415','#f5f5f5'];
        let layout_text_color = ['','#1d206a','#262d30','#263234'];
        this.model.header_color = layout_header_color[this.model.layout];
        this.model.bg_color = layout_bg_color[this.model.layout];
        this.model.text_color = layout_text_color[this.model.layout];*/
    }

    pickColor(event, colorInp) {
        // $("#colorpicker").click();
        // console.log(event.srcElement.value);
        switch (colorInp) {
            case 'header_color': this.model.header_color = event.srcElement.value;
                break;
            case 'header_text_color': this.model.header_text_color = event.srcElement.value;
                break;
            case 'bg_color': this.model.bg_color = event.srcElement.value;
                break;
            case 'text_color': this.model.text_color = event.srcElement.value;
                break;
        }
    }


    setShowIp(event) {
        // console.log(event.srcElement.value);
        this.model.show_IP_data = event.srcElement.value;
    }
    setSac(event) {
        // console.log(event.srcElement.value);
        this.model.social_show_type = event.srcElement.value;
    }
    setSmail(event) {
        // console.log(event.srcElement.value);
        this.model.email_show_type = event.srcElement.value;
    }
    setSphone(event) {
        // console.log(event.srcElement.value);
        this.model.phone_show_type = event.srcElement.value;
    }
    setSabout(event) {
        // console.log(event.srcElement.value);
        this.model.about_show_type = event.srcElement.value;
    }
    setSavatar(event) {
        // console.log(event.srcElement.value);
        this.model.avatar_show_type = event.srcElement.value;
    }

    /**
     * @func {{fetchemailtemplate}}
     * @param \{{{formobject}}\} {{myform}} {{this will pass the form object}}
     */
    fetchEmailTemplate() {
        this.emailtemplate.api_token = this.authUser.api_token,
        this.emailtemplate.message_type = 2;
        this.userService.getRemovalEmails(this.emailtemplate)
            .subscribe(
                (data: any) => {
                    console.log(data);
                    if (data.status === 'success') {
                        this.emailTemplateArray = data.feed;
                    }
                },
                error => {
                    //console.log(error);
                }
            );

    }

    fetchTeamUser(){
     let data = {
        api_token: this.authUser.api_token
        }
        this.userService.Users(data)
            .subscribe(
                (data: any) => {
                    console.log(data);
                    if (data.status === 'success') {
                        this.teamUsers = data.users;
                    }
                },
                error => {
                    //console.log(error);
                }
            );

    }
}

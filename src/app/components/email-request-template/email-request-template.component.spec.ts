import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailRequestTemplateComponent } from './email-request-template.component';

describe('ChatSetupComponent', () => {
  let component: EmailRequestTemplateComponent;
  let fixture: ComponentFixture<EmailRequestTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmailRequestTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailRequestTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

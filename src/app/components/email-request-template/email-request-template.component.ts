import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {Message} from 'primeng/primeng';
import { CommonService } from '../../services/common.service';
import { UserService } from '../../services/user.service';
import { appConfig } from '../../apiurl.config';

declare var $ : any;
@Component({
    selector: 'app-email-request-template',
    templateUrl: './email-request-template.component.html',
    styleUrls: ['./email-request-template.component.css']
})
export class EmailRequestTemplateComponent implements OnInit {
    name = 'ng2-ckeditor';
    ckeConfig: any;
    mycontent: string;
    log: string = '';
    @ViewChild("myckeditor") ckeditor: any;

    searchText = "";
    searchPrcnt = "";
    dropdownList = [];

    /**
     * @property \{{{any}}\} {{appConfig}} {{this will hold the constants for the app}}
     */

    appConfig: any = {};

    /**
     * @property \{{{Message[]}}\} {{msgs}} {{this is growl message model}}
     */

    msgs: Message[] = [];

    /**
     * @property \{{{any}}\} {{model}} {{user form model}}
     */

    model: any = {
        title: '',
        content: '',
        language: '1',
        trigger: '1',
        trigger_time: '1',
        message_type: 2,
    };

    /**
     * @property \{{{any}}\} {{authUser}} {{this will hold the authuser}}
     */

    authUser: any ={};
    /**
     * @property \{{{any}}\} {{taxList}} {{this will hold the tax List}}
     */

    dataList: any =[];
    /**
     * @property \{{{any}}\} {{selcArr}} {{this will hold the selected tax list}}
     */

    selcArr: any =[];
    edtext: string ='';
    platformList: any =[];
    languageList: any =[];
    triggerList: any =[
        {'value':1 ,'label': 'value1' },
        {'value':2 ,'label': 'value2' },
        {'value':3 ,'label': 'value3' },
    ];
    triggerTimeList: any =[
        {'value':1 ,'label': 'value1' },
        {'value':2 ,'label': 'value2' },
        {'value':3 ,'label': 'value3' },
    ];
    messageTypeList: any =[
        {'value':1 ,'label': 'Message Template' },
        {'value':2 ,'label': 'Email Template' },
        // {'value':3 ,'label': 'value3' },
    ];
    
    constructor(
        private commonService: CommonService,
        private userService: UserService) { }
        ngOnInit() {
            this.ckeConfig = {
                allowedContent: false,
                extraPlugins: 'divarea',
                forcePasteAsPlainText: true
              };
            this.authUser = this.userService.getAuthUser();
            this.appConfig = appConfig;
        }
      
        ngAfterViewInit(){
      
              this.getEmailTemplates();
      
              this.getAllPlatforms();
              this.getAllLanguages();
        }
        checkDraft(e) {
        }
        onChange($event: any): void {
            console.log("onChange",$event);
          }
          /**
           * @func {{openTaxmodal}}
           * @param \{{{string}}\} {{$data}} {{popup data val class}}
           * @description {{this will me open the popup accordingly on click by pass data val}}
           */
          openFormModal($data,obj){
            console.log("data",$data);
            console.log("obj",obj);
              this.commonService.openModal($data);
      
              this.model = {
                  title: '',
                  content: '',
                  language: '1',
                  trigger: '1',
                  trigger_time: '1',
                  message_type: 2,
              };
              this.model.type = "Add";
              if(obj){
                  this.model = obj;
                  this.model.language = obj.language_id;
                  this.model.trigger = obj.trigger_type;
                  this.model.type = "Edit";
                //   this.model.content = obj.content.replace(/[\u00A0-\u9999<>\&]/gim, function(i) {
                //                               return '&#'+i.charCodeAt(0)+';';
                //                           });
                this.model.content = obj.content;
                  if(obj.platforms){
                      this.model.platform = [];
                      obj.platforms.forEach((value: any) => {
                          this.model.platform.push(value.id );
                      });
                  }
                  console.log(this.model.platform)
              }
      
              //this.selUserArr = [];
          }
      
          getAllPlatforms() {
      
              this.userService.getPlatforms()
                  .subscribe(
                      (data: any) => {
                          //console.log(data);
                          if (data.status === 'success') {
                              // this.platformList = data.platforms;
                              if(data.platforms)
                              {
                                  data.platforms.forEach((value: any) => {
                                          this.platformList.push({'value':value.id ,'label': value.title });
                                  });
                              }
                          }
                          if (data.status === 'fail') {
                              // message
                              this.msgs = [];
                              if (data.errors && Object.keys(data.errors).length > 0)
                              {
      
                                  for (var key in data.errors) {
                                      if (data.errors.hasOwnProperty(key)) {
                                          //console.log(key + " -> " + p[key]);
                                          this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                      }
                                  }
      
                              }
                              else if(data.error_message)
                              {
                                  this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                              }
                              else
                              {
                                  this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                              }
                          }
                      },
                      error => {
                          //console.log(error);
                          this.msgs = [];
                          if (error && error.message)
                          {
                              this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});
      
                          }
                          //alert('Something went wrong!');
                      }
                  );
      
          }
      
          getAllLanguages() {
      
              this.userService.getLanguages()
                  .subscribe(
                      (data: any) => {
                          //console.log(data);
                          if (data.status === 'success') {
                              // this.platformList = data.platforms;
                              if(data.language)
                              {
                                  data.language.forEach((value: any) => {
                                          this.languageList.push({'value':value.id ,'label': value.name });
                                  });
      
                              }
                          }
                          if (data.status === 'fail') {
                              // message
                              this.msgs = [];
                              if (data.errors && Object.keys(data.errors).length > 0)
                              {
      
                                  for (var key in data.errors) {
                                      if (data.errors.hasOwnProperty(key)) {
                                          //console.log(key + " -> " + p[key]);
                                          this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                      }
                                  }
      
                              }
                              else if(data.error_message)
                              {
                                  this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                              }
                              else
                              {
                                  this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                              }
                          }
                      },
                      error => {
                          //console.log(error);
                          this.msgs = [];
                          if (error && error.message)
                          {
                              this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});
      
                          }
                          //alert('Something went wrong!');
                      }
                  );
      
          }
      
          /**
           * @func {{saveEmailTemplate}}
           * @param \{{{formobject}}\} {{myform}} {{this will pass the form object}}
           * @description {{this will submit the form data and add or update package data}}
           */
          saveEmailTemplate(myform) {
              //console.log(myform);
              this.model.api_token =  this.authUser.api_token;
              //
              console.log(this.model);
      
              this.userService.saveMailTemplate(this.model)
                  .subscribe(
                      (data: any) => {
                          //console.log(data);
                          if (data.status === 'success') {
                              // message
                              this.msgs = [];
                              this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Template Successfully saved'});
      
                              this.closeModalBox();
                              this.getEmailTemplates();
                              // go to  profile page
                          }
                          if (data.status === 'fail') {
                              // message
                              this.msgs = [];
                              if (data.errors && Object.keys(data.errors).length > 0)
                              {
      
                                  for (var key in data.errors) {
                                      if (data.errors.hasOwnProperty(key)) {
                                          //console.log(key + " -> " + p[key]);
                                          this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                      }
                                  }
      
                              }
                              else if(data.error_message)
                              {
                                  this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                              }
                              else
                              {
                                  this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                              }
                          }
                      },
                      error => {
                          console.log(error);
                          this.msgs = [];
                          if (error && error.message)
                          {
                              this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});
      
                          }
                          //alert('Something went wrong!');
                      }
                  );
          }
      
          getEmailTemplates() {
      
              let fd = new FormData();
              fd.append('api_token', this.authUser.api_token);
              fd.append('message_type', '2');
              
              this.userService.emailTemplates(fd)
                  .subscribe(
                      (data: any) => {
                          //console.log(data);
                          if (data.status === 'success') {
                              // message
                              this.msgs = [];
                              // this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Template Successfully saved'});
      
                              this.dataList = data.feed;
                          }
                          if (data.status === 'fail') {
                              // message
                              this.msgs = [];
                              if (data.errors && Object.keys(data.errors).length > 0)
                              {
      
                                  for (var key in data.errors) {
                                      if (data.errors.hasOwnProperty(key)) {
                                          //console.log(key + " -> " + p[key]);
                                          this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                      }
                                  }
      
                              }
                              else if(data.error_message)
                              {
                                  this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                              }
                              else
                              {
                                  this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                              }
                          }
                      },
                      error => {
                          console.log(error);
                          this.msgs = [];
                          if (error && error.message)
                          {
                              this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});
      
                          }
                          //alert('Something went wrong!');
                      }
                  );
          }
      
          closeModalBox(){
              $(".topsecboxbox").hide();
              $(".sectoshow").show();
              $('body').removeClass('open_modal');
              $('.formwrap').css({'display': 'none', 'top': -5200});
              setTimeout(function(){
                  $('.formwrap').css({'display': 'block', 'min-height': '1px'});
              }, 1000);
              $('.ovelay').addClass('hidden');
              $('.cntactadd .container-fluid').css({'min-height':0});
              $('.cntactadd').removeClass('open_popup');
          }
          /**
           * @func {{delStage}}
           * @description {{delete tax}}
           */
          delSingle(_id)
          {
              //console.log(this.model);
              if(confirm("Are you sure to delete the template?"))
              {
                  let fd = new FormData();
                  fd.append('_ids',_id);
                  fd.append('api_token', this.authUser.api_token);
                  //return;
                  this.userService.delEmailTemplates(fd)
                      .subscribe(
                          (data: any) => {
                              //console.log(data);
                              if (data.status === 'success') {
                                  // message
                                  this.msgs = [];
                                  this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Template Successfully Deleted'});
      
                                  // go to  my team page
                                  this.getEmailTemplates();
                              }
                              if (data.status === 'fail') {
                                  // message
                                  this.msgs = [];
                                  if (data.errors && Object.keys(data.errors).length > 0)
                                  {
      
                                      for (var key in data.errors) {
                                          if (data.errors.hasOwnProperty(key)) {
                                              //console.log(key + " -> " + p[key]);
                                              this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                          }
                                      }
      
                                  }
                                  else if(data.error_message)
                                  {
                                      this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                                  }
                                  else
                                  {
                                      this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                                  }
                              }
                          },
                          error => {
                              console.log(error);
                              this.msgs = [];
                              if (error && error.message)
                              {
                                  this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});
      
                              }
                              //alert('Something went wrong!');
                          }
                      );
      
              }
      
      
          }
      
          /**
           * @func {{delAll}}
           * @description {{delete selected taxes}}
           */
          delAll()
          {
              if(confirm("Are you sure to delete all selected templates?"))
              {
                  let fd = new FormData();
                  fd.append('_ids',this.selcArr);
                  fd.append('api_token', this.authUser.api_token);
                  //return;
                  this.userService.delEmailTemplates(fd)
                      .subscribe(
                          (data: any) => {
                              //console.log(data);
                              if (data.status === 'success') {
                                  // message
                                  this.msgs = [];
                                  this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Templates are successfully deleted'});
                                  this.getEmailTemplates();
                              }
                              if (data.status === 'fail') {
                                  // message
                                  this.msgs = [];
                                  if (data.errors && Object.keys(data.errors).length > 0)
                                  {
      
                                      for (var key in data.errors) {
                                          if (data.errors.hasOwnProperty(key)) {
                                              //console.log(key + " -> " + p[key]);
                                              this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                          }
                                      }
      
                                  }
                                  else if(data.error_message)
                                  {
                                      this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                                  }
                                  else
                                  {
                                      this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                                  }
                              }
                          },
                          error => {
                              console.log(error);
                              this.msgs = [];
                              if (error && error.message)
                              {
                                  this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});
      
                              }
                              //alert('Something went wrong!');
                          }
                      );
      
              }
          }
      
          selctToList($event) {
              //console.log($event.srcElement.checked);
              //console.log($event.srcElement.value);
              if($event.srcElement.checked == true)
              {
                  if(this.selcArr.indexOf($event.srcElement.value)== -1)
                      this.selcArr.push($event.srcElement.value);
              }else {
      
                  this.selcArr.splice(this.selcArr.indexOf($event.srcElement.value), 1);
              }
              //console.log(this.selcArr);
          }
}
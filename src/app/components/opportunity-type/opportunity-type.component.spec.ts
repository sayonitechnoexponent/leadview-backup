import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpportunityTypeComponent } from './opportunity-type.component';

describe('OpportunityTypeComponent', () => {
  let component: OpportunityTypeComponent;
  let fixture: ComponentFixture<OpportunityTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpportunityTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpportunityTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

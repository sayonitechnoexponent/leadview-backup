import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../services/common.service';
import { UserService } from '../../services/user.service';
import {Message} from 'primeng/primeng';
import { appConfig } from '../../apiurl.config';


@Component({
  selector: 'app-opportunities',
  templateUrl: './opportunities.component.html',
  styleUrls: ['./opportunities.component.css']
})
export class OpportunitiesComponent implements OnInit {

    /**
     * @property \{{{any}}\} {{appConfig}} {{this will hold the constants for the app}}
     */

    appConfig: any = {};

    /**
     * @property \{{{Message[]}}\} {{msgs}} {{this is growl message model}}
     */

    msgs: Message[] = [];

    /**
     * @property \{{{any}}\} {{model}} {{this form model}}
     */

    opportunityModel: any = {};

    /**
     * @property \{{{any}}\} {{authUser}} {{this will hold the authuser}}
     */

    authUser: any ={};
    /**
     * @property \{{{any}}\} {{myList}} {{this will hold the list data}}
     */

    myList: any;

    /**
     * @property \{{{any}}\} {{selArr}} {{this will hold the selected from list}}
     */

    selArr: any =[];
    contactList : any =[];
    companyList : any =[];
    googoleAutoData : any = {};
    opportunityTypeList : any =[];
    selectedItems : any =[];
    selectedContact : any =[];
    selectedCompany : any =[];
    allUsers: any = [];
    managerList: any = [];
    manager_filter: any = [];
    selectedMngrs: any = [];
    searchText : any;
    checkLocation:boolean = false;
    checkDate:boolean = false;
    checkTime:boolean = false;
    opportunityList : any =[];
    opportunityDd : any =[];
    is_submit:boolean = false;

    dueDate: Date;
    closerDate: Date;
    dateTime: Date;
    startDate: Date;
    endDate: Date;

    dropdownSettings = {};
    userSettings1 : any ={'inputString': ''};
    showAttachment : any = '';
    search_key : string = '';
    dudtKey : string = '';
    status_key : string = '';

    oppTypeList: any = [];
    oppStageList: any = [];
    profitMargin: number = 0;
    budgetVal: number = 0;
    profitVal: number = 0;
    stageFlt: string = '';
    current_view : string = 'L';

    constructor(
        private commonService: CommonService,
        private userService: UserService) { }

    ngOnInit() {

        this.authUser = this.userService.getAuthUser();
        this.appConfig = appConfig;
        this.fetchOpportunities();
        this.fetchTeamUsers();
        this.listContacts();
        this.fetchOppType();
        this.fetchStage();
        this.dropdownSettings = {
            singleSelection: false,
            text:"Select",
            enableCheckAll: false,
            selectAllText:'Select All',
            unSelectAllText:'UnSelect All',
            enableSearchFilter: true,
            classes:"myclass custom-class"
        };
    }

    changeView(v){
        this.current_view = v;
    }

    receiveView(data){
        this.current_view = data;
    }

    changeProfitMagin($event) {
        //console.log($event.srcElement.value);
        let jid = $event.srcElement.value;
        this.profitMargin = this.oppStageList.find(item => item.id == jid).profit_margin;
        this.calculateProfit();
    }

    getBudget($event) {
        //console.log($event.srcElement.value);
        this.budgetVal = $event.srcElement.value;
        this.calculateProfit();
    }

    calculateProfit() {
        //console.log($event.srcElement.value);
        this.profitVal = (this.budgetVal * this.profitMargin)/100;
        this.opportunityModel.expected_profit = this.profitVal;
    }
    /**
     * @func {{fetchStage}}
     * @description {{get all Stages}}
     */
    fetchStage() {
        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);

        this.userService.getOpportunityStages(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        //this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Project Post Successful'});
                        this.oppStageList =  (data.feed);
                        //console.log(this.myTeam);
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );

    }

    fetchOppType() {
        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);

        this.userService.getOpportunityTypes(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        //this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Project Post Successful'});
                        this.oppTypeList =  (data.feed);
                        //console.log(this.oppTypeList);
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );

    }

    showStatus(dueDate){
        let date = new Date();
        let currentTimestamp = date.getTime();
        var status = 'Open';
        if(dueDate !== null){
            let dudt = dueDate*1000;
            if(currentTimestamp < dudt) status = 'Open';
            else status = 'Close';
        }

        return status;
    }
    strToTimestamp(strDate){
        var datum = Date.parse(strDate);
        return datum/1000;
    }
    /**
     * @func {{onFileChange}}
     * *@param  {{event}} {{this will pass the change event}}
     * @description {{get the file}}
     */

    onFileChange(event,modType) {

        if(event.target.files && event.target.files.length > 0) {
            let file = event.target.files[0];
            //console.log(file);
            this.opportunityModel.attachment = file;
        }
    }
    /**
     * @func {{pageLoad}}
     * @description {{reload page after a certain time}}
     */
    pageLoad(){
        setTimeout(()=>{    //<<<---    using ()=> syntax
            location.reload()
        },1000);
    }

    onItemSelect(item:any){
        //console.log(item.id);

        if( this.selectedItems.indexOf(item.id)== -1)
        {
            this.selectedItems.push(item.id);
        }

        //console.log(this.selectedItems);
        console.log(this.opportunityModel.managers);
    }

    OnItemDeSelect(item:any){
        //console.log(item.id);

        if(this.selectedItems.indexOf(item.id) != -1)
            this.selectedItems.splice(this.selectedItems.indexOf(item.id), 1);
        //console.log(this.selectedItems);

    }
    /**
     * @func {{openCreateTask}}
     * @param \{{{string}}\} {{$data}} {{popup data val class}}
     * @description {{this will be open the popup accordingly on click by pass data val}}
     */
    openCreateModal($data,obj){
        this.userSettings1 ={'inputString': ''};
        this.dateTime = null;
        if(obj){
            // console.log(obj);
            this.selectedItems =[];
            this.opportunityModel = obj;
            this.opportunityModel.managers =[];
            this.opportunityModel.openType = "Edit";
            this.opportunityModel.opp_type = obj.opportunity_type_id;
            this.opportunityModel.opp_stage = obj.opportunity_stage_id;

            this.opportunityModel.expected_profit = obj.profit_margin;
            this.profitVal = obj.profit_margin;
            this.budgetVal = obj.budget;
            this.profitMargin = this.oppStageList.find(item => item.id == obj.opportunity_stage_id).profit_margin;

            if(obj.attachment &&obj.attachment !='' ){
                this.showAttachment =  this.opportunityModel.attachment;
            }
            delete this.opportunityModel.attachment;

            obj.user_posts.forEach((value: any) => {
                if(this.authUser.id != value.user_id){
                    this.selectedItems.push(value.user_id);
                    let mngr = {id: value.user_id, itemName: value.user.name};

                    this.opportunityModel.managers.push(mngr);
                }
            });
            this.closerDate = new Date(obj.expected_closer_date*1000);

        }
        else{
            this.contactList =[];
            this.companyList =[];
            this.googoleAutoData = {};
            // this.opportunityTypeList =[];
            this.selectedItems =[];
            this.selectedContact =[];
            this.selectedCompany =[];
            this.opportunityModel = {};
            this.opportunityModel.openType = "Add";
        }
        this.commonService.openModal($data);

        // this.listContacts();
        //this.listCompanies();
    }


    kywordFilter(event){
        console.log(event.srcElement.value);
        this.search_key = event.srcElement.value;
        this.fetchOpportunities();
    }
    stageFilter(event){
        console.log(event.srcElement.value);
        this.stageFlt = event.srcElement.value;

        this.fetchOpportunities();
    }
    /**
     * @func {{fetchType}}
     * @description {{get all Type}}
     */
    fetchOpportunities() {
        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);
        fd.append('search_key', this.search_key);
        fd.append('manager_filter', this.manager_filter);
        fd.append('due_date', this.dudtKey);
        fd.append('stage_filter', this.stageFlt);
        fd.append('status', this.status_key);

        this.userService.fetchAssignOpportunities(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        //this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Project Post Successful'});
                        this.myList =  (data.feed);
                        //console.log(this.myTeam);
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );

    }

    changeTaskForm($event) {
        //console.log($event.srcElement.value);
        let jid = $event.srcElement.value;
        let selctDta = this.opportunityTypeList.find(item => item.id == jid);
        //console.log(selctDta);
        this.checkLocation = selctDta.show_location == 1 ? true:false;
        this.checkDate = selctDta.show_date == 1 ? true:false;
        this.checkTime = selctDta.show_time == 1 ? true:false;
    }

    managerFilter(event){
        // console.log(event.srcElement.value);
        // this.manager_filter = event.srcElement.value;
        this.manager_filter = this.selectedMngrs;
        this.fetchOpportunities();
    }
    statusFilter(event){
        this.status_key = event.target.value;
        console.log(this.status_key);
        this.fetchOpportunities();
    }
    clearFilter(event){
        this.dueDate = null;
        this.dudtKey='';
        this.fetchOpportunities();
    }
    ddFilter(duDate){
        // console.log(event.srcElement.value);
        // this.manager_filter = event.srcElement.value;
        // this.manager_filter = this.dueDate;
        // console.log(duDate);
        this.dudtKey = (duDate.getTime()/1000).toString();
        console.log(this.dudtKey);
        this.fetchOpportunities();
    }

    /**
     * @func {{fetchTeamUsers}}
     * @description {{get user teams}}
     */
    fetchTeamUsers() {
        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);
        fd.append('page_type', 'contacts');
        this.userService.getTeamUsers(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        //this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Project Post Successful'});
                        //this.allUsers =  (data.users);
                        data.users.forEach((value: any) => {
                            if(value.id != this.authUser.id) {
                                this.allUsers.push({'id':value.id ,'itemName': value.name });
                                this.managerList.push({'value':value.id ,'label': value.name });
                            }
                        });
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});
                    }
                    //alert('Something went wrong!');
                }
            );

    }

    listContacts(){
        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);
        let group2 =[];
        this.userService.getContacts(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        //this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Project Post Successful'});
                        //conlist =  (data.feed);
                        data.feed.forEach((value: any) => {
                            if(value.type==1)    this.contactList.push({'id':value.id ,'itemName': value.name });
                            else if(value.type==2)    this.companyList.push({'id':value.id ,'itemName': value.name });

                        });
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );
        //console.log(this.contactList);

    }

    /**
     * @func {{addTask}}
     * @param \{{{formobject}}\} {{myform}} {{this will pass the form object}}
     * @description {{this will submit the form data and update the project data}}
     */
    addSaveOpportunity(myform)
    {

        this.is_submit = true;
        if(this.closerDate) this.opportunityModel.closer_date= (this.closerDate.getTime())/1000;

        //console.log(this.opportunityModel);
        let fd = new FormData();

        if(this.opportunityModel.id){
            fd.append('_id', this.opportunityModel.id);
        }
        fd.append('opportunity_type', this.opportunityModel.opp_type);
        fd.append('attachment', this.opportunityModel.attachment);
        fd.append('title', this.opportunityModel.title);
        fd.append('description', this.opportunityModel.description);
        fd.append('managers', this.selectedItems);
        fd.append('contacts', this.opportunityModel.contacts);
        fd.append('companies', this.opportunityModel.companies);
        fd.append('api_token', this.authUser.api_token);
        fd.append('closer_date', this.opportunityModel.closer_date);
        fd.append('budget', this.opportunityModel.budget);
        fd.append('opportunity_stage', this.opportunityModel.opp_stage);
        fd.append('profit_margin', this.profitVal.toString());

        fd.append('curr_user', this.authUser.id);
        fd.append('tl_type', 'user');
        // console.log(this.opportunityModel);
        //this.noteModel.api_token = this.authUser.api_token;

        this.userService.addEditOpportunity(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    this.is_submit = false;
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Opportunity saved successfully'});

                        //this.fetchUserTimeLine();
                        // reload page
                        this.pageLoad();
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    this.is_submit = false;
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );

    }

    /**
     * @func {{delTask}}
     * @description {{delete tax}}
     */
    delOpportunity(_id)
    {
        //console.log(this.model);
        if(confirm("Are you sure to delete the Opportunity?"))
        {
            let fd = new FormData();
            fd.append('_ids',_id);
            fd.append('api_token', this.authUser.api_token);
            //return;
            this.userService.deleteOpportunity(fd)
                .subscribe(
                    (data: any) => {
                        //console.log(data);
                        if (data.status === 'success') {
                            // message
                            this.msgs = [];
                            this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Opportunity Successfully Deleted'});

                            // go to  my team page
                            this.pageLoad();
                        }
                        if (data.status === 'fail') {
                            // message
                            this.msgs = [];
                            if (data.errors && Object.keys(data.errors).length > 0)
                            {

                                for (var key in data.errors) {
                                    if (data.errors.hasOwnProperty(key)) {
                                        //console.log(key + " -> " + p[key]);
                                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                    }
                                }

                            }
                            else if(data.error_message)
                            {
                                this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                            }
                            else
                            {
                                this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                            }
                        }
                    },
                    error => {
                        console.log(error);
                        this.msgs = [];
                        if (error && error.message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                        }
                        //alert('Something went wrong!');
                    }
                );

        }
    }
    /**
     * @func {{delAll}}
     * @description {{delete selected taxes}}
     */
    delAll()
    {
        if(confirm("Are you sure to delete all selected Opportunity?"))
        {
            let fd = new FormData();
            fd.append('_ids',this.selArr);
            fd.append('api_token', this.authUser.api_token);
            //return;
            this.userService.deleteOpportunity(fd)
                .subscribe(
                    (data: any) => {
                        //console.log(data);
                        if (data.status === 'success') {
                            // message
                            this.msgs = [];
                            this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Opportunities are successfully deleted'});

                            this.pageLoad();
                        }
                        if (data.status === 'fail') {
                            // message
                            this.msgs = [];
                            if (data.errors && Object.keys(data.errors).length > 0)
                            {

                                for (var key in data.errors) {
                                    if (data.errors.hasOwnProperty(key)) {
                                        //console.log(key + " -> " + p[key]);
                                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                    }
                                }

                            }
                            else if(data.error_message)
                            {
                                this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                            }
                            else
                            {
                                this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                            }
                        }
                    },
                    error => {
                        console.log(error);
                        this.msgs = [];
                        if (error && error.message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                        }
                        //alert('Something went wrong!');
                    }
                );

        }
    }

    selctToList($event) {
        //console.log($event.srcElement.checked);
        //console.log($event.srcElement.value);
        if($event.srcElement.checked == true)
        {
            if(this.selArr.indexOf($event.srcElement.value)== -1)
                this.selArr.push($event.srcElement.value);
        }else {

            this.selArr.splice(this.selArr.indexOf($event.srcElement.value), 1);
        }
        console.log(this.selArr);
    }

}

import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';

import { CommonService } from '../../../services/common.service';
import { UserService } from '../../../services/user.service';
import { Message} from 'primeng/primeng';
import { appConfig } from '../../../apiurl.config';

import { CalendarComponent } from 'ng-fullcalendar';
import { Options } from 'fullcalendar';

@Component({
  selector: 'app-opportunities-calendar',
  templateUrl: './opportunities-calendar.component.html',
  styleUrls: ['./opportunities-calendar.component.css']
})

export class OpportunitiesCalendarComponent implements OnInit {
    /**
     * @property \{{{any}}\} {{appConfig}} {{this will hold the constants for the app}}
     */

    appConfig: any = {};

    /**
     * @property \{{{Message[]}}\} {{msgs}} {{this is growl message model}}
     */

    msgs: Message[] = [];

    /**
     * @property \{{{any}}\} {{model}} {{this form model}}
     */

    authUser: any ={};
    /**
     * @property \{{{any}}\} {{myList}} {{this will hold the list data}}
     */

    taskList: any =[];
    calendarOptions: Options;
    displayEvent: any;
    @ViewChild(CalendarComponent) ucCalendar: CalendarComponent;
    @Input() taskData: any;

    @Output() viewEvent = new EventEmitter<string>();
    @Output() openModalEvent = new EventEmitter<string>();

    constructor(
        private commonService: CommonService,
        private userService: UserService) { }

    ngOnInit() {
        this.authUser = this.userService.getAuthUser();
        this.fetchEventData();
        // console.log(this.taskData);
    }

    changeView(v){
        this.viewEvent.emit(v);
    }

    openAddModal(d){
        this.openModalEvent.emit(d);
    }

    fetchEventData() {
        let that = this;
        const dateObj = new Date();
        const yearMonth = dateObj.getUTCFullYear() + '-' + (dateObj.getUTCMonth() + 1);
        // return (data);
        this.taskData.forEach((value: any) => {
            if(value.id != this.authUser.id) {
                let ed = null;
                if(value.opportunity.expected_closer_date){
                    ed = new Date(value.opportunity.expected_closer_date*1000);
                    // console.log(ed)
                }
                this.taskList.push({title: value.opportunity.title , start: value.opportunity.created_at, end: ed });
            }
        });
        // console.log(this.taskList);
        this.calendarOptions = {
            editable: true,
            eventLimit: false,
            header: {
                left: 'custom1 agendaWeek month', /*,agendaDay,listMonth*/
                center: 'title',
                right: 'prev,today,next custom2',

            },
            buttonText:{
                today:    'Today',
                month:    'Month View',
                week:     'Week View',
            },
            customButtons: {
                custom1: {
                    text: 'List View',
                    click: function() {
                        // alert('clicked custom button 1!');
                        that.changeView('L');
                    }
                },
                custom2: {
                    text: 'Add Opportunity',
                    click: function() {
                        // alert('clicked custom button 1!');
                        that.openAddModal('addoportunity');
                    }
                },

            },
            events: this.taskList
        };
    }

    clickButton(model: any) {
        // console.log(model);
        this.displayEvent = model;
    }

    eventClick(model: any) {
        model = {
            event: {
                id: model.event.id,
                start: model.event.start,
                end: model.event.end,
                title: model.event.title,
                allDay: model.event.allDay
                // other params
            },
            duration: {}
        }
        this.displayEvent = model;
    }

    updateEvent(model: any) {
        model = {
            event: {
                id: model.event.id,
                start: model.event.start,
                end: model.event.end,
                title: model.event.title
                // other params
            },
            duration: {
                _data: model.duration._data
            }
        }
        this.displayEvent = model;
    }

}

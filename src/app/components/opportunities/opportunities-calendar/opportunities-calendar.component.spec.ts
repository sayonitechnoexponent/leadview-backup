import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpportunitiesCalendarComponent } from './opportunities-calendar.component';

describe('OpportunitiesCalendarComponent', () => {
  let component: OpportunitiesCalendarComponent;
  let fixture: ComponentFixture<OpportunitiesCalendarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpportunitiesCalendarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpportunitiesCalendarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

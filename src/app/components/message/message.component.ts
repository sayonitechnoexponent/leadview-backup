import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { CommonService } from '../../services/common.service';
import { UserService } from '../../services/user.service';
import { DataService } from "../../services/data.service";
import {Message} from 'primeng/primeng';
import { appConfig } from '../../apiurl.config';

import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';


import * as socketIo from 'socket.io-client';
import {json} from "ng2-validation/dist/json";
// import moment = require("moment");
import * as moment from 'moment';
const socket = socketIo(appConfig.socketUrl);
declare var $: any;

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit {
    modalRef: BsModalRef;
    /**
     * @property \{{{any}}\} {{appConfig}} {{this will hold the constants for the app}}
     */

    appConfig: any = {};

    /**
     * @property \{{{Message[]}}\} {{msgs}} {{this is growl message model}}
     */

    msgs: Message[] = [];
    /**
     * @property \{{{any}}\} {{authUser}} {{this will hold the authuser}}
     */

    authUser: any ={};
    chatMsg: any ;
    attachList: any ;
    curr_project_users: any =[];
    room_name: string = '';
    in_room : boolean = false;
    remote_user : any = [];
    remote_ip_arr : any = [];
    live_user : any = [];
    my_socket_id : string = "";
    visitor_socket_ids : any = [];
    to_chat_socket_id : string = "";
    msg_visitor_id : string = "";
    msg_to_ip : string = "";
    myIp : string = "";
    blob_file : string = "";
    blob_file_name : string = "";
    curr_msg : string = "";
    post_arr : any = [];
    is_split : boolean = false;
    curr_activities : any = [];
    remote_page : string = "";
    flag : number = 0;
    selctGroupUsers : any = [];
    group_members : any = [];
    is_default_operator : number = 0;
    selected_contact : any = null;
    operator_chat_setting : any = null;
    user_chat_setting : any = null;

  constructor(
      private modalService: BsModalService,
      private commonService: CommonService,
      private userService: UserService,
      private dataService: DataService,
      private route: ActivatedRoute,
      private router: Router,
      private cdRef:ChangeDetectorRef) { }

    ngAfterViewChecked()
    {
        this.cdRef.detectChanges();
    }


    ngAfterViewInit(){
        $("body").on("click",".makesplit", function(){
            $("body").toggleClass("splitepage");
        });
        /*if(socktId != that.to_chat_socket_id){
            let pgurl = "";
            if(item.user_activities) pgurl = item.user_activities[0].page_url;
            that.dataService.chageLiveWindow(pgurl);
        }*/
    }

    ngOnInit() {
        this.authUser = this.userService.getAuthUser();
        // console.log(this.authUser);
        this.appConfig = appConfig;

       /*if(!localStorage.getItem('chat_setting')){
           console.log('!chat_setting');
           localStorage.setItem('chat_setting', JSON.stringify(this.authUser.chat_setting));
       }*/

        // let chat_setting  = JSON.parse(localStorage.getItem('chat_setting'));
        // console.log(chat_setting);

        if(this.user_chat_setting)    this.is_default_operator = parseInt(this.user_chat_setting.is_default_operator);

        this.fetchProjectUsers();

        this.userService.getIpFromClient().subscribe(
            (data: any) => {
                // console.log(data);
                this.myIp = data.ip;
                // this.agentChatSet();
            });

        // console.log('myIp: '+ip);

        let that = this;

        socket.on('connect', function() {
            // Connected, let's sign-up for to receive messages for this room
            if(that.authUser.live_project)    that.room_name = that.authUser.live_project.title;
            else if(that.authUser.in_projects.length > 0 && that.authUser.type==1)    that.room_name = that.authUser.in_projects[0].title;
            that.my_socket_id = socket.id;
            // console.log(that.my_socket_id);
            // console.log(that.room_name);
			if(that.authUser.chat_setting)  that.is_default_operator = that.authUser.chat_setting.is_default_operator;
            console.log(that.is_default_operator);
            let sdata = {email: that.authUser.email, userId: that.authUser.id, room: that.room_name, userType:that.authUser.type, socket_id: that.my_socket_id, isDefault: that.is_default_operator, type: 'agent'};

            if(that.room_name !='' && that.authUser.type==1){
                // console.log('connent');
                socket.emit('room',sdata);
                that.getLiveVisitors();
                // console.log(that.room_name);
                this.flag = 0;
            }
        });

        socket.on('user_offline', function(res) {
            // console.log(res);
            if (that.remote_user.indexOf(res) != -1) {
                let rmvindx = -1;
                let rmvIp = 0;
                that.live_user.find( function(item) {
                    if(item.socket_id == res){
                        item.online = false;
                        // rmvindx = that.live_user.indexOf(item);
                        that.remote_ip_arr.splice(item.usr);
                        item.messages = [];
                        // item.file_list = [];
                    }
                });
                // that.chatMsg = [];
                that.attachList = [];
                /* if(rmvindx > -1) that.live_user.splice(rmvindx,1);*/

                that.remote_user.splice(that.remote_user.indexOf(res));

                // console.log("offline : "+res);
                that.changeLiveStatus(res,0);
            }
        });

        socket.on('visitor_change', function(res) {
            // if(that.authUser.type ==2)
            console.log('vcc');
            that.getLiveVisitors();
        });

        socket.on('receive_message', function(msgParam){

            /*console.log('message sender: ' + msgParam.sender_ip);
            console.log('message: ' + msgParam.message);*/
           console.log('receive_message ',msgParam);
           setTimeout(function () {

               let msg_obj = msgParam.message;
               if(msgParam.is_file == 'Y'){
                   msg_obj = '<a download=""  target="_blank" href="'+appConfig.publicUrl+'uploads/message_attachment/'+msgParam.message+'">'+msgParam.message+'</a>';
               }
               let msgUser = msgParam.sender_ip;
               let msgUserPic = 'assets/images/ctavatar_default.png';
               if(msgParam.type=='a'){
                   msgUser = msgParam.from_user.first_name+' '+msgParam.from_user.last_name;

                   if(!msgParam.from_user.profile_picture || msgParam.from_user.profile_picture !='undefined')
                   {
                       msgUserPic = appConfig.publicUrl+'uploads/profile_picture/thumbs/'+msgParam.from_user.profile_picture;
                   }else{
                       msgUserPic = 'assets/images/ctavatar_default2.png';
                   }
               }
               let styl = (msgParam.type=='a' && msgParam.from_user.id == that.authUser.id )?'me':'';
               let mymsg = '<div class="list ">\n' +
                   '            <div class="msgUser"><img src="'+msgUserPic+'" class="img-responsive"></div>\n' +
                   '            <div class="msgTxt">\n' +
                   '              <span class="nm">'+msgUser+'<br><span class="date">'+moment().format('MMMM Do YYYY, h:mm:ss a')+'</span></span>\n' +
                   '              <p>'+msg_obj+'</p>\n' +
                   '            </div>\n' +
                   '          </div>';

               if(mymsg !='' && msgParam.type=='v' && that.msg_to_ip == msgParam.sender_ip)  that.chatMsg.push(mymsg);
               else if(mymsg !='' && msgParam.type=='a' && that.msg_to_ip == msgParam.to_ip)  that.chatMsg.push(mymsg);

               // if(that.remote_ip_arr.indexOf(msgParam.sender_ip)!= -1)
               if(msgParam.type=='v')
               {
                   let newUserFlag = 1;
                   that.live_user.find( function(item) {
                       if(item.usr == msgParam.sender_ip){
                           newUserFlag = 0;
                           if(item.usr != that.msg_to_ip)    item.unread = item.unread+1;
                           // item.messages.push(mymsg);
                           if(msgParam.is_file == 'Y'){
                               item.file_list.push({name:msgParam.message, file:msgParam.message, sender_ip:msgParam.sender_ip, created_at : moment().format('MMMM Do YYYY, h:mm:ss a')});
                           }
                       }
                   } );

                   if(newUserFlag == 1){
                       let status = true;
                       let feedMsg = [];
                       let attachMsg = [];

                       let actvts = null;
                       if(msgParam.from_user)
                       {
                           if( msgParam.from_user.activities)    actvts = msgParam.from_user.activities;
                           let tagscore = 0;
                           if(msgParam.from_user.tagscores){
                               msgParam.from_user.tagscores.forEach((tgscr: any) => {
                                   tagscore += parseInt(tgscr.score);
                               });
                           }

                           let usr_set = {usr: msgParam.sender_ip, visitor_id: msgParam.from_user.id, socket_id: msgParam.from_user.visitor_socket_id, country_code: msgParam.from_user.country_code, online: status, typing: false, unread: 0, messages: feedMsg, file_list: attachMsg.reverse(), user_activities : actvts, tag_score : tagscore};
                           that.live_user.unshift(usr_set);
                           that.remote_user.push(msgParam.from_user.visitor_socket_id);
                           that.remote_ip_arr.push(msgParam.sender_ip);
                           // that.chatMsg.push(mymsg);
                           that.setUsertoChat(msgParam.sender_ip,msgParam.from_user.id);
                       }

                   }
                   // console.log(that.live_user);
               }
               else if(msgParam.type=='a')
               {
                   that.live_user.find( function(item) {
                       if(item.usr == msgParam.to_ip){
                           if(item.usr != that.msg_to_ip)    item.unread = item.unread+1;
                           // item.messages.push(mymsg);
                           if(msgParam.is_file == 'Y'){
                               item.file_list.push({name:msgParam.message, file:msgParam.message, sender_ip:msgParam.sender_ip, created_at : moment().format('MMMM Do YYYY, h:mm:ss a')});
                           }
                       }
                   } );
               }

           },500);

           // if(this.msg_visitor_id == msgParam.from_user.id && this.msg_to_ip == msgParam.from_user.ip_address){}
               that.setUsertoChat(that.msg_to_ip,that.msg_visitor_id);

            // that.chatMsg.push(mymsg);

        });

        socket.on('start_typing', function(res){

            if(res.type=='remote'){
                if(that.remote_user.indexOf(res.user)!= -1)
                {
                    that.live_user.find( function(item) {
                        if(item.socket_id == res.user  && item.typing == false){
                            item.typing = true;
                            setTimeout(function () {
                                item.typing = false;
                            }, 1000);
                        }
                    } );
                }
                // ru.push(res.room);
                // this.remote_user=ru;
            }else{
                // this.room_name = res.room;

            }
        });

        socket.on('add_group', function(res) {
            // console.log(res);
            let members = res.members;
            console.log(members);
            if(members.indexOf(that.authUser.id) != -1){

                that.saveCMtoVisitor(res.visitor_id);
                // console.log(that.authUser.id)
                let req_data = { visitor_ip: res.visitor_ip, left_room: res.toRoom};
                socket.emit('join_group',req_data);
                setTimeout(function () {
                    that.getLiveVisitors();
                }, 1000);

            }
        });

        // if(this.authUser.type ==2)
    }


    selctUsr(event){
        if(event.srcElement.checked == true){
            console.log(event.srcElement.checked);
            // if( this.selctGroupUsers.indexOf(event.srcElement.value)== -1)
                this.selctGroupUsers.push(parseInt(event.srcElement.value));
        }else{
            console.log(event.srcElement.checked);
            this.selctGroupUsers.splice(this.selctGroupUsers.indexOf(parseInt(event.srcElement.value)));

        }
    }

    addToGroup(templt){
      console.log(this.selctGroupUsers);
        let toRoom = this.room_name+'_user';

        if(this.selctGroupUsers.length > 0 && this.msg_to_ip !='' && this.msg_visitor_id !=''){
            let req_data = {members:this.selctGroupUsers, visitor_id: this.msg_visitor_id, visitor_ip: this.msg_to_ip, toRoom: toRoom};
            socket.emit('group_req',req_data);
            this.modalRef.hide();
            this.msgs = [];
            this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Users Successfully added to group chat.'});
        }
    }

    saveCMtoVisitor(visitorId){
        let fd = new FormData();
        fd.append('visitor_id', visitorId);
        fd.append('api_token', this.authUser.api_token);
        this.userService.addCmGroup(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        //this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Project Post Successful'});
                        this.curr_project_users =  (data.users);
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);

                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }

                }
            );
    }

    openModal(template) {
        this.modalRef = this.modalService.show(template);
    }

    splitToggel(){
        if(this.is_split == true){
            this.is_split = false;
        }
        else{
             this.is_split = true;
        }
    }

    // file to base64
    getBase64(file ) {
        //console.log(file);
        return new Promise(function(resolve, reject) {
            var reader = new FileReader();
            reader.onload = function() { resolve(reader.result); };
            reader.onerror = reject;

            //console.log(reader.readAsDataURL(file));
            reader.readAsDataURL(file);
        });
    }

    selectFile(event) {
        this.blob_file = '';
        this.blob_file_name = '';
        let anyfile = event.target.files[0];
        // console.log(anyfile);

        if(anyfile){
            this.blob_file_name = anyfile.name;
            this.blob_file = anyfile;
           /* let blob = null;
            if(anyfile) blob = this.getBase64( anyfile );
            var that = this;
            blob.then(function(result) {
                //console.log(result);
                //that.blob_picture = that.sanitizer.bypassSecurityTrustUrl(result);
                that.blob_file = (result);
                console.log(that.blob_file);
            });*/
        }
    }

    nowSendMsg(event){
        // if(this.curr_msg =='')  this.curr_msg = this.blob_file_name;
        if(this.curr_msg =='' && this.blob_file_name == '')
        {
            this.msgs = [];
            this.msgs.push({severity: 'error', summary: 'Error Message', detail: 'Please write your message'});
        }else{

            this.sendMyMsg(this.curr_msg);
        }
    }
    /**
     * @func {{saveLiveVisitors}}
     * @description {{get user teams}}
     */
    /*saveLiveVisitors(param) {
        let fd = new FormData();
        fd.append('ip_address', param.vip);
        fd.append('project_id', param.project_id);
        fd.append('socket_id', param.socket_id);
        fd.append('agent_socket_id', param.agent_socket_id);
        fd.append('page_url', param.page_url);
        fd.append('page_title', param.page_title);
        fd.append('api_token', this.authUser.api_token);
        this.userService.savelivVisitor(fd)
            .subscribe(
                (data: any) => {
                    // console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        //this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Project Post Successful'});
                        // this.curr_project_users =  (data.users);
                        // localStorage.setItem('leaduser', JSON.stringify(data.user));
                        this.getLiveVisitors();
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);

                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }

                }
            );
    }*/

    /**
     * @func {{fetchTeamUsers}}
     * @description {{get user teams}}
     */
    fetchProjectUsers() {
        let fd = new FormData();
        // fd.append('page_type', 'contacts');
        fd.append('api_token', this.authUser.api_token);
        this.userService.getTeamUsers(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        //this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Project Post Successful'});
                        this.curr_project_users =  (data.users);
						
                        if(data.authUser.chat_setting)    this.user_chat_setting = data.authUser.chat_setting;
						// console.log(this.user_chat_setting);
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);

                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }

                }
            );
    }

    setUsertoChat(selcIP,visitor_id){
        // console.log(selcIP+' , '+visitor_id)
        this.msg_visitor_id = visitor_id;
        this.msg_to_ip = selcIP;
        this.getLiveUserMsg();
        /*let sendObj = {agent : this.my_socket_id, visitor : socktId};
        socket.emit('connect_agent',sendObj);*/

        let that = this;
        this.live_user.find( function(item) {
            if(item.usr == selcIP){
                item.unread = 0;
                // console.log(item.messages);
                // that.chatMsg = item.messages;
                that.attachList = item.file_list;
                // that.curr_activities = item.user_activities;

                that.msg_to_ip = item.usr;
                that.to_chat_socket_id = item.socket_id;
                /*console.log(socktId);
                console.log(that.to_chat_socket_id);*/
                // console.log(that.chatMsg);
            }
        } );
        // this.to_chat_socket_id = socktId;
        // console.log(this.to_chat_socket_id)
    }

    keyDownFunction(event,val) {
        // console.log(event.keyCode);
        // console.log(this.room_name);
        // let msg = event.srcElement.value;
        let msg = val;
        // msg= msg.trim();
        this.curr_msg = msg;
        if(event.keyCode == 13) {
            // console.log(event.srcElement.value);
            // console.log(this.room_name);

            /*if(msg !='' && this.room_name !=''){
                this.chatMsg.push(msg);
                let chatParam = {room: this.room_name, message: msg}
                socket.emit('send_message', chatParam);
            }*/
            if(this.to_chat_socket_id!=''){
                // socket.broadcast.to(this.to_chat_socket_id).emit('send_message', msg);

                this.sendMyMsg(msg);
            }
            // event.srcElement.value = '';

        }
    }

    getLiveVisitors(){
        // this.live_user
        // this.chatMsg = [];
        console.log('lv');
        this.attachList = [];
        this.curr_activities = [];
        let fd = new FormData();
        // fd.append('agent_socket', this.my_socket_id);
        fd.append('agent_ip', this.myIp);
        // fd.append('agent_ip', this.authUser.live_project.id);
        fd.append('api_token', this.authUser.api_token);
        this.userService.fetchVisitors(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // console.log('vv');
                        // message
                        this.msgs = [];
                        if(data.operator_chat_setting ){
                            this.operator_chat_setting = data.operator_chat_setting;
                        }
                        //this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Project Post Successful'});
                        /*if(data.group_visitors !== null){
                            data.group_visitors.forEach((value: any) => {
                            });
                        }*/
                        if(data.feed !== null){

                            this.live_user = [];this.remote_user=[];this.remote_user=[];

                            data.feed.forEach((value: any) => {

                                if(this.authUser.type==1 && this.is_default_operator==0){
                                    let lftroom =  this.room_name+'_user';
                                    let req_data = { visitor_ip: value.ip_address, left_room: lftroom};
                                    socket.emit('join_group',req_data);
                                }
                                let status = (value.is_online == 1)?true :false;
                                let feedMsg = [];
                                let attachMsg = [];

                                let actvts = null;
                                if(value.activities)    actvts = value.activities;
                                let tagscore = 0;
                                if(value.tagscores){
                                    value.tagscores.forEach((tgscr: any) => {
                                        tagscore += parseInt(tgscr.score);
                                    });
                                }


                                let usr_set = {
                                    usr: value.ip_address, visitor_id: value.id, socket_id: value.visitor_socket_id, country_code: value.country_code, online: status, typing: false, unread: 0, messages: feedMsg, file_list: attachMsg.reverse(), user_activities : actvts, tag_score : tagscore,
                                    first_name : value.contact.first_name,
                                    last_name : value.contact.last_name,
                                    email : value.contact.email
                                };
                                this.live_user.push(usr_set);
                                this.remote_user.push(value.visitor_socket_id);
                                this.remote_ip_arr.push(value.ip_address);
                                // console.log('fv');
                                /*let sendObj = {agent : this.my_socket_id, agent_ip : this.myIp, visitor_id : value.id, visitor : value.visitor_socket_id}
                                socket.emit('connect_agent',sendObj);*/

                                if(this.msg_visitor_id == value.id){
                                    this.setUsertoChat(value.ip_address,value.id);
                                }
                            });

                            if(data.feed[0] && data.feed[0].activities && this.flag==0){
                                this.remote_page = data.feed[0].activities[0].page_url?data.feed[0].activities[0].page_url:'';
                                // this.dataService.chageLiveWindow(this.remote_page);
                                this.setLiveFrame(this.remote_page);
                                this.setUsertoChat(data.feed[0].ip_address,data.feed[0].id);
                                this.flag = 1;
                            }

                            if(data.asgn_visitors){

                                data.asgn_visitors.forEach((item: any) => {

                                    if(item.visitor){
                                        let value = item.visitor;
                                        if(this.authUser.type==1 && this.is_default_operator==0){
                                            let lftroom =  this.room_name+'_user';
                                            let req_data = { visitor_ip: value.ip_address, left_room: lftroom};
                                            socket.emit('join_group',req_data);
                                        }
                                        let status = (value.is_online == 1)?true :false;
                                        let feedMsg = [];
                                        let attachMsg = [];

                                        // if(value.activities) this.curr_activities = value.activities;
                                        // console.log(value);
                                        let actvts = null;
                                        if(value.activities)    actvts = value.activities;

                                        let usr_set = {usr: value.ip_address, visitor_id: value.id, socket_id: value.visitor_socket_id, country_code: value.country_code, online: status, typing: false, unread: 0, messages: feedMsg, file_list: attachMsg.reverse(), user_activities : actvts,tag_score:0};
                                        this.live_user.push(usr_set);
                                        this.remote_user.push(value.visitor_socket_id);
                                        this.remote_ip_arr.push(value.ip_address);
                                    }


                                });
                            }

                        }
                    }
                    else if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);

                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }

                }
            );
        // console.log(this.authUser);

    }

    changeLiveStatus(socketOrIp,status){
        // this.live_user
        let fd = new FormData();
        fd.append('agent_socket', this.my_socket_id);
        fd.append('socketorip', socketOrIp);
        fd.append('status', status);
        fd.append('api_token', this.authUser.api_token);
        this.userService.changeLiveVisitors(fd)
            .subscribe(
                (data: any) => {
                    // console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        //this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Project Post Successful'});

                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);

                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }

                }
            );
        // console.log(this.authUser);

    }

    sendMyMsg(currMessage){
        currMessage = currMessage.trim();
        // console.log(currMessage);


        if(currMessage !=''){
            let chatParam = {from_user: this.authUser, socketid: this.to_chat_socket_id, room: this.room_name, to_ip: this.msg_to_ip, is_file:'N', message: currMessage, type:'a'}
            socket.emit('send_message', chatParam);
            let mymsg = '<div class="list me">\n' +
                '            <div class="msgUser"><img src="'+this.appConfig.publicUrl+'/uploads/profile_picture/thumbs/'+this.authUser.profile_picture+'" class="img-responsive"></div>\n' +
                '            <div class="msgTxt">\n' +
                '              <span class="nm">'+this.authUser.first_name+' '+this.authUser.last_name+'<br><span class="date">'+moment().format('MMMM Do YYYY, h:mm:ss a')+'</span></span>\n' +
                '              <p>'+this.curr_msg+'</p>\n' +
                '            </div>\n' +
                '          </div>'
            // this.chatMsg.push(mymsg);

            this.chatMsg.push(mymsg);
        }

        let fd = new FormData();
        fd.append('visitor_id', this.msg_visitor_id);
        fd.append('from_user', this.authUser.id);
        fd.append('from_ip', this.myIp);
        fd.append('to_ip', this.msg_to_ip);
        fd.append('message', currMessage);
        fd.append('chat_user', this.authUser.id);
        if(this.blob_file !="")    fd.append('message_file', this.blob_file);
        // fd.append('api_token', this.authUser.api_token);


        this.curr_msg = "";
        this.blob_file = "";
        this.blob_file_name = "";

        this.userService.sendMyMessage(fd)
            .subscribe(
                (data: any) => {
                    console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Message Successfully Sent'});

                        if(data.feed && data.feed.type == 2){
                            let chatParam = {from_user: this.authUser, socketid: this.to_chat_socket_id, room: this.room_name, to_ip: this.msg_to_ip, is_file:'Y', message: data.feed.msg_file, type:'a'}
                            socket.emit('send_message', chatParam);

                            let mymsg = '<div class="list me">\n' +
                                '            <div class="msgUser"><img src="'+this.appConfig.publicUrl+'/uploads/profile_picture/thumbs/'+this.authUser.profile_picture+'" class="img-responsive"></div>\n' +
                                '            <div class="msgTxt">\n' +
                                '              <span class="nm">'+this.authUser.name+'<br><span class="date">'+moment().format('MMMM Do YYYY, h:mm:ss a')+'</span></span>\n' +
                                '              <p><a download="" target="_blank" href="'+appConfig.publicUrl+'uploads/message_attachment/'+data.feed.msg_file+'">'+data.feed.message+'</a></p>\n' +
                                '            </div>\n' +
                                '          </div>'
                            // this.chatMsg.push(mymsg);
                            /*if(this.remote_user.indexOf(this.to_chat_socket_id)!= -1)
                            {
                                let that = this;
                                this.live_user.find( function(item) {
                                    if(item.usr == that.msg_to_ip){
                                        item.messages.push(mymsg);
                                    }
                                } );
                            }*/
                            this.chatMsg.push(mymsg);
                        }
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);

                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }

                }
            );
        // console.log(this.authUser);

    }

    setLiveFrame(rurl){
        this.remote_page = rurl;
        if(rurl !='')    this.dataService.chageLiveWindow(this.remote_page);
    }

    openAddGroupPop($data){
        // console.log($data);
        this.commonService.openModal($data);
    }

    getLiveUserMsg() {
        let fd = new FormData();
        fd.append('visitor_ip', this.msg_to_ip);
        fd.append('visitor_id', this.msg_visitor_id);
        fd.append('api_token', this.authUser.api_token);
        this.userService.fetchVisitorMsg(fd)
            .subscribe(
                (data: any) => {
                    // console.log(data);
                    if (data.status === 'success') {
                        this.group_members = data.group_users;
                        console.log(this.group_members);
                        // message
                        this.selected_contact = null;
                        if(data.contact) this.selected_contact = data.contact;


                        this.curr_activities = data.visitor.activities;
                        // console.log(this.curr_activities);
                        this.msgs = [];
                        let feedMsg = [];
                        let attachMsg = [];
                        if(data.feed){
                            data.feed.forEach((msg: any) => {
                                let msg_obj = msg.message;
                                if(msg.type == 2){
                                    attachMsg.push({name:msg.message, file:msg.msg_file, sender_ip:msg.from_ip, created_at:msg.created_at});
                                    msg_obj = '<a download="" target="_blank" href="'+appConfig.publicUrl+'uploads/message_attachment/'+msg.msg_file+'">'+msg.message+'</a>';
                                }

                                // let msgUser = msg.visitor.ip_address;
                                let msgUser = msg.visitor.name;
                                let msgUserPic = 'assets/images/ctavatar_default.png';
                                if(msg.from_user != null){
                                    msgUser = msg.user.first_name+' '+msg.user.last_name;
                                    if(!msg.user.profile_picture || msg.user.profile_picture !='undefined')
                                    {
                                        msgUserPic = appConfig.publicUrl+'uploads/profile_picture/thumbs/'+msg.user.profile_picture;
                                    }else{
                                        msgUserPic = 'assets/images/ctavatar_default2.png';
                                    }

                                }

                                let rMsg ='';
                                let styl = (msg.from_user == this.authUser.id )?'me':'';
                                rMsg = '<div class="list '+styl+'">\n' +
                                    '            <div class="msgUser"><img src="'+msgUserPic+'" class="img-responsive"></div>\n' +
                                    '            <div class="msgTxt">\n' +
                                    '              <span class="nm">'+msgUser+'<br><span class="date">'+moment(msg.created_at).format('MMMM Do YYYY, h:mm:ss a')+'</span></span>\n' +
                                    '              <p>'+msg_obj+'</p>\n' +
                                    '            </div>\n' +
                                    '          </div>';

                                feedMsg.push(rMsg);
                                /* if(msg.from_ip != this.myIp){
                                }
                               else{
                                    rMsg = '<div class="list ">\n' +
                                        '            <div class="msgUser"><img src="'+this.appConfig.publicUrl+'/uploads/profile_picture/thumbs/'+this.authUser.profile_picture+'" class="img-responsive"></div>\n' +
                                        '            <div class="msgTxt">\n' +
                                        '              <span class="nm">'+this.authUser.name+'<br><span class="date">'+( moment(msg.created_at).format('MMMM Do YYYY, h:mm:ss a'))+'</span></span>\n' +
                                        '              <p>'+msg_obj+'</p>\n' +
                                        '            </div>\n' +
                                        '          </div>';

                                }*/

                            });
                        }
                        this.chatMsg = feedMsg;
                        this.attachList =  attachMsg.reverse();
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);

                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }

                }
            );
    }


}

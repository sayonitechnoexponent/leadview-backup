import { Component, OnInit } from '@angular/core';

import { Router, ActivatedRoute } from '@angular/router';
import 'rxjs/add/operator/filter';

import { AuthServices } from './../../../services/auth.service';

import {Message} from 'primeng/primeng';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  /**
   * @property \{{{object}}\} {{model}} {{register form model}}
   */
  model: any = {
    first_name: null,
    last_name: null,
    email: null
  };
  /**
   * @property \{{{Message[]}}\} {{msgs}} {{this is growl message model}}
   */
  msgs: Message[] = [];
  /**
   * @property \{{{any}}\} {{captcha}} {{this will hold the captcha code}}
   */
  captcha: any = '';
  /**
   * @property \{{{any}}\} {{platform}} {{this will hold the user platform Id}}
   */
  platform: any = '';

  is_submit:boolean = false;

  /**
   * @constructor
   * @description {{DI will pushed here}}
   */
  constructor(
    private authService: AuthServices,
    private route: ActivatedRoute,
    private router: Router
  ) { }


  /**
   * @func {{resolved}}
   * @description {{this will get response from recaptcha}}
   */
  public resolved(captchaResponse: string) {
    // console.log(`Resolved captcha with response ${captchaResponse}:`);
    this.captcha = captchaResponse;
  }


  /**
   * @func {{register}}
   * @param \{{{formobject}}\} {{myform}} {{this will pass the form object}}
   * @description {{this will submit the form data and make first step register}}
   */
  register(myform) {

      this.is_submit = true;
    if (this.captcha === '') {
      //alert('Please confirm you are not a robot.');
        this.msgs = [];

         this.msgs.push({severity: 'error', summary: 'Error Message', detail: 'Please confirm you are not a robot.'});
        this.is_submit = false;

    } else {
      // make service subscribe
      this.authService.register(this.model)
      .subscribe(
        (data: any) => {
          // console.log(data);
          if (data.status === 'success') {
            // make form reset
            // myform.reset();

            // message
            this.msgs = [];
            this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Please check your email'});

            setTimeout(() => {
                this.router.navigate(['login']);
            }, 3000);
          }

          if (data.status === 'fail') {
            // message
              this.is_submit = false;
            this.msgs = [];
            //this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.message});

              if (data.errors && Object.keys(data.errors).length > 0)
              {

                  for (var key in data.errors) {
                      if (data.errors.hasOwnProperty(key)) {
                          //console.log(key + " -> " + p[key]);
                          this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                      }
                  }

              }
              else if(data.error_message)
              {
                  this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
              }
              else
              {
                  this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
              }
          }
        },
        error => {
          //console.log(error);
            this.msgs = [];
            this.is_submit = false;
            if (error && error.message)
            {
                this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

            }
        }
      );
    }

  }


  /**
   * @func {{ngOnInit}}
   * @description {{angular lifecycle hook}}
   */
  ngOnInit() {

      this.route.queryParams
          .filter(params => params.platform)
          .subscribe(params => {
              console.log(params); // {order: "popular"}
              //alert(params.platform)
              //this.platform = params.platform;
              if(params.platform)  this.model.platform_id = params.platform;
          });
  }

}

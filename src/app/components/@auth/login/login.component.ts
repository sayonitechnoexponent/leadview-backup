import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthServices } from './../../../services/auth.service';

import {Message} from 'primeng/primeng';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
    /**
     * @property \{{{object}}\} {{model}} {{login form model}}
     */
    model: any = {
        email: null,
        password: null,
    };
    /**
     * @property \{{{Message[]}}\} {{msgs}} {{this is growl message model}}
     */
    msgs: Message[] = [];

    /**
   * @constructor
   * @description {{DI will pushed here}}
   */
    constructor(
        private router: Router,
        private authService: AuthServices
    ) { }

    /**
     * @func {{login}}
     * @param \{{{formobject}}\} {{myform}} {{this will pass the form object}}
     * @description {{this will submit the form data and make login}}
     */
    login(myform) {
        // make service subscribe
        this.authService.login(this.model)
            .subscribe(
                (data: any) => {
                    // console.log(data);
                    if (data.status === 'success') {
                        // message
                        // this.msgs = [];
                        // this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Login Successful'});

                        // set the logged in user
                        localStorage.setItem('leaduser', JSON.stringify(data.user));
                        // console.log(data.user);
                        //alert(data.user.type);
                        // go to  profile page/admin
                        if (data.user.type == 3)        location.href = '/admin';
                        else{
                            if(data.user.type == 2 && data.user.projects.length != 1)
                            {
                                location.href = '/my-project';
                                // else    location.href = '/my-account';
                            }
                            else location.href = '/time-line';
                        }
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                    }
                },
                error => {
                    console.log(error);
                    alert('Something went wrong!');
                }
            );
    }

    /**
   * @func {{ngOnInit}}
   * @description {{angular lifecycle hook}}
   */
    ngOnInit() {
    }

}

import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { AuthServices } from './../../../services/auth.service';
import {Message} from 'primeng/primeng';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {

    /**
     * @property \{{{any}}\} {{urlid}} {{this will hold the id value from urlid}}
     */
    urlid: any = '';
    /**
     * @property \{{{any}}\} {{emailUser}} {{this will hold the user via email}}
     */
    emailUser: any = [];
    /**
     * @property \{{{any}}\} {{model}} {{register form model}}
     */
    model: any = {
        email: null
    };
    /**
     * @property \{{{Message[]}}\} {{msgs}} {{this is growl message model}}
     */
    msgs: Message[] = [];
    constructor(  private authService: AuthServices,
                  private route: ActivatedRoute,
                  private router: Router) { }


    /**
     * @func {{sendResetPasswordLink}}
     * @param \{{{formobject}}\} {{myform}} {{this will pass the form object}}
     * @description {{this will submit the form data and reset password}}
     */
    sendResetPasswordLink(myform) {
        // make service subscribe
        this.model.cpass =1;
        this.authService.resetPasswordToken(this.model)
            .subscribe(
                (data: any) => {
                    // console.log(data);
                    if (data.status === 'success') {
                        // make form reset
                        // myform.reset();

                        // message
                        this.msgs = [];
                        this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Check your inbox for an email to reset your password.'});
                        this.model = {
                            email: null
                        };
                        // go to login
                       /* setTimeout(() => {
                            window.location.pathname = '/login';
                        }, 1500);*/
                    }
                    else if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    // console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                }
            );
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            this.urlid = params['id']+'==';
        });
    }


}

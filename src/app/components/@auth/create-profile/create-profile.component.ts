import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { AuthServices } from './../../../services/auth.service';

import {Message} from 'primeng/primeng';


@Component({
  selector: 'app-create-profile',
  templateUrl: './create-profile.component.html',
  styleUrls: ['./create-profile.component.css']
})
export class CreateProfileComponent implements OnInit {
  /**
   * @property \{{{any}}\} {{urlid}} {{this will hold the id value from urlid}}
   */
  urlid: any = '';
  /**
   * @property \{{{any}}\} {{emailUser}} {{this will hold the user via email}}
   */
  emailUser: any = [];
  /**
   * @property \{{{any}}\} {{model}} {{register form model}}
   */
  model: any = {
    first_name: null,
    last_name: null,
    password: null
  };
  /**
   * @property \{{{Message[]}}\} {{msgs}} {{this is growl message model}}
   */
  msgs: Message[] = [];



  /**
   * @constructor
   * @description {{DI will pushed here}}
   */
  constructor(
    private authService: AuthServices,
    private route: ActivatedRoute,
    private router: Router
  ) { }



  /**
   * @func {{makeprofile}}
   * @param \{{{formobject}}\} {{getEmailUser}} {{this will pass the form object}}
   * @description {{this will get the user data come from email}}
   */
  getEmailUser() {
    // make service subscribe
      this.authService.authUser(this.urlid)
          .subscribe(
              (data: any) => {
                  console.log(data);
                  if (data.status === 'success') {
                      this.emailUser = data.user;
                      this.model.first_name = this.emailUser.first_name;
                      this.model.last_name = this.emailUser.last_name;
                  }
                  if (data.status === 'fail') {
                      // message
                      this.msgs = [];
                      if (data.errors && Object.keys(data.errors).length > 0)
                      {

                          for (var key in data.errors) {
                              if (data.errors.hasOwnProperty(key)) {
                                  //console.log(key + " -> " + p[key]);
                                  this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                              }
                          }

                      }
                      else if(data.error_message)
                      {
                          this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                      }
                      else
                      {
                          this.msgs.push({severity: 'error', summary: 'Error Message', detail: "This account got already confirmed. Please login."});
                      }
                  }
              },
              error => {
                  console.log(error);
                  this.msgs = [];
                  if (error && error.message)
                  {
                      this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                  }
                  //alert('Something went wrong!');
              }
          );
  }

  /**
   * @func {{makeprofile}}
   * @param \{{{formobject}}\} {{myform}} {{this will pass the form object}}
   * @description {{this will submit the form data and make second stem register}}
   */
  makeprofile(myform) {
    // make service subscribe
    this.authService.finalRegister(this.urlid, this.model)
    .subscribe(
      (data: any) => {
        console.log(data);
        if (data.status === 'success') {
          // make form reset
          // myform.reset();

          // message
          this.msgs = [];
          this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Successfully created profile'});

          // go to login
          setTimeout(() => {
            window.location.pathname = '/login';
          }, 1500);
        }
        else if (data.status === 'fail') {
            // message
            this.msgs = [];
            if (data.errors && Object.keys(data.errors).length > 0)
            {

                for (var key in data.errors) {
                    if (data.errors.hasOwnProperty(key)) {
                        //console.log(key + " -> " + p[key]);
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                    }
                }

            }
            else if(data.error_message)
            {
                this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
            }
            else
            {
                this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
            }
        }
      },
      error => {
          this.msgs = [];
          if (error && error.message)
          {
              this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

          }
      }
    );
  }


  /**
   * @func {{ngOnInit}}
   * @description {{angular lifecycle hook}}
   */
  ngOnInit() {
    // get the route id from param
    this.route.params.subscribe(params => {
      this.urlid = params['id']+'==';
      // this.urlid = 'MjI9a21UeGEwdWJLb05iN285Y1NSN3lYZFc5Z0p1OE5xdmdaS2Fad3hsRQ==';
    });
    this.getEmailUser();
  }

}

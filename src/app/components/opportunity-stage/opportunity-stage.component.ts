import { Component, OnInit } from '@angular/core';

import { CommonService } from '../../services/common.service';
import { UserService } from '../../services/user.service';
import {Message} from 'primeng/primeng';
import { appConfig } from '../../apiurl.config';

declare var $ : any;

@Component({
  selector: 'app-opportunity-stage',
  templateUrl: './opportunity-stage.component.html',
  styleUrls: ['./opportunity-stage.component.css']
})

export class OpportunityStageComponent implements OnInit {
    /**
     * @property \{{{any}}\} {{appConfig}} {{this will hold the constants for the app}}
     */

    appConfig: any = {};

    /**
     * @property \{{{Message[]}}\} {{msgs}} {{this is growl message model}}
     */

    msgs: Message[] = [];

    /**
     * @property \{{{any}}\} {{model}} {{this form model}}
     */

    model: any = {
        name : null,
        description : null,
        profit_margin : 10
    };

    /**
     * @property \{{{any}}\} {{authUser}} {{this will hold the authuser}}
     */

    authUser: any ={};
    /**
     * @property \{{{any}}\} {{myList}} {{this will hold the list data}}
     */

    myList: any;

    /**
     * @property \{{{any}}\} {{selArr}} {{this will hold the selected from list}}
     */

    selArr: any =[];

    searchText : any;

    showName: boolean = true;
    showDesc: boolean = true;
    showMargin: boolean = true;

    displayColFrm: any = {
        itemName : true,
        itemDesc : true,
        itemMargin : true,
    };

  constructor(
      private commonService: CommonService,
      private userService: UserService
  ) { }


    /**
     * @func {{saveDisplay}}
     * @description {{toggle display of list}}
     */
    saveDisplay(myForm) {

        console.log(this.displayColFrm);
        this.showName = this.displayColFrm.itemName;
        this.showDesc = this.displayColFrm.itemDesc;
        this.showMargin = this.displayColFrm.itemMargin;

    }

    /**
     * @func {{pageLoad}}
     * @description {{reload page after a certain time}}
     */
    pageLoad(){
        setTimeout(()=>{    //<<<---    using ()=> syntax
            location.reload()
        },2000);
    }

    /**
     * @func {{openCreatePop}}
     * @param \{{{string}}\} {{$data}} {{popup data val class}}
     * @description {{this will be open the popup accordingly on click by pass data val}}
     */
    openCreatePop($data){
        console.log($data);
        this.commonService.openModal($data);
        this.model.type = "Add";
        //this.model = {};
    }
    /**
     * @func {{openEditModal}}
     * @param \{{{string}}\} {{$data}} {{popup data val class}}
     * @description {{this will me open the popup accordingly on click by pass data val}}
     */
    openEditModal($data,feed){
        this.commonService.openModal($data);
        this.model = feed;
        this.model._id = feed.id;
        this.model.type = "Edit";
        this.model.profit_margin = feed.profit_margin;

        let amnt = feed.profit_margin;
        $('#amount').val(amnt);
        console.log(amnt);

        //$("#amount").val(amnt);
        $("#amount-label").text(amnt);
        $("#slider").slider({
            range: "min",
            animate: true,
            value: amnt,
            min: 0,
            max: 100,
            step: 10,
        });
        //this.selUserArr = [];

    }
    /**
     * @func {{addSaveStage}}
     * @param \{{{formobject}}\} {{myform}} {{this will pass the form object}}
     * @description {{this will submit the form data and add or update the tax data}}
     */

    addSaveStage(myform)
    {
        let amnt = ($('#amount').val());
        //alert(amnt);
        this.model.api_token = this.authUser.api_token;
        this.model.profit_margin = amnt;
        if(this.authUser.live_project)
        {
            this.model.projectId = this.authUser.live_project.id;
        }
        console.log(this.model);

        this.userService.addEditOpportunityStage(this.model)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Stage added successfully'});

                        // go to  my project page
                        this.pageLoad();
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );

    }
    /**
     * @func {{fetchStage}}
     * @description {{get all Stages}}
     */
    fetchStage() {
        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);

        this.userService.getOpportunityStages(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        //this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Project Post Successful'});
                        this.myList =  (data.feed);
                        //console.log(this.myTeam);
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );

    }
    /**
     * @func {{delStage}}
     * @description {{delete tax}}
     */
    delStage(_id)
    {
        //console.log(this.model);
        if(confirm("Are you sure to delete the stage?"))
        {
            let fd = new FormData();
            fd.append('_ids',_id);
            fd.append('api_token', this.authUser.api_token);
            //return;
            this.userService.deleteOpportunityStages(fd)
                .subscribe(
                    (data: any) => {
                        //console.log(data);
                        if (data.status === 'success') {
                            // message
                            this.msgs = [];
                            this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Stage Successfully Deleted'});

                            // go to  my team page
                            this.pageLoad();
                        }
                        if (data.status === 'fail') {
                            // message
                            this.msgs = [];
                            if (data.errors && Object.keys(data.errors).length > 0)
                            {

                                for (var key in data.errors) {
                                    if (data.errors.hasOwnProperty(key)) {
                                        //console.log(key + " -> " + p[key]);
                                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                    }
                                }

                            }
                            else if(data.error_message)
                            {
                                this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                            }
                            else
                            {
                                this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                            }
                        }
                    },
                    error => {
                        console.log(error);
                        this.msgs = [];
                        if (error && error.message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                        }
                        //alert('Something went wrong!');
                    }
                );

        }


    }

    /**
     * @func {{delAll}}
     * @description {{delete selected taxes}}
     */
    delAll()
    {
        if(confirm("Are you sure to delete all selected stages?"))
        {
            let fd = new FormData();
            fd.append('_ids',this.selArr);
            fd.append('api_token', this.authUser.api_token);
            //return;
            this.userService.deleteOpportunityStages(fd)
                .subscribe(
                    (data: any) => {
                        //console.log(data);
                        if (data.status === 'success') {
                            // message
                            this.msgs = [];
                            this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Stage are successfully deleted'});

                            this.pageLoad();
                        }
                        if (data.status === 'fail') {
                            // message
                            this.msgs = [];
                            if (data.errors && Object.keys(data.errors).length > 0)
                            {

                                for (var key in data.errors) {
                                    if (data.errors.hasOwnProperty(key)) {
                                        //console.log(key + " -> " + p[key]);
                                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                    }
                                }

                            }
                            else if(data.error_message)
                            {
                                this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                            }
                            else
                            {
                                this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                            }
                        }
                    },
                    error => {
                        console.log(error);
                        this.msgs = [];
                        if (error && error.message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                        }
                        //alert('Something went wrong!');
                    }
                );

        }
    }

    selctToList($event) {
        //console.log($event.srcElement.checked);
        //console.log($event.srcElement.value);
        if($event.srcElement.checked == true)
        {
            if(this.selArr.indexOf($event.srcElement.value)== -1)
                this.selArr.push($event.srcElement.value);
        }else {

            this.selArr.splice(this.selArr.indexOf($event.srcElement.value), 1);
        }
        //console.log(this.selcArr);
    }

    ngOnInit() {

      this.authUser = this.userService.getAuthUser();
      this.appConfig = appConfig;
      this.fetchStage();

    }

}

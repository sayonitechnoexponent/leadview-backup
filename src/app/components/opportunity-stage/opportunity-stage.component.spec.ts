import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpportunityStageComponent } from './opportunity-stage.component';

describe('OpportunityStageComponent', () => {
  let component: OpportunityStageComponent;
  let fixture: ComponentFixture<OpportunityStageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpportunityStageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpportunityStageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

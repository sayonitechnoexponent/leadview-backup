import { Component, OnInit } from '@angular/core';

import { CommonService } from '../../../services/common.service';
import { UserService } from '../../../services/user.service';
import {Message} from 'primeng/primeng';
import { appConfig } from '../../../apiurl.config';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css']
})
export class ProjectsComponent implements OnInit {
    /**
     * @property \{{{any}}\} {{appConfig}} {{this will hold the constants for the app}}
     */

    appConfig: any = {};
    /**
     * @property \{{{any}}\} {{model}} {{security form model}}
     */

    model: any = {
        project_image : null,
        title : null,
        description : null,
        country : null,
        timezone : null,
    };

    /**
     * @property \{{{any}}\} {{authUser}} {{this will hold the authuser}}
     */

    authUser: any ={};
    /**
     * @property \{{{any}}\} {{allProjects}} {{this will hold the projects}}
     */

    allProjects: any =[];
    /**
     * @property \{{{any}}\} {{selArr}} {{this will hold the all selected projects}}
     */

    selArr: any =[];


    /**
     * @property \{{{Message[]}}\} {{msgs}} {{this is growl message model}}
     */

    msgs: Message[] = [];
    /**
     * @property \{{{any}}\} {{searchText}} {{this will hold the search data}}
     */

    searchText: any =[];
    searchEmail: any =[];

  constructor(
      private commonService: CommonService,
      private userService: UserService
  ) { }

  ngOnInit() {
      this.authUser = this.userService.getAuthUser();
      this.appConfig = appConfig;
      this.getAllProjects();
  }

    /**
     * @func {{selctToList}}
     * @description {{this will hold selected project list}}
     */
  selctToList($event) {
      //console.log($event.srcElement.checked);
      //console.log($event.srcElement.value);
      if($event.srcElement.checked == true)
      {
          if(this.selArr.indexOf($event.srcElement.value)== -1)
              this.selArr.push($event.srcElement.value);
      }else {

          this.selArr.splice(this.selArr.indexOf($event.srcElement.value), 1);
      }

      //console.log(this.selArr);
  }
    /**
     * @func {{getMyProjects}}
     * @description {{get user projects}}
     */
    getAllProjects() {
        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);
        this.userService.allProjects(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        //this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Project Post Successful'});
                        this.allProjects =  (data.projects);

                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );


    }

    /**
     * @func {{delAll}}
     * @description {{delete selected team}}
     */
    delAll()
    {
        if(confirm("Are you sure to delete all selected projects?"))
        {
            let fd = new FormData();
            fd.append('project_id',this.selArr);
            fd.append('api_token', this.authUser.api_token);
            //return;
            this.userService.deleteProject(fd)
                .subscribe(
                    (data: any) => {
                        //console.log(data);
                        if (data.status === 'success') {
                            // message
                            this.msgs = [];
                            this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Projects Successfully Deleted'});

                            // go to projects page
                            location.reload();
                        }
                        if (data.status === 'fail') {
                            // message
                            this.msgs = [];
                            if (data.errors && Object.keys(data.errors).length > 0)
                            {

                                for (var key in data.errors) {
                                    if (data.errors.hasOwnProperty(key)) {
                                        //console.log(key + " -> " + p[key]);
                                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                    }
                                }

                            }
                            else if(data.error_message)
                            {
                                this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                            }
                            else
                            {
                                this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                            }
                        }
                    },
                    error => {
                        console.log(error);
                        this.msgs = [];
                        if (error && error.message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                        }
                        //alert('Something went wrong!');
                    }
                );

        }
    }


    /**
     * @func {{changeMassMailling}}
     * @description {{this will change Mass Mailing status}}
     */
    changeMassMailing($event) {
        console.log($event.srcElement.checked);
        console.log($event.srcElement.value);
        if($event.srcElement.checked == true)
        {

                $event.srcElement.value;
        }else {

            $event.srcElement.value;

        }

        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);
        fd.append('project_id', $event.srcElement.value);
        fd.append('status', $event.srcElement.checked);
        fd.append('status_type', 'mass_mail');
        this.userService.changeProjectStatus(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Status successfully changed'});
                        this.getAllProjects();

                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );

        //console.log(this.selArr);
    }

}

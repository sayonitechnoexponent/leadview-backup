import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectsDescriptionComponent } from './project-description.component';

describe('ProjectsDescriptionComponent', () => {
  let component: ProjectsDescriptionComponent;
  let fixture: ComponentFixture<ProjectsDescriptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectsDescriptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectsDescriptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

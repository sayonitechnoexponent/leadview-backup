import { Component, OnInit } from '@angular/core';
import {Message} from 'primeng/primeng';
import { appConfig } from '../../../apiurl.config';
import { CommonService } from '../../../services/common.service';
import { UserService } from '../../../services/user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { DataService } from '../../../services/data.service';

@Component({
  selector: 'app-project-description',
  templateUrl: './project-description.component.html',
  styleUrls: ['./project-description.component.css']
})
export class ProjectsDescriptionComponent implements OnInit {
      /**
   * @property \{{{any}}\} {{model}} {{description form model}}
   */

  model: any = {
    content: null,
  };

  noteList: any = [];
  /**
   * @property \{{{Message[]}}\} {{msgs}} {{this is growl message model}}
   */

  msgs: Message[] = [];
  /**
* @property \{{{any}}\} {{appConfig}} {{this will hold the constants for the app}}
*/

  appConfig: any = {};
  /**
* @property \{{{any}}\} {{authUser}} {{this will hold the authuser}}
*/
  /**
   * @property \{{{any}}\} {{isLoggedinUser}} {{this will hold the isLoggedinUser}}
   */
  isLoggedinUser: boolean = false;
  project_Id: any;
  authUser: any = {};
    constructor(private commonService: CommonService,
        private userService: UserService,
        private activeRoute: ActivatedRoute,
        private dataService: DataService
      ) { }
    
      ngOnInit() {
        this.activeRoute.params.subscribe(routeParams => {
            this.project_Id = routeParams.id;
          });
          this.authUser = this.userService.getAuthUser();
          this.appConfig = appConfig;
          this.fetchNotes();
      }
      openModal($data) {
        this.commonService.openModal($data);
        this.model = {
          content: null,
        };
    
        this.model.type = "Add";
      }
      openInvoiceModal($data) {
        this.commonService.openModal($data);
        this.model = {
          invoice_No: null,
          invoice_Date: null,
          due_Date: null,
          invoice_Amount: null,
          description: null,
        };
    
        this.model.type = "Add";
      }
      closeModalBox(){
        $(".topsecboxbox").hide();
        $(".sectoshow").show();
        $('body').removeClass('open_modal');
        $('.formwrap').css({'display': 'none', 'top': -5200});
        setTimeout(function(){
            $('.formwrap').css({'display': 'block', 'min-height': '1px'});
        }, 1000);
        $('.ovelay').addClass('hidden');
        $('.cntactadd .container-fluid').css({'min-height':0});
        $('.cntactadd').removeClass('open_popup');
    }
      /**
       * @func {{addSaveNote}}
       * @param \{{{formobject}}\} {{myform}} {{this will pass the form object}}
       * @description {{this will submit the form data and add or update the Note data}}
       */
      addSaveNote(myform) {
        this.model.api_token = this.authUser.api_token;
        this.model.project_id = this.project_Id;
        console.log(this.model)
        this.userService.addNote(this.model)
          .subscribe(
            (data: any) => {
              console.log(data);
              if (data.status === 'success') {
                // message
                this.msgs = [];
                this.msgs.push({ severity: 'success', summary: 'Success Message', detail: 'Note added successfully' });
                this.closeModalBox();
                this.fetchNotes();
                // go to  my project page
                // this.pageLoad();
              }
              if (data.status === 'fail') {
                // message
                this.msgs = [];
                if (data.errors && Object.keys(data.errors).length > 0) {
    
                  for (var key in data.errors) {
                    if (data.errors.hasOwnProperty(key)) {
                      //console.log(key + " -> " + p[key]);
                      this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                    }
                  }
    
                }
                else if (data.error_message) {
                  this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                }
                else {
                  this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                }
              }
            },
            error => {
              //console.log(error);
              this.msgs = [];
              if (error && error.message) {
                this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });
    
              }
            }
          );
    
      }
    
      /**
     * @func {{getMyTeam}}
     * @description {{get user teams}}
     */
      fetchNotes() {
        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);
        fd.append(' project_id', this.project_Id);
    
        this.userService.getNotes(fd)
          .subscribe(
            (data: any) => {
              console.log(data);
              if (data.status === 'success') {
    
                this.noteList = data.project.admin_notes;
              } console.log(this.noteList);
              if (data.status === 'fail') {
                // message
                this.msgs = [];
                if (data.errors && Object.keys(data.errors).length > 0) {
    
                  for (var key in data.errors) {
                    if (data.errors.hasOwnProperty(key)) {
                      //console.log(key + " -> " + p[key]);
                      this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                    }
                  }
    
                }
                else if (data.error_message) {
                  this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                }
                else {
                  this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                }
              }
            },
            error => {
              //console.log(error);
              this.msgs = [];
              if (error && error.message) {
                this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });
    
              }
            }
          );
    
      }
}
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlatFormPricingPlanComponent } from './platform-pricing-plan.component';

describe('MailAccountComponent', () => {
  let component: PlatFormPricingPlanComponent;
  let fixture: ComponentFixture<PlatFormPricingPlanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlatFormPricingPlanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlatFormPricingPlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

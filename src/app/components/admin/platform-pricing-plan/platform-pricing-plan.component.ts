import { Component, OnInit,ViewChild } from '@angular/core';
import { Message } from 'primeng/primeng';
import { appConfig } from '../../../apiurl.config';
import { UserService } from '../../../services/user.service';
import { CommonService } from '../../../services/common.service';
import { Router, ActivatedRoute } from '@angular/router';
import { DataService } from '../../../services/data.service';
declare var $: any;

@Component({
  selector: 'app-platform-pricing-plan',
  templateUrl: './platform-pricing-plan.component.html',
  styleUrls: ['./platform-pricing-plan.component.css']
})
export class PlatFormPricingPlanComponent implements OnInit {
  name = 'ng2-ckeditor';
  ckeConfig: any;
  mycontent: string;
  log: string = '';
  @ViewChild("myckeditor") ckeditor: any;

  model: any = {
    title: null,
    content: null,
    stylesheet: null
  };
  authUser: any = {};
  platformContent: any;
  platformId: any;
  /**
   * @property \{{{Message[]}}\} {{msgs}} {{this is growl message model}}
   */

  msgs: Message[] = [];

  title: any;
  content: any;
  stylesheet: any;
  id: any;
  someSubscription: any;
  constructor(private commonService: CommonService,
    private userService: UserService,
    private activeRoute: ActivatedRoute,
    private router: Router,
    private dataService: DataService) { }

  ngOnInit() {
    this.authUser = this.userService.getAuthUser();
    this.ckeConfig = {
      allowedContent: false,
      extraPlugins: 'divarea',
      forcePasteAsPlainText: true
    };
    this.someSubscription = this.dataService.platform.subscribe(item => {
      this.platformContent = item;
    })
    console.log(this.platformContent)
    if(this.platformContent.id != null){
      this.platformId = this.platformContent.id;
    }
    console.log(this.platformId)
    if (this.platformContent.pricing_content != null) {
      this.model.title = this.platformContent.pricing_content.title;
      this.model.content = this.platformContent.pricing_content.content;
      this.model.stylesheet = this.platformContent.pricing_content.stylesheet;
      this.model.id = this.platformContent.pricing_content.id;
    }
  }
  checkDraft(e) {
  }
  onChange($event: any): void {
    console.log("onChange",$event);
    //this.log += new Date() + "<br />";
  }
// add plan pricing form
  addPricingPlan(myform) {
    this.model.api_token = this.authUser.api_token;
    this.model.platform_id = this.platformId;
    console.log(this.model)
    this.userService.addPlatformPricingData(this.model)
      .subscribe(
        (data: any) => {
          console.log(data);
          if (data.status === 'success') {
            // message
            this.msgs = [];
            this.msgs.push({ severity: 'success', summary: 'Success Message', detail: 'Platform pricing details added successfully' });
            setTimeout(() => {
              this.router.navigate(['/admin/platform']);         
            }, 1000);
          }
          if (data.status === 'fail') {
            // message
            this.msgs = [];
            if (data.errors && Object.keys(data.errors).length > 0) {

              for (var key in data.errors) {
                if (data.errors.hasOwnProperty(key)) {
                  //console.log(key + " -> " + p[key]);
                  this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                }
              }

            }
            else if (data.error_message) {
              this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
            }
            else {
              this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
            }
          }
        },
        error => {
          //console.log(error);
          this.msgs = [];
          if (error && error.message) {
            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

          }
        }
      );

  }
  close() {
    // this.someSubscription.unsubscribe();
    this.model = {};
    this.router.navigate(['/admin/platform']);
  }
}
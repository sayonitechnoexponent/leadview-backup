import { Component, OnInit,ViewChild } from '@angular/core';
import { CommonService } from '../../../services/common.service';
import { UserService } from '../../../services/user.service';
import {Message} from 'primeng/primeng';
import { appConfig } from '../../../apiurl.config';

declare var $ : any;

@Component({
  selector: 'app-default-package',
  templateUrl: './default-package.component.html',
  styleUrls: ['./default-package.component.css']
})
export class DefaultPackageComponent implements OnInit {
    /**
     * @property \{{{any}}\} {{appConfig}} {{this will hold the constants for the app}}
     */

    appConfig: any = {};

    /**
     * @property \{{{Message[]}}\} {{msgs}} {{this is growl message model}}
     */

    msgs: Message[] = [];

    /**
     * @property \{{{any}}\} {{authUser}} {{this will hold the authuser}}
     */

    authUser: any ={};
    constructor(
        private commonService: CommonService,
        private userService: UserService) { }

  ngOnInit() {
      this.authUser = this.userService.getAuthUser();
      this.appConfig = appConfig;
  }

  ngAfterViewInit(){
  }
}

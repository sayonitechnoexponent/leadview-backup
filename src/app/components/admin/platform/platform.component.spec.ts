import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlatFormComponent } from './platform.component';

describe('MailAccountComponent', () => {
  let component: PlatFormComponent;
  let fixture: ComponentFixture<PlatFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlatFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlatFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

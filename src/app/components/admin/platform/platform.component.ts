import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../../services/common.service';
import { UserService } from '../../../services/user.service';
import { Message, FileUpload } from 'primeng/primeng';
import { appConfig } from '../../../apiurl.config';
import { DataService } from '../../../services/data.service';
import { Router, ActivatedRoute } from '@angular/router';

declare var $: any;
// const URL = '/api/';
@Component({
  selector: 'app-platform-account',
  templateUrl: './platform.component.html',
  styleUrls: ['./platform.component.css']
})
export class PlatFormComponent implements OnInit {
  model: any = {
    title: null,
    image: null,
    attachment: null
  };
  searchText = "";
  /**
   * @property \{{{Message[]}}\} {{msgs}} {{this is growl message model}}
   */

  msgs: Message[] = [];
  /**
   * @property \{{{any}}\} {{authUser}} {{this will hold the authuser}}
   */
  appConfig: any = {};
  authUser: any = {};

  platFormList: any = [];
  platformImage: any;

  url: any;
  selcArr: any = [];
  ImageArray: any = [];
  cssFiles: any = [];
  myFiles: any = [];
  platFormImagePath: string = '';
  filename: any = [];
  constructor(private commonService: CommonService,
    private userService: UserService,
    private dataService: DataService,
    private router: Router) { }

  ngOnInit() {
    console.log(this.platFormImagePath)
    this.authUser = this.userService.getAuthUser();
    this.appConfig = appConfig;
    this.fetchPlatForms();

    this.platFormImagePath = appConfig.publicUrl + 'uploads/platform/attachments/';
  }
  // platform add modal open
  openModal($data) {
    this.commonService.openModal($data);
    this.model = {
      content: null,
      image: null,
      attachment: null,
    };

    this.model.type = "Add";
    this.url = "";
  }
  // single file select
  selectFile(event) {
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      this.platformImage = event.target.files[0];
      reader.onload = (value: any) => {
        this.url = value.target.result;
      };
      reader.readAsDataURL(event.target.files[0]);
    }
  }
  //  multiple file select
  fileChangeEvent(event: any) {
    this.myFiles = event.target.files;
    console.log(this.myFiles);
    for (let i = 0; i < event.target.files.length; i++) {
      if (event.target.files && event.target.files[i]) {
        this.cssFiles.push(event.target.files[i]);
        console.log(this.cssFiles)
      }
    }
  }

  // delete image
  deleteImage(filename: string, attachment_name: string, index) {
    this.filename.push(filename);
    if (confirm('Are you sure you want to delete this image?')) {
      if (attachment_name) {
        this.ImageArray.push(attachment_name);
        console.log(this.ImageArray);
      }
      this.cssFiles.splice(index, 1);
      console.log(this.cssFiles)
    }
  }
  // open edit Modal
  openEditPlatFormmodal($data, platform) {
    console.log(platform);
    this.commonService.openModal($data);
    if ((platform.image != null) && (platform.image != undefined)) {
      this.url =
        appConfig.publicUrl + '/uploads/platform/' + platform.image;
    }
    else {
      this.url = "";
    }
    this.model.title = platform.title;
    if (platform.attachments.length > 0) {
      this.cssFiles = platform.attachments.map(item => {
        return {
          'name': item.original_name,
          'attachment_name': item.attachment_name,
          'lastModified': '',
          'lastModifiedDate': '',
          'size': '',
          'type': '',
          'webkitRelativePath': '',
        };
      });
      console.log(this.cssFiles)
    }
    this.model.id = platform.id;
    this.model.type = "Edit";
  }
  // select item for delete
  selctToList($event) {
    if ($event.srcElement.checked == true) {
      if (this.selcArr.indexOf($event.srcElement.value) == -1)
        this.selcArr.push($event.srcElement.value);
    } else {

      this.selcArr.splice(this.selcArr.indexOf($event.srcElement.value), 1);
    }
  }

  // close moadal box
  closeModalBox() {
    $(".topsecboxbox").hide();
    $(".sectoshow").show();
    $('body').removeClass('open_modal');
    $('.formwrap').css({ 'display': 'none', 'top': -5200 });
    setTimeout(function () {
      $('.formwrap').css({ 'display': 'block', 'min-height': '1px' });
    }, 1000);
    $('.ovelay').addClass('hidden');
    $('.cntactadd .container-fluid').css({ 'min-height': 0 });
    $('.cntactadd').removeClass('open_popup');
    this.model = {};
    this.cssFiles = [];
  }

  // platform create
  addPlatform(form): void {
    const fd: FormData = new FormData();
    fd.append('title', this.model.title);
    if (this.platformImage !== undefined) {
      fd.append('image', this.platformImage);
    }

    if (this.myFiles) {
      for (let i = 0; i < this.myFiles.length; i++) {
        if (this.filename) {
          this.filename.map(item => {
            item !== this.myFiles[i]['name']
            fd.append("attachment[]", this.myFiles[i], this.myFiles[i]['name']);
          });
        }
      }
    }

    if (this.ImageArray) {
      fd.append('delete_attachment', this.ImageArray);
    }
    if (this.model.id !== undefined) {
      fd.append('id', this.model.id);
    }
    this.userService.addPlatformData(fd).subscribe(
      (data: any) => {
        console.log(data);
        if (data.status === 'success') {
          // message
          this.msgs = [];
          this.msgs.push({ severity: 'success', summary: 'Success Message', detail: 'Platform added successfully' });
          this.closeModalBox();
          this.fetchPlatForms();

        }
        if (data.status === 'fail') {
          // message
          this.msgs = [];
          if (data.errors && Object.keys(data.errors).length > 0) {

            for (var key in data.errors) {
              if (data.errors.hasOwnProperty(key)) {
                this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
              }
            }

          }
          else if (data.error_message) {
            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
          }
          else {
            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
          }
        }
      },
      error => {
        this.msgs = [];
        if (error && error.message) {
          this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

        }
      }
    );
  }

  //  platform list
  fetchPlatForms() {
    this.userService.getPlatforms()
      .subscribe(
        (data: any) => {
          console.log(data);
          if (data.status === 'success') {

            this.platFormList = data.platforms;
          } console.log(this.platFormList);
          if (data.status === 'fail') {
            // message
            this.msgs = [];
            if (data.errors && Object.keys(data.errors).length > 0) {

              for (var key in data.errors) {
                if (data.errors.hasOwnProperty(key)) {
                  //console.log(key + " -> " + p[key]);
                  this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                }
              }

            }
            else if (data.error_message) {
              this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
            }
            else {
              this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
            }
          }
        },
        error => {
          //console.log(error);
          this.msgs = [];
          if (error && error.message) {
            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

          }
        }
      );

  }

  // delete platform
  delPlatform(platformId) {
    //console.log(this.model);
    if (confirm("Are you sure to delete the platform type?")) {
      let fd = new FormData();
      fd.append('_id', platformId);
      fd.append('api_token', this.authUser.api_token);
      //return;
      this.userService.deletePlatform(fd)
        .subscribe(
          (data: any) => {
            //console.log(data);
            if (data.status === 'success') {
              // message
              this.msgs = [];
              this.msgs.push({ severity: 'success', summary: 'Success Message', detail: 'Platform Successfully Deleted' });

              // go to  my team page
              // this.pageLoad();
              this.fetchPlatForms();
            }
            if (data.status === 'fail') {
              // message
              this.msgs = [];
              if (data.errors && Object.keys(data.errors).length > 0) {

                for (var key in data.errors) {
                  if (data.errors.hasOwnProperty(key)) {
                    //console.log(key + " -> " + p[key]);
                    this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                  }
                }

              }
              else if (data.error_message) {
                this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
              }
              else {
                this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
              }
            }
          },
          error => {
            console.log(error);
            this.msgs = [];
            if (error && error.message) {
              this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

            }
            //alert('Something went wrong!');
          }
        );

    }


  }

  /**
   * @func {{delAll}}
   * @description {{delete selected platforms}}
   */
  delAllPlatforms() {
    if (confirm("Are you sure to delete all selected platforms?")) {
      let fd = new FormData();
      fd.append('_id', this.selcArr);
      fd.append('api_token', this.authUser.api_token);
      //return;
      this.userService.deletePlatform(fd)
        .subscribe(
          (data: any) => {
            console.log(data);
            if (data.status === 'success') {
              // message
              this.fetchPlatForms();
              this.msgs = [];
              this.msgs.push({ severity: 'success', summary: 'Success Message', detail: 'All Platforms  are successfully deleted' });
            }
            if (data.status === 'fail') {
              // message
              this.msgs = [];
              if (data.errors && Object.keys(data.errors).length > 0) {

                for (var key in data.errors) {
                  if (data.errors.hasOwnProperty(key)) {
                    //console.log(key + " -> " + p[key]);
                    this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                  }
                }

              }
              else if (data.error_message) {
                this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
              }
              else {
                this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
              }
            }
          },
          error => {
            console.log(error);
            this.msgs = [];
            if (error && error.message) {
              this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

            }
          }
        );

    }
  }
  planData(data) {
    console.log(data)
    if (data != null) {
      this.dataService.sendplanPricingContent(data);
    }
    this.router.navigate(['/admin/platform-pricing-plan']);
  }
  close() {
    this.cssFiles = [];
  }
}

import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../../services/common.service';
import { UserService } from '../../../services/user.service';
import {Message} from 'primeng/primeng';
import { appConfig } from '../../../apiurl.config';

declare var $: any;

@Component({
  selector: 'app-mail-account',
  templateUrl: './mail-account.component.html',
  styleUrls: ['./mail-account.component.css']
})
export class MailAccountComponent implements OnInit {

    searchText = "";
    searchPrcnt = "";
    dropdownList = [];
    projectList = [];

    /**
     * @property \{{{any}}\} {{appConfig}} {{this will hold the constants for the app}}
     */

    appConfig: any = {};

    /**
     * @property \{{{Message[]}}\} {{msgs}} {{this is growl message model}}
     */

    msgs: Message[] = [];

    /**
     * @property \{{{any}}\} {{model}} {{user form model}}
     */

    model: any = {
        projectId: '',
        user_name: '',
        password: '',
        email_address: '',
    };

    /**
     * @property \{{{any}}\} {{authUser}} {{this will hold the authuser}}
     */

    authUser: any ={};
    /**
     * @property \{{{any}}\} {{taxList}} {{this will hold the tax List}}
     */

    dataList: any =[];
    /**
     * @property \{{{any}}\} {{selcArr}} {{this will hold the selected tax list}}
     */

    selcArr: any =[];
    edtext: string ='';
    platformList: any =[];
    languageList: any =[];

    constructor(
        private commonService: CommonService,
        private userService: UserService) { }

    ngOnInit() {
        this.authUser = this.userService.getAuthUser();
        this.appConfig = appConfig;
    }

    ngAfterViewInit(){

        this.getMailAccounts();
        this.getProjects();

    }
    /**
     * @func {{openTaxmodal}}
     * @param \{{{string}}\} {{$data}} {{popup data val class}}
     * @description {{this will me open the popup accordingly on click by pass data val}}
     */
    openFormModal($data,obj){
        this.commonService.openModal($data);
        console.log(obj);
        this.model = {
            projectId: '',
            user_name: '',
            password: '',
            email_address: '',
        };
        this.model.type = "Add";
        if(obj){
            this.model= obj;
            this.model.projectId = obj.project_id;
            /*this.model.user_name = obj.user_name;
            this.model.password = obj.password;
            this.model.email_address = obj.email_address;
            this.model.from_name = obj.from_name;*/
            this.model.type = "Edit";
        }

        console.log(this.model);
        //this.selUserArr = [];
    }

    /**
     * @func {{saveEmailTemplate}}
     * @param \{{{formobject}}\} {{myform}} {{this will pass the form object}}
     * @description {{this will submit the form data and add or update package data}}
     */
    saveEmailAccount(myform) {
        //console.log(myform);
        this.model.api_token =  this.authUser.api_token;
        //
        console.log(this.model);

        this.userService.saveMailAccount(this.model)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Template Successfully saved'});

                        this.closeModalBox();
                        this.getMailAccounts();
                        // go to  profile page
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );
    }

    getMailAccounts() {

        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);


        this.userService.fetchMailAccounts(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        // this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Template Successfully saved'});

                        this.dataList = data.feed;
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );
    }

    getProjects() {

        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);


        this.userService.allProjects(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        // this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Template Successfully saved'});

                        this.projectList = data.projects;
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );
    }

    closeModalBox(){
        $(".topsecboxbox").hide();
        $(".sectoshow").show();
        $('body').removeClass('open_modal');
        $('.formwrap').css({'display': 'none', 'top': -5200});
        setTimeout(function(){
            $('.formwrap').css({'display': 'block', 'min-height': '1px'});
        }, 1000);
        $('.ovelay').addClass('hidden');
        $('.cntactadd .container-fluid').css({'min-height':0});
        $('.cntactadd').removeClass('open_popup');
    }
    /**
     * @func {{delStage}}
     * @description {{delete tax}}
     */
    delSingle(_id)
    {
        //console.log(this.model);
        if(confirm("Are you sure to delete the template?"))
        {
            let fd = new FormData();
            fd.append('_ids',_id);
            fd.append('api_token', this.authUser.api_token);
            //return;
            this.userService.deleteMailAccounts(fd)
                .subscribe(
                    (data: any) => {
                        //console.log(data);
                        if (data.status === 'success') {
                            // message
                            this.msgs = [];
                            this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Template Successfully Deleted'});

                            // go to  my team page
                            this.getMailAccounts();
                        }
                        if (data.status === 'fail') {
                            // message
                            this.msgs = [];
                            if (data.errors && Object.keys(data.errors).length > 0)
                            {

                                for (var key in data.errors) {
                                    if (data.errors.hasOwnProperty(key)) {
                                        //console.log(key + " -> " + p[key]);
                                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                    }
                                }

                            }
                            else if(data.error_message)
                            {
                                this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                            }
                            else
                            {
                                this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                            }
                        }
                    },
                    error => {
                        console.log(error);
                        this.msgs = [];
                        if (error && error.message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                        }
                        //alert('Something went wrong!');
                    }
                );

        }


    }

    /**
     * @func {{delAll}}
     * @description {{delete selected taxes}}
     */
    delAll()
    {
        if(confirm("Are you sure to delete all selected templates?"))
        {
            let fd = new FormData();
            fd.append('_ids',this.selcArr);
            fd.append('api_token', this.authUser.api_token);
            //return;
            this.userService.deleteMailAccounts(fd)
                .subscribe(
                    (data: any) => {
                        //console.log(data);
                        if (data.status === 'success') {
                            // message
                            this.msgs = [];
                            this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Templates are successfully deleted'});
                            this.getMailAccounts();
                        }
                        if (data.status === 'fail') {
                            // message
                            this.msgs = [];
                            if (data.errors && Object.keys(data.errors).length > 0)
                            {

                                for (var key in data.errors) {
                                    if (data.errors.hasOwnProperty(key)) {
                                        //console.log(key + " -> " + p[key]);
                                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                    }
                                }

                            }
                            else if(data.error_message)
                            {
                                this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                            }
                            else
                            {
                                this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                            }
                        }
                    },
                    error => {
                        console.log(error);
                        this.msgs = [];
                        if (error && error.message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                        }
                        //alert('Something went wrong!');
                    }
                );

        }
    }

    selctToList($event) {
        //console.log($event.srcElement.checked);
        //console.log($event.srcElement.value);
        if($event.srcElement.checked == true)
        {
            if(this.selcArr.indexOf($event.srcElement.value)== -1)
                this.selcArr.push($event.srcElement.value);
        }else {

            this.selcArr.splice(this.selcArr.indexOf($event.srcElement.value), 1);
        }
        console.log(this.selcArr);
    }

}

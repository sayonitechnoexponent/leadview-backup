import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MailAccountComponent } from './mail-account.component';

describe('MailAccountComponent', () => {
  let component: MailAccountComponent;
  let fixture: ComponentFixture<MailAccountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MailAccountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MailAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UrlTimeLineComponent } from './url-time-line.component';

describe('UrlTimeLineComponent', () => {
  let component: UrlTimeLineComponent;
  let fixture: ComponentFixture<UrlTimeLineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UrlTimeLineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UrlTimeLineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

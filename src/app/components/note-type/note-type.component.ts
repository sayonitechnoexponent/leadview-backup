import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../services/common.service';
import { UserService } from '../../services/user.service';
import {Message} from 'primeng/primeng';
import { appConfig } from '../../apiurl.config';

declare var $ : any;

@Component({
  selector: 'app-note-type',
  templateUrl: './note-type.component.html',
  styleUrls: ['./note-type.component.css']
})
export class NoteTypeComponent implements OnInit {

  /**
     * @property \{{{any}}\} {{appConfig}} {{this will hold the constants for the app}}
     */

    appConfig: any = {};

    /**
     * @property \{{{Message[]}}\} {{msgs}} {{this is growl message model}}
     */

    msgs: Message[] = [];

    /**
     * @property \{{{any}}\} {{model}} {{this form model}}
     */

    model: any = {
        name : null,
        description : null,
        managers : [],
    };

    /**
     * @property \{{{any}}\} {{authUser}} {{this will hold the authuser}}
     */

    authUser: any ={};
    /**
     * @property \{{{any}}\} {{myList}} {{this will hold the list data}}
     */

    myList: any;

    /**
     * @property \{{{any}}\} {{selArr}} {{this will hold the selected from list}}
     */

    selArr: any =[];

    searchText : any;

    showName: boolean = true;
    showDesc: boolean = true;
    showMargin: boolean =true;

    displayColFrm: any = {
        itemName : true,
        itemDesc : true,
        itemMargin : true,
    };

    allUsers: any =[];
    selectedMngrs: any = [];

  constructor(
      private commonService: CommonService,
      private userService: UserService
  ) { }

    /**
     * @func {{saveDisplay}}
     * @description {{toggle display of list}}
     */
    saveDisplay(myForm) {

        console.log(this.displayColFrm);
        this.showName = this.displayColFrm.itemName;
        this.showDesc = this.displayColFrm.itemDesc;
        this.showMargin = this.displayColFrm.itemMargin;

    }
    /**
     * @func {{pageLoad}}
     * @description {{reload page after a certain time}}
     */
    pageLoad(){
        setTimeout(()=>{    //<<<---    using ()=> syntax
            location.reload()
        },2000);
    }

    /**
     * @func {{openCreatePop}}
     * @param \{{{string}}\} {{$data}} {{popup data val class}}
     * @description {{this will be open the popup accordingly on click by pass data val}}
     */
    openCreatePop($data){
        //console.log($data);
        this.commonService.openModal($data);
        this.model = {
            managers: []
        };
        this.model.type = "Add";
        //this.model = {};
    }

    /**
     * @func {{openEditModal}}
     * @param \{{{string}}\} {{$data}} {{popup data val class}}
     * @description {{this will me open the popup accordingly on click by pass data val}}
     */
    openEditModal($data,feed){
        this.model = {};
        let mngr = feed.managers;
        this.commonService.openModal($data);
        this.model.name = feed.name;
        this.model._id = feed.id;
        this.model.type = "Edit";
        this.model.managers = [];

        // console.log(mngr);
        if(mngr.length > 0){
            mngr.forEach((value: any) => {
                if(value.id != this.authUser.id){
                    this.model.managers.push(value.id );
                }
                // this.allUsers.push({'id':value.id ,'itemName': value.name });
            });
        }
        console.log(this.model.managers);
        mngr = [];
    }

    /**
     * @func {{addSaveType}}
     * @param \{{{formobject}}\} {{myform}} {{this will pass the form object}}
     * @description {{this will submit the form data and add or update the tax data}}
     */

    addSaveType(myform)
    {
        //alert(amnt);
        this.model.api_token = this.authUser.api_token;
        if(this.authUser.live_project)
        {
            this.model.projectId = this.authUser.live_project.id;
        }
        console.log(this.model);

        this.userService.addEditNoteType(this.model)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Type added successfully'});

                        // go to  my project page
                        this.pageLoad();
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );

    }
    /**
     * @func {{fetchType}}
     * @description {{get all Stages}}
     */
    fetchType() {
        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);

        this.userService.getNoteTypes(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        //this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Project Post Successful'});
                        this.myList =  (data.feed);
                        //console.log(this.myTeam);
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );

    }
    /**
     * @func {{delType}}
     * @description {{delete type}}
     */
    delType(_id)
    {
        //console.log(this.model);
        if(confirm("Are you sure to delete the type?"))
        {
            let fd = new FormData();
            fd.append('_ids',_id);
            fd.append('api_token', this.authUser.api_token);
            //return;
            this.userService.deleteNoteTypes(fd)
                .subscribe(
                    (data: any) => {
                        //console.log(data);
                        if (data.status === 'success') {
                            // message
                            this.msgs = [];
                            this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Type Successfully Deleted'});

                            // go to  my team page
                            this.pageLoad();
                        }
                        if (data.status === 'fail') {
                            // message
                            this.msgs = [];
                            if (data.errors && Object.keys(data.errors).length > 0)
                            {

                                for (var key in data.errors) {
                                    if (data.errors.hasOwnProperty(key)) {
                                        //console.log(key + " -> " + p[key]);
                                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                    }
                                }

                            }
                            else if(data.error_message)
                            {
                                this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                            }
                            else
                            {
                                this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                            }
                        }
                    },
                    error => {
                        console.log(error);
                        this.msgs = [];
                        if (error && error.message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                        }
                        //alert('Something went wrong!');
                    }
                );

        }


    }

    /**
     * @func {{delAll}}
     * @description {{delete selected taxes}}
     */
    delAll()
    {
        if(confirm("Are you sure to delete all selected types?"))
        {
            let fd = new FormData();
            fd.append('_ids',this.selArr);
            fd.append('api_token', this.authUser.api_token);
            //return;
            this.userService.deleteNoteTypes(fd)
                .subscribe(
                    (data: any) => {
                        //console.log(data);
                        if (data.status === 'success') {
                            // message
                            this.msgs = [];
                            this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Types are successfully deleted'});

                            this.pageLoad();
                        }
                        if (data.status === 'fail') {
                            // message
                            this.msgs = [];
                            if (data.errors && Object.keys(data.errors).length > 0)
                            {

                                for (var key in data.errors) {
                                    if (data.errors.hasOwnProperty(key)) {
                                        //console.log(key + " -> " + p[key]);
                                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                    }
                                }

                            }
                            else if(data.error_message)
                            {
                                this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                            }
                            else
                            {
                                this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                            }
                        }
                    },
                    error => {
                        console.log(error);
                        this.msgs = [];
                        if (error && error.message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                        }
                        //alert('Something went wrong!');
                    }
                );

        }
    }

    selctToList($event) {
        //console.log($event.srcElement.checked);
        //console.log($event.srcElement.value);
        if($event.srcElement.checked == true)
        {
            if(this.selArr.indexOf($event.srcElement.value)== -1)
                this.selArr.push($event.srcElement.value);
        }else {

            this.selArr.splice(this.selArr.indexOf($event.srcElement.value), 1);
        }
        //console.log(this.selcArr);
    }

    ngOnInit() {
        this.authUser = this.userService.getAuthUser();
        this.appConfig = appConfig;
        this.fetchType();
        this.fetchTeamUsers();

    }

    /**
     * @func {{fetchTeamUsers}}
     * @description {{get user teams}}
     */
    fetchTeamUsers() {
        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);
        fd.append('page_type', 'contacts');
        this.userService.getTeamUsers(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        //this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Project Post Successful'});
                        //this.allUsers =  (data.users);
                        data.users.forEach((value: any) => {
                            if(value.id != this.authUser.id){
                                let _name = value.first_name +' ' +value.last_name;
                                this.allUsers.push({'value':value.id ,'label': _name });
                            }
                            // this.allUsers.push({'id':value.id ,'itemName': value.name });
                        });
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});
                    }
                    //alert('Something went wrong!');
                }
            );

    }

}

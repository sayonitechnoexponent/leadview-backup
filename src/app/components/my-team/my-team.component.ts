import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonService } from '../../services/common.service';
import { UserService } from '../../services/user.service';
import {Message} from 'primeng/primeng';
import { appConfig } from '../../apiurl.config';

@Component({
  selector: 'app-my-team',
  templateUrl: './my-team.component.html',
  styleUrls: ['./my-team.component.css']
})
export class MyTeamComponent implements OnInit {
    /**
     * @property \{{{any}}\} {{appConfig}} {{this will hold the constants for the app}}
     */

    appConfig: any = {};

    /**
     * @property \{{{Message[]}}\} {{msgs}} {{this is growl message model}}
     */

    msgs: Message[] = [];

    /**
     * @property \{{{any}}\} {{model}} {{security form model}}
     */

    model: any = {
        title : null
    };

    /**
     * @property \{{{any}}\} {{authUser}} {{this will hold the authuser}}
     */

    authUser: any ={};
    /**
     * @property \{{{any}}\} {{myTeam}} {{this will hold the myTeam}}
     */

    myTeam: any;

    /**
     * @property \{{{any}}\} {{delTeamArr}} {{this will hold the Teams to deleted}}
     */

    delTeamArr: any =[];

    /**
     * @property \{{{any}}\} {{showTeamCol}} {{this will hold the Team column is show or not}}
     */

    showTeamCol: boolean =true;
    /**
     * @property \{{{any}}\} {{showUserCol}} {{this will hold the User column is show or not}}
     */

    showUserCol: boolean =true;
    /**
     * @property \{{{any}}\} {{showCreateDateCol}} {{this will hold the Create Date column is show or not}}
     */

    showCreateDateCol: boolean =true;

    /**
     * @property \{{{any}}\} {{displayColFrm}} {{this will hold the display form dta}}
     */

    displayColFrm: any = {
        teamName : true,
        noofUser : true,
        createDate : true
    };

    /**
     * @property \{{{any}}\} {{searchText}} {{this will hold the search data}}
     */

    searchText: any =[];

  constructor(
      private commonService: CommonService,
      private userService: UserService,
      private route: ActivatedRoute,
      private router: Router
  ) { }

  ngOnInit() {
      this.authUser = this.userService.getAuthUser();
      this.appConfig = appConfig;
      this.getMyTeam();
	  $(".leftpanelfilteremenuicon").click(function(event) {
			event.preventDefault();
			$('.leftpanelpopup').addClass("show");
			$('.filtermask').addClass("show");
			
		});
		$(".filtermask").click(function(event) {
			event.preventDefault();
			$('.leftpanelpopup').removeClass("show");
			$('.filtermask').removeClass("show");
			
		});
  }

    /**
     * @func {{saveDisplay}}
     * @description {{toggle display of list}}
     */
    saveDisplay(myForm) {

        //console.log(this.displayColFrm);
        this.showTeamCol = this.displayColFrm.teamName;
        this.showUserCol = this.displayColFrm.noofUser;
        this.showCreateDateCol = this.displayColFrm.createDate;

    }
    /**
     * @func {{openCreateTeam}}
     * @param \{{{string}}\} {{$data}} {{popup data val class}}
     * @description {{this will be open the popup accordingly on click by pass data val}}
     */
    openCreateTeam($data){
        this.commonService.openModal($data);
        this.model = {};
    }

    /**
     * @func {{editTeam}}
     * @param \{{{string}}\} {{$data}} {{popup data val class}}
     * @description {{this will be open the popup accordingly on click by pass data val}}
     */
    editTeam($data,team){
        this.commonService.openModal($data);
        //let pject = this.myProjects[ky]
        this.model = team;
        this.model.formType = 'edit';
        /*console.log(project);
        console.log(this.model);*/

    }
    /**
     * @func {{createTeam}}
     * @param \{{{formobject}}\} {{myform}} {{this will pass the form object}}
     * @description {{this will submit the form data and update the team data}}
     */
    createTeam(myform) {

        //console.log(this.model);
        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);
        fd.append('title', this.model.title);
        if(this.model.id)
        {
            fd.append('team_id', this.model.id);
        }

        this.userService.addTeam(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Team Successfully Saved'});

                        // go to  my team page
                        // this.router.navigate['/manage/my-team'] ;
                        location.reload();
                    }
                    else if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );

    }
    /**
     * @func {{getMyTeam}}
     * @description {{get user teams}}
     */
    getMyTeam() {
        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);
        this.userService.getTeam(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        //this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Project Post Successful'});
                        this.myTeam =  (data.teams);
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );


    }

    /**
     * @func {{delPTeam}}
     * @description {{delete user team}}
     */
    delPTeam(teamId)
    {
        //console.log(this.model);
        if(confirm("Are you sure to delete the team?"))
        {
            let fd = new FormData();
            fd.append('team_id',teamId);
            fd.append('api_token', this.authUser.api_token);
            //return;
            this.userService.deleteTeam(fd)
                .subscribe(
                    (data: any) => {
                        //console.log(data);
                        if (data.status === 'success') {
                            // message
                            this.msgs = [];
                            this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Team Successfully Deleted'});

                            // go to  my team page
                            location.reload();
                        }
                        if (data.status === 'fail') {
                            // message
                            this.msgs = [];
                            if (data.errors && Object.keys(data.errors).length > 0)
                            {

                                for (var key in data.errors) {
                                    if (data.errors.hasOwnProperty(key)) {
                                        //console.log(key + " -> " + p[key]);
                                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                    }
                                }

                            }
                            else if(data.error_message)
                            {
                                this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                            }
                            else
                            {
                                this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                            }
                        }
                    },
                    error => {
                        console.log(error);
                        this.msgs = [];
                        if (error && error.message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                        }
                        //alert('Something went wrong!');
                    }
                );

        }


    }

    selctToDel($event) {
        //console.log($event.srcElement.checked);
        //console.log($event.srcElement.value);
        if($event.srcElement.checked == true)
        {
            if(this.delTeamArr.indexOf($event.srcElement.value)== -1)
                this.delTeamArr.push($event.srcElement.value);
        }else {

            this.delTeamArr.splice(this.delTeamArr.indexOf($event.srcElement.value), 1);
        }
        console.log(this.delTeamArr);
    }

    /**
     * @func {{delAll}}
     * @description {{delete selected team}}
     */
    delAll()
    {
        if(confirm("Are you sure to delete all selected teams?"))
        {
            let fd = new FormData();
            fd.append('team_id',this.delTeamArr);
            fd.append('api_token', this.authUser.api_token);
            //return;
            this.userService.deleteTeam(fd)
                .subscribe(
                    (data: any) => {
                        //console.log(data);
                        if (data.status === 'success') {
                            // message
                            this.msgs = [];
                            this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Team Successfully Deleted'});

                            // go to  my team page
                            location.href = '/my-team';
                        }
                        if (data.status === 'fail') {
                            // message
                            this.msgs = [];
                            if (data.errors && Object.keys(data.errors).length > 0)
                            {

                                for (var key in data.errors) {
                                    if (data.errors.hasOwnProperty(key)) {
                                        //console.log(key + " -> " + p[key]);
                                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                    }
                                }

                            }
                            else if(data.error_message)
                            {
                                this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                            }
                            else
                            {
                                this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                            }
                        }
                    },
                    error => {
                        console.log(error);
                        this.msgs = [];
                        if (error && error.message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                        }
                        //alert('Something went wrong!');
                    }
                );

        }
    }
}

import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonService } from '../../../services/common.service';
import { UserService } from '../../../services/user.service';
import {Message} from 'primeng/primeng';
import { appConfig } from '../../../apiurl.config';

@Component({
  selector: 'app-team-users',
  templateUrl: './team-users.component.html',
  styleUrls: ['./team-users.component.css']
})
export class TeamUsersComponent implements OnInit {

    searchText = "";
    dropdownList = [];
    selectedItems:any = [];
    dropdownSettings = {};
    dropdownSettings2 = {};
    /**
     * @property \{{{any}}\} {{urlid}} {{this will hold the id value from urlid}}
     */
    urlid: any = '';
    /**
     * @property \{{{any}}\} {{appConfig}} {{this will hold the constants for the app}}
     */

    appConfig: any = {};

    /**
     * @property \{{{Message[]}}\} {{msgs}} {{this is growl message model}}
     */

    msgs: Message[] = [];

    /**
     * @property \{{{any}}\} {{model}} {{user form model}}
     */

    model: any = {
        profile_picture: null,
        first_name: '',
        last_name: '',
        name: '',
        email: null,
        phone_number: '',
        designation: '',
        teams:[],
        language:1,
        email_signature: ''
    };
    /**
     * @property \{{{any}}\} {{modelAutoAssign}} {{auto assign form model}}
     */

    modelAutoAssign: any = {
        user_id: null,
        countries: [],
        tags: [],
        states: [],
        languages: [],
    };
    /**
     * @property \{{{any}}\} {{passwordModel}} {{ password change form model}}
     */

    passwordModel: any = {
        password: '',
        user_id: '',
    };

    /**
     * @property \{{{any}}\} {{languages}} {{this will hold the all languages}}
     */

    languages: any ;
    bwlanguages: any =[];

    /**
     * @property \{{{any}}\} {{authUser}} {{this will hold the authuser}}
     */

    authUser: any ={};
    /**
     * @property \{{{any}}\} {{myTeam}} {{this will hold the myTeam}}
     */

    myTeam: any = [];
    /**
     * @property \{{{any}}\} {{allTeamUsers}} {{this will hold the all Team Users}}
     */

    allTeamUsers: any;
    /**
     * @property \{{{any}}\} {{selUserArr}} {{this will hold the all selected Users}}
     */

    selUserArr: any = [];
    /**
     * @property \{{{any}}\} {{permissions}} {{this will hold the all permissions}}
     */

    permissions: any;
    /**
     * @property \{{{any}}\} {{selPermissionArr}} {{this will hold the all selected permissions}}
     */

    selPermissionArr: any = [];
    /**
     * @property \{{{any}}\} {{currentUser}} {{this will hold the currently selected user}}
     */

    currentUser: any = {
        permission_arr: [],
        id: null
    };
    /**
     * @property \{{{any}}\} {{allTags}} {{this will hold the current user added all tags}}
     */

    allTags: any = [];
    /**
     * @property \{{{any}}\} {{countries}} {{this will hold all countries}}
     */

    allCountries: any = [];
    countries: any = [];
    slStates: any = [];

    /**
     * @property \{{{any}}\} {{displayColFrm}} {{this will hold the display form dta}}
     */

    displayColFrm: any = {
        avatar : true,
        firstName : true,
        email : true,
        lastName : true
    };

    showAvatar: boolean = true;
    showFirstName: boolean = true;
    showLastName: boolean = true;
    showEmail: boolean = true;
    blob_picture:any = null;

    constructor(
              private commonService: CommonService,
              private userService: UserService,
              private route: ActivatedRoute,
              private router: Router
    ) { }

    /**
     * @func {{openAssignModal}}
     * @param \{{{string}}\} {{$data}} {{popup data val class}}
     * @description {{this will me open the popup accordingly on click by pass data val}}
     */
    openAssignModal($data,user){
        this.commonService.openModal($data);
        this.modelAutoAssign.user_id = user.id;
        this.modelAutoAssign.tags =[];
        this.modelAutoAssign.countries =[];
        this.modelAutoAssign.states =[];
        this.modelAutoAssign.languages =[];

        user.tags.forEach((value: any) => {
            //dataArr.push(value[key]);
            //console.log(value.name);
            this.modelAutoAssign.tags.push({'id':value.id ,'itemName': value.title });
        });
        user.countries.forEach((value: any) => {
            this.modelAutoAssign.countries.push({'id':value.id ,'itemName': value.name });
        });
        user.states.forEach((value: any) => {
            this.modelAutoAssign.states.push({'id':value.id ,'itemName': value.name });
        });
        user.languages.forEach((value: any) => {
            this.modelAutoAssign.languages.push({'id':value.id ,'itemName': value.name });
        });


    }

    /**
     * @func {{saveDisplay}}
     * @description {{this will change list display}}
     */
    saveDisplay(myForm) {
        //console.log(this.displayColFrm);
        this.showAvatar = this.displayColFrm.avatar;
        this.showFirstName = this.displayColFrm.firstName;
        this.showLastName = this.displayColFrm.lastName;
        this.showEmail = this.displayColFrm.email;
    }
    /**
     * @func {{fetchTeamUsers}}
     * @description {{get user teams}}
     */
    fetchTeamUsers() {
        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);
        fd.append('team_id', this.urlid);
        this.userService.getTeamUsers(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        //this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Project Post Successful'});
                        this.allTeamUsers =  (data.users);
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);

                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );

    }

    /**
     * @func {{fetchAllPermissions}}
     * @description {{get all permissions}}
     */
    fetchAllPermissions() {
        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);
        fd.append('locale', this.authUser.locale);

        this.userService.getAllPermissions(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        //this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Project Post Successful'});
                        this.permissions =  (data.permissions);
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );


    }
    /**
     * @func {{getAllLanguages}}
     * @param \{{{formobject}}\} {{myform}} {{this will hold all languages }}
     */
    getAllLanguages() {

        this.userService.getLanguages()
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        this.languages = data.language;
                        this.languages.forEach((value: any) => {
                            //dataArr.push(value[key]);
                            //console.log(value.name);
                            this.bwlanguages.push({'id':value.id ,'itemName': value.name });
                        });
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );

    }
    /**
     * @func {{listCountries}}
     * @param \{{{formobject}}\} {{myform}} {{this will hold all countries }}
     */
    listCountries() {

        this.userService.allCountries()
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        this.allCountries = data.countries;
                        let vals = data.countries;
                        vals.forEach((value: any) => {
                            //dataArr.push(value[key]);
                            //console.log(value.name);
                            this.countries.push({'id':value.id ,'itemName': value.name });
                        });
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );

    }

    /**
     * @func {{getMyTeam}}
     * @description {{get user teams}}
     */
    getMyTeam() {
        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);
        fd.append('list_type', 'select');
        this.userService.getTeam(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        //this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Project Post Successful'});
                        this.myTeam =  (data.teams);
                        //console.log(this.myTeam);
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );

    }

    /**
     * @func {{getAllTags}}
     * @description {{get all Tags}}
     */
    getAllTags() {
        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);
        this.userService.featchTags(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        //this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Project Post Successful'});
                        //this.allTags =  (data.tags);
                        data.tags.forEach((value: any) => {
                            //console.log(value);
                            this.allTags.push({'id':value.id ,'itemName': value.title });
                        });
                        //console.log(this.myTeam);
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );


    }

    /**
     * @func {{openAssignteam}}
     * @param \{{{string}}\} {{$data}} {{popup data val class}}
     * @description {{this will me open the popup accordingly on click by pass data val}}
     */
    openAssignteam($data){
        this.commonService.openModal($data);
        //this.selUserArr = [];
        this.selectedItems = [];
    }
    /**
     * @func {{openCreateUser}}
     * @param \{{{string}}\} {{$data}} {{popup data val class}}
     * @description {{this will me open the popup accordingly on click by pass data val}}
     */
    openCreateUser($data){
        this.commonService.openModal($data);
        this.model = {
            profile_picture: null,
            first_name: '',
            last_name: '',
            name: '',
            email: null,
            phone_number: '',
            designation: '',
            teams:[],
            language:1
        };
    }

    /**
     * @func {{editUser}}
     * @param \{{{string}}\} {{$data}} {{popup data val class}}
     * @description {{this will me open the popup accordingly on click by pass data val}}
     */
    editUser($data,user){
        this.commonService.openModal($data);
        //let pject = this.myProjects[ky]
        this.blob_picture = null;
        this.model = user;
        this.model.user_id = user.id;
        this.model.userimage = user.profile_picture;
        // this.model.profile_picture = user.profile_picture;
        this.model.formType = 'edit';
        this.model.teams = user.selcteams;
        this.selectedItems = user.team_arr;
        this.model.email_signature = user.email_signature?user.email_signature.content:'';

        //console.log(this.model);

    }
    /**
     * @func {{openPermissionmodal}}
     * @param \{{{string}}\} {{$data}} {{popup data val class}}
     * @description {{this will me open the popup accordingly on click by pass data val}}
     */
    openPermissionmodal($data,user){
        this.commonService.openModal($data);
        this.currentUser = user;
        //console.log(this.currentUser);

    }
    /**
     * @func {{openResourcesmodal}}
     * @param \{{{string}}\} {{$data}} {{popup data val class}}
     * @description {{this will me open the popup accordingly on click by pass data val}}
     */
    openResourcesmodal($data,user){
        this.commonService.openModal($data);

        //console.log(this.model);

    }
    /**
     * @func {{openChangepassModal}}
     * @param \{{{string}}\} {{$data}} {{popup data val class}}
     * @description {{this will me open the popup accordingly on click by pass data val}}
     */
    openChangepassModal($data,user){
        this.commonService.openModal($data);
        this.passwordModel.user_id = user.id;
        //console.log(user);
    }

    /**
     * @func {{ngOnInit}}
     * *@param  {{event}} {{this will pass the change event}}
     * @description {{get the file}}
     */

    onFileChange(event) {

        if(event.target.files && event.target.files.length > 0) {
            let file = event.target.files[0];
            //console.log(file);
            this.model.user_picture = file;
        }
    }

    // file to base64
    getBase64(file ) {
        //console.log(file);
        return new Promise(function(resolve, reject) {
            var reader = new FileReader();
            reader.onload = function() { resolve(reader.result); };
            reader.onerror = reject;

            //console.log(reader.readAsDataURL(file));
            reader.readAsDataURL(file);
        });
    }
    selectFile(event) {
        //console.log(event);
        if(event.target.files && event.target.files.length > 0) {

            let imgfile = event.target.files[0];
            this.model.user_picture = imgfile;
            //console.log(this.contactModel.profile_picture);
            //this.blob_picture = imgfile;
            let blob = null;
            if (imgfile) blob = this.getBase64(imgfile);
            var that = this;
            blob.then(function (result) {
                //console.log(result);
                //that.blob_picture = that.sanitizer.bypassSecurityTrustUrl(result);
                that.blob_picture = (result);
                //alert(that.blob_picture);
            });
        }
    }
    /**
     * @func {{createUser}}
     * @param \{{{formobject}}\} {{myform}} {{this will pass the form object}}
     * @description {{this will submit the form data and update the project data}}
     */
    createUser(myform)
    {
        //console.log(this.model);

        let fd = new FormData();
        fd.append('profile_picture', this.model.user_picture);
        fd.append('api_token', this.authUser.api_token);
        fd.append('first_name', this.model.first_name);
        fd.append('last_name', this.model.last_name);
        fd.append('name', this.model.name	);
        fd.append('email', this.model.email);
        fd.append('phone_number', this.model.phone_number);
        fd.append('designation', this.model.designation);
        fd.append('teams', this.selectedItems);
        fd.append('email_signature', this.model.email_signature);

        //console.log(fd);
        if(this.model.user_id)
        {
            fd.append('user_id', this.model.user_id);
        }

        this.userService.createTeamUser(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'User added successfully'});

                        // go to  my project page
                        location.reload();
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );

    }

    /**
     * @func {{delTeamUser}}
     * @description {{delete user }}
     */
    delTeamUser(event,usrId)
    {
        //console.log(this.model);
        if(confirm("Are you sure to delete the user?"))
        {
            let fd = new FormData();
            if(usrId==0)    fd.append('user_id',this.selUserArr);
            else            fd.append('user_id',usrId);
            fd.append('api_token', this.authUser.api_token);

            this.userService.deleteTuser(fd)
                .subscribe(
                    (data: any) => {
                        //console.log(data);
                        if (data.status === 'success') {
                            // message
                            this.msgs = [];
                            this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'User Successfully Deleted'});

                            // go to  my project page
                            location.reload();
                        }
                        if (data.status === 'fail') {
                            // message
                            this.msgs = [];
                            if (data.errors && Object.keys(data.errors).length > 0)
                            {

                                for (var key in data.errors) {
                                    if (data.errors.hasOwnProperty(key)) {
                                        //console.log(key + " -> " + p[key]);
                                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                    }
                                }

                            }
                            else if(data.error_message)
                            {
                                this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                            }
                            else
                            {
                                this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                            }
                        }
                    },
                    error => {
                        //console.log(error);
                        this.msgs = [];
                        if (error && error.message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                        }
                        //alert('Something went wrong!');
                    }
                );

        }
    }

    onItemSelect(item:any){
        //console.log(item.id);

        if(this.selectedItems.indexOf(item.id)== -1)
            this.selectedItems.push(item.id);

        //console.log(this.selectedItems);
    }
    OnItemDeSelect(item:any){
        //console.log(item.id);

        if(this.selectedItems.indexOf(item.id) != -1)
            this.selectedItems.splice(this.selectedItems.indexOf(item.id), 1);
        //console.log(this.selectedItems);

    }
    onSelectAll(items: any){
        //console.log(items);
    }
    onDeSelectAll(items: any){
       // console.log(items);
    }
    selctUsers($event) {
        //console.log($event.srcElement.checked);
        //console.log($event.srcElement.value);
        if($event.srcElement.checked == true)
        {
            if(this.selUserArr.indexOf($event.srcElement.value)== -1)
                this.selUserArr.push($event.srcElement.value);
        }else {

            this.selUserArr.splice(this.selUserArr.indexOf($event.srcElement.value), 1);
        }

        //console.log(this.selUserArr);
    }

    /**
     * @func {{assignUsersToTeam}}
     * @description {{this will assign Users To Teams}}
     */
    assignUsersToTeam()
    {
        //console.log(this.selUserArr);
        //console.log(this.selectedItems);
        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);
        fd.append('teams', this.selectedItems);
        fd.append('users', this.selUserArr);

        this.userService.assignTeamUser(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Team assigned successfully'});

                        // go to  my project page
                        location.reload();
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );
    }
    /**
     * @func {{sortTeam}}
     * @param \{{{formobject}}\} {{event}} {{this will pass the team Id}}
     * @description {{this will sort Users by Teams}}
     */
    sortTeam(event)
    {
        //alert(event.srcElement.value);
        if(event.srcElement.value !=0)
        {
            let fd = new FormData();
            fd.append('api_token', this.authUser.api_token);
            fd.append('team_id', event.srcElement.value);

            this.userService.getTamDetails(fd)
                .subscribe(
                    (data: any) => {
                        //console.log(data);
                        if (data.status === 'success') {
                            // message
                            this.msgs = [];
                            this.allTeamUsers =  (data.team.users);
                            // go to  my project page
                            //location.reload();
                        }
                        if (data.status === 'fail') {
                            // message
                            this.msgs = [];
                            if (data.errors && Object.keys(data.errors).length > 0)
                            {

                                for (var key in data.errors) {
                                    if (data.errors.hasOwnProperty(key)) {
                                        //console.log(key + " -> " + p[key]);
                                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                    }
                                }

                            }
                            else if(data.error_message)
                            {
                                this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                            }
                            else
                            {
                                this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                            }
                        }
                    },
                    error => {
                        //console.log(error);
                        this.msgs = [];
                        if (error && error.message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                        }
                        //alert('Something went wrong!');
                    }
                );
        }else{
            this.fetchTeamUsers();
        }
    }

    /**
     * @func {{changePassword}}
     * @param \{{{formobject}}\} {{user}} {{this will pass the form object}}
     * @description {{this will submit the form data and change user password}}
     */

    changePassword(user)
    {
        //console.log(this.passwordModel);

        this.passwordModel.user_id = user.id;
        this.passwordModel.api_token = this.authUser.api_token;
        this.userService.setUserPassword(this.passwordModel)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Successfully sent password link in '+user.email});

                        // go to  my project page
                        //location.reload();
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );
    }

    selctPermissions($event) {
        //console.log($event.srcElement.checked);
        //console.log($event.srcElement.value);
        if($event.srcElement.checked == true)
        {
            if(this.selPermissionArr.indexOf($event.srcElement.value)== -1)
                this.selPermissionArr.push($event.srcElement.value);
        }else {

            this.selPermissionArr.splice(this.selPermissionArr.indexOf($event.srcElement.value), 1);
        }

        //console.log(this.selPermissionArr);
    }

    /**
     * @func {{assignUsersToTeam}}
     * @description {{this will assign Users To Teams}}
     */
    assignUserPermissions()
    {
        //console.log(this.currentUser);
        //console.log(this.selPermissionArr);
        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);
        fd.append('permissions', this.selPermissionArr);
        fd.append('user_id', this.currentUser.id);

        this.userService.setUserPermissions(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Permission saved successful'});

                        // go to  my project page
                        location.reload();
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );
    }

    onCountrySelect(item:any){

        this.allCountries[item.id].states.forEach((value: any) => {
            //dataArr.push(value[key]);
            //console.log(value.name);
            this.slStates.push({'id':value.id ,'itemName': value.name });
        });
    }

    onCountryDeSelect(item:any){
        //console.log(item.id);
        //this.slStates.splice(this.slStates.indexOf(item.id), 1);
    }
    /**
     * @func {{setAutoAssign}}
     * @param \{{{formobject}}\} {{myform}} {{this will pass the form object}}
     * @description {{this will submit the form data and change user password}}
     */

    setAutoAssign(myform)
    {
        this.modelAutoAssign.api_token = this.authUser.api_token;
        //console.log(this.modelAutoAssign);
        this.userService.setAutoAssignment(this.modelAutoAssign)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Auto Assignment saved successful'});

                        // go to  my project page
                        location.reload();
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );
    }

    ngOnInit() {

        // get the route id from param
        this.route.params.subscribe(params => {
            // console.log(params);
            this.urlid = params['teamId'];
        });
        this.authUser = this.userService.getAuthUser();
        this.appConfig = appConfig;
        this.getAllLanguages();
        this.getMyTeam();
        this.fetchTeamUsers();
        this.fetchAllPermissions();
        this.getAllTags();
        this.listCountries();

        this.dropdownSettings = {
            singleSelection: false,
            text:"Select Team",
            enableCheckAll: false,
            selectAllText:'Select All',
            unSelectAllText:'UnSelect All',
            enableSearchFilter: true,
            classes:"myclass custom-class"
        };

        this.dropdownSettings2 = {
            singleSelection: false,
            text:"Select",
            enableCheckAll: false,
            enableSearchFilter: true,
            classes:"myclass custom-class"
        };
		$(".leftpanelfilteremenuicon").click(function(event) {
			event.preventDefault();
			$('.leftpanelpopup').addClass("show");
			$('.filtermask').addClass("show");
			
		});
		$(".filtermask").click(function(event) {
			event.preventDefault();
			$('.leftpanelpopup').removeClass("show");
			$('.filtermask').removeClass("show");
			
		});
        //this.model.teams = [];
    }


}

import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonService } from '../../services/common.service';
import { UserService } from '../../services/user.service';
import {Message} from 'primeng/primeng';
import { appConfig } from '../../apiurl.config';


@Component({
  selector: 'app-auto-tag',
  templateUrl: './auto-tag.component.html',
  styleUrls: ['./auto-tag.component.css']
})
export class AutoTagComponent implements OnInit {
    searchText :string = '';
    selcArr :any =[];
    /**
     * @property \{{{any}}\} {{appConfig}} {{this will hold the constants for the app}}
     */

    appConfig: any = {};

    /**
     * @property \{{{Message[]}}\} {{msgs}} {{this is growl message model}}
     */

    msgs: Message[] = [];

    /**
     * @property \{{{any}}\} {{model}} {{user form model}}
     */

    model: any = {
        score: 0,
        status: 1,
        delay: 0,
        scroll: 0,
        remove_score: 0
    };

    /**
     * @property \{{{any}}\} {{authUser}} {{this will hold the authuser}}
     */

    authUser: any ={};

    /**
     * @property \{{{any}}\} {{allTags}} {{this will hold the current user added all tags}}
     */

    allTags: any = [];
    allTagUrl: any = [];
    selectActivity: any = {};
    tag_scores: any = [];

  constructor(
      private commonService: CommonService,
      private router: Router,
      private userService: UserService) { }

  ngOnInit() {
      this.authUser = this.userService.getAuthUser();
      this.appConfig = appConfig;
      this.liveProject();
  }
    /**
     * @func {{liveProject}}
     * @description {{get live Project & Tags}}
     */
    liveProject() {
        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);
        this.userService.getLiveProject(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        //this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Project Post Successful'});
                        if(data.live_project.visitor_activities)
                            this.allTagUrl =  (data.live_project.visitor_activities);
                            this.allTags =  (data.tags);
                        /*data.tags.forEach((value: any) => {
                            //console.log(value);
                            this.allTags.push({'id':value.id ,'itemName': value.title });
                        });*/
                        //console.log(this.myTeam);
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );


    }
    /**
     * @func {{openTagModal}}
     * @param \{{{string}}\} {{$data}} {{popup data val class}}
     * @description {{this will me open the popup accordingly on click by pass data val}}
     */
    openTagModal($data,tagActivity){
        this.model= {
            score:0,
            status: 1,
            delay: 0,
            scroll: 0,
            remove_score: 0
        };
        this.selectActivity = tagActivity;
        this.model.type = 'Add';
        // console.log(this.selectActivity);
        this.getAllActivityTags();
        this.commonService.openModal($data);
    }

    editTag(tagscorr){
        this.model = tagscorr;
        // console.log(this.model);
        if(this.model.status == 0){
            this.model.status = false;
        }else{
            this.model.status = true;
        }
        if(this.model.remove_score == 0){
            this.model.remove_score = false;
        }else{
            this.model.remove_score = true;
        }
    }
    /**
     * @func {{getAllTags}}
     * @description {{get all Tags}}
     */
    addSaveTagScore(myform) {
        this.model.api_token = this.authUser.api_token;
        this.model.activity_id = this.selectActivity.activity_id;
        if(this.model.status == false){
            this.model.status = 0;
        }else{
            this.model.status = 1;
        }
        if(!this.model.remove_score)    this.model.remove_score = 0;
        // console.log(this.model);
        this.userService.createTagScore(this.model)
            .subscribe(
                (data: any) => {
                    // console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Tag successfully added'});
                        if(data.activity) this.tag_scores = data.activity.tag_scores;
                        this.model= {
                            score: 0,
                            status: 1,
                            delay: 0,
                            scroll: 0
                        };
                        this.getAllActivityTags();
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );
    }

    /**
     * @func {{getAllTags}}
     * @description {{get all Tags}}
     */
    getAllActivityTags() {
        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);
        fd.append('activity_id', this.selectActivity.activity_id);
        this.userService.fetchTagScores(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        //this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Project Post Successful'});
                        if(data.activity) this.tag_scores = data.activity.tag_scores;
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );
    }

    delTagScore(tagId) {
        if(confirm("Are you sure to delete the tag?"))
        {
            let fd = new FormData();
            fd.append('api_token', this.authUser.api_token);
            fd.append('activity_id', this.selectActivity.activity_id);
            fd.append('tag_id', tagId);
            this.userService.deleteTagScore(fd)
                .subscribe(
                    (data: any) => {
                        //console.log(data);
                        if (data.status === 'success') {
                            // message
                            this.msgs = [];
                            //this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Project Post Successful'});
                            if(data.tag_list) this.tag_scores = data.tag_list;
                        }
                        if (data.status === 'fail') {
                            // message
                            this.msgs = [];
                            if (data.errors && Object.keys(data.errors).length > 0)
                            {

                                for (var key in data.errors) {
                                    if (data.errors.hasOwnProperty(key)) {
                                        //console.log(key + " -> " + p[key]);
                                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                    }
                                }

                            }
                            else if(data.error_message)
                            {
                                this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                            }
                            else
                            {
                                this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                            }
                        }
                    },
                    error => {
                        //console.log(error);
                        this.msgs = [];
                        if (error && error.message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                        }
                        //alert('Something went wrong!');
                    }
                );
        }

    }

    openFileTimeLine(fid){
        localStorage.setItem('urlTl', fid);
        this.router.navigate(['/user/url-timeline']);
    }

}

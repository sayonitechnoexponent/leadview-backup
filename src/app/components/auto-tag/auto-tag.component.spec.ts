import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutoTagComponent } from './auto-tag.component';

describe('AutoTagComponent', () => {
  let component: AutoTagComponent;
  let fixture: ComponentFixture<AutoTagComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutoTagComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutoTagComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

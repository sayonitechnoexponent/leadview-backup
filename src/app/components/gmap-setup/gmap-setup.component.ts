import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../services/common.service';
import { UserService } from '../../services/user.service';
import {Message} from 'primeng/primeng';
import { appConfig } from '../../apiurl.config';
declare var $ : any;
declare var google : any;

@Component({
  selector: 'app-gmap-setup',
  templateUrl: './gmap-setup.component.html',
  styleUrls: ['./gmap-setup.component.css']
})
export class GmapSetupComponent implements OnInit {
    /**
     * @property \{{{any}}\} {{appConfig}} {{this will hold the constants for the app}}
     */

    appConfig: any = {};

    /**
     * @property \{{{Message[]}}\} {{msgs}} {{this is growl message model}}
     */

    msgs: Message[] = [];
    /**
     * @property \{{{any}}\} {{authUser}} {{this will hold the auth User data}}
     */

    authUser: any = {};

    /**
     * @property \{{{any}}\} {{model}} {{user form model}}
     */

    model: any = {
        show_company : 1,
        show_contact : 1
    };

    constructor( private commonService: CommonService,
                 private userService: UserService
    ){}

  ngOnInit() {
      this.authUser = this.userService.getAuthUser();
      this.appConfig = appConfig;
      this.fetchMapSetting();
  }

  /**
     * @func {{saveMapSetting}}
     * @param \{{{formobject}}\} {{myform}} {{this will pass the form object}}
     * @description {{this will submit the form data and save the ChatBox data}}
     */
    saveMapSetting(myform)
    {
        console.log(this.model);
        this.model.api_token = this.authUser.api_token;
        this.userService.saveMapFeature(this.model)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Map setup successfully saved'});

                        // go to  my project page
                        // location.reload();
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );

    }

    /**
     * @func {{fetchChatBox}}
     * @description {{this will submit the form data and save the ChatBox data}}
     */
    fetchMapSetting()
    {
        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);

        this.userService.getMapFeature(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        // this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'ChatBox successfully saved'});
                        if(data.map_setup != null)    this.model = data.map_setup;
                        console.log(this.model);
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );

    }

    checkOnOff(val){
        // console.log(val);
        let shwStatus='checked'
        if(val==0)  shwStatus='';

        return  shwStatus;
    }

    setOnOffVal(event){
        // console.log(event.srcElement.checked);
        // console.log(event.srcElement.value);
        let shwStatus=1;
        if(event.srcElement.checked==false)  shwStatus = 0;

        this.model[event.srcElement.name] = shwStatus;
        // console.log(this.model);
    }

}

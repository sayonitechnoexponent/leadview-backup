import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GmapSetupComponent } from './gmap-setup.component';

describe('GmapSetupComponent', () => {
  let component: GmapSetupComponent;
  let fixture: ComponentFixture<GmapSetupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GmapSetupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GmapSetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

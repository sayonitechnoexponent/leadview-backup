import {
    Component,
    OnInit,
    AfterViewInit,
    OnDestroy,
    ViewChild,
    ElementRef,
    ChangeDetectorRef
} from '@angular/core';

import { NgForm } from '@angular/forms';
import { CommonService } from '../../services/common.service';
import { UserService } from '../../services/user.service';
import { Message } from 'primeng/primeng';
import { appConfig } from '../../apiurl.config';
import { json } from "ng2-validation/dist/json";
import { Router, ActivatedRoute } from '@angular/router';

declare var $: any;
declare var stripe: any;
declare var elements: any;
@Component({
    selector: 'app-my-project',
    templateUrl: './my-project.component.html',
    styleUrls: ['./my-project.component.css']
})
export class MyProjectComponent implements OnInit, AfterViewInit, OnDestroy {
    @ViewChild('cardInfo') cardInfo: ElementRef;

    card: any;
    cardHandler = this.onChange.bind(this);
    error: string;
    /**
     * @property \{{{any}}\} {{appConfig}} {{this will hold the constants for the app}}
     */

    appConfig: any = {};
    /**
     * @property \{{{any}}\} {{model}} {{security form model}}
     */

    model: any = {
        project_image: null,
        title: null,
        description: null,
        country: '231',
        timezone: null,
    };
    billingModel: any = {
        company_name: null,
        tax_number: null,
        street_address: null,
        unit: null,
        country_id: null,
        city: null,
        region: null,
        zipcode: null,
        phone_number: null,
        cardNumber: null
    }
    /**
     * @property \{{{any}}\} {{authUser}} {{this will hold the authuser}}
     */

    authUser: any = {};
    /**
     * @property \{{{any}}\} {{myProjects}} {{this will hold the authuser}}
     */

    myProjects: any = [];
    /**
     * @property \{{{any}}\} {{timezones}} {{this will hold the authuser}}
     */
    project_Id: any;
    payment_token: any;
    billing_details: any = [];
    timezones: any = [];
    /**
     * @property \{{{Message[]}}\} {{msgs}} {{this is growl message model}}
     */

    msgs: Message[] = [];

    img_path: string = "";
    browsImage: any;
    allCountries: any = [];
    showCard: boolean = true;
    showCardNumber: boolean = true;
    meaasge: string;
    messageSubscription: string;
    showSubscription: boolean = false;
    /**
     * @constructor
     * @description {{DI will pushed here}}
     */
    constructor(
        private commonService: CommonService,
        private userService: UserService,
        private router: Router,
        private cd: ChangeDetectorRef
    ) { }

    /**
     * @func {{ngOnInit}}
     * @description {{this is lifecycle hook}}
     */
    ngOnInit() {
        this.authUser = this.userService.getAuthUser();
        this.appConfig = appConfig;
        this.img_path = this.appConfig.publicUrl + '/uploads/project/thumbs/';
        this.getMyProjects();
        this.getAllCountries();
    }
    getAllCountries() {

        this.userService.allCountries()
            .subscribe(
                (data: any) => {
                    // console.log(data);
                    if (data.status === 'success') {
                        this.allCountries = data.countries;
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0) {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                                }
                            }

                        }
                        else if (data.error_message) {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                        }
                        else {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message) {
                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });
                    }
                    //alert('Something went wrong!');
                }
            );

    }

    /**
     * @func {{addProject}}
     * @param \{{{string}}\} {{$data}} {{popup data val class}}
     * @description {{this will me open the popup accordingly on click by pass data val}}
     */
    addProject($data) {
        this.commonService.openModal($data);
        this.model = {};
        this.model.project_image = null;
        this.model.country = 231;
        this.model.formType = 'add';
        //this.model = null;
        //console.log(this.projectData);
    }

    editProject($data, project) {
        this.commonService.openModal($data);
        //let pject = this.myProjects[ky]
        this.model = project;
        this.model.project_id = project.id;
        this.model.projimage = project.project_image;
        this.model.project_image = null;
        this.model.formType = 'edit';
        /*console.log(project);
        console.log(this.model);*/

    }
    /**
     * @func {{getMyProjects}}
     * @description {{get user projects}}
     */
    getMyProjects() {
        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);
        this.userService.myProjects(fd)
            .subscribe(
                (data: any) => {
                    console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        //this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Project Post Successful'});
                        this.myProjects = (data.projects);
                        if(this.myProjects.length == 0){
                            this.router.navigate(['/my-project']);    
                        }
                        this.timezones = JSON.parse(data.timezones);
                        console.log(this.myProjects);
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0) {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                                }
                            }

                        }
                        else if (data.error_message) {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                        }
                        else {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                        }
                    }
                },
                error => {
                    console.log(error);
                    this.msgs = [];
                    if (error && error.message) {
                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

                    }
                    //alert('Something went wrong!');
                }
            );


    }

    // file to base64
    getBase64(file) {
        //console.log(file);
        return new Promise(function (resolve, reject) {
            var reader = new FileReader();
            reader.onload = function () { resolve(reader.result); };
            reader.onerror = reject;

            //console.log(reader.readAsDataURL(file));
            reader.readAsDataURL(file);
        });
    }

    /**
     * @func {{ngOnInit}}
     * *@param  {{event}} {{this will pass the change event}}
     * @description {{get the file}}
     */
    onFileChange(event) {

        if (event.target.files && event.target.files.length > 0) {
            let file = event.target.files[0];
            //console.log(file);
            this.model.project_image = file;

            let blob = null;
            if (file) blob = this.getBase64(file);
            var that = this;
            blob.then(function (result) {
                //console.log(result);
                //that.blob_picture = that.sanitizer.bypassSecurityTrustUrl(result);
                that.browsImage = (result);
            });
        }
    }
    /**
     * @func {{postProject}}
     * @param \{{{formobject}}\} {{myform}} {{this will pass the form object}}
     * @description {{this will submit the form data and update the project data}}
     */
    postProject(myform) {

        console.log(this.model);
        let fd = new FormData();
        fd.append('project_image', this.model.project_image);
        fd.append('api_token', this.authUser.api_token);
        fd.append('title', this.model.title);
        fd.append('description', this.model.description);
        fd.append('country', this.model.country);
        fd.append('timezone', this.model.timezone);
        if (this.model.project_id) {
            fd.append('project_id', this.model.project_id);
        }

        this.userService.saveProject(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        this.msgs.push({ severity: 'success', summary: 'Success Message', detail: 'Project Post Successful' });

                        // go to  my project page
                        location.href = '/my-project';
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0) {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                                }
                            }

                        }
                        else if (data.error_message) {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                        }
                        else {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message) {
                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

                    }
                    //alert('Something went wrong!');
                }
            );

    }

    /**
     * @func {{delProject}}
     * @description {{delete user project}}
     */
    delProject(projectId) {
        //console.log(this.model);
        if (confirm("Are you sure to delete the project?")) {
            let fd = new FormData();
            fd.append('project_id', projectId);
            fd.append('api_token', this.authUser.api_token);
            //return;
            this.userService.deleteProject(fd)
                .subscribe(
                    (data: any) => {
                        //console.log(data);
                        if (data.status === 'success') {
                            // message
                            this.msgs = [];
                            this.msgs.push({ severity: 'success', summary: 'Success Message', detail: 'Project Successfully Deleted' });

                            // go to  my project page
                            location.href = '/my-project';
                        }
                        if (data.status === 'fail') {
                            // message
                            this.msgs = [];
                            if (data.errors && Object.keys(data.errors).length > 0) {

                                for (var key in data.errors) {
                                    if (data.errors.hasOwnProperty(key)) {
                                        //console.log(key + " -> " + p[key]);
                                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                                    }
                                }

                            }
                            else if (data.error_message) {
                                this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                            }
                            else {
                                this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                            }
                        }
                    },
                    error => {
                        console.log(error);
                        this.msgs = [];
                        if (error && error.message) {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

                        }
                        //alert('Something went wrong!');
                    }
                );

        }


    }

    setProjectlive(projectId) {
        // alert(projectId);
        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);
        fd.append('project_id', projectId);
        fd.append('live_status', '1');

        this.userService.setUserProjectLive(fd)
            .subscribe(
                (data: any) => {
                    console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        //this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Project Post Successful'});
                        this.myProjects = (data.projects);
                        this.authUser.live_project = data.live_project;
                        localStorage.setItem('leaduser', JSON.stringify(this.authUser));
                        // this.timezones = JSON.parse(data.timezones);
                        location.href = '/time-line';
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0) {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                                }
                            }

                        }
                        else if (data.error_message) {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                        }
                        else {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                        }
                    }
                },
                error => {
                    console.log(error);
                    this.msgs = [];
                    if (error && error.message) {
                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

                    }
                    //alert('Something went wrong!');
                }
            );
    }
    closeModalBox() {
        $(".topsecboxbox").hide();
        $(".sectoshow").show();
        $('body').removeClass('open_modal');
        $('.formwrap').css({ 'display': 'none', 'top': -5200 });
        setTimeout(function () {
            $('.formwrap').css({ 'display': 'block', 'min-height': '1px' });
        }, 1000);
        $('.ovelay').addClass('hidden');
        $('.cntactadd .container-fluid').css({ 'min-height': 0 });
        $('.cntactadd').removeClass('open_popup');
        this.billingModel = {};
        this.cardInfo.nativeElement == null;
    }

    ngAfterViewInit() {
        const style = {
            base: {
                lineHeight: '24px',
                fontFamily: 'monospace',
                fontSmoothing: 'antialiased',
                fontSize: '19px',
                '::placeholder': {
                    color: 'purple'
                }
            }
        };

        this.card = elements.create('card', { style });
        this.card.mount(this.cardInfo.nativeElement);    
        this.card.addEventListener('change', this.cardHandler);
    }

    ngOnDestroy() {
        this.card.removeEventListener('change', this.cardHandler);
        this.card.destroy();
    }
    onChange({ error }) {
        if (error) {
            this.error = error.message;
          } else {
            this.error = null;
            this.meaasge = "";
          }
          this.cd.detectChanges();
          this.meaasge = "";
    }
    /**
  * @func {{addBillingAddr}}
  * @param \{{{string}}\} {{$data}} {{popup data val class}}
  * @description {{this will me open the popup accordingly on click by pass data val}}
  */
    addBillingAddr($data, project_id, billingDetails) {
        this.billing_details = billingDetails;
        this.project_Id = project_id;
        
        if ($data && project_id && (billingDetails == null)) {
            console.log(this.billing_details)
            this.commonService.openModal($data);
            this.billingModel.formType = 'add';
            this.billingModel.title = 'Save';
            this.showCardNumber = false;
            this.showCard = true;
            $('.checkout').css('display', 'block');
            this.card.removeEventListener('change', this.cardHandler);
            this.card.destroy();
            this.meaasge = "";
            this.error = "";
            const style = {
                base: {
                    lineHeight: '24px',
                    fontFamily: 'monospace',
                    fontSmoothing: 'antialiased',
                    fontSize: '19px',
                    '::placeholder': {
                        color: 'purple'
                    }
                }
            };
            this.card = elements.create('card', { style });
            this.card.mount(this.cardInfo.nativeElement);
            console.log(this.card);
            this.card.addEventListener('change', this.cardHandler);
        }
        else if ($data && (billingDetails != null)) {
            console.log(this.billing_details)
            this.meaasge = "";
            this.error = "";
            $('.checkout').css('display', 'none');

            this.commonService.openModal($data);
            if (billingDetails.customers.length > 0) {
                this.showCardNumber = true;
                this.showCard = false;
                this.billingModel.customer_id = billingDetails.customers[0].customer_id,
                    this.billingModel.cardNumber = billingDetails.customers[0].card_number ? `XXXX XXXX XXXX ${billingDetails.customers[0].card_number}` : ''
            }
            else if((billingDetails.customers.length == 0)){
                this.showCardNumber = false;
                this.showCard = true;
                $('.checkout').css('display', 'block');
                this.card.removeEventListener('change', this.cardHandler);
                this.card.destroy();
                const style = {
                    base: {
                        lineHeight: '24px',
                        fontFamily: 'monospace',
                        fontSmoothing: 'antialiased',
                        fontSize: '19px',
                        '::placeholder': {
                            color: 'purple'
                        }
                    }
                };
                this.card = elements.create('card', { style });
                this.card.mount(this.cardInfo.nativeElement);
                this.card.addEventListener('change', this.cardHandler);
            }
            this.billingModel.id = billingDetails.id,
                this.billingModel.company_name = billingDetails.company_name,
                this.billingModel.tax_number = billingDetails.tax_number,
                this.billingModel.street_address = billingDetails.street_address,
                this.billingModel.unit = billingDetails.unit,
                this.billingModel.country_id = billingDetails.country_id,
                this.billingModel.city = billingDetails.city,
                this.billingModel.region = billingDetails.region,
                this.billingModel.zipcode = billingDetails.zipcode,
                this.billingModel.phone_number = billingDetails.phone_number,
                this.billingModel.formType = 'edit';
                this.billingModel.title = 'Save';
            // this.showCardNumber = true;
            // this.showCard = false;
        }
    }

    //  final card delete
    finalCardDelete() {
        console.log(this.billing_details)
        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);
        fd.append('customer_id', this.billing_details.customers[0].customer_id);
        fd.append('card_id', this.billing_details.customers[0].default_card_id);
        this.userService.deleteCard(fd)
            .subscribe(
                (data: any) => {
                    console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        this.msgs.push({ severity: 'success', summary: 'Success Message', detail: 'Card Successfully Deleted' });

                        // go to  my project page
                        // location.href = '/my-project';
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0) {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                                }
                            }

                        }
                        else if (data.error_message) {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                        }
                        else {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                        }
                    }
                },
                error => {
                    console.log(error);
                    this.msgs = [];
                    if (error && error.message) {
                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

                    }
                    //alert('Something went wrong!');
                }
            );
    }

    // manual card delete
    deletecard() {

        if (confirm("Are you sure to delete the card?")) {
            this.showCardNumber = false;
            this.showCard = true;
            $('.checkout').css('display', 'block');
            this.card.removeEventListener('change', this.cardHandler);
            this.card.destroy();
            this.meaasge = "Please enter the new card details"
            const style = {
                base: {
                    lineHeight: '24px',
                    fontFamily: 'monospace',
                    fontSmoothing: 'antialiased',
                    fontSize: '19px',
                    '::placeholder': {
                        color: 'purple'
                    }
                }
            };
            this.card = elements.create('card', { style });
            this.card.mount(this.cardInfo.nativeElement);
            console.log(this.card);
            this.card.addEventListener('change', this.cardHandler);
        }
    }

    // save billingModel
    async saveBillingDetails(form) {
        if (this.billingModel.formType == 'add') {
            const { token, error } = await stripe.createToken(this.card);

            if (error) {
              console.log('Something is wrong:', error);
            } else {
              console.log('Success!', token);
              this.payment_token = token ;
              // ...send the token to the your backend to process the charge
            }
            if (this.payment_token) {
                this.billingModel.title = 'Saving';
                this.billingModel.stripe_token = this.payment_token.id;
                this.billingModel.card_number = this.payment_token.card.last4
            }
            this.billingModel.api_token = this.authUser.api_token;
            this.billingModel.project_id = this.project_Id;
            this.billingModel.billing_method = "1";
            console.log(this.billingModel);
            if (this.billingModel.stripe_token) {
                this.userService.addBillingDetails(this.billingModel)
                    .subscribe(
                        (data: any) => {
                            console.log(data);
                            if (data.status === 'success') {
                                // message
                                this.msgs = [];
                                this.msgs.push({ severity: 'success', summary: 'Success Message', detail: 'Biiling Details  added successfully' });
                                this.closeModalBox();
                                this.getMyProjects();
                            }
                            if (data.status === 'fail') {
                                // message
                                this.msgs = [];
                                if (data.errors && Object.keys(data.errors).length > 0) {

                                    for (var key in data.errors) {
                                        if (data.errors.hasOwnProperty(key)) {
                                            //console.log(key + " -> " + p[key]);
                                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                                        }
                                    }

                                }
                                else if (data.error_message) {
                                    this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                                }
                                else {
                                    this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                                }
                            }
                        },
                        error => {
                            //console.log(error);
                            this.msgs = [];
                            if (error && error.message) {
                                this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

                            }
                        }
                    );
            }

        }
        else if (this.billingModel.formType == 'edit') {
            if (this.showCardNumber == false) {
                // this.finalCardDelete();
                const { token, error } = await stripe.createToken(this.card);

                if (error) {
                    console.log('Something is wrong:', error);
                } else {
                    console.log('token', token)
                    //   console.log('Success!', token);
                    this.payment_token = token
                    // ...send the token to the your backend to process the charge

                }
                if (this.billing_details.customers[0]) {
                    this.billingModel.title = 'Saving...';
                    this.billingModel.customer_id = this.billing_details.customers[0].customer_id;
                    this.billingModel.card_id = this.billing_details.customers[0].default_card_id;
                }
                this.billingModel.stripe_token = this.payment_token.id;
                this.billingModel.api_token = this.authUser.api_token;
                this.billingModel.project_id = this.project_Id;
                this.billingModel.billing_method = "1";
                this.billingModel.card_number = this.payment_token.card.last4,
                    console.log(this.billingModel);
                if (this.billingModel.stripe_token) {
                    this.userService.addBillingDetails(this.billingModel)
                        .subscribe(
                            (data: any) => {
                                console.log(data);
                                if (data.status === 'success') {
                                    // message
                                    this.msgs = [];
                                    this.msgs.push({ severity: 'success', summary: 'Success Message', detail: 'Biiling Details  updated successfully' });
                                    this.closeModalBox();
                                    this.getMyProjects();
                                }
                                if (data.status === 'fail') {
                                    // message
                                    this.msgs = [];
                                    if (data.errors && Object.keys(data.errors).length > 0) {

                                        for (var key in data.errors) {
                                            if (data.errors.hasOwnProperty(key)) {
                                                //console.log(key + " -> " + p[key]);
                                                this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                                            }
                                        }

                                    }
                                    else if (data.error_message) {
                                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                                    }
                                    else {
                                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                                    }
                                }
                            },
                            error => {
                                //console.log(error);
                                this.msgs = [];
                                if (error && error.message) {
                                    this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

                                }
                            }
                        );
                }
            }
            else {
                this.billingModel.api_token = this.authUser.api_token;
                this.billingModel.project_id = this.project_Id;
                this.billingModel.billing_method = "1";
                this.billingModel.card_number = "";
                console.log(this.billingModel);
                this.userService.addBillingDetails(this.billingModel)
                    .subscribe(
                        (data: any) => {
                            console.log(data);
                            if (data.status === 'success') {
                                // message
                                this.msgs = [];
                                this.msgs.push({ severity: 'success', summary: 'Success Message', detail: 'Biiling Details  updated successfully' });
                                this.closeModalBox();
                                this.getMyProjects();
                            }
                            if (data.status === 'fail') {
                                // message
                                this.msgs = [];
                                if (data.errors && Object.keys(data.errors).length > 0) {

                                    for (var key in data.errors) {
                                        if (data.errors.hasOwnProperty(key)) {
                                            //console.log(key + " -> " + p[key]);
                                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                                        }
                                    }

                                }
                                else if (data.error_message) {
                                    this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                                }
                                else {
                                    this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                                }
                            }
                        },
                        error => {
                            //console.log(error);
                            this.msgs = [];
                            if (error && error.message) {
                                this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

                            }
                        }
                    );
            }

        }
    }
    billingHistory(project){
    if(!project.billing_details){
        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: 'Please add billing detals'});
    }
    else if(project.billing_details){
    this.router.navigate(['my-project/project-details/'+ project.id ]);
    }
    }
}

import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../services/user.service';
import { appConfig } from '../../../apiurl.config';
import { Message, FileUpload } from 'primeng/primeng';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertComponent } from 'ngx-bootstrap';
import { Alert } from 'selenium-webdriver';
declare var $: any;

@Component({
  selector: 'app-plans',
  templateUrl: './plans.component.html',
  styleUrls: ['./plans.component.css']
})

export class PlansComponent implements OnInit {
    platformId: any;
    projectId: any;
    /**
   * @property \{{{any}}\} {{authUser}} {{this will hold the authuser}}
   */
    authUser: any = {};
    /**
   * @property \{{{Message[]}}\} {{msgs}} {{this is growl message model}}
   */
  
    msgs: Message[] = [];
    pricing_plan_content: string;
    shortcode: string = '';
    constructor(private userService: UserService,
        private activeRoute: ActivatedRoute,
        private router: Router){

    }
    ngOnInit(){
        this.activeRoute.params.subscribe(routeParams => {
            this.projectId = routeParams.id;
          });
          this.authUser = this.userService.getAuthUser();
          this.platformId = this.authUser.platform_id
          this.getSingleplatformDetails();
      
          let that = this;
          $(document).on('click', 'a.price-btn', function () {
            if (confirm("Are you sure to choose the plan?")) {
              var txt = $(this).children('strong').text();
              that.shortcode = txt;
              console.log(that.shortcode);
              that.subscribeprojecplan(that.shortcode);
            }
          });
    }

    ngAfterViewInit() {
        this.projectAllSubscriptions();
      }
      getSingleplatformDetails(): void {
        if ((this.platformId !== undefined) && (this.platformId !== null)) {
          const fd: FormData = new FormData();
          fd.append('platform_id', this.platformId);
          this.userService.getSinglePlatform(fd).subscribe(
            (data: any) => {
              if (data.status === 'success') {
                if (data.page_content) {
                  // if(data.feed.pricing_content.content){
                  this.pricing_plan_content = data.page_content;
                  // this.pricing_plan_content = data.feed.pricing_content.content;
                  let res_style = data.feed.pricing_content.stylesheet;
                  this.pageStyle(res_style);
                  this.buttonShortCode(this.pricing_plan_content);
                }
              }
              if (data.status === 'fail') {
                // message
                this.msgs = [];
                if (data.errors && Object.keys(data.errors).length > 0) {
    
                  for (var key in data.errors) {
                    if (data.errors.hasOwnProperty(key)) {
                      this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                    }
                  }
    
                }
                else if (data.error_message) {
                  this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                }
                else {
                  this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                }
              }
            },
            error => {
              this.msgs = [];
              if (error && error.message) {
                this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });
    
              }
            }
          );
        }
      }
      // subscribe each plan by clicking on the choose button
      subscribeprojecplan(shortcode) {
        if ((this.projectId !== undefined) && (this.projectId !== null) && (shortcode != '')) {
        const fd: FormData = new FormData();
        fd.append('api_token', this.authUser.api_token);
        fd.append('plan_short_code', shortcode);
        fd.append('project_id', this.projectId);
    
          this.userService.subscribeProjecPlan(fd).subscribe(
            (data: any) => {
              if (data.status === 'success') {
                // message
                // this.msgs = [];
                // this.msgs.push({ severity: 'success', summary: 'Success Message', detail: 'Platform pricing subscribed successfully' });
                $(document).ready(function () {
                  $(".container").prepend('<div class="alert alert-success pull-right prepended"><strong>Success!</strong><br/>Platform pricing subscribed successfully.</div>');
                  setTimeout(() => {
                    $(".prepended").remove();
                  }, 5000);
                });
                this.subscribedPlans(data.current_plan,data.upcoming_plan);
              }
              if (data.status === 'fail') {
                // message
                this.msgs = [];
                if (data.errors && Object.keys(data.errors).length > 0) {
    
                  for (var key in data.errors) {
                    if (data.errors.hasOwnProperty(key)) {
                      this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                    }
                  }
    
                }
                else if (data.error_message) {
                  this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                }
                else {
                  this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                }
              }
            },
            error => {
              this.msgs = [];
              if (error && error.message) {
                this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });
    
              }
            }
          );
      }
    }
      buttonShortCode(details) {
        $(document).ready(function () {
          $("a").find("strong").css("display", "none");
        });
      }
    
      pageStyle(styleString) {
        $('head').append('<style type="text/css">' + styleString + '</style>');
      }
    
      // get all project subscription data
      projectAllSubscriptions() {
        if ((this.projectId !== undefined) && (this.projectId !== null)) {
          const fd: FormData = new FormData();
          fd.append('project_id', this.projectId);
          fd.append('api_token', this.authUser.api_token);
          this.userService.getAllProjectSubscriptions(fd).subscribe(
            (data: any) => {
              if (data.status === 'success') {
                // message
                // this.msgs = [];
                this.subscribedPlans(data.current_plan, data.upcoming_plan);
              }
              if (data.status === 'fail') {
                // message
                // this.msgs = [];
                if (data.errors && Object.keys(data.errors).length > 0) {
    
                  for (var key in data.errors) {
                    if (data.errors.hasOwnProperty(key)) {
                      this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                    }
                  }
    
                }
                else if (data.error_message) {
                  this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                }
                else {
                  this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                }
              }
            },
            error => {
              // this.msgs = [];
              if (error && error.message) {
                this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });
    
              }
            }
          );
        }
      }
    
      subscribedPlans(currentPlan, upcomingPlan) {
    
        if (currentPlan != null) {
          $('.price-btn').each(function () {
            var th = $(this);
            var thtxt = th.children('strong').text();
            console.log(thtxt);
            if (th.parent().children('.text').text() == 'Current Plan') {
              th.parent().children('.text').text("");
              th.removeClass('disabled');
              th.prop("disabled", false);
            }
            if (thtxt == currentPlan.package.short_code) {
              th.parent().children('.text').text("Current Plan");
              th.addClass('disabled');
              th.prop("disabled", true);
            }
          });
        }
    
        if (upcomingPlan != null) {
          $('.price-btn').each(function () {
            var th = $(this);
            var thtxt = th.children('strong').text();
            console.log(thtxt);
            if (th.parent().children('.text').text() == 'Upcoming Plan') {
              th.parent().children('.text').text("");
              th.removeClass('disabled');
              th.prop("disabled", false);
            }
            if (thtxt == upcomingPlan.package.short_code) {
              th.parent().children('.text').text("Upcoming Plan");
              th.addClass('disabled');
              th.prop("disabled", true);
            }
          });
        }
    
    
      }
}
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { UserService } from './../../../services/user.service';

import { Message } from 'primeng/primeng';
import * as jsPDF from 'jspdf';

declare var $: any;

@Component({
    selector: 'app-project-details',
    templateUrl: './project-details.component.html',
    styleUrls: ['./project-details.component.css']
})
export class ProjectDetailsComponent implements OnInit {
    /**
     * @property \{{{any}}\} {{urlid}} {{this will hold the id value from urlid}}
     */
    urlid: any = '';

    /**
     * @property \{{{any}}\} {{model}} {{register form model}}
     */
    model: any = {};

    /**
     * @property \{{{Message[]}}\} {{msgs}} {{this is growl message model}}
     */
    msgs: Message[] = [];
    /**
     * @property \{{{any}}\} {{authUser}} {{this will hold the authuser}}
     */

    authUser: any = {};
    billingDetailsList: any = [];
    billingHistory: any;
    pdfLink: any;
    constructor(
        private userService: UserService,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    ngOnInit() {
        this.authUser = this.userService.getAuthUser();
        // get the route id from param
        this.route.params.subscribe(params => {
            this.urlid = params['id'];
        });
    }
    ngAfterViewInit() {
        this.getProject();
        this.billingDetails();
    }
    getProject() {
        let fd = new FormData();
        fd.append('project_id', this.urlid);
        fd.append('api_token', this.authUser.api_token);
        this.userService.getProject(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        this.model = (data.project);
                        console.log(this.model);
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0) {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                                }
                            }

                        }
                        else if (data.error_message) {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                        }
                        else {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                        }
                    }
                },
                error => {
                    console.log(error);
                    this.msgs = [];
                    if (error && error.message) {
                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

                    }
                    //alert('Something went wrong!');
                }
            );


    }

    billingDetails() {
        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);
        fd.append('user_id', this.authUser.id);
        fd.append('project_id', this.urlid);
        this.userService.BillingDetails(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    this.billingDetailsList = data.bill_details;
                    this.billingHistory = data;
                    $('.billing_history').addClass('show');
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0) {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                                }
                            }

                        }
                        else if (data.error_message) {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                        }
                        else {
                            this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                        }
                    }
                },
                error => {
                    console.log(error);
                    this.msgs = [];
                    if (error && error.message) {
                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

                    }
                }
            );


    }
    downloadPdf(billingDetailsId) {
        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);
        fd.append('subscription_id', billingDetailsId);
        fd.append('user_id', this.urlid);
        this.userService.pdfDownload(fd)
            .subscribe(
                (res: any) => {
                    console.log(res);
                    // this.pdfLink = data.pdf;
                    // window.open(this.pdfLink);
                    // this.billingDetailsList.map((item)=>{
                    //     item['pdf'] = this.pdfLink;
                    // });
                   
                    // if (data.status === 'success') {
                    //     // message
                    //     this.msgs = [];
                    // }
                    // if (data.status === 'fail') {
                    //     // message
                    //     this.msgs = [];
                    //     if (data.errors && Object.keys(data.errors).length > 0) {

                    //         for (var key in data.errors) {
                    //             if (data.errors.hasOwnProperty(key)) {
                    //                 this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.errors[key] });
                    //             }
                    //         }

                    //     }
                    //     else if (data.error_message) {
                    //         this.msgs.push({ severity: 'error', summary: 'Error Message', detail: data.error_message });
                    //     }
                    //     else {
                    //         this.msgs.push({ severity: 'error', summary: 'Error Message', detail: "Undefined Error" });
                    //     }
                    // }
                },
                error => {
                    console.log(error);
                    // // file.save(file);
                    // window.open(fileURL);
                    this.msgs = [];
                    if (error && error.message) {
                        this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.message });

                    }
                }
            );
    }
}

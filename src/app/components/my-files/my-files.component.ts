import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { CommonService } from '../../services/common.service';
import { UserService } from '../../services/user.service';
import { Message} from 'primeng/primeng';
import { appConfig } from '../../apiurl.config';
declare var $: any;

@Component({
  selector: 'app-my-files',
  templateUrl: './my-files.component.html',
  styleUrls: ['./my-files.component.css']
})
export class MyFilesComponent implements OnInit {
    /**
     * @property \{{{any}}\} {{appConfig}} {{this will hold the constants for the app}}
     */

    appConfig: any = {};
    /**
     * @property \{{{any}}\} {{model}} {{security form model}}
     */

    model: any = {
        attachment : null,
        parent_id : null
    };

    /**
     * @property \{{{any}}\} {{authUser}} {{this will hold the authuser}}
     */

    authUser: any ={};
    /**
     * @property \{{{any}}\} {{myProjects}} {{this will hold the authuser}}
     */

    dataList: any =[];
    /**
     * @property \{{{any}}\} {{timezones}} {{this will hold the authuser}}
     */

    timezones: any = [];

    /**
     * @property \{{{Message[]}}\} {{msgs}} {{this is growl message model}}
     */

    msgs: Message[] = [];

    file_path : string ="";
    browsImage : any ;
    old_file : any ;
    selArr: any =[];
    owner_list: any =[];

    search_key : string ='';
    create_by : string ='';
    stdtKey : string ='';
    dudtKey : string ='';
    fromDate: Date;
    dueDate: Date;
    is_browse : boolean = false;
    /**
     * @constructor
     * @description {{DI will pushed here}}
     */
    constructor(
        private commonService: CommonService,
        private userService: UserService,
        private router: Router
    ) { }

  ngOnInit() {
      this.authUser = this.userService.getAuthUser();
      this.appConfig = appConfig;
      this.file_path = this.appConfig.publicUrl+'/uploads/project/thumbs/';
      this.getAssignedFiles();
  }
    /**
     * @func {{pageLoad}}
     * @description {{reload page after a certain time}}
     */
    pageLoad(){
        setTimeout(()=>{    //<<<---    using ()=> syntax
            location.reload()
        },2000);
    }

    selctToList($event) {
        //console.log($event.srcElement.checked);
        //console.log($event.srcElement.value);
        if($event.srcElement.checked == true)
        {
            if(this.selArr.indexOf($event.srcElement.value)== -1)
                this.selArr.push($event.srcElement.value);
        }else {

            this.selArr.splice(this.selArr.indexOf($event.srcElement.value), 1);
        }
        //console.log(this.selcArr);
    }

    onFileChange(event) {
        // console.log(event);
        if(event.files && event.files.length > 0) {
            this.is_browse = true;
            let file = event.files[0];
            // console.log(file);
            this.model.attachment = file;
            this.saveFileDocument();
        }
    }

    addFile(){
        this.old_file = null;
        this.model.parent_id = null;
    }
    editMyfile(fileData){
        console.log(fileData);
        this.old_file = fileData.file_title;
        this.model.parent_id = fileData.id;
    }



    /**
     * @func {{saveFileDocument}}
     * @param \{{{formobject}}\} {{myform}} {{this will pass the form object}}
     * @description {{this will submit the form data and update the project data}}
     */
    saveFileDocument() {
        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);
        fd.append('attachment', this.model.attachment);
        fd.append('parent_id', this.model.parent_id);
        // console.log(this.model);

        this.userService.saveMyFile(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'File Post Successful'});
                        if(data.feed)    this.dataList.push(data.feed);
                        this.model.parent_id = null;
                        this.old_file = null;

                        console.log(this.dataList);
                        this.pageLoad();
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );
    }


    /**
     * @func {{delType}}
     * @description {{delete type}}
     */
    delObj(_id)
    {
        //console.log(this.model);
        if(confirm("Are you sure to delete the file?"))
        {
            let fd = new FormData();
            fd.append('_ids',_id);
            fd.append('api_token', this.authUser.api_token);
            //return;
            this.userService.deleteMyFiles(fd)
                .subscribe(
                    (data: any) => {
                        //console.log(data);
                        if (data.status === 'success') {
                            // message
                            this.msgs = [];
                            this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'File Successfully Deleted'});

                            // go to  my team page
                            this.pageLoad();
                        }
                        if (data.status === 'fail') {
                            // message
                            this.msgs = [];
                            if (data.errors && Object.keys(data.errors).length > 0)
                            {

                                for (var key in data.errors) {
                                    if (data.errors.hasOwnProperty(key)) {
                                        //console.log(key + " -> " + p[key]);
                                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                    }
                                }

                            }
                            else if(data.error_message)
                            {
                                this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                            }
                            else
                            {
                                this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                            }
                        }
                    },
                    error => {
                        console.log(error);
                        this.msgs = [];
                        if (error && error.message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                        }
                        //alert('Something went wrong!');
                    }
                );

        }
    }

    /**
     * @func {{delAll}}
     * @description {{delete selected taxes}}
     */
    delAll()
    {
        if(confirm("Are you sure to delete all selected files?"))
        {
            let fd = new FormData();
            fd.append('_ids',this.selArr);
            fd.append('api_token', this.authUser.api_token);
            //return;
            this.userService.deleteMyFiles(fd)
                .subscribe(
                    (data: any) => {
                        //console.log(data);
                        if (data.status === 'success') {
                            // message
                            this.msgs = [];
                            this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Files are successfully deleted'});

                            this.pageLoad();
                        }
                        if (data.status === 'fail') {
                            // message
                            this.msgs = [];
                            if (data.errors && Object.keys(data.errors).length > 0)
                            {

                                for (var key in data.errors) {
                                    if (data.errors.hasOwnProperty(key)) {
                                        //console.log(key + " -> " + p[key]);
                                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                    }
                                }

                            }
                            else if(data.error_message)
                            {
                                this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                            }
                            else
                            {
                                this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                            }
                        }
                    },
                    error => {
                        console.log(error);
                        this.msgs = [];
                        if (error && error.message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                        }
                        //alert('Something went wrong!');
                    }
                );

        }
    }


    kywordFilter(event){
        // console.log(event.srcElement.value);
        this.search_key = event.srcElement.value;
        this.getAssignedFiles();
    }

    createByFilter(event){
        // console.log(event.srcElement.value);
        this.create_by = event.srcElement.value;
        this.getAssignedFiles();
    }

    fromDateFilter(duDate){
        // console.log(event.srcElement.value);
        // this.manager_filter = event.srcElement.value;
        // this.manager_filter = this.dueDate;
        // console.log(duDate);
        this.stdtKey = (duDate.getTime()/1000).toString();
        //console.log(this.stdtKey);
        this.getAssignedFiles();
    }

    ddFilter(duDate){
        // console.log(event.srcElement.value);
        // this.manager_filter = event.srcElement.value;
        // this.manager_filter = this.dueDate;
        // console.log(duDate);
        this.dudtKey = (duDate.getTime()/1000).toString();
        //console.log(this.dudtKey);
        this.getAssignedFiles();
    }
    /**
     * @func {{getAssignedFiles}}
     * @description {{get user files}}
     */
    getAssignedFiles() {
        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);
        fd.append('search_key', this.search_key);
        fd.append('create_by', this.create_by);
        fd.append('from_date', this.stdtKey);
        fd.append('to_date', this.dudtKey);
        this.userService.fetchMyFiles(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        //this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Project Post Successful'});
                        this.dataList =  (data.feed);
                        this.owner_list = data.added_by;
                        //console.log(this.dataList);
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );

    }

    openFileTimeLine(fid){
        localStorage.setItem('leadFile', fid);
        this.router.navigate(['/user/file-timeline']);
    }

    getFileLink(fileObj){
        // console.log(fileObj);
        let file_name = fileObj.original_file;
        // console.log(fileObj.status);
        if(fileObj.status=='0' && fileObj.sub_files.length > 0)
        {
            file_name = fileObj.sub_files.find(item => item.status == '1').original_file;
            // console.log(file_name);
        }
        let fpath = '';
        switch (fileObj.post_type){
            case'N':
                fpath = this.appConfig.publicUrl+'uploads/note_attachment/' +file_name;
                break;
            case'T':
                fpath = this.appConfig.publicUrl+'uploads/task_attachment/' +file_name;
                break;
            case'O':
                fpath = this.appConfig.publicUrl+'uploads/opportunity_attachment/' +file_name;
                break;
            case'C':
                fpath = this.appConfig.publicUrl+'uploads/comment_attachment/' +file_name;
                break;

            default: fpath = this.appConfig.publicUrl+'uploads/attachment/' +file_name;
        }
        return fpath;
    }

    onOpenFile(fileObj){
        // console.log(fileObj);
        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);
        fd.append('file_id', fileObj.id);

        this.userService.fileOpened(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        //this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Project Post Successful'});

                        //console.log(this.dataList);
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );

    }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailTimelineComponent } from './email-timeline.component';

describe('EmailTimelineComponent', () => {
  let component: EmailTimelineComponent;
  let fixture: ComponentFixture<EmailTimelineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmailTimelineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailTimelineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../services/common.service';
import { UserService } from '../../services/user.service';
import { Message} from 'primeng/primeng';
import { appConfig } from '../../apiurl.config';
declare var $ : any;
@Component({
  selector: 'app-email-timeline',
  templateUrl: './email-timeline.component.html',
  styleUrls: ['./email-timeline.component.css']
})
export class EmailTimelineComponent implements OnInit {
    /**
     * @property \{{{any}}\} {{appConfig}} {{this will hold the constants for the app}}
     */

    appConfig: any = {};

    /**
     * @property \{{{any}}\} {{authUser}} {{this will hold the authuser}}
     */

    authUser: any ={};
    /**
     * @property \{{{any}}\} {{dataList}} {{this will hold the dataList}}
     */

    dataList: any =[];

    /**
     * @property \{{{Message[]}}\} {{msgs}} {{this is growl message model}}
     */

    msgs: Message[] = [];
    emailId:string = '';
    mailThread: any =[];
    replys: any =[];

    postCommentModel: any = {};
    postCommentFile: any=[];
    is_submit: boolean = false;
    postEmailModel: any = {};

    templateList: any=[];
    draftStop : boolean = false;
    opportunityRel: any = [];
    managerList: any = [];

    opportunityList: any = [];
    mail_opportunities: any  = [];
    mylabels: any  = [];
    filter_id: string = '';
    labelModel: any = { };

    show_main :boolean = true;
    archives: any  = [];

    is_archive :boolean = true;

    reminderModel: any = {
        upto_val:'1',
        upto_dur:'w',
        repeat_val:'1',
    };
    userToNotify: any = [];
    mail_reminders: any = [];
    selcted_reminder: any = [];

  constructor(
      private commonService: CommonService,
      private userService: UserService) { }

  ngOnInit() {

      this.appConfig = appConfig;
      let emailId = localStorage.getItem('leadEmailId');
      this.emailId = emailId;
      this.authUser = this.userService.getAuthUser();

      // console.log(this.emailId);
  }

  ngAfterViewInit(){
      this.mailTimeLine();
      this.getEmailTemplates();

      $(document).on('click','a.ldmore',function(){
          //alert('ldmore ');
          if($(this).text()=='View more'){
              $(this).text('View less');
              $(this).parent().parent().children('.commntList').children('li').show();
          }
          else{
              $(this).text('View more');
              $(this).parent().parent().children('.commntList').children('li').hide();
          }
      });
  }

    selctNotifyUsr($event)
    {
        /*console.log($event.srcElement.value);
        console.log($event.srcElement.checked);
        console.log(type_post_id);*/
        if($event.srcElement.checked == true)
        {
            if(this.userToNotify.indexOf($event.srcElement.value)== -1)
                this.userToNotify.push($event.srcElement.value);
        }else {

            this.userToNotify.splice(this.userToNotify.indexOf($event.srcElement.value), 1);
        }
        console.log(this.userToNotify);
    }
    openReminder($event,mail_id)
    {
        /*if( this.selctedPost != post_id)
        {
            this.selctedPost = post_id;
            this.userToNotify =[];
            this.userToNotify.push(this.authUser.id.toString());
        }*/
        this.userToNotify =[];
        this.userToNotify.push(this.authUser.id.toString());
        this.reminderModel = {
            upto_val:'1',
            upto_dur:'w',
            repeat_val:'1',
        };

        this.reminderModel.mail_id= mail_id;
    }
    /**
     * @func {{saveReminder}}
     * @param \{{{formobject}}\} {{myform}} {{this will pass the form object}}
     * @description {{this will submit the form data and update the project data}}
     */
    saveReminder(myform){
        this.reminderModel.notify_user= this.userToNotify;
        this.reminderModel.api_token= this.authUser.api_token;
        this.reminderModel.main_mailId= this.mailThread.id;
        console.log(this.reminderModel);
        this.userService.setMailReminder(this.reminderModel)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Set Reminder Successful'});
                        // console.log(data.feed);
                        this.userToNotify =[];
                        this.mailTimeLine();
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );
    }


    checkOpptStatus(closerDate){
        let d = new Date();
        let n = (d.getTime())/1000;
        // console.log(n);
        // return n > parseInt(closerDate) ? '<span class="badge pull-right">Closed</span>' :'<span class="badge bagsucces pull-right">Open</span>';
        return n > (closerDate) ? 'Closed' :'Open';
    }

    /**
     * @func {{fetchOpportunity}}
     * @description {{get user User Time Line}}
     */
    /*fetchOpportunity() {
        this.opportunityRel = [];
        //alert(this.listTab);
        let fd = new FormData();
        fd.append('list_tab', '#allpost');
        fd.append('api_token', this.authUser.api_token);
        this.userService.getOpportunity(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        this.opportunityList = data.feed;
                        this.opportunityList.forEach((value: any) => {
                            if(value.id){
                                // this.opportunityDd.push({'id':value.id ,'itemName': value.title });
                                this.opportunityRel.push({'value':value.id ,'label': value.title });
                            }
                        });
                        //console.log(this.opportunityDd);
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});
                    }
                    //alert('Something went wrong!');
                }
            );

    }*/

    setdef(){

        $(".subjett").hide();
    }
    /**
     * @func {{fetchTeamUsers}}
     * @description {{get user teams}}
     */
    mailTimeLine() {
        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);
        fd.append('mailId', this.emailId);
        fd.append('filter_id', this.filter_id);
        this.userService.fetchMailTimeLine(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        this.mail_opportunities = [];
                        this.mailThread = [];
                        this.replys = [];
                        this.mylabels = [];
                        this.mylabels = data.labels;
                        this.is_archive = data.is_archive;
                        this.mail_reminders = (data.mail_reminders);
                        // console.log(this.archives)
                        this.opportunityList = data.curr_project_opportunities;
                        this.opportunityList.forEach((value: any) => {
                            if(value.id){
                                // this.opportunityDd.push({'id':value.id ,'itemName': value.title });
                                this.opportunityRel.push({'value':parseInt(value.id) ,'label': value.title });
                            }
                        });
                        //this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Project Post Successful'});
                        this.mailThread =  (data.feed);
                        if(this.mailThread.opportunities.length >0)
                        {
                            this.mailThread.opportunities.forEach((oppt: any) => {
                                this.mail_opportunities.push(oppt);
                            });
                        }
                        this.replys =  (data.reply_mails);
                        data.reply_mails.forEach((value: any) => {
                            if(value.opportunities.length >0)
                            {
                                value.opportunities.forEach((oppt: any) => {
                                    this.mail_opportunities.push(oppt);
                                });
                            }

                        });

                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});
                    }
                    //alert('Something went wrong!');
                }
            );

    }

    onFileChange(event,modType,pid) {
        this.postCommentFile =[];
        this.postCommentModel.attachment = null;

        if(event.target.files && event.target.files.length > 0) {
            let file = event.target.files[0];
            console.log(file);

            if (modType == 'm')  this.postEmailModel.attachment = file;
            else if (modType == 'cmnt'){
                this.postCommentModel.attachment = file;
                if(pid!='')    this.postCommentFile.push({post_id:pid,file:file});
            }
        }

        // console.log(this.postCommentFile)
    }

    shwCntFile(postId){

        if(this.postCommentFile.find(item => item.post_id == postId)){
            // console.log(this.postCommentFile.find(item => item.post_id == postId));
            return this.postCommentFile.find(item => item.post_id == postId).file.name;
        }
        //
        //name
    }

    setComment(event) {
        // console.log(event.srcElement.value);
        this.postCommentModel.content = event.srcElement.value;
    }

    keyDownFunction(event,pid) {
        // console.log(event.keyCode);
        if(event.keyCode == 13) {
            // alert('you just clicked enter');
            // rest of your code
            if(this.is_submit == false)    this.createComment(pid);
            //console.log(f);
        }
    }

    createComment(pid)
    {
        this.is_submit = true;
        //console.log(myform);
        this.postCommentModel.api_token = this.authUser.api_token;
        this.postCommentModel.type = 'M';
        this.postCommentModel.post_id = pid;
        // console.log(this.postCommentModel);

        let fd = new FormData();
        if(this.postCommentModel.attachment)    fd.append('attachment', this.postCommentModel.attachment);
        fd.append('type', this.postCommentModel.type);
        fd.append('post_id', pid);
        fd.append('content', this.postCommentModel.content);
        fd.append('api_token', this.postCommentModel.api_token);
        fd.append('tl_type', 'email');

        this.userService.savePostComment(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {

                        $('.commntarea').val('');
                        // message
                        this.msgs = [];
                        this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Comment successful'});
                        this.postCommentModel = {};
                        this.postCommentFile = [];
                        this.mailTimeLine();
                        this.is_submit = false;

                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                        this.is_submit = false;
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    this.is_submit = false;
                    //alert('Something went wrong!');
                }
            );

    }

    cutStr(str){
        let pos = 0;
        if(str) pos = str.indexOf( "@" );
        if((pos) != 0)   return str.substring( 0, pos );
        else return '';
    }

    fetchTeamUsers() {
        this.managerList = [];
        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);
        fd.append('page_type', 'contacts');
        this.userService.getTeamUsers(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        //this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Project Post Successful'});
                        // this.projectUsers =  (data.users);
                        if(data.users)
                        {
                            data.users.forEach((value: any) => {
                                let nam = value.first_name +' '+ value.last_name;
                                if(value.id != this.authUser.id) {
                                    // this.allUsers.push({'id':value.id ,'itemName': nam });
                                    this.managerList.push({'value':value.id ,'label': nam });
                                }
                            });
                        }
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});
                    }
                    //alert('Something went wrong!');
                }
            );

    }

    setTemplate(){
        let templtId = this.postEmailModel.template;
        console.log(templtId);
        if(templtId == ''){
            this.msgs=[];
            this.msgs.push({severity: 'error', summary: 'Error Message', detail: 'Please choose a valid template'});
        }
        else    this.postEmailModel.content = this.templateList.find(item => item.id == templtId).content;
    }

    getEmailTemplates() {

        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);


        this.userService.emailTemplates(fd)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        // message
                        this.msgs = [];
                        // this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Template Successfully saved'});

                        this.templateList = data.feed;
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );
    }

    setReply(data,toEmail){

        this.postEmailModel.managers = [];
        this.postEmailModel.opportunitys = [];
        this.draftStop = false;
        // this.postEmailModel.to_email = data.user.email;
        this.postEmailModel.to_email = toEmail;
        this.postEmailModel.subject = data.subject;
        this.postEmailModel.parent_id = data.id;
        this.postEmailModel.attachment = null;
        // console.log( this.postEmailModel);
        this.fetchTeamUsers();
    }

    mailSend(myform){
        this.draftStop = false;
        // console.log(this.postEmailModel);
        // this.postEmailModel.api_token = this.authUser.api_token;
        // this.postEmailModel.to_email = this.contactData.email;
        if(this.is_submit == false){
        let fd = new FormData();
        fd.append('api_token', this.authUser.api_token);
        fd.append('template', this.postEmailModel.template);
        fd.append('to_email', this.postEmailModel.to_email);
        fd.append('cc_email', this.postEmailModel.cc_email);
        fd.append('bcc_email', this.postEmailModel.bcc_email);
        fd.append('subject', this.postEmailModel.subject);
        fd.append('content', this.postEmailModel.content);
        fd.append('managers', this.postEmailModel.managers);
        fd.append('opportunitys', this.postEmailModel.opportunitys);
        fd.append('attachment', this.postEmailModel.attachment);

        if( this.postEmailModel.parent_id){
            fd.append('parent_id',  this.postEmailModel.parent_id);
        }


            this.is_submit = true;
            this.userService.saveSendEmail(fd)
                .subscribe(
                    (data: any) => {
                        //console.log(data);
                        if (data.status === 'success') {
                            // message
                            this.msgs = [];
                            this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Mail Send Successfull'});

                            this.postEmailModel={
                                content : '',
                                template: ''
                            };

                            this.is_submit = false;
                            $('#newmessage2').modal('toggle');
                            this.mailTimeLine();
                            // this.templateList = data.feed;
                        }
                        if (data.status === 'fail') {
                            // message

                            this.is_submit = false;
                            this.msgs = [];
                            if (data.errors && Object.keys(data.errors).length > 0)
                            {

                                for (var key in data.errors) {
                                    if (data.errors.hasOwnProperty(key)) {
                                        //console.log(key + " -> " + p[key]);
                                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                    }
                                }

                            }
                            else if(data.error_message)
                            {
                                this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                            }
                            else
                            {
                                this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                            }
                        }
                    },
                    error => {
                        console.log(error);

                        this.is_submit = false;
                        this.msgs = [];
                        if (error && error.message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                        }
                        //alert('Something went wrong!');
                    }
                );
        }
    }

    checkDraft(e){

    }

    showPp(mailData){
        // console.log('mailData');
        let pp = 'assets/images/avatar.png';
        if(mailData.sender_type==1){
            if(mailData.user && mailData.user.profile_picture!=null){
                pp = this.appConfig.publicUrl+'/uploads/profile_picture/'+mailData.user.profile_picture;
            }
        }
        else if(mailData.sender_type==2){
            if(mailData.contact && mailData.contact.profile_picture!=null){
                pp = this.appConfig.publicUrl+'/uploads/profile_picture/'+mailData.contact.profile_picture;
            }
        }
        return pp;
    }

    removeFileAttachment(id) {

        if(confirm("Are you sure to delete the file from your account?")) {
            let fd = new FormData();
            fd.append('api_token', this.authUser.api_token);
            fd.append('_id', id);


            this.userService.removeAttachment(fd)
                .subscribe(
                    (data: any) => {
                        //console.log(data);
                        if (data.status === 'success') {
                            // message
                            this.msgs = [];
                            // this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Template Successfully saved'});

                            this.mailTimeLine();
                        }
                        if (data.status === 'fail') {
                            // message
                            this.msgs = [];
                            if (data.errors && Object.keys(data.errors).length > 0) {

                                for (var key in data.errors) {
                                    if (data.errors.hasOwnProperty(key)) {
                                        //console.log(key + " -> " + p[key]);
                                        this.msgs.push({
                                            severity: 'error',
                                            summary: 'Error Message',
                                            detail: data.errors[key]
                                        });
                                    }
                                }

                            }
                            else if (data.error_message) {
                                this.msgs.push({
                                    severity: 'error',
                                    summary: 'Error Message',
                                    detail: data.error_message
                                });
                            }
                            else {
                                this.msgs.push({
                                    severity: 'error',
                                    summary: 'Error Message',
                                    detail: "Undefined Error"
                                });
                            }
                        }
                    },
                    error => {
                        console.log(error);
                        this.msgs = [];
                        if (error && error.message) {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                        }
                        //alert('Something went wrong!');
                    }
                );
        }
    }

    checkIsRemoved(removers){
        let status = true;
        if(removers && removers.length >0){
            removers.forEach((value: any) => {
                if(value.id == this.authUser.id)    status = false;
            });
        }
        return status;
    }

    getFilterData(filterId){
        this.filter_id = filterId;
        this.mailTimeLine();

        let lblCnt = 0;
        if(this.mailThread.labels && this.mailThread.labels.length>0){
            this.mailThread.labels.forEach((value: any) => {
                if(value.id == filterId) lblCnt++;
            });
        }


        if(lblCnt == 0 && this.filter_id!=''){
            this.show_main = false;
        }else this.show_main = true;
    }

    /* @func {{createLabel}}
    * @param \{{{formobject}}\} {{myform}} {{this will pass the form object}}
    * @description {{this will submit the form data and update the label data}}
    */
    createLabel(myform,post_id)
    {
        //alert(etyp);
        // this.labelModel.curr_user =this.leadFileId;
        this.labelModel.tl_type = 'email';
        this.labelModel.api_token = this.authUser.api_token;
        this.labelModel.post_id = post_id.toString();
        // console.log(this.labelModel);
        this.userService.addLabel(this.labelModel)
            .subscribe(
                (data: any) => {
                    //console.log(data);
                    if (data.status === 'success') {
                        this.mylabels = data.feed;
                        // message
                        this.msgs = [];
                        this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Label successfully saved'});
                        $('.openlabelbx').hide();
                        $('.offforcreatelabel').show();
                        this.mailTimeLine();

                        //this.fetchUserEmails();
                        // reload page
                        //this.pageLoad();
                    }
                    if (data.status === 'fail') {
                        // message
                        this.msgs = [];
                        if (data.errors && Object.keys(data.errors).length > 0)
                        {

                            for (var key in data.errors) {
                                if (data.errors.hasOwnProperty(key)) {
                                    //console.log(key + " -> " + p[key]);
                                    this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                }
                            }

                        }
                        else if(data.error_message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                        }
                        else
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                        }
                    }
                },
                error => {
                    //console.log(error);
                    this.msgs = [];
                    if (error && error.message)
                    {
                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                    }
                    //alert('Something went wrong!');
                }
            );

    }

    addToLabel($event,post_id) {
        //console.log($event.srcElement.checked);
        //console.log($event.srcElement.value);
        if($event.srcElement.value && post_id)
        {
            //alert($event.srcElement.value+'='+post_id);
            let fd = new FormData();
            fd.append('api_token', this.authUser.api_token);
            fd.append('post_id', post_id);
            fd.append('label_id', $event.srcElement.value);
            fd.append('tl_type', 'email');

            this.userService.setLabel(fd)
                .subscribe(
                    (data: any) => {
                        //console.log(data);
                        if (data.status === 'success') {
                            // message
                            this.msgs = [];
                            this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Label successfully saved'});
                            this.mailTimeLine();
                        }
                        if (data.status === 'fail') {
                            // message
                            this.msgs = [];
                            if (data.errors && Object.keys(data.errors).length > 0)
                            {

                                for (var key in data.errors) {
                                    if (data.errors.hasOwnProperty(key)) {
                                        //console.log(key + " -> " + p[key]);
                                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                    }
                                }

                            }
                            else if(data.error_message)
                            {
                                this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                            }
                            else
                            {
                                this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                            }
                        }
                    },
                    error => {
                        //console.log(error);
                        this.msgs = [];
                        if (error && error.message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                        }
                        //alert('Something went wrong!');
                    }
                );
        }
    }

    checkIsChecked(lblList,lblId){
        let res = false;
        if(lblList.length > 0){
            lblList.forEach((value: any) => {
                if(value.id == lblId) res = true;
            });
        }
        return res;
    }

    archiveToggel(event) {
        //console.log(event.srcElement.checked);
        this.is_archive = event.srcElement.checked;
        /*if(event.srcElement.checked == true)
        {

                this.archives.push(parseInt(event.srcElement.value));
        }else {

            if(this.archives.indexOf(parseInt(event.srcElement.value))!= -1)
                this.archives.splice(this.archives.indexOf(parseInt(event.srcElement.value)), 1);
        }*/

        // console.log(this.archives);
        if(event.srcElement.value)
        {
            //alert($event.srcElement.value+'='+post_id);
            let fd = new FormData();
            fd.append('api_token', this.authUser.api_token);
            // fd.append('mailId', this.archives);
            fd.append('main_mailId', this.mailThread.id);

            this.userService.archiveMail(fd)
                .subscribe(
                    (data: any) => {
                        //console.log(data);
                        if (data.status === 'success') {
                            // message
                            this.msgs = [];
                            this.msgs.push({severity: 'success', summary: 'Success Message', detail: 'Archive status successfully changed'});
                            this.mailTimeLine();
                        }
                        if (data.status === 'fail') {
                            // message
                            this.msgs = [];
                            if (data.errors && Object.keys(data.errors).length > 0)
                            {

                                for (var key in data.errors) {
                                    if (data.errors.hasOwnProperty(key)) {
                                        //console.log(key + " -> " + p[key]);
                                        this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.errors[key]});
                                    }
                                }

                            }
                            else if(data.error_message)
                            {
                                this.msgs.push({severity: 'error', summary: 'Error Message', detail: data.error_message});
                            }
                            else
                            {
                                this.msgs.push({severity: 'error', summary: 'Error Message', detail: "Undefined Error"});
                            }
                        }
                    },
                    error => {
                        //console.log(error);
                        this.msgs = [];
                        if (error && error.message)
                        {
                            this.msgs.push({severity: 'error', summary: 'Error Message', detail: error.message});

                        }
                        //alert('Something went wrong!');
                    }
                );
        }
    }

    showReminderPopup(reminder){
        console.log(reminder);
        this.selcted_reminder =  reminder;
    }

}

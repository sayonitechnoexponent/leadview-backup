import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule } from '@angular/forms';
import { Routes } from '@angular/router';
import { RouterModule } from '@angular/router';

import {AdminGuard} from './guards/admin.guard';
import {PublicGuard} from './guards/public.guard';
import {AuthGuard} from './guards/auth.guard';

import {GrowlModule} from 'primeng/primeng';
import {EditorModule} from 'primeng/editor';
import { CKEditorModule } from 'ng2-ckeditor';
import { MultiSelectModule } from 'primeng/multiselect';
//import { FilterPipe } from './filter.pipe';

import { SharedModule } from './shared.module';

import { AdminComponent } from './components/admin/admin.component';
import { ProjectsComponent } from './components/admin/projects/projects.component';
import { PricingComponent } from './components/pricing/pricing.component';
import { AddPackageComponent } from './components/pricing/add-package/add-package.component';
import { TaxTypeComponent } from './components/admin/tax-type/tax-type.component';
import { EmailTemplatesComponent } from './components/admin/email-templates/email-templates.component';
import { LanguagesComponent } from './components/admin/languages/languages.component';
import { MailAccountComponent } from './components/admin/mail-account/mail-account.component';
import { ProjectsDescriptionComponent } from './components/admin/project-description/project-description.component';
import { PlatFormComponent } from './components/admin/platform/platform.component';
import { PlatFormPricingPlanComponent } from './components/admin/platform-pricing-plan/platform-pricing-plan.component';
import { FeaturesComponent } from './components/admin/features/features.component';
import { DefaultPackageComponent } from './components/admin/default-package/default-package.component';

const routes: Routes = [
    { path: '',  component: AdminComponent, canActivate: [AdminGuard] },
    { path: 'projects',  component: ProjectsComponent, canActivate: [AdminGuard] },
    { path: 'project-description/:id',  component: ProjectsDescriptionComponent, canActivate: [AdminGuard] },
    { path: 'pricing',  component: PricingComponent, canActivate: [AdminGuard] },
    { path: 'create-package/:type',  component: AddPackageComponent, canActivate: [AdminGuard] },
    { path: 'create-package/default',  component: DefaultPackageComponent, canActivate: [AdminGuard] },
    { path: 'edit-package/:type/:id',  component: AddPackageComponent, canActivate: [AdminGuard] },
    { path: 'tax-type',  component: TaxTypeComponent, canActivate: [AdminGuard] },
    { path: 'message-templates',  component: EmailTemplatesComponent, canActivate: [AdminGuard] },
    { path: 'languages',  component: LanguagesComponent, canActivate: [AdminGuard] },
    { path: 'mail-accounts',  component: MailAccountComponent, canActivate: [AdminGuard] },
    { path: 'platform',  component: PlatFormComponent, canActivate: [AdminGuard] },
    { path: 'platform-pricing-plan',  component: PlatFormPricingPlanComponent, canActivate: [AdminGuard] },
    { path: 'features',  component: FeaturesComponent, canActivate: [AdminGuard] }
];

@NgModule({
  imports: [
      CommonModule,
      SharedModule,
      FormsModule,
      GrowlModule,
      EditorModule,
      CKEditorModule,
      MultiSelectModule,
      RouterModule.forChild(routes)
  ],
  declarations: [
      AdminComponent,
      ProjectsComponent,
      ProjectsDescriptionComponent,
      PricingComponent,
      AddPackageComponent,
      TaxTypeComponent,
      EmailTemplatesComponent,
      LanguagesComponent,
      MailAccountComponent,
      PlatFormComponent,
      PlatFormPricingPlanComponent,
      FeaturesComponent,
      DefaultPackageComponent
  ],
    providers: [
        PublicGuard,
        AdminGuard,
        AuthGuard
    ],
})
export class AdminModule { }

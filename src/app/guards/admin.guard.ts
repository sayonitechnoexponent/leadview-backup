import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthServices } from '../services/auth.service';

@Injectable()
export class AdminGuard implements CanActivate {

  constructor(
    private router: Router,
    private authenticationService: AuthServices
  ) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

        if (this.authenticationService.isAdminLoggedIn()) {
            // logged in so return true
            return true;
        }
        let authUser = this.authenticationService.getAuthUser();
        //console.log(authUser);
        if(authUser && authUser.type !=3)
        {
            this.router.navigate(['/my-account']);
            return false;
        }else{
            // not logged in so redirect
            this.router.navigate(['/login']);
            return false;
        }
    }
}

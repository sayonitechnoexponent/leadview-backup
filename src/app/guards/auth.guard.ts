import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthServices } from '../services/auth.service';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(
    private router: Router,
    private authenticationService: AuthServices
  ) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

    if (this.authenticationService.isLoggedIn()) {
        // logged in so return true

      return true;

    }

      let authUser = this.authenticationService.getAuthUser();
      //console.log(authUser);
      if(authUser && authUser.type==3)
      {
          this.router.navigate(['/admin']);
          return false;
      }else{
          // not logged in so redirect
          this.router.navigate(['/login']);
          return false;
      }

  }

}

import { Injectable } from '@angular/core';
import { Router,CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AuthServices } from '../services/auth.service';

@Injectable()
export class PublicGuard implements CanActivate {

    constructor(
        private router: Router,
        private authenticationService: AuthServices
    ) {}

  /*canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return true;
  }*/
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

        if (this.authenticationService.isLoggedIn()) {
            let authUser = this.authenticationService.getAuthUser();
            console.log(authUser);
            if(authUser && authUser.type!=3)
            {
                // not logged in so redirect
                this.router.navigate(['/my-account']);
            }

            // logged in so return false
            return false;

        }
        if (this.authenticationService.isAdminLoggedIn()) {
            let authUser = this.authenticationService.getAuthUser();
            console.log(authUser);

            if(authUser && authUser.type==3)
            {
                this.router.navigate(['/admin']);
            }

            // logged in so return false
            return false;

        }

        return true;


    }
}

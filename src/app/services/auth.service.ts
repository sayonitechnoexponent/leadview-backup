import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpResponse } from "@angular/common/http";
import { appConfig } from "../apiurl.config";

@Injectable()
export class AuthServices {
  // header create
  private headers = new HttpHeaders().set(
    "Content-Type",
    "application/json; charset=utf-8"
  );

  // constructor
  constructor(private http: HttpClient) {}

  /**
   * @func {{getAuthUser}}
   * @description {{this will send you the auth user}}
   */
  authUser(token) {

      return this.http.get(`${appConfig.apiUrl}auth-user?api_token=`+token, {
          headers: this.headers
      });
  }

  /**
   * @func {{getAuthUser}}
   * @description {{this will send you the auth user}}
   */
  getAuthUser() {
    return JSON.parse(localStorage.getItem('leaduser'));
  }

  /**
   * @func {{isLoggedIn}}
   * @description {{this will check if has auth user}}
   */
  isLoggedIn(): boolean {
    if (localStorage.getItem('leaduser')) {
        let authUser = JSON.parse(localStorage.getItem('leaduser'));
        if(authUser['type'] != 3) return true;
        else return false;
    }
    return false;
  }
  /**
   * @func {{isAdminLoggedIn}}
   * @description {{this will check if has auth Admin user}}
   */
  isAdminLoggedIn(): boolean {
    if (localStorage.getItem('leaduser')) {
      let authUser = JSON.parse(localStorage.getItem('leaduser'));
      if(authUser['type'] == 3) return true;
      else return false;
    }
    return false;
  }

  /**
   * @func {{register}}
   * @param \{{{any}}\} {{$registerData}} {{this will pass the registration user data}}
   * @description {{this will make register first step}}
   */
  register($registerData: any) {
    return this.http.post(`${appConfig.apiUrl}register`, $registerData, {
      headers: this.headers
    });
  }

  /**
   * @func {{finalRegister}}
   * @param \{{{any}}\} {{$registerData}} {{this will pass the registration user data}}
   * @param \{{{any}}\} {{token}} {{this will the token from url}}
   * @description {{this will make registration second step}}
   */
  finalRegister(token: any, $registerData: any) {
    return this.http.post(
      `${appConfig.apiUrl}register-final?api_token=${token}`,
      $registerData,
      {
        headers: this.headers
      }
    );
  }
  /**
   * @func {{login}}
   * @param \{{{any}}\} {{$loginData}} {{this will pass the login user data}}
   * @description {{this will make registration second step}}
   */
  login($loginData: any) {
    return this.http.post(`${appConfig.apiUrl}login`, $loginData, {
      headers: this.headers
    });
  }

    /**
     * @func {{resetPasswordToken}}
     * @param \{{{any}}\} {{$tokenData}} {{this will send reset password link to the given email}}
     * @description {{this will send reset password link to the given email}}
     */
    resetPasswordToken($tokenData: any) {
        return this.http.post(`${appConfig.apiUrl}password/email-token`, $tokenData, {
            headers: this.headers
        });
    }
}

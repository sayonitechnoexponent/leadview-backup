import { Injectable } from '@angular/core';
import {BehaviorSubject} from "rxjs/BehaviorSubject";

@Injectable()
export class DataService {

    private messageSource = new BehaviorSubject<string>('');
    private PlatformSource = new BehaviorSubject<Object>('');
    currentMessage  = this.messageSource.asObservable();
    platform = this.PlatformSource.asObservable();
  constructor() { }


chageLiveWindow(message: string) {
        this.messageSource.next(message)
    }
    sendplanPricingContent(message: any) {
        this.PlatformSource.next(message)
    }
}

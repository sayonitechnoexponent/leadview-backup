import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { appConfig } from '../apiurl.config';

@Injectable()
export class UserService {

    newlpostsubmit;

    /**
     * @func {{postadd}}
     * @description {{this will call and after subscribe next message will call}}
     */
    postadd(message: string) {
        this.newlpostsubmit.next(message);
    }

    private allheaders = new HttpHeaders().set(
        'Access-Control-Allow-Origin', '*'
    );
    private headers = new HttpHeaders().set(
        'enctype', 'multipart/form-data'
    );

  constructor(private http : HttpClient) { }

    getIpFromClient(){
        return this.http.get(`${appConfig.apiUrl}get-ip`, {
            headers: this.headers
        });
    }

  getAuthUser() {
      return JSON.parse(localStorage.getItem('leaduser'));
  }
  getUser($profileData: any) {
      return this.http.post(`${appConfig.apiUrl}fetch-user`, $profileData, {
          headers: this.headers
      });
  }
    getLanguages() {
        return this.http.get(`${appConfig.apiUrl}language`, {
            headers: this.headers
        });
    }
    editProfile($profileData: any) {
        return this.http.post(`${appConfig.apiUrl}user/edit`, $profileData, {
            headers: this.headers
        });
    }

    uploadProfileImage($profileData: any) {
        return this.http.post(`${appConfig.apiUrl}user/save-profile-picture`, $profileData, {
            headers: this.headers
        });
    }
    changePassword($profileData: any) {
        return this.http.post(`${appConfig.apiUrl}user/change-password`, $profileData, {
            headers: this.headers
        });
    }
    updateChatSetting($profileData: any) {
        return this.http.post(`${appConfig.apiUrl}user/save-chat-setting`, $profileData, {
            headers: this.headers
        });
    }

    saveProject($projectData: any) {
        return this.http.post(`${appConfig.apiUrl}user/save-project`, $projectData, {
            headers: this.headers
        });
    }

    myProjects($projectData: any) {
        return this.http.post(`${appConfig.apiUrl}user/my-projects`, $projectData, {
            headers: this.headers
        });
    }
    allProjects($projectData: any) {
        return this.http.post(`${appConfig.apiUrl}all-projects`, $projectData, {
            headers: this.headers
        });
    }
    deleteProject($projectData: any) {
        return this.http.post(`${appConfig.apiUrl}user/delete-project`, $projectData, {
            headers: this.headers
        });
    }
    getProject($projectData: any) {
        return this.http.post(`${appConfig.apiUrl}user/get-project`, $projectData, {
            headers: this.headers
        });
    }
    addNote($param: any) {
        return this.http.post(`${appConfig.apiUrl}create-admin-project-note`, $param, {
            headers: this.headers
        });
    }

    getNotes($param: any) {
        return this.http.post(`${appConfig.apiUrl}user/get-project`, $param, {
            headers: this.headers
        });
    }
    addPlatformData($param: any) {
        return this.http.post(`${appConfig.apiUrl}add-platform`, $param, {
            headers: this.headers
        });
    }
    // getPlatformList() {
    //     return this.http.get(`${appConfig.apiUrl}platforms`, {
    //         headers: this.headers
    //     });
    // }
    deletePlatform($param: any) {
        return this.http.post(`${appConfig.apiUrl}delete-platform`, $param, {
            headers: this.headers
        });
    }
    addBillingDetails($param: any) {
        return this.http.post(`${appConfig.apiUrl}save-billing-details`, $param, {
            headers: this.headers
        });
    }
    deleteCard($param: any) {
        return this.http.post(`${appConfig.apiUrl}delete-Card`, $param, {
            headers: this.headers
        });
    }
    addPlatformPricingData($param: any) {
        return this.http.post(`${appConfig.apiUrl}save-platform-plan-content`, $param, {
            headers: this.headers
        });
    }
    getSinglePlatform($param: any){
        return this.http.post(`${appConfig.apiUrl}single-platform`, $param, {
            headers: this.headers
        });
    }
    getAllProjectSubscriptions($param: any){
        return this.http.post(`${appConfig.apiUrl}project-subscriptions`, $param, {
            headers: this.headers
        });
    }
    subscribeProjecPlan($param){
        return this.http.post(`${appConfig.apiUrl}subscribe-project-plan`, $param, {
            headers: this.headers
        });
    }
    addonpackageSubscription($param){
        return this.http.post(`${appConfig.apiUrl}add-addon-subscription`, $param, {
            headers: this.headers
        });
    }
    addonpackageRemove($param){
        return this.http.post(`${appConfig.apiUrl}remove-addon-subscription`, $param, {
            headers: this.headers
        });
    }
    getAddonPackageList($param){
        return this.http.post(`${appConfig.apiUrl}get-addon`, $param, {
            headers: this.headers
        });
    }
    getTeam($projectData: any) {
        return this.http.post(`${appConfig.apiUrl}user/get-team`, $projectData, {
            headers: this.headers
        });
    }
    addTeam($projectData: any) {
        return this.http.post(`${appConfig.apiUrl}user/add-team`, $projectData, {
            headers: this.headers
        });
    }
    deleteTeam($projectData: any) {
        return this.http.post(`${appConfig.apiUrl}user/delete-team`, $projectData, {
            headers: this.headers
        });
    }

    getPlatforms() {
        return this.http.get(`${appConfig.apiUrl}platforms`, {
            headers: this.headers
        });
    }
    addPackage($projectData: any) {
        return this.http.post(`${appConfig.apiUrl}user/add-package`, $projectData, {
            headers: this.headers
        });
    }
    getCreatedPackages($projectData: any) {
        return this.http.post(`${appConfig.apiUrl}user/get-created-packages`, $projectData, {
            headers: this.headers
        });
    }
    deletePackage($param: any) {
        return this.http.post(`${appConfig.apiUrl}user/delete-package`, $param, {
            headers: this.headers
        });
    }
    deleteAddonPackage($param: any) {
        return this.http.post(`${appConfig.apiUrl}user/delete-addon-package`, $param, {
            headers: this.headers
        });
    }
    getFeatures($param: any) {
        return this.http.post(`${appConfig.apiUrl}package-features`, $param, {
            headers: this.headers
        });
    }
    findPackage($param: any) {
        return this.http.post(`${appConfig.apiUrl}user/find-package`, $param, {
            headers: this.headers
        });
    }
    createTeamUser($param: any) {
        return this.http.post(`${appConfig.apiUrl}create-team-user`, $param, {
            headers: this.headers
        });
    }
    getTeamUsers($param: any) {
        return this.http.post(`${appConfig.apiUrl}get-team-users`, $param, {
            headers: this.headers
        });
    }
    deleteTuser($param: any) {
        return this.http.post(`${appConfig.apiUrl}delete-user`, $param, {
            headers: this.headers
        });
    }

    assignTeamUser($param: any) {
        return this.http.post(`${appConfig.apiUrl}assign-team-user`, $param, {
            headers: this.headers
        });
    }
    getTamDetails($param: any) {
        return this.http.post(`${appConfig.apiUrl}team-details`, $param, {
            headers: this.headers
        });
    }
    setUserPassword($param: any) {
        return this.http.post(`${appConfig.apiUrl}set-user-password`, $param, {
            headers: this.headers
        });
    }

    getAllPermissions($param: any) {
        return this.http.post(`${appConfig.apiUrl}all-permissions`, $param, {
            headers: this.headers
        });
    }
    setUserPermissions($param: any) {
        return this.http.post(`${appConfig.apiUrl}set-user-permissions`, $param, {
            headers: this.headers
        });
    }
    featchTags($param: any) {
        return this.http.post(`${appConfig.apiUrl}get-tags`, $param, {
            headers: this.headers
        });
    }

    allCountries() {
        return this.http.get(`${appConfig.apiUrl}countries-list`, {
            headers: this.headers
        });
    }

    setAutoAssignment($param: any) {
        return this.http.post(`${appConfig.apiUrl}set-auto-assign`, $param, {
            headers: this.headers
        });
    }
    createTag($param: any) {
        return this.http.post(`${appConfig.apiUrl}add-tag`, $param, {
            headers: this.headers
        });
    }

    deleteTag($param: any) {
        return this.http.post(`${appConfig.apiUrl}delete-tag`, $param, {
            headers: this.headers
        });
    }

    addEditTax($param: any) {
        return this.http.post(`${appConfig.apiUrl}add-tax`, $param, {
            headers: this.headers
        });
    }

    getTaxes($param: any) {
        return this.http.post(`${appConfig.apiUrl}get-taxes`, $param, {
            headers: this.headers
        });
    }

    deleteTaxes($param: any) {
        return this.http.post(`${appConfig.apiUrl}delete-tax`, $param, {
            headers: this.headers
        });
    }

    addEditOpportunityStage($param: any) {
        return this.http.post(`${appConfig.apiUrl}save-opportunity-stage`, $param, {
            headers: this.headers
        });
    }

    getOpportunityStages($param: any) {
        return this.http.post(`${appConfig.apiUrl}get-opportunity-stages`, $param, {
            headers: this.headers
        });
    }

    deleteOpportunityStages($param: any) {
        return this.http.post(`${appConfig.apiUrl}delete-opportunity-stage`, $param, {
            headers: this.headers
        });
    }

    addEditOpportunityType($param: any) {
        return this.http.post(`${appConfig.apiUrl}save-opportunity-type`, $param, {
            headers: this.headers
        });
    }

    getOpportunityTypes($param: any) {
        return this.http.post(`${appConfig.apiUrl}get-opportunity-types`, $param, {
            headers: this.headers
        });
    }

    deleteOpportunityTypes($param: any) {
        return this.http.post(`${appConfig.apiUrl}delete-opportunity-type`, $param, {
            headers: this.headers
        });
    }

    addEditNoteType($param: any) {
        return this.http.post(`${appConfig.apiUrl}save-note-type`, $param, {
            headers: this.headers
        });
    }

    getNoteTypes($param: any) {
        return this.http.post(`${appConfig.apiUrl}get-note-types`, $param, {
            headers: this.headers
        });
    }

    deleteNoteTypes($param: any) {
        return this.http.post(`${appConfig.apiUrl}delete-note-type`, $param, {
            headers: this.headers
        });
    }

    addEditNote($param: any) {
        return this.http.post(`${appConfig.apiUrl}save-note`, $param, {
            headers: this.headers
        });
    }

    getContacts($param: any) {
        return this.http.post(`${appConfig.apiUrl}get-contacts`, $param, {
            headers: this.headers
        });
    }

    getCompanies($param: any) {
        return this.http.post(`${appConfig.apiUrl}get-companies`, $param, {
            headers: this.headers
        });
    }

    addEditOpportunity($param: any) {
        return this.http.post(`${appConfig.apiUrl}save-opportunity`, $param, {
            headers: this.headers
        });
    }
    deleteOpportunity($param: any) {
        return this.http.post(`${appConfig.apiUrl}delete-opportunity`, $param, {
            headers: this.headers
        });
    }
    addEditTask($param: any) {
        return this.http.post(`${appConfig.apiUrl}save-task`, $param, {
            headers: this.headers
        });
    }
    deleteTask($param: any) {
        return this.http.post(`${appConfig.apiUrl}delete-tasks`, $param, {
            headers: this.headers
        });
    }
    getUserTimeLine($param: any)
    {
        return this.http.post(`${appConfig.apiUrl}get-time-line`, $param, {
            headers: this.headers
        });
    }


    addEditTaskType($param: any) {
        return this.http.post(`${appConfig.apiUrl}save-task-type`, $param, {
            headers: this.headers
        });
    }

    getTaskTypes($param: any) {
        return this.http.post(`${appConfig.apiUrl}get-task-types`, $param, {
            headers: this.headers
        });
    }

    deleteTaskTypes($param: any) {
        return this.http.post(`${appConfig.apiUrl}delete-task-type`, $param, {
            headers: this.headers
        });
    }

    addEditUserEmail($param: any) {
        return this.http.post(`${appConfig.apiUrl}save-email`, $param, {
            headers: this.headers
        });
    }

    getUserEmails($param: any) {
        return this.http.post(`${appConfig.apiUrl}get-user-emails`, $param, {
            headers: this.headers
        });
    }

    deleteUserEmail($param: any) {
        return this.http.post(`${appConfig.apiUrl}delete-user-email`, $param, {
            headers: this.headers
        });
    }
    getOpportunity($param: any) {
        return this.http.post(`${appConfig.apiUrl}get-opportunity`, $param, {
            headers: this.headers
        });
    }

    addLabel($param: any) {
        return this.http.post(`${appConfig.apiUrl}add-label`, $param, {
            headers: this.headers
        });
    }

    setLabel($param: any) {
        return this.http.post(`${appConfig.apiUrl}set-label`, $param, {
            headers: this.headers
        });
    }
    changePostScope($param: any) {
        return this.http.post(`${appConfig.apiUrl}change-post-status`, $param, {
            headers: this.headers
        });
    }
    setReminder($param: any) {
        return this.http.post(`${appConfig.apiUrl}save-reminder`, $param, {
            headers: this.headers
        });
    }
    saveUserPhNumber($param: any) {
        return this.http.post(`${appConfig.apiUrl}save-user-phone`, $param, {
            headers: this.headers
        });
    }
    getUserPhNumbers($param: any) {
        return this.http.post(`${appConfig.apiUrl}get-user-phones`, $param, {
            headers: this.headers
        });
    }
    deleteUserPhNumber($param: any) {
        return this.http.post(`${appConfig.apiUrl}delete-user-phone`, $param, {
            headers: this.headers
        });
    }
    savePostComment($param: any) {
        return this.http.post(`${appConfig.apiUrl}save-comment`, $param, {
            headers: this.headers
        });
    }
    saveUserAddr($param: any) {
        return this.http.post(`${appConfig.apiUrl}save-user-address`, $param, {
            headers: this.headers
        });
    }
    getUserAddresses($param: any) {
        return this.http.post(`${appConfig.apiUrl}get-user-address`, $param, {
            headers: this.headers
        });
    }
    deleteUserAddrs($param: any) {
        return this.http.post(`${appConfig.apiUrl}delete-user-addresses`, $param, {
            headers: this.headers
        });
    }
    saveUserGroup($param: any) {
        return this.http.post(`${appConfig.apiUrl}save-user-group`, $param, {
            headers: this.headers
        });
    }

    saveContact($param: any) {
        return this.http.post(`${appConfig.apiUrl}save-contact`, $param, {
            headers: this.headers
        });
    }
    saveCompany($param: any) {
        return this.http.post(`${appConfig.apiUrl}save-company`, $param, {
            headers: this.headers
        });
    }
    deleteContacts($param: any) {
        return this.http.post(`${appConfig.apiUrl}delete-contacts`, $param, {
            headers: this.headers
        });
    }
    getUserGroups($param: any) {
        return this.http.post(`${appConfig.apiUrl}get-user-groups`, $param, {
            headers: this.headers
        });
    }
    assignGroups($param: any) {
        return this.http.post(`${appConfig.apiUrl}assign-contact-groups`, $param, {
            headers: this.headers
        });
    }
    assignContactToManagers($param: any) {
        return this.http.post(`${appConfig.apiUrl}assign-contact-manager`, $param, {
            headers: this.headers
        });
    }

    getSingleContact($param: any) {
        return this.http.post(`${appConfig.apiUrl}single-contact`, $param, {
            headers: this.headers
        });
    }
    searchCompanies($param: any) {
        return this.http.post(`${appConfig.apiUrl}search-company`, $param, {
            headers: this.headers
        });
    }
    assignContactToCompany($param: any) {
        return this.http.post(`${appConfig.apiUrl}assign-contact-company`, $param, {
            headers: this.headers
        });
    }
    getCompanyContacts($param: any) {
        return this.http.post(`${appConfig.apiUrl}company-contacts`, $param, {
            headers: this.headers
        });
    }
    companyUnassigned($param: any) {
        return this.http.post(`${appConfig.apiUrl}unassign-company-contact`, $param, {
            headers: this.headers
        });
    }
    fetchGroups($param: any) {
        return this.http.post(`${appConfig.apiUrl}all-groups`, $param, {
            headers: this.headers
        });
    }
    deleteGroups($param: any) {
        return this.http.post(`${appConfig.apiUrl}del-groups`, $param, {
            headers: this.headers
        });
    }
    mergeContacts($param: any) {
        return this.http.post(`${appConfig.apiUrl}contacts-merge`, $param, {
            headers: this.headers
        });
    }
    unAssignContact($param: any) {
        return this.http.post(`${appConfig.apiUrl}unassign-contact`, $param, {
            headers: this.headers
        });
    }
    mapField($param: any) {
        return this.http.post(`${appConfig.apiUrl}map-fields`, $param, {
            headers: this.headers
        });
    }
    addSaveSocial($param: any) {
        return this.http.post(`${appConfig.apiUrl}save-social`, $param, {
            headers: this.headers
        });
    }
    getSocials($param: any) {
        return this.http.post(`${appConfig.apiUrl}get-socials`, $param, {
            headers: this.headers
        });
    }
    delSocials($param: any) {
        return this.http.post(`${appConfig.apiUrl}delete-social`, $param, {
            headers: this.headers
        });
    }
    unassignManagerContact($param: any) {
        return this.http.post(`${appConfig.apiUrl}unassign-contact-manager`, $param, {
            headers: this.headers
        });
    }
    postArchiveToggel($param: any) {
        return this.http.post(`${appConfig.apiUrl}change-archive-status`, $param, {
            headers: this.headers
        });
    }
    setUserProjectLive($param: any) {
        return this.http.post(`${appConfig.apiUrl}set-user-live-project`, $param, {
            headers: this.headers
        });
    }
    saveChatBoxFeature($param: any) {
        return this.http.post(`${appConfig.apiUrl}save-chat-box`, $param, {
            headers: this.headers
        });
    }
    getChatBox($param: any) {
        return this.http.post(`${appConfig.apiUrl}fetch-chat-box`, $param, {
            headers: this.headers
        });
    }
    saveMapFeature($param: any) {
        return this.http.post(`${appConfig.apiUrl}save-map-setting`, $param, {
            headers: this.headers
        });
    }
    getMapFeature($param: any) {
        return this.http.post(`${appConfig.apiUrl}fetch-map-setting`, $param, {
            headers: this.headers
        });
    }
    searchOnMap($param: any) {
        return this.http.post(`${appConfig.apiUrl}search-map-contacts`, $param, {
            headers: this.headers
        });
    }
    savelivVisitor($param: any) {
        return this.http.post(`${appConfig.apiUrl}save-visitor`, $param, {
            headers: this.headers
        });
    }
    fetchVisitors($param: any) {
        return this.http.post(`${appConfig.apiUrl}get-live-visitors`, $param, {
            headers: this.headers
        });
    }

    changeLiveVisitors($param: any) {
        return this.http.post(`${appConfig.apiUrl}change-live-visitors`, $param, {
            headers: this.headers
        });
    }
    sendMyMessage($param: any) {
        return this.http.post(`${appConfig.apiUrl}send-message`, $param, {
            headers: this.headers
        });
    }

    setAgenttoChat($param: any) {
        return this.http.post(`${appConfig.apiUrl}set-chat-agent`, $param, {
            headers: this.headers
        });
    }

    fetchVisitorMsg($param: any) {
        return this.http.post(`${appConfig.apiUrl}visitor-msg`, $param, {
            headers: this.headers
        });
    }

    addCmGroup($param: any) {
        return this.http.post(`${appConfig.apiUrl}add-cm-group`, $param, {
            headers: this.headers
        });
    }
    getLiveProject($param: any) {
        return this.http.post(`${appConfig.apiUrl}live-project`, $param, {
            headers: this.headers
        });
    }
    createTagScore($param: any) {
        return this.http.post(`${appConfig.apiUrl}add-tag-scores`, $param, {
            headers: this.headers
        });
    }
    fetchTagScores($param: any) {
        return this.http.post(`${appConfig.apiUrl}get-tag-scores`, $param, {
            headers: this.headers
        });
    }
    deleteTagScore($param: any) {
        return this.http.post(`${appConfig.apiUrl}delete-tag-scores`, $param, {
            headers: this.headers
        });
    }
    projectVisitors($param: any) {
        return this.http.post(`${appConfig.apiUrl}get-project-visitors`, $param, {
            headers: this.headers
        });
    }
    fetchAssignTasks($param: any) {
        return this.http.post(`${appConfig.apiUrl}get-assign-tasks`, $param, {
            headers: this.headers
        });
    }
    fetchAssignOpportunities($param: any) {
        return this.http.post(`${appConfig.apiUrl}get-assign-opportunities`, $param, {
            headers: this.headers
        });
    }
    fetchMyFiles($param: any) {
        return this.http.post(`${appConfig.apiUrl}get-my-files`, $param, {
            headers: this.headers
        });
    }

    saveMyFile($param: any) {
        return this.http.post(`${appConfig.apiUrl}save-my-file`, $param, {
            headers: this.headers
        });
    }

    deleteMyFiles($param: any) {
        return this.http.post(`${appConfig.apiUrl}delete-my-files`, $param, {
            headers: this.headers
        });
    }
    fileTimeLine($param: any) {
        return this.http.post(`${appConfig.apiUrl}get-file-time-line`, $param, {
            headers: this.headers
        });
    }
    fileOpened($param: any) {
        return this.http.post(`${appConfig.apiUrl}opened-file`, $param, {
            headers: this.headers
        });
    }
    setFileVersion($param: any) {
        return this.http.post(`${appConfig.apiUrl}set-file-version`, $param, {
            headers: this.headers
        });
    }
    fetchUrlTimeLine($param: any) {
        return this.http.post(`${appConfig.apiUrl}get-url-timeline`, $param, {
            headers: this.headers
        });
    }

    fetchDashboardUser($param: any) {
        return this.http.post(`${appConfig.apiUrl}get-dashboard-user`, $param, {
            headers: this.headers
        });
    }

    noteApproval($param: any) {
        return this.http.post(`${appConfig.apiUrl}approve-note`, $param, {
            headers: this.headers
        });
    }
    saveMailTemplate($param: any) {
        return this.http.post(`${appConfig.apiUrl}save-mail-template`, $param, {
            headers: this.headers
        });
    }
    emailTemplates($param: any) {
        return this.http.post(`${appConfig.apiUrl}email-templates`, $param, {
            headers: this.headers
        });
    }
    delEmailTemplates($param: any) {
        return this.http.post(`${appConfig.apiUrl}delete-email-templates`, $param, {
            headers: this.headers
        });
    }
    saveSendEmail($param: any) {
        return this.http.post(`${appConfig.apiUrl}save-send-email`, $param, {
            headers: this.headers
        });
    }
    getMailList($param: any) {
        return this.http.post(`${appConfig.apiUrl}get-mails`, $param, {
            headers: this.headers
        });
    }
    changeMailStatus($param: any) {
        return this.http.post(`${appConfig.apiUrl}change-mail-stage`, $param, {
            headers: this.headers
        });
    }
    saveMailAsDraft($param: any) {
        return this.http.post(`${appConfig.apiUrl}save-draft-mail`, $param, {
            headers: this.headers
        });
    }
    saveLanguage($param: any) {
        return this.http.post(`${appConfig.apiUrl}save-language`, $param, {
            headers: this.headers
        });
    }
    deleteLanguage($param: any) {
        return this.http.post(`${appConfig.apiUrl}delete-language`, $param, {
            headers: this.headers
        });
    }

    saveMailAccount($param: any) {
        return this.http.post(`${appConfig.apiUrl}save-mail-account`, $param, {
            headers: this.headers
        });
    }
    fetchMailAccounts($param: any) {
        return this.http.post(`${appConfig.apiUrl}get-mail-accounts`, $param, {
            headers: this.headers
        });
    }

    deleteMailAccounts($param: any) {
        return this.http.post(`${appConfig.apiUrl}delete-mail-accounts`, $param, {
            headers: this.headers
        });
    }
    fetchMailTimeLine($param: any) {
        return this.http.post(`${appConfig.apiUrl}get-mail-timeline`, $param, {
            headers: this.headers
        });
    }

    removeAttachment($param: any) {
        return this.http.post(`${appConfig.apiUrl}remove-attachment`, $param, {
            headers: this.headers
        });
    }

    mergeMail($param: any) {
        return this.http.post(`${appConfig.apiUrl}merge-mail`, $param, {
            headers: this.headers
        });
    }
    archiveMail($param: any) {
        return this.http.post(`${appConfig.apiUrl}archive-mail`, $param, {
            headers: this.headers
        });
    }
    modifyMailRelation($param: any) {
        return this.http.post(`${appConfig.apiUrl}modify-relation`, $param, {
            headers: this.headers
        });
    }
    setMailReminder($param: any) {
        return this.http.post(`${appConfig.apiUrl}save-mail-reminder`, $param, {
            headers: this.headers
        });
    }
    changeProjectStatus($param: any) {
        return this.http.post(`${appConfig.apiUrl}change-project-status`, $param, {
            headers: this.headers
        });
    }
    massMailToContacts($param: any) {
        return this.http.post(`${appConfig.apiUrl}send-contact-mass-mail`, $param, {
            headers: this.headers
        });
    }
    getRemovalEmails($param: any) {
        return this.http.post(`${appConfig.apiUrl}email-templates`, $param, {
            headers: this.headers
        });
    }
    Users($param: any) {
        return this.http.post(`${appConfig.apiUrl}get-team-users`, $param, {
            headers: this.headers
        });
    }
    toggleChatBox($param: any) {
        return this.http.post(`${appConfig.apiUrl}toggle-chat-box-post`, $param, {
            headers: this.headers
        });
    }
    BillingDetails($projectData: any) {
        return this.http.post(`${appConfig.apiUrl}billing-history`, $projectData, {
            headers: this.headers
        });
    }
    fetchDataActivity($param: any) {
        return this.http.post(`${appConfig.apiUrl}get-data-activity`, $param, {
            headers: this.headers
        });
    }
    changeUndoRedoContact($param: any) {
        return this.http.post(`${appConfig.apiUrl}undo-redo-contact-info`, $param, {
            headers: this.headers
        });
    }
    deleteContactChnageLog($param: any) {
        return this.http.post(`${appConfig.apiUrl}del-change-info-log`, $param, {
            headers: this.headers
        });
    }
    pdfDownload($billingDetailsData: any) {
        return this.http.post(`${appConfig.apiUrl}billing-history-pdf`, $billingDetailsData, {
            headers: this.headers
        });
    }
    addEditFeature($param: any) {
        return this.http.post(`${appConfig.apiUrl}add-feature`, $param, {
            headers: this.headers
        });
    }
}

import { Injectable } from "@angular/core";

declare var $: any;

@Injectable()
export class CommonService {

  constructor() {}


  /**
   * @func {{openModal}}
   * @param \{{{string}}\} {{dataVal}} {{pass the dataval string}}
   * @description {{this will be open the popup accordingly}}
   */
  openModal(dataVal){
      const formwrap = dataVal;
      const screenH = $(window).height();
      const formwrapH = $('.' + formwrap).innerHeight() - 30;
      const headH = $('header').innerHeight();
      const footH = $('footer').innerHeight();
      const midbodyH = screenH - (headH + footH);

      $('.' + formwrap).css({'top': - formwrapH, 'opacity': '1'});
      $('.cntactadd .container-fluid').css({'min-height': midbodyH});

      $('body').addClass('open_modal');
      $('html,body').animate({
        scrollTop: 0
      }, '500');
      $('.formwrap').css({'top': -5200});
      $('.' + formwrap).css({'top': '0px', 'min-height': midbodyH});
      $('.ovelay').removeClass('hidden');
      $('.cntactadd').removeClass('no-blur').addClass('open_popup');
      if (midbodyH < formwrapH) {
        $('.cntactadd .container-fluid').css({'min-height': formwrapH});
        $('.' + formwrap).css({'min-height': '1px'});
      }
      // dashNavHeight();
  }

  
  openChildModal(dataVal){
    const formwrap = dataVal;
    const screenH = $(window).height();
    const formwrapH = $('.' + formwrap).innerHeight() - 30;
    const headH = $('header').innerHeight();
    const footH = $('footer').innerHeight();
    const midbodyH = screenH - (headH + footH);

    $('.' + formwrap).css({'top': - formwrapH, 'opacity': '1'});
    $('.cntactadd .container-fluid').css({'min-height': midbodyH});

    $('body').addClass('open_modal');
    $('html,body').animate({
      scrollTop: 0
    }, '500');
    $('.formwrap').css({'top': -5200});
    $('.' + formwrap).css({'top': '0px', 'min-height': midbodyH});
    $('.ovelay').removeClass('hidden');
    $('.cntactadd').addClass('open_popup').addClass('no-blur');
    if (midbodyH < formwrapH) {
      $('.cntactadd .container-fluid').css({'min-height': formwrapH});
      $('.' + formwrap).css({'min-height': '1px'});
    }
    // dashNavHeight();
}

  startLoader(domId){
      //alert(domId);
      $(domId).addClass('custmLoder');
      //console.log();
      if (domId =="#all")
      {
          setTimeout(()=>{
              // console.log($(domId));
              $(domId).addClass('custmLoder');
          },1500);
      }
  }

  stopLoader(domId){
      $(domId).removeClass('custmLoder');

  }


}

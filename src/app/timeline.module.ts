import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes } from '@angular/router';
import { RouterModule } from '@angular/router';

import {AuthGuard} from './guards/auth.guard';
import {PublicGuard} from './guards/public.guard';

import {GrowlModule} from 'primeng/primeng';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { Ng4GeoautocompleteModule } from 'ng4-geoautocomplete';

import {CalendarModule} from 'primeng/calendar';
import {NgAutoCompleteModule} from "ng-auto-complete";
import { MomentModule } from 'angular2-moment';
import {NgxPaginationModule} from 'ngx-pagination';
import {AutoCompleteModule} from 'primeng/autocomplete';
import {GMapModule} from 'primeng/gmap';
import {TableModule} from 'primeng/table';
import {EditorModule} from 'primeng/editor';
import { MultiSelectModule } from 'primeng/multiselect';

declare var $: any;

import { UsersTimelineComponent } from './components/users-timeline/users-timeline.component';
import { DataActivityComponent } from './components/users-timeline/data-activity/data-activity.component';
import {CreateNewAutocompleteGroup, SelectedAutocompleteItem, NgAutocompleteComponent} from "ng-auto-complete";
const routes: Routes = [
    { path: '',  component: UsersTimelineComponent, canActivate: [AuthGuard] },
];


@NgModule({
  imports: [
      CommonModule,
      FormsModule,
      RouterModule.forChild(routes),
      GrowlModule,
      CalendarModule,
      AngularMultiSelectModule,
      NgAutoCompleteModule,
      Ng4GeoautocompleteModule.forRoot(),
      MomentModule,
      NgxPaginationModule,
      AutoCompleteModule,
      GMapModule,
      TableModule,
      EditorModule,
      MultiSelectModule
  ],
  declarations: [UsersTimelineComponent, DataActivityComponent],
    providers: [
        // CommonService,
        // AuthServices,
        // UserService,
        PublicGuard,
        AuthGuard,
        NgAutocompleteComponent
    ],
})
export class TimelineModule {

}
